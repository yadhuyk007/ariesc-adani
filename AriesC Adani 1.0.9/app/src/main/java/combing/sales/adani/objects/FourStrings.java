package combing.sales.adani.objects;

public class FourStrings {
    public String qty;
    public String mrp;
    public String rate;
//    public String uom;
//    public String upc;
    public String packageType;
   
    public String taxid;
    public String discount;
    public String discountper;

   /* public FourStrings(String qty, String mrp, String rate, String uom,
	    String upc,String taxid,String discount,String discountper) {
	this.qty = qty;
	this.mrp = mrp;
	this.rate = rate;
	this.uom = uom;
	this.upc = upc;
	this.taxid = taxid;
	this.discount = discount;
	this.discountper = discountper;
    }*/
    
    public FourStrings(String qty, String mrp, String rate, String packageType,
                       String taxid, String discount, String discountper) {
    	this.qty = qty;
    	this.mrp = mrp;
    	this.rate = rate;
    	this.packageType = packageType;     
    	this.taxid = taxid;
    	this.discount = discount;
    	this.discountper = discountper;
        }
    
   /* public FourStrings(String qty, String mrp, String rate,String uom,String upc) {
    	this.qty = qty;
    	this.mrp = mrp;
    	this.rate = rate;
    	this.uom = uom;
    	this.upc = upc;
        }
*/
    
    public FourStrings(String qty, String mrp, String rate, String packageType) {
    	this.qty = qty;
    	this.mrp = mrp;
    	this.rate = rate;
    	this.packageType = packageType;
    	 
        }
    
    
}
