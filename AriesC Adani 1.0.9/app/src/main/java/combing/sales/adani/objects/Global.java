package combing.sales.adani.objects;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import combing.sales.adani.ariesc.R;


/**
 * Created by hp on 04/04/2019.
 */

public class Global {
    public static String date = "";
    public static boolean force = false;
    public static boolean refresh = false;
    public static boolean onSync;
    public static String lastacttime = "";
    public static boolean synnav = false;
    public static Bitmap sharedcustimg;
    public static Bitmap sharedinsideimg;
    public static boolean resStart;
    public static String lastSetOn;
    public static String schedule="";
    public static int spinnerpos=0;


    public static boolean isNewVersionAvailable(Context c) {
        boolean temp = false;
        SharedPreferences pref = c.getSharedPreferences(
                "Config", Context.MODE_PRIVATE);
        temp=pref.getBoolean("versionavailable",false);
        return temp;
    }

    public static void Toast(Activity mAct, String string, int length,
                             String style) {
        Toast toast = Toast.makeText(mAct, string, length);
        // toast.setGravity(Gravity.CENTER, 0, 0);
        LinearLayout tView = (LinearLayout) toast.getView();
        tView.setBackgroundResource(R.color.level1);
        //Font a = new Font(mAct, tView, style);
        toast.show();
    }

    public static String setLastTime() {
        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        lastacttime = dateTimeFormat.format(c.getTime());
        return lastacttime;
    }

    public static AlertDialog.Builder Alert(Activity activity, String title,
                                            String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertdialog, null);
        ((TextView) dialogView.findViewById(R.id.title)).setText(title);
        ((TextView) dialogView.findViewById(R.id.message)).setText(message);
        Font a = new Font(activity,
                (TextView) dialogView.findViewById(R.id.message), Font.Regular);
        a = new Font(activity, (TextView) dialogView.findViewById(R.id.title),
                Font.Semibold);
        builder.setView(dialogView);
        return builder;
    }

    public static void setDialogFont(Activity activitiy, AlertDialog dialog) {
        Button ok = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        ok.setBackgroundResource(R.drawable.dhi_payment_button);
        ok.setTextColor(Color.parseColor("#FFFFFF"));
        Font a = new Font(activitiy, ok, Font.Semibold);
        ok = dialog.getButton(AlertDialog.BUTTON_NEUTRAL);
        ok.setBackgroundResource(R.drawable.dhi_payment_button2);
        ok.setTextColor(Color.parseColor("#FFFFFF"));
        a = new Font(activitiy, ok, Font.Semibold);
        ok = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        ok.setBackgroundResource(R.drawable.dhi_payment_button2);
        ok.setTextColor(Color.parseColor("#FFFFFF"));
        a = new Font(activitiy, ok, Font.Semibold);
    }


    public static String readFromFile(String path) {

        Log.e("PathofImage", path);
        String stringifiedbitmap = "";
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(path, options);

            stringifiedbitmap = getStringFromBitmap(bitmap);
        } catch (NullPointerException nl) {
            stringifiedbitmap = "RXJyb3JGaWxl";
        } catch (Exception e) {
            stringifiedbitmap = "";
            e.printStackTrace();
            Log.e("FileErInGlob", e.getMessage());

        }
        return stringifiedbitmap;
        // Bitmap decodedbitmap=getBitmapFromString(stringifiedbitmap);


    }

    // convert bitmap image to string
    private static String getStringFromBitmap(Bitmap bitmapPicture) {
        /*
		 * This functions converts Bitmap picture to a string which can be
		 * JSONified.
		 */
        final int COMPRESSION_QUALITY = 75;
        String encodedImage;
        ByteArrayOutputStream byteArrayBitmapStream = new ByteArrayOutputStream();
        bitmapPicture.compress(Bitmap.CompressFormat.JPEG, COMPRESSION_QUALITY,
                byteArrayBitmapStream);
        byte[] b = byteArrayBitmapStream.toByteArray();
        encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }

    public static String readFromTextFile(String file) {
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        } catch (IOException e) {
        }
        String string = text.toString();
        return string;
    }
}
