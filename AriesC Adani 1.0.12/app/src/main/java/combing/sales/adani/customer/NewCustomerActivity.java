package combing.sales.adani.customer;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.CallLog;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import combing.sales.adani.ariesc.R;
import combing.sales.adani.database.DatabaseHandler;
import combing.sales.adani.gps.GPSAccuracy;
import combing.sales.adani.gps.PlayTracker;
import combing.sales.adani.landing.LandingActivity;
import combing.sales.adani.objects.Font;
import combing.sales.adani.objects.Global;
import combing.sales.adani.objects.Locations;
import combing.sales.adani.objects.Scheduledata;
import combing.sales.adani.objects.Scheduledetail;
import combing.sales.adani.objects.Stacklogger;
import combing.sales.adani.objects.webconfigurration;

public class NewCustomerActivity extends AppCompatActivity {

    private static final int CAMERA_REQUEST = 1888;
    private static final int REQUEST_CODE_GPS_ACCURACY = 1;
    private Activity activity;
    public static boolean OTP_verificationstatus;
    public static String OTP_submitTime = "";
    private static Uri mImageUri;
    private static String type = "";
    private static String mobilenumber;
    List<String> categories = new ArrayList<String>();
    List<String> sizelist = new ArrayList<String>();
    List<String> sellprodu = new ArrayList<String>();
    List<String> onlieplat = new ArrayList<String>();
    Hashtable<String, String> customercategorymap = new Hashtable<String, String>();
    private static DatabaseHandler db ;
    private static Button verification;
    private static ImageView tick;
    private Bitmap customerphoto;
    private Bitmap insidephoto;
    private boolean verify = false;
    private CountDownTimer yourCountDownTimer;
    private Activity mAct = NewCustomerActivity.this;
    private EditText contactnumber;
    private TextView mTextField;
    private Spinner spin;
    private String cat;
    public static boolean[] selectedarray;
    public static String[] selectedarraytext;
    private String custname;
    private String address;
    private String phone;
    private String pin;
    private String category;
    private String custcategory;
    private String[] categorycode;
    private String customerid = "";
    private String createtime = "";
    private String gstin;
    private String fssai;
    private String shopname;
    private Spinner size;
    private String storesize;
    private RadioButton rb11;
    private int brandedshop;
    private int visitbybrand;
    private String sellbrandprod;
    int rb1=1;
    int rb2=1;
    private String buyonline="";
    private Spinner buyfromspinr;
    private Spinner onlineplatspinr;
    private String buybrandproductsfrom;
    private String onlineplatformname;
    private int dobuybrandproductsfrom;
    private int doonlineplatformname;
    private String helpers;
    private boolean checkbuyfromspinr;
    private boolean checkonlineplatspinr;
    private RadioGroup rg1;
    private RadioGroup rg2;
    private RadioGroup rg3;
    private RadioGroup rg5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_customer);
        Thread.setDefaultUncaughtExceptionHandler(new Stacklogger(this));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
        Bundle bundle = getIntent().getExtras();
        selectedarray = bundle.getBooleanArray("selectedarray");
        selectedarraytext = bundle.getStringArray("selectedarraytext");
        categorycode=bundle.getStringArray("categorycode");

        try {
            intialisecategories();
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        //---------------------yadhu------------------------

        //arraylist for 'Where Do you buy AWL / Fortune brand products from?'
        sellprodu.add("SELECT");
        sellprodu.add("Distrubutor");
        sellprodu.add("Wholesaler");
        sellprodu.add("Others");

        //arraylist for 'Name of the online B2B platform'
        onlieplat.add("SELECT");
        onlieplat.add("UDAAN");
        onlieplat.add("Jumbotail");
        onlieplat.add("Shopkirana");
        onlieplat.add("Metro Taikee");
        onlieplat.add("Gramfactory");
        onlieplat.add("Amazon B2B");
        onlieplat.add("Reliance");
        onlieplat.add("Big Basket");
        onlieplat.add("Grofers");
        onlieplat.add("Ondoor");
        onlieplat.add("Others");
        rg1 = (RadioGroup) findViewById(R.id.rg1);       // to get input of 'Branded Shop Board Presence'
        rg1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rb11:
                        brandedshop=0;
                        break;
                    case R.id.rb12:
                        brandedshop=1;
                        break;
                }
            }
        });
        rg2 = (RadioGroup) findViewById(R.id.rg2);       //to get input of 'Any visit by AWL (Adani Wilmar Ltd.)/ Fortune brand employee in the past?'
        rg2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rb21:
                        visitbybrand=0;
                        break;
                    case R.id.rb22:
                        visitbybrand=1;
                        break;
                }
            }
        });
        rg3 = (RadioGroup) findViewById(R.id.rg3);
        rg3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rb31:

                        //to get input of 'Do you sell AWL / Fortune brand products?'

                        LinearLayout layout = (LinearLayout) findViewById(R.id.sellprod);
                        layout.setVisibility(View.GONE);
                        dobuybrandproductsfrom=0;
                        break;

                    case R.id.rb32:

                        //to get input of 'Where Do you buy AWL / Fortune brand products from?' if button clicked is 'Yes'

                        LinearLayout sellprod = (LinearLayout) findViewById(R.id.sellprod);
                        sellprod.setVisibility(View.VISIBLE);
                        dobuybrandproductsfrom=1;
                        buyfromspinr = (Spinner) findViewById(R.id.buyfrom);
                        checkbuyfromspinr = true;
                        ArrayAdapter<String> Adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, sellprodu) {

                            @Override
                            public View getView(int position, View convertView, ViewGroup parent) {
                                TextView tv = (TextView) super.getView(position, convertView,
                                        parent);
                                try {
                                    tv.setTextColor(Color.parseColor("#000000"));
                                    tv.setTextSize(14);

                                    String fontPath = "fonts/segoeui.ttf";
                                    Typeface m_typeFace = Typeface.createFromAsset(
                                            activity.getAssets(), fontPath);
                                    tv.setTypeface(m_typeFace);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return tv;
                            }

                        };
                        Adapter.setDropDownViewResource(R.layout.spinner_additem_dropdown);
                        buyfromspinr.setAdapter(Adapter);
                        break;
                }
            }
        });
        rg5 = (RadioGroup) findViewById(R.id.rg5);
        rg5.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.rb51:

                        //to get the input of 'Do you buy from online B2B platforms?'

                        LinearLayout layout =(LinearLayout) findViewById(R.id.onlineplatform);
                        layout.setVisibility(View.GONE);
                        doonlineplatformname=0;
                        break;
                    case R.id.rb52:

                        //to get the input of 'Name of the online B2B platform' if button clicked is yes

                        LinearLayout platform =(LinearLayout) findViewById(R.id.onlineplatform);
                        platform.setVisibility(View.VISIBLE);
                        doonlineplatformname=1;
                        onlineplatspinr = (Spinner) findViewById(R.id.onlineplat);
                        checkonlineplatspinr = true;
                        ArrayAdapter<String> Adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, onlieplat) {

                            @Override
                            public View getView(int position, View convertView, ViewGroup parent) {
                                TextView tv = (TextView) super.getView(position, convertView,
                                        parent);
                                try {
                                    tv.setTextColor(Color.parseColor("#000000"));
                                    tv.setTextSize(14);

                                    String fontPath = "fonts/segoeui.ttf";
                                    Typeface m_typeFace = Typeface.createFromAsset(
                                            activity.getAssets(), fontPath);
                                    tv.setTypeface(m_typeFace);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                return tv;
                            }

                        };
                        Adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                        onlineplatspinr.setAdapter(Adapter);
                        break;
                }
            }
        });

        //arraylist items for 'Store Size'
        sizelist.add("SELECT");
        sizelist.add("0-100 sq ft.");
        sizelist.add("100-300 sq ft.");
        sizelist.add("300-500 sq ft.");
        sizelist.add("500+ sq ft.");

        //spinner for store size

        Spinner spinr = (Spinner) findViewById(R.id.spinerstoresize);
        ArrayAdapter<String> Adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, sizelist) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,
                        parent);
                try {
                    tv.setTextColor(Color.parseColor("#000000"));
                    tv.setTextSize(14);

                    String fontPath = "fonts/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            activity.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }

        };
        Adapter.setDropDownViewResource(R.layout.spinner_additem_dropdown);
        spinr.setAdapter(Adapter);
        //--------------------------------end---------------------------------------

        Spinner spinner = (Spinner) findViewById(R.id.spinnercustcat);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, categories) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,
                        parent);
                try {
                    tv.setTextColor(Color.parseColor("#000000"));
                    tv.setTextSize(14);

                    String fontPath = "fonts/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(
                            activity.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }

        };
        dataAdapter.setDropDownViewResource(R.layout.spinner_additem_dropdown);
        spinner.setAdapter(dataAdapter);

        contactnumber = (EditText) findViewById(R.id.ContactNumber);
        verification = (Button) findViewById(R.id.verify);
        tick = (ImageView) findViewById(R.id.tickbutton);
        OTP_verificationstatus = false;
        Button verify = (Button) findViewById(R.id.verify);
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verify();
            }
        });
        //take photo
        ImageButton photo_camera = (ImageButton) findViewById(R.id.photo_camera);
        photo_camera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(NewCustomerActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(NewCustomerActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_REQUEST);
                } else {
                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    File photo;
                    try {
                        photo = createTemporaryFile("picture", ".jpg");
                        photo.delete();
                    } catch (Exception e) {
                        Toast.makeText(NewCustomerActivity.this, R.string.shot_is_impossible, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    mImageUri = Uri.fromFile(photo);
                    type = "photo";
                    startActivityForResult(intent, CAMERA_REQUEST);
                }
            }
        });
        //take photo
        ImageButton insidephoto = (ImageButton) findViewById(R.id.inside_photo);
        insidephoto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(NewCustomerActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(NewCustomerActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_REQUEST);
                } else {
                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    File photo;
                    try {
                        photo = createTemporaryFile("picture", ".jpg");
                        photo.delete();
                    } catch (Exception e) {
                        Toast.makeText(NewCustomerActivity.this, R.string.shot_is_impossible, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    mImageUri = Uri.fromFile(photo);
                    type = "inside";
                    startActivityForResult(intent, CAMERA_REQUEST);
                }
            }
        });
    }
    private void turnOnGPS() {

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!statusOfGPS) {

            AlertDialog.Builder builder = new AlertDialog.Builder(NewCustomerActivity.this);
            builder.setTitle(R.string.switch_on_gps);
            builder.setMessage(R.string.gps_off_alert_message);


            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            startActivityForResult(
                                    new Intent(
                                            Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                    0);
                        }
                    });


            builder.show();

            return;
        }
    }
    private void intialisecategories() throws JSONException {

        SharedPreferences pref = getApplicationContext().getSharedPreferences("Config",
                Context.MODE_PRIVATE);
        String personrole=pref.getString(
                "personrole", "");
        db = new DatabaseHandler(getApplicationContext());
        JSONArray categoryarr = db.getcustomercategories(personrole);
        categories.add(getResources().getString(R.string.SELECT));
        for (int i = 0; i < categoryarr.length(); i++) {
            JSONObject obj = categoryarr.optJSONObject(i);
            if (!customercategorymap.containsKey(obj.optString("catname"))) {
                customercategorymap.put(obj.optString("catname"),
                        obj.optString("catcode"));
                categories.add(obj.optString("catname"));
            }
        }
    }

    private void verify() {
        View mView = NewCustomerActivity.this.getWindow().getDecorView();
        // getActivity().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        InputMethodManager imm = (InputMethodManager) mAct
                .getSystemService(Context.INPUT_METHOD_SERVICE);
//    imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        EditText vertxt = ((EditText) mView
                .findViewById(R.id.ContactNumber));
        //vertxt.setFocusable(false);
        //ctpsn.setFocusable(false);
        mAct.getWindow()
                .setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        // Button myBtn = (Button) mView.findViewById(R.id.verify);
        // myBtn.requestFocus();

        mobilenumber = ((EditText) mView
                .findViewById(R.id.ContactNumber)).getText().toString();
        // TODO Auto-generated method stub

        if (OTP_verificationstatus) {
            Global.Toast(mAct, "OTP already Verified",
                    Toast.LENGTH_SHORT, Font.Regular);
            return;
        }
        if (mobilenumber.length() < 10) {
            Global.Toast(mAct, "Enter a valid mobile number!",
                    Toast.LENGTH_SHORT, Font.Regular);
            return;
        }
        LayoutInflater inflater = getLayoutInflater();
        final View popupView1 = inflater.inflate(
                R.layout.enter_otp_option, null);
        final PopupWindow popupWindow1 = new PopupWindow(popupView1,
                WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        popupWindow1.setTouchable(true);
        //popupWindow1.setFocusable(true);
        popupWindow1.setOutsideTouchable(false);
        popupWindow1
                .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        // popupWindow1.dismiss();
        popupWindow1.setTouchInterceptor(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    popupWindow1.dismiss();
                    return true;
                }
                return false;
            }

        });
        LinearLayout l1 = (LinearLayout) mView
                .findViewById(R.id.linear1);
        popupWindow1.showAtLocation(l1, Gravity.CENTER, 0, 0);
        popupWindow1.setBackgroundDrawable(new ColorDrawable(
                Color.WHITE));
        // l1.clearFocus();
        l1.setFocusable(false);
        InputMethodManager immr = (InputMethodManager) mAct
                .getSystemService(Context.INPUT_METHOD_SERVICE);
//    immr.hideSoftInputFromWindow(mAct.getView().getWindowToken(), 0);
        ImageView close1 = (ImageView) popupView1
                .findViewById(R.id.close_window);
        Button call = (Button) popupView1.findViewById(R.id.call);
        Button sms = (Button) popupView1.findViewById(R.id.sms);
        // mTextField = (TextView)
        // popupView1.findViewById(R.id.message);
        TextView txtnum = (TextView) popupView1
                .findViewById(R.id.textView2);
        txtnum.setText("Verify " + mobilenumber);
        //txtnum.requestFocus();
        ((RelativeLayout) popupView1.findViewById(R.id.timer))
                .setVisibility(View.GONE);
//        popupWindow1.setCancelable(false);
        // /dialog.getContext().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        close1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (yourCountDownTimer != null) {
                    yourCountDownTimer.cancel();
                }
                popupWindow1.dismiss();
            }
        });
        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject cred = new JSONObject();
                try {
                    cred = GetOtprequest();
                    popupWindow1.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String[] params = new String[1];
                params[0] = cred.toString();
                if (isNetworkAvailable())
                    new OTPrequest(verification, mobilenumber, cred
                            .toString()).execute(params);
                else
                    Toast.makeText(mAct,
                            "Check Your Network Connection",
                            Toast.LENGTH_SHORT).show();
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (yourCountDownTimer != null) {
                    yourCountDownTimer.cancel();
                }
                //((ImageView) popupView1.findViewById(R.id.close_window))
                //	.setVisibility(View.GONE);
                ((Button) popupView1.findViewById(R.id.call))
                        .setEnabled(false);
                ((Button) popupView1.findViewById(R.id.sms))
                        .setEnabled(false);
                // popupWindow1.dismiss();
                ((RelativeLayout) popupView1.findViewById(R.id.timer))
                        .setVisibility(View.VISIBLE);
                mTextField = (TextView) popupView1
                        .findViewById(R.id.message);
                Calendar c = Calendar.getInstance();
                DateFormat dateTimeFormat = new SimpleDateFormat(
                        "yyyyMMddHHmmss");
                final String submittime = dateTimeFormat.format(c
                        .getTime());
                // boolean verify=false;
                yourCountDownTimer = new CountDownTimer(60000, 1000) {
                    public void onTick(long millisUntilFinished) {
                        mTextField.setText("" + millisUntilFinished
                                / 1000);
                        if (((millisUntilFinished / 1000) % 3) == 0)
                            if (getCallDetails(mAct, mobilenumber,
                                    popupWindow1, submittime)) {
                                verify = true;
                                onFinish();
                            }
                    }

                    @Override
                    public void onFinish() {
                        if (yourCountDownTimer != null) {
                            yourCountDownTimer.cancel();
                        }
                        // TODO Auto-generated method stub
                        mTextField.setText("0");
                        ((RelativeLayout) popupView1
                                .findViewById(R.id.timer))
                                .setVisibility(View.GONE);
                        ((Button) popupView1.findViewById(R.id.call))
                                .setEnabled(true);
                        ((Button) popupView1.findViewById(R.id.sms))
                                .setEnabled(true);
                        if (verify) {
                            Global.Toast(mAct, "Mobile Number Verified",
                                    Toast.LENGTH_SHORT, Font.Regular);
                        } else {
                            Global.Toast(mAct,
                                    "Mobile Number Verification Failed",
                                    Toast.LENGTH_SHORT, Font.Regular);
                        }
                    }
                }.start();
            }
        });


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private File createTemporaryFile(String part, String ext) throws Exception {

        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }
    private int locationaccuracy() {
        int locationMode = -1;
        try {
            locationMode = Settings.Secure.getInt(this.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return locationMode;
    }
    public void next(View v) {
        shopname = ((TextView) findViewById(R.id.shopname)).getText().toString();
        custname = ((TextView) findViewById(R.id.custname)).getText().toString();
        address = ((TextView) findViewById(R.id.address)).getText().toString();
        phone = ((TextView) findViewById(R.id.ContactNumber)).getText().toString();
        pin = ((TextView) findViewById(R.id.pin)).getText().toString();
        gstin = ((TextView) findViewById(R.id.gstin)).getText().toString();
        fssai = ((TextView) findViewById(R.id.fssai)).getText().toString();
        helpers= ((TextView) findViewById(R.id.helpers)).getText().toString();

        onlineplatspinr = (Spinner)findViewById(R.id.onlineplat);
        if (checkonlineplatspinr){
            onlineplatformname = onlineplatspinr.getSelectedItem().toString();
        }



        buyfromspinr = (Spinner)findViewById(R.id.buyfrom);
        if (checkbuyfromspinr){
            buybrandproductsfrom = buyfromspinr.getSelectedItem().toString();
        }


        size = (Spinner)findViewById(R.id.spinerstoresize);
        storesize = size.getSelectedItem().toString();

        spin = (Spinner)findViewById(R.id.spinnercustcat);
        cat = spin.getSelectedItem().toString();

        String isvalid = valid(custname, address, phone, pin, cat,helpers,storesize,shopname,onlineplatformname,buybrandproductsfrom);
        if (!isvalid.contentEquals("TRUE")) {
            /*Global.Toast(NewCustomerActivity.this, isvalid,
                    Toast.LENGTH_SHORT, Font.Regular);*/
            Toast.makeText(NewCustomerActivity.this, isvalid, Toast.LENGTH_SHORT).show();
            return;
        }
        Global.sharedcustimg = customerphoto;
        Global.sharedinsideimg = insidephoto;
        Log.e("isvalid",isvalid);
        //isvalid = "TRUE";
        if (!isvalid.contentEquals("TRUE")) {
            /*Global.Toast(NewCustomerActivity.this, isvalid,
                    Toast.LENGTH_SHORT, Font.Regular);*/
            Toast.makeText(NewCustomerActivity.this, isvalid, Toast.LENGTH_SHORT).show();
            return;
        }
        turnOnGPS();
        if (!PlayTracker.intime()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(NewCustomerActivity.this);
            builder.setTitle(R.string.gps_unavailable);

            String lastset="";
            if(!Global.lastSetOn.contentEquals(""))
                lastset=Global.lastSetOn;

            builder.setMessage(getResources().getString(R.string.check_your_gps)+" \nGPS Fixed on: "+lastset);

            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

            AlertDialog dialog = builder.create();
            builder.show();

            return;
        }
        if (locationaccuracy() != 3) {
            AlertDialog.Builder builder = new AlertDialog.Builder(NewCustomerActivity.this);
            builder.setTitle(R.string.enable_high_loc_accuracy);
            builder.setMessage(R.string.loc_acc_low);

            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    });

            builder.show();

            return;
        }

//        Intent intent = new Intent(SurrogatesActivity.this, LandingActivity.class);
//        startActivity(intent);
        //method for getting location data without GPSAccuracy activity

        Intent intent = new Intent(NewCustomerActivity.this, GPSAccuracy.class);
        intent.putExtra("operation", "addlocation");
        startActivityForResult(intent, REQUEST_CODE_GPS_ACCURACY);
        overridePendingTransition(0, 0);
    }

    private String valid(String custname, String address, String phone, String pin, String category, String helpers,String size, String shopname,String platname,String brandprod) {
        Log.e("cateogry",category);
        String temp = "TRUE";

        if (shopname.contentEquals(""))
            return getResources().getString(R.string.shop_name_required);
        if (address.contentEquals(""))
            return getResources().getString(R.string.addr_required);
        if (category == null || category.contentEquals("SELECT"))
            return "Customer Outlet Type Required!";
        if (custname.contentEquals(""))
            return getResources().getString(R.string.name_required);

        if (phone.contentEquals(""))
            return getResources().getString(R.string.number_required);

//        if (phone.length() < 10)
//            return getResources().getString(R.string.number_notvalid);

        if (size == null || size.contentEquals("SELECT"))
            return "Store size required!";

        if (helpers.contentEquals(""))
            return "No. Of Helpers Required";
        if (Integer.parseInt(helpers)>20){
            return "No. of Helpers can Only be a value Less than 20";
        }

        if (rg1.getCheckedRadioButtonId() == -1){
            TextView mand=(TextView)findViewById(R.id.branded);
            mand.setTextColor(getResources().getColor(R.color.red));
            int pos= mand.getTop();
            ScrollView scroll=(ScrollView)findViewById(R.id.scroll);
            scroll.smoothScrollTo(0,pos);
            return  "Please select a choice";
        }
        else{
            TextView mand=(TextView)findViewById(R.id.branded);
            mand.setTextColor(getResources().getColor(R.color.textDark));
        }
        if (rg2.getCheckedRadioButtonId() == -1){
            TextView mand=(TextView)findViewById(R.id.visit);
            mand.setTextColor(getResources().getColor(R.color.red));
            ScrollView scroll=(ScrollView)findViewById(R.id.scroll);
            int pos= mand.getTop();
            scroll.smoothScrollTo(0,pos);
            return  "Please select a choice";
        }else{
            TextView mand=(TextView)findViewById(R.id.visit);
            mand.setTextColor(getResources().getColor(R.color.textDark));
        }
        if (rg3.getCheckedRadioButtonId() == -1){
            TextView mand=(TextView)findViewById(R.id.brand);
            mand.setTextColor(getResources().getColor(R.color.red));
            ScrollView scroll=(ScrollView)findViewById(R.id.scroll);
            int pos= mand.getTop();
            scroll.smoothScrollTo(0,pos);


            //scroll.smoothScrollTo(0,pos);
            return  "Please select a choice";
        }else{
            TextView mand=(TextView)findViewById(R.id.visit);
            mand.setTextColor(getResources().getColor(R.color.textDark));
        }
        if (doonlineplatformname==1){
            if (platname == null || platname.contentEquals("SELECT")){
                return "Please Select Name of Online B2B platforms?";
            }

        }
        if (rg5.getCheckedRadioButtonId() == -1){
            TextView mand=(TextView)findViewById(R.id.platform);
            mand.setTextColor(getResources().getColor(R.color.red));
            ScrollView scroll=(ScrollView)findViewById(R.id.scroll);
            int pos= mand.getTop();
            scroll.smoothScrollTo(0,pos);

            return  "Please select a choice!";
        }else{
            TextView mand=(TextView)findViewById(R.id.visit);
            mand.setTextColor(getResources().getColor(R.color.textDark));
        }

        if (dobuybrandproductsfrom==1){

            if (brandprod == null || brandprod.contentEquals("SELECT")){
                return "Please Select Where Do you buy AWL / Fortune brand products from?";
            }

        }

        if (customerphoto == null)
            return getResources().getString(R.string.photo_required);
        if (insidephoto == null)
            return getResources().getString(R.string.inside_required);



        return temp;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 1:if (requestCode == REQUEST_CODE_GPS_ACCURACY) {
                if (resultCode == Activity.RESULT_OK) {


                    Double lati = data.getDoubleExtra("latit", 0);
                    Double longi = data.getDoubleExtra("longit", 0);
                    Float acc = data.getFloatExtra("minacc", 0);
                    String provi = data.getStringExtra("prov");
                    String operation = data.getStringExtra("operation");
                    String fixtime = data.getStringExtra("fixtime");
                    String gpsTime=data.getStringExtra("gpsTime");
                    // Toast.makeText(getApplicationContext(),
                    // lati + ", " + longi + ", " + provi + ", " + acc,
                    // Toast.LENGTH_LONG).show();
                    // gowithcurrent(lati, longi, acc, provi);
                    if (lati > 0 && longi > 0)
                        saveData(lati, longi, acc, provi,gpsTime);
                    else
                        Global.Toast(NewCustomerActivity.this,
                                "Location not found. Please try again!",
                                Toast.LENGTH_LONG, Font.Regular);
                }
                if (locationaccuracy() != 3) {
                    AlertDialog.Builder builder = Global.Alert(NewCustomerActivity.this,
                            "Enable High Location Accuracy", "Currently location accuracy low.");
                    builder.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                    Global.setDialogFont(NewCustomerActivity.this, dialog);
                    return;
                }
                if (resultCode == Activity.RESULT_CANCELED) {
                    Global.Toast(NewCustomerActivity.this, "Cancelled!",
                            Toast.LENGTH_LONG, Font.Regular);
                    // Write your code if there's no result
                }
            }
                break;

            case 1888:try {
                if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                    Bundle extras = data.getExtras();
                    Bitmap mImageBitmap = (Bitmap) extras.get("data");
                    ImageView proof;
                    ImageView inside;
                    if (type == "photo") {
                        customerphoto = getResizedBitmap(mImageBitmap, 500, 500);
                        proof = (ImageView) findViewById(R.id.addcust);
                        proof.setImageBitmap(customerphoto);
                    } else if (type == "inside") {
                        insidephoto = getResizedBitmap(mImageBitmap, 500, 500);
                        inside = (ImageView) findViewById(R.id.inside);
                        inside.setImageBitmap(insidephoto);
                    }
                }
            } catch (Exception e) {
                Toast.makeText(this, R.string.turn_off_high_quality_img, Toast.LENGTH_SHORT).show();
            }
                break;
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth,
                                   int bitmapHeight) {
        return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
    }

    @Override
    public void onBackPressed() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.linear1);
        LayoutInflater layoutInflater = (LayoutInflater) NewCustomerActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.popuplayout, null);
        TextView header = (TextView) customView.findViewById(R.id.header);
        header.setText(R.string.savecustomer);
        TextView msg = (TextView) customView.findViewById(R.id.message);
        msg.setText(R.string.savecustomermsg);
        Button closePopupBtn = (Button) customView.findViewById(R.id.close);
        Button confirm = (Button) customView.findViewById(R.id.yes);
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        final PopupWindow popupWindow = new PopupWindow(customView, width, height);
        popupWindow.setFocusable(true);

        //display the popup window
        popupWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        //close the popup window on button click
        closePopupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                //super.onBackPressed();
                Intent intent = new Intent(NewCustomerActivity.this, LandingActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
    }

    public JSONObject GetOtprequest() throws JSONException {
        webconfigurration C = new webconfigurration(getApplicationContext());
        String params = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"OtpGeneration\",\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";

        JSONObject cred = new JSONObject(params);
        try {
            JSONObject updatestatus = getstatusobject();
            cred.put("updatestatus", updatestatus);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject data = new JSONObject();
        data.put("operation", "verifyOtp").put("Operation", "verifyOtp")
                .put("MobileNumber", mobilenumber).put("custid", "");
        cred.getJSONObject("data").getJSONArray("data").put(data);

        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        cred.put("auth", auth);

        return cred;
    }

    private JSONObject getstatusobject() throws Exception {
        JSONObject temp = new JSONObject();
        SharedPreferences pref = getSharedPreferences("Config",
                Context.MODE_PRIVATE);

        String person = "";
        String lat = "";
        String lng = "";
        String operation = "";
        String version = "";
        String createdon = "";
        String uuid = "";
        String appname = "";
        String versioncode = "";
        person = pref.getString("personid", "0");

        operation = webconfigurration.status;
        appname = webconfigurration.appname;
        lat = webconfigurration.lat;
        lng = webconfigurration.lng;

        PackageInfo pInfo = getPackageManager().getPackageInfo(
                getPackageName(), 0);
        version = pInfo.versionName;
        versioncode = String.valueOf(pInfo.versionCode);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        createdon = df.format(c.getTime());

        uuid = UUID.randomUUID().toString();

        temp.put("person", person).put("lat", lat).put("lng", lng)
                .put("operation", operation).put("version", version)
                .put("versioncode", versioncode).put("appname", appname)
                .put("createdon", createdon).put("uuid", uuid);
        return temp;
    }

    public void Intialise_OTP_popup(View v, String mobilenumber_in,
                                    String Requeststring_in, boolean send_status, String otp) {

        final String otp_gen = otp;
        final String mobilenumber = mobilenumber_in;
        final String Requeststring = Requeststring_in;

        AlertDialog.Builder builder = new AlertDialog.Builder(mAct);
        LayoutInflater inflater = getLayoutInflater();
        final View popupView = inflater.inflate(R.layout.enter_otp, null);
        builder.setView(popupView);
        final AlertDialog popupWindow = builder.create();
        popupWindow.show();
        popupWindow.setCanceledOnTouchOutside(false);
        //final View popupView = inflater.inflate(R.layout.enter_otp, null);
        //final PopupWindow popupWindow = new PopupWindow(popupView,
        //	LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        //popupWindow.setTouchable(true);
        //popupWindow.setFocusable(true);
        //popupWindow.setOutsideTouchable(true);
        //popupWindow
        //		.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

        //popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);

        ImageView close = (ImageView) popupView.findViewById(R.id.close_window);

        final TextView error = (TextView) popupView
                .findViewById(R.id.textview4);
        error.setVisibility(View.GONE);

        ImageView resend = (ImageView) popupView.findViewById(R.id.Resend);
        final TextView staus_message = (TextView) popupView
                .findViewById(R.id.textView2);

        final EditText code = (EditText) popupView.findViewById(R.id.otpvalue);

        Button confirm = (Button) popupView.findViewById(R.id.confirm);

        Button cancel = (Button) popupView.findViewById(R.id.cancel);
        resend.setVisibility(View.VISIBLE);
        ((TextView) popupView.findViewById(R.id.textview6))
                .setVisibility(View.VISIBLE);
        if (!send_status) {
            staus_message.setText("*Cannot send OTP to " + mobilenumber
                    + "..Try Resend");
            staus_message.setTextColor(Color.parseColor("#FF0000"));
            confirm.setEnabled(false);

        } else {
            staus_message.setText("OTP Send to " + mobilenumber);
            staus_message.setTextColor(Color.parseColor("#008000"));
            confirm.setEnabled(true);
        }

        close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                popupWindow.dismiss();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {

            private String verificationcode_entered;

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String code_value = code.getText().toString();
                if (otp_gen.equals(code_value) && !code_value.equals("")) {
                    // Toast.makeText(mContext, "codematched",
                    // Toast.LENGTH_SHORT).show();
                    error.setVisibility(View.VISIBLE);
                    error.setText("Code Matched");
                    error.setTextColor(Color.parseColor("#008000"));
                    popupWindow.dismiss();
                    OTP_verificationstatus = true;
                    verificationcode_entered = code_value;
                    contactnumber.setEnabled(false);
                    verification.setText("VERIFIED");
                    tick.setVisibility(View.VISIBLE);
//                    if (OTP_verificationstatus) {
//                        //ImageView tickbutton = (ImageView) mView.findViewById(R.id.tickbutton);
//                        tick.setVisibility(View.VISIBLE);
//                    }

                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat(
                            "dd/MM/yyyy HH:mm:ss");
                    String current = sdf.format(c.getTime());
                    OTP_submitTime = current;

                } else {
                    error.setText("* Error Code");
                    error.setTextColor(Color.parseColor("#FF0000"));
                    error.setVisibility(View.VISIBLE);

                }
            }
        });

        resend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String[] cred = new String[1];
                cred[0] = Requeststring;

                if (isNetworkAvailable()) {

                    Toast.makeText(mAct, "Resending code", Toast.LENGTH_SHORT)
                            .show();
                    popupWindow.dismiss();
                    new OTPrequest(v, mobilenumber, Requeststring)
                            .execute(cred);
                } else {
                    error.setVisibility(View.VISIBLE);
                    error.setText("* You are offline");
                }
            }

        });

        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                popupWindow.dismiss();

            }
        });

    }

    // ----------------------Number Call Verification
    // --------------------
    private Boolean getCallDetails(Context context,
                                   String mobilenumber1, PopupWindow popupWindow1,
                                   String submittime) {
        StringBuffer stringBuffer = new StringBuffer();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {

            return false;
        }
        Cursor cursor = getContentResolver().query(
                CallLog.Calls.CONTENT_URI, null, null, null,
                CallLog.Calls.DATE + " DESC");
        int number = cursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = cursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = cursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = cursor.getColumnIndex(CallLog.Calls.DURATION);
        Boolean verified = false;
        int count = 0;
        while (cursor.moveToNext()) {
            if (count < 3) {

                String phNumber = cursor.getString(number);
                String callType = cursor.getString(type);
                String callDate = cursor.getString(date);
                Date callDayTime = new Date(Long.valueOf(callDate));
                String callDuration = cursor.getString(duration);
                String dir = null;
                long dateTimeMillis = cursor.getLong(cursor
                        .getColumnIndex(CallLog.Calls.DATE));
                Date datetime = new Date(dateTimeMillis);
                String dateString = new SimpleDateFormat(
                        "yyyyMMddHHmmss").format(datetime);
                double intdateString = Double.parseDouble(dateString);
                double intsubmittime = Double.parseDouble(submittime);
                if (intdateString > intsubmittime) {
                    int len = phNumber.length();
                    phNumber = phNumber.substring(len - 10, len);
                    int dircode = Integer.parseInt(callType);
                    switch (dircode) {
                        case 1:
                            if (mobilenumber.contentEquals(phNumber)) {
                                popupWindow1.dismiss();
                                OTP_verificationstatus = true;
                                // verificationcode_entered = code_value;
                                contactnumber.setEnabled(false);
                                verification.setText("VERIFIED");
                                if (OTP_verificationstatus) {
                                    ImageView tickbutton = (ImageView) findViewById(R.id.tickbutton);
                                    tickbutton.setVisibility(View.VISIBLE);
                                }
                                Calendar c = Calendar.getInstance();
                                SimpleDateFormat sdf = new SimpleDateFormat(
                                        "dd/MM/yyyy HH:mm:ss");
                                String current = sdf.format(c.getTime());
                                OTP_submitTime = current;
                                verified = true;
                            }
                            break;
                        case 2:// CallLog.Calls.INCOMING_TYPE: dir =
                            // "INCOMING";
                            // Toast.makeText(context,
                            // "OUTGOING call Number=="+phNumber
                            // +" Type=="+callType + "date=="
                            // +callDayTime+
                            // "duration=="+callDuration,
                            // Toast.LENGTH_SHORT).show();
                            break;
                        case 3:
                            if (mobilenumber.contentEquals(phNumber)) {

                                // Toast.makeText(context,
                                // "Equal MISSED call Number=="+phNumber,
                                // Toast.LENGTH_SHORT).show();
                                popupWindow1.dismiss();
                                OTP_verificationstatus = true;
                                // verificationcode_entered = code_value;
                                contactnumber.setEnabled(false);
                                verification.setText("VERIFIED");
                                if (OTP_verificationstatus) {
                                    ImageView tickbutton = (ImageView) findViewById(R.id.tickbutton);
                                    tickbutton.setVisibility(View.VISIBLE);
                                }
                                Calendar c = Calendar.getInstance();
                                SimpleDateFormat sdf = new SimpleDateFormat(
                                        "dd/MM/yyyy HH:mm:ss");
                                String current = sdf.format(c.getTime());
                                OTP_submitTime = current;
                                verified = true;

                            }
                            break;

                        case 5:
                            if (mobilenumber.contentEquals(phNumber)) {

                                // Toast.makeText(context,
                                // "Equal MISSED call Number=="+phNumber,
                                // Toast.LENGTH_SHORT).show();
                                popupWindow1.dismiss();
                                OTP_verificationstatus = true;
                                // verificationcode_entered = code_value;
                                contactnumber.setEnabled(false);
                                verification.setText("VERIFIED");
                                if (OTP_verificationstatus) {
                                    ImageView tickbutton = (ImageView) findViewById(R.id.tickbutton);
                                    tickbutton.setVisibility(View.VISIBLE);
                                }
                                Calendar c = Calendar.getInstance();
                                SimpleDateFormat sdf = new SimpleDateFormat(
                                        "dd/MM/yyyy HH:mm:ss");
                                String current = sdf.format(c.getTime());
                                OTP_submitTime = current;
                                verified = true;

                            }
                            break;
                    }
                    count++;
                }
            }

        }
        cursor.close();
        return verified;// .toString();
    }

    // otp request operation
    public class OTPrequest extends AsyncTask<String, Integer, String> {
        final Dialog innerdialog = new Dialog(mAct);
        private View v;
        private String mobilenumber;
        private String Requeststring;

        public OTPrequest(View v, String mobilenumber, String Requeststring) {
            this.v = v;
            this.mobilenumber = mobilenumber;
            this.Requeststring = Requeststring;
        }

        @Override
        protected void onPreExecute() {
            innerdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            innerdialog.getWindow().clearFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            innerdialog.setContentView(R.layout.adapter_progressdialog);
            TextView txt = (TextView) innerdialog.findViewById(R.id.text);
            txt.setText("Please wait..");
            innerdialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
            innerdialog.show();
            innerdialog.setCancelable(false);

        }

        @Override
        protected String doInBackground(String... credentials) {

            boolean result = false;
            int trycount = 0;
            while (!result && trycount < 2) {
                trycount++;
                try {

                    String targetURL = webconfigurration.url;
                    URL connUrl = new URL(targetURL);
                    HttpURLConnection conn = (HttpURLConnection) connUrl
                            .openConnection();
                    conn.setRequestProperty("Content-Type",
                            "application/json; charset=utf-16");
                    conn.setRequestProperty("Accept-Encoding", "identity");

                    conn.setConnectTimeout(10000);
                    conn.setUseCaches(false);
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setReadTimeout(10000);
                    conn.setRequestMethod("POST");

                    int totalLength = (credentials[0].toString().getBytes(
                            "utf-16").length);
                    conn.setFixedLengthStreamingMode(totalLength);
                    DataOutputStream request = new DataOutputStream(
                            conn.getOutputStream());
                    request.write(credentials[0].toString().getBytes("utf-16"));
                    request.flush();
                    request.close();

                    InputStream is = conn.getInputStream();

                    BufferedReader rd = new BufferedReader(
                            new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while ((line = rd.readLine()) != null) {
                        response.append(line);
                    }
                    final String responsefrom = response.toString();
                    rd.close();
                    result = true;
                    return responsefrom;
                } catch (Exception e) {
                    result = false;
                    e.printStackTrace();
                }
            }
            return null;
        }

        protected void onPostExecute(String result) {
            innerdialog.dismiss();

            // validating status of sms send
            String otp = "";
            boolean send_status = true;
            if (result == null) {
                send_status = false;

            } else {

                try {
                    JSONObject resp = new JSONObject(result);
                    String status = resp.getString("status");
                    otp = resp.getString("otp");
                    if (otp.equals("") || !status.equalsIgnoreCase("success"))
                        send_status = false;
                } catch (JSONException e) {
                    send_status = false;
                }
            }

            Intialise_OTP_popup(v, mobilenumber, Requeststring, send_status,
                    otp);
        }

    }
    public void saveData(double lati, double longi, float acc, String provi,String fixtime) {
        if (lati > 0 && longi > 0) {
            String schbegintime = db.getschedulebegintime();
            if (schbegintime == null || schbegintime == "") {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.SECOND, (c.get(Calendar.SECOND) - 10));
                DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                String sync = dateTimeFormat.format(c.getTime());
                //db.intialisesynctable(sync);

                c = Calendar.getInstance();
                c.set(Calendar.SECOND, (c.get(Calendar.SECOND) - 5));
                schbegintime = dateTimeFormat.format(c.getTime());
                db.updatescheduleheaderbegintime(schbegintime,sync);

                SharedPreferences pref = getApplicationContext().getSharedPreferences(
                        "Config", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("Schbegintime", schbegintime);
                editor.commit();
                Log.e("Schedulebegins", schbegintime);
            }
            try {
                String uuid = UUID.randomUUID().toString();


                String name = shopname;
                String latitude = lati + "";
                String longitude = longi + "";
                String custcode = "";
                String category = cat;
                String landmark = fssai;
                String contactperson = "";
                String contactnumber = phone;
                String othercompanies = gstin;
                String town = "";
                String categoryids = "";
                if (!category.equals(""))
                    categoryids = db.getcustomercategoriesId(category);
                String pan = "";
                String email = "";
                boolean otpverified = OTP_verificationstatus;
                String locprovider = provi+"";
                String locaccuracy = acc+"";
                String directcov = "";
                String closingday = "";
                String storeclosed = "";
                boolean checkedpng = false;
                String branch = "";
                String drugLicienceNo = "";
                String locality = custname;
                String city = helpers;
                String state = "";
                String coverageDay = "";
                String week1 = String.valueOf(doonlineplatformname);
                String week2 = String.valueOf(dobuybrandproductsfrom);
                String week3 = String.valueOf(visitbybrand);
                String week4 = String.valueOf(brandedshop);
                String visitFrequency = onlineplatformname;
                String type = "";
                String wholeSale = "";
                String metro = buybrandproductsfrom;
                String classification = storesize;
                String latitudePhoto = lati+"";
                String longitudePhoto = longi+"";
                String remarks = "";
                String marketname = "";
                String startTime = "";
                String endTime = "";
                String otpsubtime = OTP_submitTime;

                String productcats = "";
                String selectedproductcategories = "";
                for (int i = 0; i < selectedarray.length; i++) {
                    if (selectedarray[i]) {
                        selectedproductcategories += categorycode[i] + ",";
                    }
                }
                if (!selectedproductcategories.equals(""))
                    productcats = selectedproductcategories;

                Addlocation(name, address, latitude, longitude, custcode, category,
                        landmark,contactperson, contactnumber, othercompanies, town, productcats,
                        categoryids, pan, email, otpverified ? "1" : "0", locprovider,
                        locaccuracy, uuid, directcov, closingday, String.valueOf(storeclosed),
                        checkedpng ? "1" : "0", branch, drugLicienceNo, locality,
                        city, state, pin, coverageDay, week1, week2, week3, week4,
                        visitFrequency, type, wholeSale, metro, classification,
                        latitudePhoto, longitudePhoto, schbegintime, remarks,
                        marketname, selectedarraytext, categorycode,
                        startTime, endTime, otpsubtime,fixtime);
                if (customerphoto != null) {
                    String filename = uuid + ".png";
                    createDirectoryAndsavetempfile(customerphoto, "photo_"
                            + filename);
                }
                String uuid1 = UUID.randomUUID().toString();
                HashMap<String, Bitmap> photos = new HashMap<String, Bitmap>();
                photos.put(uuid1, insidephoto);
                if (photos != null && !customerid.equals("") && photos.size() > 0) {
                    db.insertDocumentPhotos(customerid, photos, createtime, "0");
                }
                if (photos != null && !customerid.equals("") && photos.size() > 0) {
                    for (String key : photos.keySet()) {
                        try {
                            Bitmap bitmap = photos.get(key);
                            String filename = key + ".png";
                            createDirectoryAndsavetempfile(bitmap, "document_"
                                    + filename);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(NewCustomerActivity.this, R.string.please_try_again, Toast.LENGTH_SHORT).show();

                try {
                    File tempDir = Environment.getExternalStorageDirectory();
                    tempDir = new File(tempDir.getAbsolutePath() + "/AriesErrors/");
                    if (!tempDir.exists()) {
                        tempDir.mkdirs();
                    }
                    Calendar c = Calendar.getInstance();
                    String myFormat = "dd-MM-yyyy HH:mm:ss";
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                    myFormat = "yyyyMMddHHmmss";
                    sdf = new SimpleDateFormat(myFormat, Locale.US);
                    String filedate = sdf.format(c.getTime());
                    File myExternalFile = new File(tempDir, "errorlog_" + filedate
                            + ".txt");
                    String error = filedate + "-----------";
                    error += e.toString();
                    FileOutputStream fos = new FileOutputStream(myExternalFile);
                    OutputStreamWriter osw = new OutputStreamWriter(fos);
                    osw.append(error);
                    osw.close();
                } catch (IOException ioe) {
                    // ...
                }
            }finally {
                finishActivityFromHere();
            }
        }
    }
    public void Addlocation(String name, String adress, String latitude,
                            String longitude, String costomercode, String customercategory,
                            String nearestlandmarkvalue,
                            String contactpersonvalue, String Contactpersonnumbervalue,
                            String anyothercompanyvalue, String townclassvalue,
                            String selectedproductcategories, String customercatogorycode,
                            String pan, String email, String otpverified, String locprovider,
                            String locaccuracy, String uuid, String directcov,
                            String closingday, String storeclosed, String pngcovered,
                            String branch, String drugLicienceNo, String locality, String city,
                            String state, String pin, String coverageDay, String week1,
                            String week2, String week3, String week4, String visitFrequency,
                            String type, String wholeSale, String metro, String classification,
                            String latitudePhoto, String longitudePhoto, String schbegintime,
                            String remarks, String marketname, String[] selectedarraytext,
                            String[] categorycodearray, String startTime, String endTime, String otpsubtime, String fixtime)
            throws Exception {
        Locations loctodb = new Locations();
        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String crtime = dateTimeFormat.format(c.getTime());
        Log.e("CreatedCust", crtime);
        String schdulineguid = UUID.randomUUID().toString();

        loctodb.Locationname = name;
        loctodb.Locationadress = adress;
        loctodb.Latitude = latitude;
        loctodb.Longitude = longitude;
        loctodb.Locationid = UUID.randomUUID().toString();
        customerid = loctodb.Locationid;
        loctodb.createtime = crtime;
        createtime = crtime;
        loctodb.costomercode = costomercode;
        loctodb.customercategory = customercategory;
        //loctodb.tinnumber = tinnumber;

        //by yadhu
//        loctodb.fssainumber = fssaino;
//        loctodb.gstinnumber = gstino;
//        loctodb.custshopname = custshopname;
//        loctodb.custstoresize = custstoresize;
//        loctodb.custonlineplatform = custonlineplatform;
//        loctodb.buybrandproducts = buybrandproducts;
//        loctodb.custvisitbybrands = custvisitbybrands;
//        loctodb.custbrandedshop = custbrandedshop;
        //-----

        loctodb.nearestlandmarkvalue = nearestlandmarkvalue;
        loctodb.contactpersonvalue = contactpersonvalue;
        loctodb.Contactpersonnumbervalue = Contactpersonnumbervalue;
        loctodb.anyothercompanyvalue = anyothercompanyvalue;
        loctodb.townclassvalue = townclassvalue;
        loctodb.selectedproductcategories = selectedproductcategories;
        loctodb.customercatogorycode = customercatogorycode;
        loctodb.schdulineguid = schdulineguid;
        loctodb.pan = pan;
        loctodb.otpverified = otpverified;
        loctodb.locprovider = locprovider;
        loctodb.locaccuracy = locaccuracy;
        loctodb.email = email;
        loctodb.photouuid = uuid;
        loctodb.directcov = directcov;
        loctodb.closingday = closingday;
        loctodb.storeclosed = storeclosed;
        loctodb.pngcovered = pngcovered;
        loctodb.branch = branch;

        loctodb.drugLicienceNo = drugLicienceNo;
        loctodb.locality = locality;
        loctodb.city = city;
        loctodb.state = state;
        loctodb.pin = pin;
        loctodb.coverageDay = coverageDay;
        loctodb.week1 = week1;
        loctodb.week2 = week2;
        loctodb.week3 = week3;
        loctodb.week4 = week4;
        loctodb.visitFrequency = visitFrequency;
        loctodb.type = type;
        loctodb.wholeSale = wholeSale;
        loctodb.metro = metro;
        loctodb.classification = classification;
        loctodb.latitudePhoto = latitudePhoto;
        loctodb.longitudePhoto = longitudePhoto;
        loctodb.remarks = remarks;
        loctodb.marketname = marketname;
        loctodb.startTime = startTime;
        loctodb.endTime = endTime;
        loctodb.otpsubtime = otpsubtime;
        loctodb.fixtime=fixtime;

        db.addnewlocation(loctodb);

        String schedheadid = db.isnewlocationsexist();
        if (schedheadid.equals("0")) {
            Calendar c2 = Calendar.getInstance();
            DateFormat dateTimeFormat2 = new SimpleDateFormat("MM/dd/yyyy");
            String schdate = dateTimeFormat2.format(c2.getTime());
            SharedPreferences pref = getApplicationContext()
                    .getSharedPreferences("Config", MODE_PRIVATE);
            Scheduledata headerdatatodb = new Scheduledata();
            headerdatatodb.Scheduleid = UUID.randomUUID().toString();
            headerdatatodb.Schedulename = "New Locations";
            headerdatatodb.Salespersonid = pref.getString("personid", "0");
            headerdatatodb.salespersonname = pref.getString("personname", "");
            headerdatatodb.Routenetworkid = "0";
            headerdatatodb.Headerstatus = "";
            headerdatatodb.Scheduledate = schdate;
            headerdatatodb.begintime = schbegintime;

            Scheduledetail detail = new Scheduledetail();

            detail.Scheduledetailid = schdulineguid;
            detail.locationid = loctodb.Locationid;
            detail.locationname = loctodb.Locationname;
            detail.locationadress = loctodb.Locationadress;
            detail.sequencenumber = "1";
            detail.status = "pending";
            detail.latitude = loctodb.Latitude;
            detail.longitude = loctodb.Longitude;
            detail.Customercatogory = loctodb.customercategory;
            detail.CustomercatogoryId = db
                    .getcustomercategoriesId(loctodb.customercategory);
            detail.invoicenumber = "";
            detail.picklistnumber = "";
            detail.customercode = loctodb.costomercode;
            detail.locationlock = "0";
            detail.Closingday = "";
            detail.mobilenumber = loctodb.Contactpersonnumbervalue;
            //detail.tinnumber = loctodb.tinnumber;
            detail.pan = loctodb.pan;
            detail.landmark = loctodb.nearestlandmarkvalue;
            detail.town = townclassvalue;
            detail.contactperson = loctodb.contactpersonvalue;
            detail.othercompany = loctodb.anyothercompanyvalue;
            detail.otpverified = otpverified;
            detail.locprovider = locprovider;
            detail.locaccuracy = locaccuracy;
            detail.photoUUID = uuid;
            detail.email = email;
            detail.directcov = directcov;
            detail.selectedproductcategories = selectedproductcategories;
            detail.Closingday = closingday;
            detail.storeclosed = storeclosed;
            detail.pngcovered = pngcovered;
            detail.branch = branch;

            detail.drugLicienceNo = drugLicienceNo;
            detail.locality = locality;
            detail.city = city;
            detail.state = state;
            detail.pin = pin;
            detail.coverageDay = coverageDay;
            detail.week1 = week1;
            detail.week2 = week2;
            detail.week3 = week3;
            detail.week4 = week4;
            detail.visitFrequency = visitFrequency;
            detail.type = type;
            detail.wholeSale = wholeSale;
            detail.metro = metro;
            detail.classification = classification;
            loctodb.marketname = marketname;
            detail.otpsubtime = otpsubtime;

            db.firstlocationadd(headerdatatodb, detail,crtime);
        } else {
            Scheduledetail detail = new Scheduledetail();
            detail.Scheduledetailid = schdulineguid;
            detail.locationid = loctodb.Locationid;
            detail.locationname = loctodb.Locationname;
            detail.locationadress = loctodb.Locationadress;
            detail.sequencenumber = "1";
            detail.status = "pending";
            detail.latitude = loctodb.Latitude;
            detail.longitude = loctodb.Longitude;
            detail.Customercatogory = loctodb.customercategory;
            detail.CustomercatogoryId = db
                    .getcustomercategoriesId(loctodb.customercategory);
            detail.invoicenumber = "";
            detail.picklistnumber = "";
            detail.customercode = loctodb.costomercode;
            detail.locationlock = "0";
            detail.Closingday = "";
            detail.mobilenumber = loctodb.Contactpersonnumbervalue;
            //detail.tinnumber = loctodb.tinnumber;
            detail.pan = loctodb.pan;
            detail.landmark = loctodb.nearestlandmarkvalue;
            detail.town = townclassvalue;
            detail.contactperson = loctodb.contactpersonvalue;
            detail.othercompany = loctodb.anyothercompanyvalue;
            detail.otpverified = otpverified;
            detail.locprovider = locprovider;
            detail.locaccuracy = locaccuracy;
            detail.photoUUID = uuid;
            detail.email = email;
            detail.directcov = directcov;
            detail.selectedproductcategories = selectedproductcategories;
            detail.Closingday = closingday;
            detail.storeclosed = storeclosed;
            detail.pngcovered = pngcovered;
            detail.branch = branch;

            detail.drugLicienceNo = drugLicienceNo;
            detail.locality = locality;
            detail.city = city;
            detail.state = state;
            detail.pin = pin;
            detail.coverageDay = coverageDay;
            detail.week1 = week1;
            detail.week2 = week2;
            detail.week3 = week3;
            detail.week4 = week4;
            detail.visitFrequency = visitFrequency;
            detail.type = type;
            detail.wholeSale = wholeSale;
            detail.metro = metro;
            detail.classification = classification;
            loctodb.marketname = marketname;
            detail.otpsubtime = otpsubtime;

            db.addtoexistingnewlocations(schedheadid, detail, crtime);
        }
        if (selectedarraytext.length > 0) {
            for (int i = 0; i < selectedarraytext.length; i++) {
                String datas = selectedarraytext[i];
                boolean chk = selectedarray[i];
                if(chk == true && datas == null)
                {
                    db.insertproductinputs(schdulineguid, categorycodearray[i],
                            "");
                }
                if (datas != null) {
                    // Toast.makeText(AddLocationNewActivity.this, datas,
                    // Toast.LENGTH_LONG).show();
                    db.insertproductinputs(schdulineguid, categorycodearray[i],
                            selectedarraytext[i]);
                }
            }
        }
    }
    private void finishActivityFromHere() {
        Intent intent = new Intent(NewCustomerActivity.this, LandingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
        finish();
    }
    private void createDirectoryAndsavetempfile(Bitmap imageToSave,
                                                String fileName) {

        File rootsd = Environment.getExternalStorageDirectory();
        File direct = new File(rootsd.getAbsolutePath() + "/Aries");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        File file = new File(direct, fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 75, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
