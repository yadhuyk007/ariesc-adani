package combing.sales.adani.gps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import combing.sales.adani.database.DatabaseHandler;
import combing.sales.adani.objects.Global;
import combing.sales.adani.objects.Vehicledata;
import combing.sales.adani.objects.webconfigurration;


/**
 * Created by hp on 22/04/2019.
 */

public class SyncNavData {
    private static DatabaseHandler db;
    private static Context context;

    public static void syncnavdata(Context c) {
        context = c;
        db = new DatabaseHandler(context);
        List<Vehicledata> Navigdata = db.Getallnavigationdata();
        if (Navigdata.size() > 0) {
            try {
                Log.d("Insert:", "Reading all data..");
                Log.d("Insert:", "allvalues are readed");
                String count = String.valueOf(Navigdata.size());
                Log.d("Insert:", count);
                String params = jsonSerialise(Navigdata);
                Log.d("Insert:", params);
                String ids = getpkidstring(Navigdata);
                Log.d("Insert:", ids);
                for (int i = 0; i < Navigdata.size(); i++) {
                    Log.d("Insert:",
                            Navigdata.get(i).PlanId + " "
                                    + Navigdata.get(i).VehicleId + " "
                                    + Navigdata.get(i).DeviceTime + " "
                                    + Navigdata.get(i).Latitude + " "
                                    + Navigdata.get(i).Longitude + " "
                                    + Navigdata.get(i).Speed);
                }
                excutePostdatstream(params, ids);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    public static String getpkidstring(List<Vehicledata> Navigationdata) {
        // (1, 4, 6, 7)
        String pkdata = "(";
        int last = Navigationdata.size() - 1;
        for (int i = 0; i < (Navigationdata.size() - 1); i++) {
            pkdata = pkdata + String.valueOf(Navigationdata.get(i).pkid) + ",";

        }
        pkdata = pkdata + String.valueOf(Navigationdata.get(last).pkid) + ")";
        return pkdata;
    }

    public static String jsonSerialise(List<Vehicledata> Vehdata) throws JSONException {
        JSONArray data = new JSONArray();
        PackageInfo pinfo;
        int versionNumber = 0;
        String versionName = "";
        String android_id = "";
        String imei = "";

        try {
            pinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionNumber = pinfo.versionCode;
            versionName = pinfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            android_id = Settings.Secure.getString(context
                    .getContentResolver(), Settings.Secure.ANDROID_ID);

            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                String TODO="";
                return TODO;
            }
            imei = telephonyManager.getDeviceId();
        } catch (Exception e) {
            if (android_id != null && android_id.equals(""))
                android_id = "NA";

            if (imei != null && imei.equals(""))
                imei = "NA";

            e.printStackTrace();
        }

        for (int i = 0; i < Vehdata.size(); i++) {
            JSONObject obj = new JSONObject();
            obj.put("SalesPersonId", Vehdata.get(i).PlanId);
            obj.put("NavTime", Vehdata.get(i).DeviceTime);
            obj.put("lat", Vehdata.get(i).Latitude);
            obj.put("lng", Vehdata.get(i).Longitude);
            obj.put("provider", Vehdata.get(i).provider);
            obj.put("accuracy", Vehdata.get(i).accuracy);
            obj.put("speed", Vehdata.get(i).Speed);
            obj.put("bearing", Vehdata.get(i).bearing);
            obj.put("distance", Vehdata.get(i).distance);
            obj.put("AppVersion", versionName);
            obj.put("AndroidId", android_id);
            obj.put("imei", imei);
            obj.put("customer", Vehdata.get(i).customer);
            obj.put("timediff", Vehdata.get(i).timediff);
            obj.put("inactiveflag", Vehdata.get(i).inactive);
            data.put(obj);

        }
        return data.toString();

    }
    public static void excutePostdatstream(String urlParameters, String pkidlist)
            throws JSONException {

        webconfigurration C = new webconfigurration(context);

        String inputParamsStr = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"SalesproOperations\",\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";

        JSONObject inputParamObj = new JSONObject(inputParamsStr);
        JSONObject data = new JSONObject();
        data.put("operation", "SalesPersonNav").put("data",
                new JSONArray(urlParameters));
        inputParamObj.getJSONObject("data").getJSONArray("data").put(data);
        Log.e("Salespersonnav", urlParameters);
        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        inputParamObj.put("auth", auth);
        String string = "";
        try {
            string = inputParamObj.toString();
        } catch (Exception ex) {
            ex.toString();
        }
        if (!string.contentEquals("")) {
            MyRunnable myRunnable = new MyRunnable(string, pkidlist);
            Thread t = new Thread(myRunnable);
            if(!Global.synnav){
                t.start();
                Global.synnav=true;
            }
        }
    }

    public static class MyRunnable implements Runnable {

        private String urlParameters;
        private String pkidlist;

        public MyRunnable(String urlParameters, String pkidlist) {
            this.urlParameters = urlParameters;
            this.pkidlist = pkidlist;
        }

        @Override
        public void run() {
            if(db.NavDatacounttoSync()!=pkidlist.split(",").length)
            {
                if(Global.synnav){
                    Global.synnav=false;
                }
                return;
            }

            webconfigurration C = new webconfigurration(context);
            String targetURL = C.url;
            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-8");
                conn.setReadTimeout(30000);
                conn.setConnectTimeout(5000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);

                conn.setRequestMethod("POST");
                // conn.setRequestProperty("Connection", "Keep-Alive");
                // conn.setRequestProperty("Cache-Control", "no-cache");

                int totalLength = (urlParameters.getBytes().length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.writeBytes(urlParameters);
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();
                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                String responsefrom = response.toString();
                JSONObject resp = new JSONObject(responsefrom);
                responsefrom = resp.getString("data");
                Log.d("post", responsefrom);
                rd.close();

                if (responsefrom.equals("Success")) {

                    db.cleartable(pkidlist);
                    notifyReturn(resp);
                }
                if (conn != null) {
                    conn.disconnect();
                }
            } catch (Exception e) {
                Log.d("post", "cannot create connection");
                e.printStackTrace();

            }finally{
                if(Global.synnav){
                    Global.synnav=false;
                }
            }

        }
    }
    public static void notifyReturn(JSONObject resp) throws Exception {
        try {
            JSONArray arr = resp.optJSONArray("NotificationData");
            Log.d("Return:", arr.toString());

            if (arr.length() > 0) {
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject obj = arr.getJSONObject(i);
                    // foreTest(obj);
                }
            }
        } catch (JSONException e) {
        }
    }

}
