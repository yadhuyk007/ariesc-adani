package combing.sales.adani.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import combing.sales.adani.objects.FourStrings;
import combing.sales.adani.objects.Global;
import combing.sales.adani.objects.Locations;
import combing.sales.adani.objects.Scheduledata;
import combing.sales.adani.objects.Scheduledetail;
import combing.sales.adani.objects.Vehicledata;
import combing.sales.adani.objects.product;


/**
 * Created by hp on 03/04/2019.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Vehiclenavigationdb";
    private static final String TABLE_NAVIGATIONDATA = "navigationdatatable";
    private static final String KEY_NAVIGATIONID = "navigationid";
    private static final String KEY_PLANID = "planid";
    private static final String KEY_VEHICLEID = "vehicleid";
    private static final String KEY_DEVICETIME = "devicetime";
    private static final String KEY_LATITUDE = "latitude";
    private static final String KEY_LONGITUDE = "longitude";
    private static final String KEY_SPEED = "speed";
    private static final String KEY_PROVIDER = "provider";
    private static final String KEY_BEARING = "bearing";
    private static final String KEY_ACCURACY = "accuracy";
    private static final String KEY_DISTANCE = "distance";
    private static final String NAVIGATIONDATA_UPFLAG = "upflag";
    private static final String KEY_CUSTOMER = "customerid";
    private static final String KEY_TIMEDIFF = "timediff";
    private static final String KEY_MONGODT = "mongodt";
    private static final String KEY_INACTIVE = "inactive";
    private static final String TABLE_JSONS = "jsons";

    // private static final String TABLE_JSONS2 = "jsons2";
    // private static final String JSONS_ID2 = "id";
    // private static final String JSONS_TITLE2 = "title";
    // private static final String JSONS_DATA2 = "data";
    // private static final String JSONS_DATE2 = "curDate";
    private static final String JSONS_ID = "id";
    private static final String JSONS_TITLE = "title";
    private static final String JSONS_DATA = "data";
    private static final String JSONS_DATE = "curDate";
    // mobile services
    private static final String TABLE_MOBSERVICES = "mobileServices";
    private static final String MOBILESERVICE_ID = "id";
    private static final String MOBILESERVICE_NAME = "title";
    private static final String MOBILESERVICE_CODE = "code";
    private static final String MOBILESERVICE_DISABLE_INVENTORY = "inventoryFlag";
    // schedule header
    private static final String TABLE_SCHEDULE_HEADER = "scheduleheader";
    private static final String SCHEDULE_HEADER_PKID = "scheduleheaderpkid";
    private static final String SCHEDULE_ID = "scheduleid";
    private static final String SCHEDULE_NAME = "schedulename";
    private static final String SALESPERSON_ID = "salespersonid";
    private static final String SALESPERSON_NAME = "salespersonname";
    private static final String SALEROUTE_ID = "salesrouteid";
    private static final String SCHEDULE_DATE = "scheduledate";
    private static final String HEADER_STATUS = "headerstatus";
    private static final String ROUTENETWORK_ID = "routenetworkid";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private static final String SCHEDULE_COMPLETION_STATUS = "cmpletionstatus";
    private static final String SCHEDULE_BEGIN_TIME = "begintime";
    private static final String SCHEDULE_COMPLETION_TIME = "cmpletiontime";
    /*-------------SOJU START-----------------------*/
    // schedule streetname
    private static final String SCHEDULEDETAIL_STREETNAME = "streetname";
    private static final String SCHEDULEDETAIL_AREA = "areaname";
    private static final String SCHEDULEDETAIL_LANDLINE = "landline";
    private static final String SCHEDULEDETAIL_SHOP = "shop";
    private static final String SCHEDULEDETAIL_CITY = "city";
    private static final String SCHEDULEDETAIL_PIN = "pin";
    private static final String SCHEDULEDETAIL_PLAN = "plan";
    // asset detail
    private static final String TABLE_ASSETS = "assettable";
    private static final String ASSETS_PKID = "id";
    private static final String ASSETS_ID = "assetid";
    private static final String ASSETS_ASSETQNHDR = "assetqnhdr";
    private static final String ASSETS_QSTRING = "qstring";
    private static final String ASSETS_SLINEID = "slineid";
    private static final String ASSETS_QLINEID = "qlineid";
    private static final String ASSETS_CUSTOMER = "customer";
    private static final String ASSETS_QRCODE = "qrstring";
    private static final String ASSETS_ANSWER = "answer";
    private static final String ASSETS_PHOTOUUID = "photo";
    private static final String ASSETS_CREATEDTIME = "crtime";
    private static final String ASSETS_PHOTOREQ = "photoreq";
    // orgtable detail
    private static final String TABLE_ORGANIZATION = "orgtable";
    private static final String ORGANIZATION_PKID = "id";
    private static final String ORGANIZATION_ORGLAT = "orglat";
    private static final String ORGANIZATION_ORGLONG = "orglong";
    private static final String ORGANIZATION_ACCURACY = "acc";
    private static final String ORGANIZATION_OSRMPORT = "osrmport";
    private static final String ORGANIZATION_LOCATIONACCURACY = "locacc";
    // mobile organization level services
    private static final String TABLE_ORGLEVELSERVICES = "tableorglevelsvs";
    private static final String ORGLEVELSERVICES_ID = "id";
    private static final String ORGLEVELSERVICES_NAME = "title";
    private static final String ORGLEVELSERVICES_CODE = "code";
    private static final String TABLE_DRIVER = "tabledriver";
    private static final String DRIVER_PKID = "id";
    private static final String DRIVER_ID = "driverid";
    private static final String DRIVER_CODE = "drivercode";
    private static final String DRIVER_NAME = "drivername";
    private static final String DRIVER_MOBILE = "drivermobile";
    private static final String TABLE_VEHICLE = "tablevehicle";
    private static final String VEHICLE_PKID = "id";
    private static final String VEHICLE_ID = "vehid";
    private static final String VEHICLE_REGNO = "vehreg";
    private static final String TABLE_ROUTE = "routetable";
    private static final String ROUTE_PKID = "id";
    private static final String ROUTE_CATEGORY = "routecategory";
    private static final String SCHEDULEDETAIL_DEL_PHOTOUUID = "delphotouuid";
    private static final String SCHEDULEDETAIL_DEL_PHOTOLATTITUDE = "delphotolat";
    private static final String SCHEDULEDETAIL_DEL_PHOTOLONGITUDE = "delphotolong";
    private static final String SCHEDULEDETAIL_DEL_PHOTOFLAG = "delphotoflag";
    private static final String SCHEDULEDETAIL_DEL_ACCURACY = "delaccracy";
    private static final String SCHEDULEDETAIL_REASON_ACC = "resonaccracy";
    private static final String SCHEDULEDETAIL_PHOTO_ACCURACY = "photoaccuracy";
    private static final String SCHEDULEDETAIL_PHOTO_EDITTIME = "photoedittime";
    private static final String SCHEDULEDETAIL_LATITUDE_ACCURACY = "lattaccuracy";
    private static final String SCHEDULEDETAIL_STARTTIME = "starttime";
    private static final String SCHEDULEDETAIL_ENDTIME = "endtime";
    private static final String SCHEDULEDETAIL_NEEDOTP = "needotp";
    private static final String SCHEDULEDETAIL_OTPSUBTIME = "otpsubtime";
    private static final String SCHEDULEDETAIL_OTPLAT = "otplat";
    private static final String SCHEDULEDETAIL_OTPLONG = "otplong";
    private static final String INVOICE_STATICBALANCE = "staticbalance";
    private static final String INVOICE_PERSONID = "personid";
    private static final String INVOICE_PERSONMOB = "personmob";
    private static final String INVOICE_CUSTCODE = "invoivecustcode";
    private static final String INVOICE_CUSTNAME = "invoicecustname";
    private static final String INVOICE_PERSONCODE = "invoicepersoncode";
    private static final String INVOICE_OUTAMT = "invoiveoutamt";
    private static final String INVOICE_TYPE = "invoicetype";
    private static final String INVOICE_ASOFDATE = "invoiceasofdate";
    private static final String SCHEDULE_EXPIRY_STATUS = "expirystatus";
    private static final String SCHEDULE_ODOMETER = "odometer";
    private static final String SCHEDULE_DRIVER = "driver";
    private static final String SCHEDULE_VEHICLE = "vehicle";
    private static final String SCHEDULE_ODOMETER_UUID = "odometeruuid";
    private static final String SCHEDULE_COMPETE_ODOMETER = "completeodometer";
    private static final String SCHEDULE_COMPETE_UUID = "competeodometeruuid";
    private static final String SCHEDULE_SYNC_TIME = "synctime";
    private static final String REASON_SENDBACK = "reasonsendback";
    private static final String PRODUCTCHECKLIST_TYPE = "checklisttype";
    private static final String SEARCHCUSTOMERS_ROUTES = "custroute";
    private static final String SEARCHCUSTOMERS_OTPVERIFIED = "otpverified";
    private static final String SEARCHCUSTOMERS_OTPLAT = "otplat";
    private static final String SEARCHCUSTOMERS_OTPLONG = "otplong";
    /*-------------SOJU END-----------------------*/
    private static final String SCHEDULEDETAIL_DEL_STATUS = "deliverstatus";
    private static final String SCHEDULEDETAIL_DEL_SUBMITTIME = "submittimedel";
    private static final String SCHEDULEDETAIL_DEL_STATUSCRTIME = "deliverstatuscrt";
    private static final String SCHEDULEDETAIL_DEL_LATTITUDE = "dellat";
    private static final String SCHEDULEDETAIL_DEL_LONGITUDE = "dellong";
    // scheduled detail
    private static final String TABLE_SCHEDULE_DETAIL = "scheduledetail";
    private static final String SCHEDULE_DETAIL_PKID = "scheduledetailpkid";
    private static final String SCHEDULE_DETAIL_ID = "scheduledetailid";
    private static final String SCHEDULE_HEADER_ID = "scheduleheadid";
    private static final String LOCATION_ID = "locationid";
    private static final String LOCATION_NAME = "locationname";
    private static final String LOCATION_ADRESS = "locationadress";
    private static final String SEQNO = "sequencenumber";
    private static final String DETAIL_STATUS = "status";
    private static final String SCHEDULEDETAIL_CREATETIME = "createtime";
    private static final String LATITUDE_NEW = "latitudenew";
    private static final String LONGITUDE_NEW = "longitudeNEW";
    private static final String CUSTOMERCATOGORY = "customercatogory";
    private static final String RETAILPRICE = "retailprice";
    private static final String INVOCENUMBER = "invoicenumber";
    private static final String PICKLISTNUMBER = "picklistnumber";
    private static final String STOREVISIT_REASON = "reason";
    private static final String STOREVISIT_LATTITUDE = "storevisitlatitude";
    private static final String STOREVISIT_LONGITUDE = "storevisitlongitude";
    private static final String STOREVISIT_TIME = "storevisittime";
    private static final String SCHED_CUSTOMERCODE = "customercode";
    private static final String SCHED_LOCUPDATETIME = "locupdatetime";
    private static final String LOCATIONLOCK = "locationlock";
    private static final String CLOSING_DAY = "closingday";
    private static final String CLOSING_DAY_UPDATETIME = "closingdayupdatetime";
    private static final String SCHEDULE_DETAIL_PRODUCTIVE = "productive";
    private static final String SCHEDULE_DETAIL_VISITED = "visited";
    private static final String CUSTOMERCATOGORYID = "customercatogoryid";
    private static final String SCHEDULEDETAIL_MOBILE = "mobilenumber";
    private static final String SCHEDULEDETAIL_GSTNO = "gstno";
    private static final String SCHEDULEDETAIL_FSSAINO = "fssai";
    private static final String SCHEDULEDETAIL_OTP = "otpnumber";
    private static final String SCHEDULEDETAIL_TINNUMBER = "tinnumber";
    private static final String SCHEDULEDETAIL_PAN = "pan";
    private static final String SCHEDULEDETAIL_EDITTIME = "edittime";
    private static final String SCHEDULEDETAIL_PHOTOUUID = "photouuid";
    private static final String SCHEDULEDETAIL_IMGLAT = "imglat";
    private static final String SCHEDULEDETAIL_IMGLONG = "imglong";
    private static final String SCHEDULEDETAIL_LANDMARK = "landmark";
    private static final String SCHEDULEDETAIL_TOWN = "town";
    private static final String SCHEDULEDETAIL_CONTACTPERSON = "contactperson";
    private static final String SCHEDULEDETAIL_OTHERCOMPANY = "othercompany";
    private static final String SCHEDULEDETAIL_CRATES = "crates";
    private static final String SCHEDULEDETAIL_OTPVERIFIED = "otpverified";
    private static final String SCHEDULEDETAIL_LOCPROVIDER = "locprovider";
    private static final String SCHEDULEDETAIL_LOCACCURACY = "accuracy";
    private static final String SCHEDULEDETAIL_EMAIL = "email";
    private static final String SCHEDULEDETAIL_DIRECTCOV = "directcov";
    private static final String SCHEDULEDETAIL_SELPRODCAT = "selprodcat";
    private static final String SCHEDULEDETAIL_STORECLOSED = "storeclosed";
    private static final String SCHEDULEDETAIL_PNGCOVERED = "pngcovered";
    private static final String SCHEDULEDETAIL_BRANCH = "branch";
    private static final String SCHEDULEDETAIL_PASSBY = "passby";
    private static final String SCHEDULEDETAIL_PASSBYCRTIME = "passcrtime";
    private static final String SCHEDULEDETAIL_ALTERCUSTCODE = "AlternateCustcode";
    private static final String SCHEDULEDETAIL_LATTITUDE = "storereasonlatitude";
    private static final String SCHEDULEDETAIL_LONGITUDE = "storereasonlongitude";
    private static final String SCHEDULEDETAIL_CUSTTYPE = "custtype";
    private static final String SCHEDULEDETAIL_TARGET_TYPE = "targetType";
    private static final String SCHEDULEDETAIL_TARGET_VALUE = "targetVal";
    private static final String SCHEDULEDETAIL_CREDIT_LIMIT = "creditLimit";
    private static final String SCHEDULEDETAIL_CREDIT_BALANCE = "creditBal";
    private static final String SCHEDULEDETAIL_GSTTYPE = "gsttype";
    private static final String SCHEDULEDETAIL_FSSI = "fssi";
    private static final String SCHEDULEDETAIL_UPDATETIME = "updatetime";
    private static final String SCHEDULEDETAIL_CUSTIMGURL = "custimgurl";

    // order header
    private static final String TABLE_ORDER_HEADER = "orderheader";
    private static final String ORDER_HEADER_PKID = "orderheaderpkid";
    private static final String ORDER_SCHEDULE_DETAIL_ID = "scheduledetailid";
    private static final String ORDER_STATUS = "status";
    private static final String ORDERBEGIN_TIME = "orderbegintime";
    private static final String ORDERSUBMIT_TIME = "ordersubmittime";
    private static final String ORDER_LATITUDE = "latitude";
    private static final String ORDER_LONGITUDE = "longitude";
    private static final String ORDER_SALESPERSONID = "salespersonid";
    private static final String ORDER_ROUTENETWORKID = "routenetworkid";
    private static final String ORDER_LOCATIONID = "locationid";
    // order detail
    private static final String TABLE_ORDER_DETAIL = "orderdetail";
    private static final String TABLE_ORDERDETAIL_PKID = "orderheaddetailpkid";
    private static final String ORDER_HEAD_ID = "orderheaderid";
    private static final String PRODUCTID = "productid";
    private static final String QUANTITY = "quantity";
    private static final String DELIVERYDATE = "deliverydate";
    private static final String CREATETIME = "creattime";
    private static final String DELETEFLAG = "deleteflag";
    private static final String RATE = "rate";
    private static final String PRODUCT_TAX = "tax";
    private static final String TOTTAL_AMOUNT = "tottalamount";
    // Sync table
    private static final String TABLE_SYNCTABLE = "syncronisingtable";
    private static final String INDEXPKID = "indexpkid";
    private static final String TABLEINSYNC = "syncingtable";
    private static final String SYNCTIME = "synctime";
    // Location table
    private static final String TABLE_LOCATIONS = "loactiontable";
    private static final String LOCATION_PKID = "locationpkid";
    private static final String LOCATIONID = "locationid";
    private static final String NAME = "name";

    //by yadhu

    private static final String FSSAI_NO = "fssai";
    private static final String GSTIN_NO = "gstin";
    private static final String SHOP_NAME = "shopname";
    private static final String CUST_STORESIZE = "custstoresize";
    private static final String CUST_ONLINEPLATFORM = "onlioneplatform";
    private static final String CUST_BUYBRAND_PROD = "brandproducts";
    private static final String CUST_VISITBY_BRANDS = "visitbybrands";
    private static final String CUST_BRANDEDSHOP = "brandedshop";

    private static final String ADDRESS = "adress";
    private static final String LOCATION_LATITUDE = "latitude";
    private static final String LOCATION_LONGITUDE = "longitude";
    private static final String LOCATION_CREATETIME = "createtime";
    private static final String CUSTOMER_CODE = "costomercode";
    private static final String CUSTOMER_CATOGORY = "customercatogory";
    private static final String TIN_NUMBER = "tinnumber";
    private static final String NEAREST_LANDMARK = "nearestlandmark";
    private static final String CONTACT_PERSONNAME = "contactpersonname";
    private static final String CONTACT_PERSON_NUMBER = "contactnumber";
    private static final String ANT_OTHERCOMPANYVALUE = "anyothercompany";
    private static final String TOWNCLASS = "townclass";
    private static final String SELECTED_PRODUCT_CATEGORIES = "selectedproductcategories";
    private static final String CUSTOMERCATEGOY_CODE = "customercategorycode";
    private static final String CUSTOMER_SCHEDULELINE_GUID = "schedulelineguid";
    private static final String LOCATION_PAN = "Pannumber";
    private static final String LOCATION_OTPVERIFIED = "otpverified";
    private static final String LOCATION_LOCPROVIDER = "locprovider";
    private static final String LOCATION_LOCACCURACY = "locaccuracy";
    private static final String LOCATION_EMAIL = "email";
    private static final String LOCATION_PHOTOUUID = "photouuid";
    private static final String LOCATION_DIRECTCOV = "directcov";
    private static final String LOCATION_CLOSING_DAY = "closingday";
    private static final String LOCATION_STOREISCLOSED = "storeclosed";
    private static final String LOCATION_PNGCOVERED = "pngcovered";
    private static final String LOCATION_BRANCH = "branch";
    private static final String LOCATION_DRUG = "drug";
    private static final String LOCATION_LOCALITY = "localty";
    private static final String LOCATION_CITY = "city";
    private static final String LOCATION_STATE = "state";
    private static final String LOCATION_PIN = "pin";
    private static final String LOCATION_COVDAY = "covday";
    private static final String LOCATION_WEEK1 = "week1";
    private static final String LOCATION_WEEK2 = "week2";
    private static final String LOCATION_WEEK3 = "week3";
    private static final String LOCATION_WEEK4 = "week4";
    private static final String LOCATION_VISTFRQ = "vistfrq";
    private static final String LOCATION_TYPE = "typee";
    private static final String LOCATION_WHOLSAL = "wholsal";
    private static final String LOCATION_METRO = "metro";
    private static final String LOCATION_CLASSIF = "classif";
    private static final String LOCATION_LATITUDEPHOTO = "latitudePhoto";
    private static final String LOCATION_LONGITUDEPHOTO = "longitudePhoto";
    private static final String LOCATION_REMARKS = "remarks";
    private static final String LOCATION_MARKETNAME = "marketname";
    private static final String LOCATION_STARTTIME = "starttime";
    private static final String LOCATION_ENDTIME = "endtime";
    private static final String LOCATION_OTPSUBTIME = "otpsubtime";
    private static final String LOCATION_OTPLAT = "otplat";
    private static final String LOCATION_OTPLONG = "otplong";
    private static final String LOCATION_FIXTIME = "fixtime";
    // Lpreviousorderscachetable
    private static final String TABLE_PREVIOUSORDERS = "previousorders";
    private static final String PREVIOUSORDERS_PKID = "previousorderspkid";
    private static final String PREVIOUSORDERS_LOCATIONID = "locationid";
    private static final String PREVIOUSORDERS_SCHEDULEDETAILID = "scheduledetailid";
    private static final String DATA = "data";
    // delivery payment table
    private static final String TABLE_PAYMENTS = "payments";
    private static final String PAYMENT_PKID = "Pymentid";
    private static final String PAYMENT_SCHEDULEDETAILID = "scheduledetailid";
    private static final String PAYMENT_METHOD = "paymentmethode";
    private static final String AMOUNT = "amount";
    private static final String CHEQUENUMBER = "chequenumber";
    private static final String IFSC = "ifsc";
    private static final String PAYMENT_CREATETIME = "createtime";
    private static final String DELETE_FLAG = "deleteflag";
    private static final String PAYMENTGUID = "paymentid";
    private static final String SALESPERSONID = "salespersonid";
    private static final String PAYMENT_LOCATIONID = "locationid";
    private static final String CHEQE_DATE = "chequedate";
    private static final String PAYMENT_DATE = "paymentdate";
    private static final String PAYMENT_INVOCENUMBER = "invoicenumber";
    private static final String PAYMENT_PICKLISTNUMBER = "picklistnumber";
    private static final String PAYMENT_LATTITUDE = "lattitude";
    private static final String PAYMENT_LONGITUDE = "longitude";
    private static final String PAYMENT_ACTUALMODE = "actualmode";
    private static final String PAYMENT_BANKCODE = "bankcode";
    private static final String PAYMENT_APROOVEFLAG = "aprooveflag";
    private static final String PAYMENT_SIGNING_PERSON = "signingperson";
    private static final String PAYMENT_FILEID = "fileid";
    private static final String PAYMENT_VERIFICATIONCODE = "verificationcode";
    private static final String PAYMENT_STATUS = "Status";
    // Pricetable
    private static final String TABLE_PRICEDETAILS = "pricelist";
    private static final String PRICEDETAILS_PKID = "pricetableid";
    private static final String PRICEDETAILS_PRODUCTID = "productid";
    private static final String PRICEDETAILS_CUSTOMERCATEGORY = "customercategory";
    private static final String PRICE = "price";
    private static final String VALIDFROM = "validfrom";
    private static final String VALIDTO = "validto";
    private static final String MRP = "mrp";
    // invoice table
    private static final String TABLE_INVOICE = "invoice";
    private static final String INVOICE_PKID = "invoicepkid";
    private static final String INVOICE_ID = "invoiceid";
    private static final String INVOICE_NO = "invoicenumber";
    private static final String INVOICE_CUSTOMERID = "customerid";
    private static final String INVOICE_TOTTALAMOUNT = "tottalamount";
    private static final String INVOICE_BALANCEAMOUNT = "balanceamount";
    private static final String INVOICE_STATUS = "status";
    private static final String INVOICE_DATE = "invoicedate";
    private static final String INVOICE_ORDERUUID = "orderUUID";
    //private static final String INVOICE_STATICBALANCE = "staticbalance";
    // invoice payment table
    private static final String TABLE_INVOICE_PAYMENTS = "invoicepayments";
    private static final String INVOICEPAYMENT_PKID = "invoicepaymentpkid";
    private static final String INVOICEPAYMENT_HEADERID = "paymentheaderid";
    private static final String INVOICEPAYMENT_ID = "invoiceid";
    private static final String INVOICEPAYMENT_AMOUNTPAYED = "amount";
    private static final String INVOICEPAYMENT_BALANCE = "balanceamount";
    private static final String INVOICEPAYMENT_INVOICENO = "invoiceno";
    private static final String INVOICEPAYMENT_DELETEFLAG = "deleteflag";
    private static final String ORDERAMOUNTDATA_PKID = "orderamountfromstorpkid";
    // tottal order amount table
    private static final String TABLE_ORDERAMOUNTDATA = "orderamountfromstore";
    private static final String ORDERAMOUNTDATA_AMOUNT = "amount";
    private static final String ORDERAMOUNTDATA_SCHEDULEDETAILID = "scheduledetailid";
    private static final String ORDERAMOUNTDATA_CUSTOMERID = "customerid";
    private static final String ORDERAMOUNTDATA_PERSONID = "personid";
    private static final String ORDERAMOUNTDATA_TIME = "time";
    private static final String ORDERAMOUNTDATA_CREATETIME = "createtime";
    private static final String ORDERAMOUNTDATA_LATTITUDE = "lattitude";
    private static final String ORDERAMOUNTDATA_LONGITUDE = "longitude";
    private static final String ORDERAMOUNTDATA_GUID = "amountguid";
    // reason table
    private static final String TABLE_REASONS = "reasons";
    private static final String REASONS_PKID = "reasonpkid";
    private static final String REASONS_CODE = "reasoncode";
    private static final String REASON_DETAIL = "reasondetail";
    // customer serarch table
    private static final String TABLE_SEARCHCUSTOMERS = "customersearch";
    private static final String SEARCHCUSTOMERS_PKID = "pkid";
    private static final String SEARCHCUSTOMERS_ID = "custid";
    private static final String SEARCHCUSTOMERS_NAME = "customername";
    private static final String SEARCHCUSTOMERS_ADRESS = "adress";
    private static final String SEARCHCUSTOMERS_CODE = "customercode";
    private static final String SEARCHCUSTOMERS_LATTITUDE = "lattitude";
    private static final String SEARCHCUSTOMERS_LONGITUDE = "longitude";
    private static final String SEARCHCUSTOMERS_CATEGORY = "custcategory";
    private static final String SEARCHCUSTOMERS_LOCATIONLOCK = "locationlock";
    private static final String SEARCHCUSTOMERS_CLOSINGDAY = "closingday";
    private static final String SEARCHCUSTOMERS_MOBILENUMBER = "mobilenumber";
    private static final String SEARCHCUSTOMERS_CUSTTYPE = "custtype";
    // caustomercategorylist
    private static final String TABLE_CUSTOMERCATEGORIES = "customercategorylist";
    private static final String CUSTOMERCATEGORIES_PKID = "id";
    private static final String CUSTOMERCATEGORIES_CODE = "categorycode";
    private static final String CUSTOMERCATEGORIES_NAME = "name";
    private static final String CUSTOMERCATEGORIES_TYPE = "cattype";
    // productchecklist
    private static final String TABLE_PRODUCTCHECKLIST = "CHECKLIST";
    private static final String PRODUCTCHECKLIST_PKID = "id";
    private static final String PRODUCTCHECKLIST_CODE = "checklistcode";
    private static final String PRODUCTCHECKLIST_NAME = "checklistname";
    private static final String PRODUCTCHECKLIST_SEQNO = "seqno";
    // townlist
    private static final String TABLE_TOWN = "towns";
    private static final String TOWN_PKID = "id";
    private static final String TOWN_CODE = "towncode";
    private static final String TOWN_NAME = "townname";
    private static final String TOWN_ORG = "townorg";
    // denominationdata
    private static final String TABLE_DENOMINATIONS = "denominationdata";
    private static final String DENOMINATIONS_PKID = "id";
    private static final String DENOMINATIONS_CODE = "denominationcode";
    private static final String DENOMINATIONS_AMOUNT = "denominationamount";
    // create schedukesummary and cash denomination data
    private static final String TABLE_SCHEDULESUMMARY = "schedulesummary";
    private static final String SCHEDULESUMMARY_PKID = "id";
    private static final String SCHEDULESUMMARY_PERSONID = "personid";
    private static final String SCHEDULESUMMARY_CREATETIME = "createtime";
    private static final String SCHEDULESUMMARY_SUBMITTIME = "submittime";
    private static final String SCHEDULESUMMARY_LAT = "lattitude";
    private static final String SCHEDULESUMMARY_LONGI = "longitude";
    private static final String SCHEDULESUMMARY_UNIQUEID = "uniqueid";
    private static final String SCHEDULESUMMARY_DENOMEDATA = "denominationdata";
    private static final String SCHEDULESUMMARY_TOTTALCASH = "tottalcashamount";
    private static final String SCHEDULESUMMARY_SCHEDULEID = "scheduleid";
    private static final String SCHEDULESUMMARY_SCHEDULEIDLOCAL = "scheduleidlocal";
    private static final String SCHEDULESUMMARY_COMPLETION_TIME = "completiontime";
    private static final String SCHEDULESUMMARY_BEGIN_TIME = "begintime";
    private static final String SCHEDULESUMMARY_BEGINODOMETER = "beginodometer";
    private static final String SCHEDULESUMMARY_BEGINODOMETERUUID = "beginodometeruuid";
    private static final String SCHEDULESUMMARY_VEHICLE = "beginovehicle";
    private static final String SCHEDULESUMMARY_DRIVER = "begindriver";
    private static final String SCHEDULESUMMARY_COMPLETIONODOMETER = "completionodometer";
    private static final String SCHEDULESUMMARY_COMPLETIONUUID = "completionodometeruuid";
    private static final String TABLE_BANKMASTER = "bankmaster";

    // createbank master table
    private static final String BANKMASTER_PKID = "bankmasterpkid";
    private static final String BANKMASTER_CODE = "bankmastercode";
    private static final String BANKMASTER_NAME = "bankmastername";
    private static final String BANKMASTER_BRANCHNAME = "branchname";
    private static final String TABLE_ORDERHDR = "tableorderhdr";
    private static final String ORDERHDR_PKID = "id";
    private static final String ORDERHDR_SCHLINEID = "slineid";
    private static final String ORDERHDR_CUSTOMER = "customer";
    private static final String ORDERHDR_PERSONID = "personid";
    private static final String ORDERHDR_CUSTID = "custid";
    private static final String ORDERHDR_CREATETIME = "creattime";
    private static final String ORDERHDR_APPROVEFLAG = "approveflag";
    private static final String ORDERHDR_LATITUDE = "latitude";
    private static final String ORDERHDR_LONGITUDE = "longitude";
    private static final String ORDERHDR_NOTE = "notes";
    private static final String ORDERHDR_INVOICENO = "invoiceno";
    private static final String ORDERHDR_STATUS = "Status";
    private static final String ORDERHDR_PARENT_INVOICENO = "parentInvoiceno";
    private static final String TABLE_ORDERLINE = "tableorderline";
    private static final String ORDERLINE_PKID = "id";
    private static final String ORDERLINE_HEADID = "headid";
    private static final String ORDERLINE_PRODID = "prodid";
    private static final String ORDERLINE_QTY = "qty";
    private static final String ORDERLINE_MRP = "mrp";
    private static final String ORDERLINE_RATE = "rate";
    private static final String ORDERLINE_CREATETIME = "creattime";
    private static final String ORDERLINE_DELETEFLAG = "deleteflag";
    /*	private static final String ORDERLINE_UOM = "uom";
    private static final String ORDERLINE_UPC = "upc";*/
    private static final String ORDERLINE_PM = "pm";
    private static final String ORDERLINE_TAX = "tax";
    private static final String ORDERLINE_BATCH = "batch";
    private static final String ORDERLINE_DISCOUNT = "discount";
    private static final String ORDERLINE_DISCOUNTPER = "discountper";
    private static final String ORDERLINE_PACKAGE_TYPE = "packagetype";
    private static final String ORDERLINE_CANCELFLAG = "cancelflag";
    private static final String ORDERLINE_COMPLIMENT = "complimentFlag";
    private static final String TABLE_APPUSAGE = "tableappusage";
    private static final String APPUSAGE_PKID = "id";
    private static final String APPUSAGE_ID = "useid";
    private static final String APPUSAGE_PACKAGE = "packagename";
    private static final String APPUSAGE_TIME = "time";
    private static final String APPUSAGE_CRTIME = "crtime";
    private static final String APPUSAGE_APPNAME = "appname";
    // product table
    private static final String TABLE_PRODUCT = "producttable";
    private static final String PRODUCT_PKID = "productid";
    private static final String PRODUCT_ID = "productidunique";
    private static final String PRODUCT_NAME = "productname";
    private static final String PRODUCT_CODE = "productcode";
    private static final String PRODUCT_CAT = "productcat";
    private static final String PRODUCT_BRAND = "productbrand";
    private static final String PRODUCT_FORM = "productform";
    private static final String PRODUCT_HSN = "hsn";
    private static final String TABLE_MRP = "mrptable";
    private static final String MRP_PKID = "id";
    private static final String MRP_ID = "mrpid";
    private static final String MRP_PRODUCT = "product";
    private static final String MRP_VALUE = "value";
    private static final String MRP_FROM = "fromdat";
    private static final String MRP_TO = "todat";
    private static final String MRP_BATCH = "batch";
    private static final String MRP_CONSOFF = "consumeroff";
    private static final String MRP_FLATOFF = "flatoff";
    private static final String MRP_TAX = "tax";
    private static final String TABLE_PARTYMARG = "partymarg";
    private static final String PARTYMARG_PKID = "id";
    private static final String PARTYMARG_ID = "partyid";
    private static final String PARTYMARG_PRODUCT = "product";
    private static final String PARTYMARG_CAT = "cat";
    private static final String PARTYMARG_VALUE = "value";
    private static final String PARTYMARG_FROM = "fromdat";
    private static final String PARTYMARG_TO = "todat";
    private static final String PARTYMARG_CUSTOMER = "customer";
    private static final String TABLE_ADDLDISC = "addinltable";
    private static final String ADDLDISC_PKID = "id";
    private static final String ADDLDISC_ID = "discid";
    private static final String ADDLDISC_CAT = "cat";
    private static final String ADDLDISC_VALUE = "value";
    private static final String ADDLDISC_FROM = "fromdat";
    private static final String ADDLDISC_TO = "todat";
    private static final String TABLE_FOCUSEDPRDS = "focusedprods";
    private static final String FOCUSEDPRDS_PKID = "id";
    private static final String FOCUSEDPRDS_ID = "uiqueid";
    private static final String FOCUSEDPRDS_PRODUCT = "product";
    private static final String FOCUSEDPRDS_FROM = "fromdat";
    private static final String FOCUSEDPRDS_TO = "todat";
    private static final String FOCUSEDPRDS_CUSTCAT = "custcat";
    private static final String FOCUSEDPRDS_FLAG = "flag";
    private static final String FOCUSEDPRDS_MINQTY = "minqty";
    private static final String TABLE_PRODUCTCAT = "productcat";
    private static final String PRODUCTCAT_PKID = "id";
    private static final String PRODUCTCAT_ID = "prodid";
    private static final String PRODUCTCAT_NAME = "name";
    private static final String TABLE_BRAND = "brand";
    private static final String BRAND_PKID = "id";
    private static final String BRAND_ID = "brandid";
    private static final String BRAND_NAME = "name";
    private static final String TABLE_FORM = "form";
    private static final String FORM_PKID = "id";
    private static final String FORM_ID = "formid";
    private static final String FORM_NAME = "name";
    private static final String TABLE_INVENTORY = "inventorytable";
    private static final String INVENTORY_PKID = "id";
    private static final String INVENTORY_ID = "uniqeid";
    private static final String INVENTORY_PRODUCT = "invproduct";
    private static final String INVENTORY_QTY = "invqty";
    private static final String TABLE_SALESRETURN = "salesreturntable";
    private static final String SALESRETURN_PKID = "id";
    private static final String SALESRETURN_SCHLINEID = "slineid";
    private static final String SALESRETURN_PERSONID = "personid";
    private static final String SALESRETURN_CUSTID = "custid";
    private static final String SALESRETURN_CREATETIME = "creattime";
    private static final String TABLE_SALERTNLINE = "tablesalesrtnline";
    private static final String SALERTNLINE_PKID = "id";
    private static final String SALERTNLINE_HEADID = "headid";
    private static final String SALERTNLINE_PRODID = "prodid";
    private static final String SALERTNLINE_QTY = "qty";
    private static final String SALERTNLINE_MRP = "mrp";
    private static final String SALERTNLINE_INVOICE = "invoiceno";
    private static final String SALERTNLINE_CREATETIME = "creattime";
    private static final String SALERTNLINE_DELETEFLAG = "deleteflag";
    private static final String TABLE_CONSTANTS = "tableconstants";
    private static final String CONSTANTS_PKID = "id";
    private static final String CONSTANTS_KEY = "constkeys";
    private static final String CONSTANTS_VALUE = "constvalues";
    private static final String TABLE_CREDITNOTES = "tablecreditnote";
    private static final String CREDITNOTE_PKID = "id";
    private static final String CREDITNOTE_SCHDLELINEID = "schdlineid";
    private static final String CREDITNOTE_AMOUNT = "amount";
    private static final String CREDITNOTE_REASON = "reason";
    private static final String CREDITNOTE_DESCRIPTION = "description";
    private static final String CREDITNOTE_CREATEDTIME = "createdtime";
    private static final String CREDITNOTE_DELETEFLAG = "deleteflag";
    private static final String TABLE_CREDITREASONS = "tablecreditnotereasons";
    private static final String CREDITREASONS_PKID = "id";
    private static final String CREDITREASONS_ID = "uniqueid";
    private static final String CREDITREASONS_REASON = "reason";
    private static final String TABLE_CATEGORYPHOTOS = "tablecatphotos";
    private static final String CATEGORYPHOTOS_PKID = "id";
    private static final String CATEGORYPHOTOS_CUSTID = "custid";
    private static final String CATEGORYPHOTOS_CATEGORY = "category";
    private static final String CATEGORYPHOTOS_UUID = "uuid";
    private static final String CATEGORYPHOTOS_CREATETIME = "createtime";
    private static final String CATEGORYPHOTOS_UPDATEFLAG = "upflag";
    private static final String TABLE_DOCUMENTPHOTOS = "tabledocphotos";
    private static final String DOCUMENTPHOTOS_PKID = "id";
    private static final String DOCUMENTPHOTOS_CUSTID = "custid";
    private static final String DOCUMENTPHOTOS_DOCUMENT = "document";
    private static final String DOCUMENTPHOTOS_UUID = "uuid";
    private static final String DOCUMENTPHOTOS_CREATETIME = "createtime";
    private static final String DOCUMENTPHOTOS_UPDATEFLAG = "upflag";
    private static final String TABLE_CRATES = "tablecrates";
    private static final String CRATES_PKID = "id";
    private static final String CRATES_SLINEID = "slineid";
    private static final String CRATES_IN = "inval";
    private static final String CRATES_OUT = "outval";
    private static final String CRATES_CRTIME = "crtime";
    private static final String CREATES_UUID = "uuid";
    private static final String CRATES_CRTIMESYNC = "synctime";
    private static final String TABLE_OTHERCOMPANY = "tableothercomp";
    private static final String OTHERCOMPANY_PKID = "id";
    private static final String OTHERCOMPANY_ID = "otherid";
    private static final String OTHERCOMPANY_NAME = "name";
    private static final String TABLE_BRANCHES = "tablebranches";
    private static final String BRANCHES_PKID = "id";
    private static final String BRANCHES_ID = "orgid";
    private static final String BRANCHES_NAME = "orgname";
    private static final String TABLE_UNITS = "tableunits";
    private static final String UNITS_PKID = "id";
    private static final String UNITS_ID = "unitid";
    private static final String UNITS_ABBRV = "abbrv";
    private static final String UNITS_NAME = "name";
    private static final String TABLE_EXCUST = "tableexistingc";

    /*	private static final String TABLE_UNITSMAP = "tableunitmap";
    private static final String UNITSMAP_PKID = "id";
	private static final String UNITSMAP_PRODUCT = "product";
	private static final String UNITSMAP_UOM = "uom";
	private static final String UNITSMAP_UPC = "upc";*/
    private static final String EXCUST_PKID = "id";
    private static final String EXCUST_ID = "custid";
    private static final String EXCUST_CODE = "code";
    private static final String EXCUST_NAME = "name";
    private static final String EXCUST_ADDRESS = "address";
    private static final String TABLE_MINQTY = "tableminqty";
    private static final String MINQTY_PKID = "id";
    private static final String MINQTY_CUSTCAT = "custcat";
    private static final String MINQTY_PRODUCT = "product";
    private static final String MINQTY_QUANTITY = "minqty";
    private static final String TABLE_CUSTOMERTYPE = "tablecusttypes";
    private static final String CUSTOMERTYPE_PKID = "id";
    private static final String CUSTOMERTYPE_ID = "realid";
    private static final String CUSTOMERTYPE_NAME = "name";
    private static final String TABLE_REGIONS = "regions";
    private static final String REGIONS_PKID = "id";
    private static final String REGIONS_NAME = "name";
    private static final String REGIONS_TARGT = "targt";
    private static final String REGIONS_ACHIVD = "achvd";
    private static final String REGIONS_REGIONNAME = "regionname";
    private static final String REGIONS_STATUS = "status";
    /*
     * Added by Reny For Van Sales Inventory management. May 22 2018
     */
    private static final String TABLE_PRODUCTBATCH = "productbatch";
    private static final String PRODUCTBATCH_PKID = "id";
    private static final String PRODUCTBATCH_ID = "productbatchid";
    private static final String PRODUCTBATCH_BATCH = "Batch";
    private static final String TABLE_PRODUCTLINE = "productline";
    private static final String PRODUCTLINE_PKID = "id";
    private static final String PRODUCTLINE_ID = "productlineid";
    private static final String PRODUCTLINE_PRODUCT = "Product";
    private static final String PRODUCTLINE_BATCH = "BatchId";
    private static final String TABLE_INVENTORYACCOUNT = "inventoryaccount";

    /*
     * private static final String TABLE_PRODUCTMRP ="mrp"; private static final
     * String PRODUCTMRP_PKID = "id"; private static final String PRODUCTMRP_ID
     * = "mrpid"; private static final String PRODUCTMRP_PRODUCT = "Product" ;
     * private static final String PRODUCTMRP_BATCH = "ProductBatch"; private
     * static final String PRODUCTMRP_MRP = "MRP"; private static final String
     * PRODUCTMRP_KEY = "ProductBatchKey"; private static final String
     * PRODUCTMRP_UOM = "UOM"; private static final String PRODUCTMRP_UPC =
     * "UPC"; private static final String PRODUCTMRP_TAX = "TAX";
     */
    private static final String INVENTORYACCOUNT_PKID = "id";
    private static final String INVENTORYACCOUNT_ID = "inventoryaccountid";
    private static final String INVENTORYACCOUNT_NAME = "AccountName";
    private static final String TABLE_INVENTORYJRNLHDR = "inventoryjournalhdr";
    private static final String INVENTORYJRNLHDR_PKID = "id";
    private static final String INVENTORYJRNLHDR_ID = "inventoryjournalid";
    private static final String INVENTORYJRNLHDR_NO = "JournalNo";
    private static final String INVENTORYJRNLHDR_DATE = "JournalDate";
    private static final String INVENTORYJRNLHDR_PRODUCT = "Product";
    private static final String INVENTORYJRNLHDR_ORG = "Org";
    private static final String INVENTORYJRNLHDR_SOURCE = "Source";
    private static final String INVENTORYJRNLHDR_SOURCEID = "SourceID";
    private static final String INVENTORYJRNLHDR_BATCH = "BatchNo";
    private static final String INVENTORYJRNLHDR_UOM = "UOM";
    private static final String TABLE_INVENTORYJRNLLINE = "inventoryjournalline";
    private static final String INVENTORYJRNLLINE_PKID = "id";
    private static final String INVENTORYJRNLLINE_ID = "inventoryjournallineid";
    private static final String INVENTORYJRNLLINE_HDR = "JournalHdr";
    private static final String INVENTORYJRNLLINE_AMNTCREDIT = "AmountCredited";
    private static final String INVENTORYJRNLLINE_AMNTDEBIT = "AmountDebited";
    private static final String INVENTORYJRNLLINE_ACCOUNT = "InventoryAccount";
    private static final String INVENTORYJRNLLINE_QTYCREDIT = "QuantityCredited";
    private static final String INVENTORYJRNLLINE_QTYDEBIT = "QuantityDebited";
    private static final String TABLE_INVOICENOGEN = "invoicenogen";
    private static final String INVOICENOGEN_PKID = "id";
    private static final String INVOICENOGEN_ACTIVE = "Active";
    private static final String INVOICENOGEN_CURRVAL = "CurrVal";
    private static final String INVOICENOGEN_MAXVAL = "MaxVal";
    private static final String INVOICENOGEN_MINVAL = "MinVal";
    // private static final String INVOICENOGEN_ORG = "Organization";
    private static final String INVOICENOGEN_ORGCODE = "OrgCode";
    private static final String TABLE_TAXLINE = "taxline";
    private static final String TAXLINE_PKID = "id";
    private static final String TAXLINE_ID = "taxlineid";
    private static final String TAXLINE_DESCRIP = "description";
    private static final String TAXLINE_RATE = "rate";
    private static final String TAXLINE_TYPE = "taxtype";
    private static final String TAXLINE_NAME = "taxlinename";
    private static final String TAXLINE_TAX = "taxid";
    private static final String TABLE_TAX = "tax";
    // private static final String TAXLINE_MINAMNT = "MinimumAmount";
    // private static final String TAXLINE_OUTPUTACC = "OutputAccount";
    // private static final String TAXLINE_INPUTACC = "InputAccount";
    // private static final String TAXLINE_TAXCODE = "TaxCode";
    // private static final String TAXLINE_VALIDFROM = "ValidFrom";
    // private static final String TAXLINE_VALIDTO = "ValidTo";
    private static final String TAX_PKID = "id";
    private static final String TAX_ID = "taxid";
    private static final String TAX_NAME = "taxname";
    private static final String TABLE_ORDERTAXLINE = "ordertaxline";
    // private static final String TAX_PRDCTCAT = "ProductCategory";
    private static final String ORDERTAXLINE_PKID = "id";
    private static final String ORDERTAXLINE_TAXLINEID = "taxlineid";
    private static final String ORDERTAXLINE_AMOUNT = "taxableamount";
    private static final String ORDERTAXLINE_TAXAMOUNT = "taxamount";
    private static final String ORDERTAXLINE_CODE = "taxcode";
    private static final String ORDERTAXLINE_RATE = "rate";
    private static final String ORDERTAXLINE_ORDRDETAILID = "orderdetailid";
    private static final String ORDERTAXLINE_ORDRDID = "orderid";
    private static final String ORDERTAXLINE_DELETEFLAG = "deleteflag";
    /**
     * FOR DISCOUNT IMPLEMENTATION
     */
    private static final String TABLE_OFFERS = "offers";
    private static final String OFFERS_PKID = "id";
    private static final String OFFERS_ID = "offerId";
    private static final String OFFERS_FREEPRODUCT = "FreeProduct";
    private static final String OFFERS_MINQTY = "MinQty";
    private static final String OFFERS_FREEITEMSNO = "NoOfFreeItems";
    private static final String OFFERS_OFFTYPE = "OfferType";
    private static final String OFFERS_OFFVALUE = "OffValue";
    //	private static final String OFFERS_PRODUCT = "ProductName";
    //	private static final String OFFERS_PRODUCTCATEGORY = "ProductCategory";
    private static final String OFFERS_RATETYPE = "RateType";
    private static final String OFFERS_UOM = "UOM";
    private static final String OFFERS_INDIVIDUAL_FLAG = "individualFlag";
    private static final String OFFERS_CUST_CATEGORY = "CustomerCategory";
    private static final String TABLE_OFFERS_DETAILS = "offersdtls";
    private static final String OFFERS_DETAILS_PKID = "id";
    private static final String OFFERS_DETAILS_ID = "offerDtlsId";
    private static final String OFFERS_DETAILS_OFFER = "offer";
    private static final String OFFERS_DETAILS_PRODUCT = "ProductName";
    private static final String OFFERS_DETAILS_PRODUCTCATEGORY = "ProductCategory";
    /**
     * EXPENSE
     */
    private static final String TABLE_EXPENSETYPE = "expensetype";
    private static final String EXPENSETYPE_PKID = "id";
    private static final String EXPENSETYPE_ID = "expensetypeid";
    private static final String EXPENSETYPE_NAME = "Name";
    private static final String TABLE_EXPENSE = "expenses";
    private static final String EXPENSE_PKID = "id";
    private static final String EXPENSE_AMOUNT = "Amount";
    private static final String EXPENSE_DESCRIPTION = "Description";
    private static final String EXPENSE_EXPENSETYPE = "ExpenseType";
    private static final String EXPENSE_CREATETIME = "createdOn";
    /**
     * IMPLEMENT PACKAGE TYPE
     */

    private static final String TABLE_PACKAGE_TYPE = "packagetype";
    private static final String PACKAGE_TYPE_PKID = "id";
    private static final String PACKAGE_TYPE_ID = "packagetypeId";
    private static final String PACKAGE_TYPE_CODE = "code";
    private static final String PACKAGE_TYPE_NAME = "name";
    private static final String TABLE_PRODUCT_PACKAGE_MAPPING = "productpackagemapping";
    private static final String PRODUCT_PACKAGE_MAP_PKID = "id";
    private static final String PRODUCT_PACKAGE_MAP_ID = "packagemapId";
    private static final String PRODUCT_PACKAGE_MAP_PACKAGE_TYPE = "packagetype";
    private static final String PRODUCT_PACKAGE_MAP_PRODUCT = "product";
    private static final String PRODUCT_PACKAGE_MAP_UOM = "UOM";
    private static final String PRODUCT_PACKAGE_MAP_SELLABLE = "sellable";
    private static final String PRODUCT_PACKAGE_MAP_STOCKABLE = "stockable";
    private static final String PRODUCT_PACKAGE_MAP_BASE_UNIT = "baseunit";
    private static final String PRODUCT_PACKAGE_MAP_CONVERSION_FACTOR = "conversionfactor";
    // productchecklistinput
    private static final String TABLE_CHECKLISTINPUT = "CHECKLISTINPUT";
    private static final String CHECKLISTINPUT_PKID = "id";
    private static final String CHECKLISTINPUT_CODE = "checklistinputcode";
    private static final String CHECKLISTINPUT_CUSTNAME = "checklistcustomer";
    private static final String CHECKLISTINPUT_INPUT = "checklistinput";
    private static final String TABLE_COMBINGSUMMARY = "combgsumarytabl";
    private static final String COMBINGSUMMARY_PKID = "id";
    private static final String COMBINGSUMMARY_CUSTOMERCOUNT = "custcount";
    private static final String COMBINGSUMMARY_CZONECOUNT = "czoncount";
    private static final String COMBINGSUMMARY_CZONETARGET = "czontgt";
    private static final String COMBINGSUMMARY_REJECTED = "rejected";
    private static final String SCHEDULEDETAIL_STATUS = "Delivered";
    public boolean checkWithSyncTime = true;

    //COMBING HISTORY
    private static final String TABLE_COMBINGCOUNT = " COMBINGCOUNT ";
    private static final String COMBING_COUNTID = "id";
    private static final String TOTAL_COUNT = "totalcombed";
    private static final String TOTAL_COMBED = "totalvisited";
    private static final String TOT_DAYS = "days";

    //Nearest Customer Table
    private static final String TABLE_NEARESTCUST = "NEARESTCUST";
    private static final String  NEAREST_ID= "id";
    private static final String NEAREST_CUST_ID ="cust_id";
    private static final String NEAREST_CUST_CODE = "cust_code";
    private static final String NEAREST_CUST_NAME = "cust_name";
    private static final String NEAREST_CUST_LAT ="cust_lat";
    private static final String NEAREST_CUST_LONG = "cust_long";
    private static final String NEAREST_CUST_ADDR = "cust_addr";
    private static final String NEAREST_CUST_DIST = "Distance";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        String CREATE_TABLE_PACKAGE_TYPE = "CREATE TABLE " + TABLE_PACKAGE_TYPE
                + "(" + PACKAGE_TYPE_PKID + " INTEGER PRIMARY KEY,"
                + PACKAGE_TYPE_ID + " TEXT,"
                + PACKAGE_TYPE_NAME + " TEXT,"
                + PACKAGE_TYPE_CODE + " TEXT )";
        db.execSQL(CREATE_TABLE_PACKAGE_TYPE);


        String CREATE_TABLE_PRODUCT_PACKAGE_MAPPING = "CREATE TABLE " + TABLE_PRODUCT_PACKAGE_MAPPING
                + "(" + PRODUCT_PACKAGE_MAP_PKID + " INTEGER PRIMARY KEY,"
                + PRODUCT_PACKAGE_MAP_ID + " TEXT,"
                + PRODUCT_PACKAGE_MAP_PACKAGE_TYPE + " TEXT,"
                + PRODUCT_PACKAGE_MAP_PRODUCT + " TEXT,"
                + PRODUCT_PACKAGE_MAP_SELLABLE + " TEXT,"
                + PRODUCT_PACKAGE_MAP_STOCKABLE + " TEXT,"
                + PRODUCT_PACKAGE_MAP_BASE_UNIT + " TEXT,"
                + PRODUCT_PACKAGE_MAP_CONVERSION_FACTOR + " TEXT,"
                + PRODUCT_PACKAGE_MAP_UOM + " TEXT )";
        db.execSQL(CREATE_TABLE_PRODUCT_PACKAGE_MAPPING);

        String CREATE_TABLE_EXPENSETYPE = "CREATE TABLE " + TABLE_EXPENSETYPE
                + "(" + EXPENSETYPE_PKID + " INTEGER PRIMARY KEY,"
                + EXPENSETYPE_ID + " TEXT,"
                + EXPENSETYPE_NAME + " TEXT )";
        db.execSQL(CREATE_TABLE_EXPENSETYPE);

        String CREATE_TABLE_EXPENSE = "CREATE TABLE " + TABLE_EXPENSE
                + "(" + EXPENSE_PKID + " INTEGER PRIMARY KEY,"
                + EXPENSE_AMOUNT + " TEXT,"
                + EXPENSE_EXPENSETYPE + " TEXT,"
                + EXPENSE_CREATETIME + " TEXT,"
                + EXPENSE_DESCRIPTION + " TEXT )";
        db.execSQL(CREATE_TABLE_EXPENSE);

        String CREATE_TABLE_OFFERS = "CREATE TABLE " + TABLE_OFFERS
                + "(" + OFFERS_PKID + " INTEGER PRIMARY KEY,"
                + OFFERS_ID + " TEXT,"
                + OFFERS_FREEPRODUCT + " TEXT,"
                + OFFERS_MINQTY + " TEXT,"
                + OFFERS_FREEITEMSNO + " TEXT,"
                + OFFERS_OFFTYPE + " TEXT,"
                + OFFERS_OFFVALUE + " TEXT,"
                //				+ OFFERS_PRODUCT + " TEXT,"
                + OFFERS_RATETYPE + " TEXT,"
                + OFFERS_UOM + " TEXT ,"
                + OFFERS_CUST_CATEGORY + " TEXT ,"
                + OFFERS_INDIVIDUAL_FLAG + " TEXT )";
        db.execSQL(CREATE_TABLE_OFFERS);

        String CREATE_TABLE_OFFERS_DETAILS = "CREATE TABLE " + TABLE_OFFERS_DETAILS
                + "(" + OFFERS_DETAILS_PKID + " INTEGER PRIMARY KEY,"
                + OFFERS_DETAILS_ID + " TEXT,"
                + OFFERS_DETAILS_OFFER + " TEXT,"
                + OFFERS_DETAILS_PRODUCT + " TEXT,"
                + OFFERS_DETAILS_PRODUCTCATEGORY + " TEXT )";
        db.execSQL(CREATE_TABLE_OFFERS_DETAILS);

        String CREATE_ORDERTAXLINE_TABLE = "CREATE TABLE " + TABLE_ORDERTAXLINE
                + "(" + ORDERTAXLINE_PKID + " INTEGER PRIMARY KEY,"
                + ORDERTAXLINE_TAXLINEID + " TEXT," + ORDERTAXLINE_AMOUNT
                + " TEXT," + ORDERTAXLINE_TAXAMOUNT + " TEXT,"
                + ORDERTAXLINE_CODE + " TEXT," + ORDERTAXLINE_RATE + " TEXT,"
                + ORDERTAXLINE_ORDRDETAILID + " TEXT,"
                + ORDERTAXLINE_DELETEFLAG + " TEXT,"
                + ORDERTAXLINE_ORDRDID + " TEXT )";
        db.execSQL(CREATE_ORDERTAXLINE_TABLE);

        String CREATE_TAX_TABLE = "CREATE TABLE " + TABLE_TAX + "(" + TAX_PKID
                + " INTEGER PRIMARY KEY," + TAX_ID + " TEXT," + TAX_NAME
                + " TEXT )";
        db.execSQL(CREATE_TAX_TABLE);

        String CREATE_TAXLINE_TABLE = "CREATE TABLE " + TABLE_TAXLINE + "("
                + TAXLINE_PKID + " INTEGER PRIMARY KEY," + TAXLINE_ID
                + " TEXT," + TAXLINE_DESCRIP + " TEXT," + TAXLINE_RATE
                + " TEXT," + TAXLINE_TYPE + " TEXT," + TAXLINE_NAME + " TEXT,"
                + TAXLINE_TAX + " TEXT )";
        db.execSQL(CREATE_TAXLINE_TABLE);

        String CREATE_INVOICENOGEN_TABLE = "CREATE TABLE " + TABLE_INVOICENOGEN
                + "(" + INVOICENOGEN_PKID + " INTEGER PRIMARY KEY,"
                + INVOICENOGEN_ACTIVE + " TEXT," + INVOICENOGEN_CURRVAL
                + " TEXT," + INVOICENOGEN_MAXVAL + " TEXT,"
                + INVOICENOGEN_MINVAL + " TEXT," + INVOICENOGEN_ORGCODE
                + " TEXT)";
        db.execSQL(CREATE_INVOICENOGEN_TABLE);
        // new
        String CREATE_ORDERHRD_TABLE = "CREATE TABLE " + TABLE_ORDERHDR + "("
                + ORDERHDR_PKID + " INTEGER PRIMARY KEY," + ORDERHDR_SCHLINEID
                + " TEXT," + ORDERHDR_PERSONID + " TEXT," + ORDERHDR_CUSTID
                + " TEXT," + ORDERHDR_CREATETIME + " TEXT,"
                + ORDERHDR_APPROVEFLAG + " TEXT," + ORDERHDR_LATITUDE
                + " TEXT," + ORDERHDR_LONGITUDE + " TEXT," + ORDERHDR_CUSTOMER
                + " TEXT," + ORDERHDR_INVOICENO + " TEXT," + ORDERHDR_STATUS + " TEXT,"
                + ORDERHDR_NOTE + " TEXT," + ORDERHDR_PARENT_INVOICENO + " TEXT )";
        db.execSQL(CREATE_ORDERHRD_TABLE);

        String CREATE_TABLE_ORDERLINE = "CREATE TABLE " + TABLE_ORDERLINE + "("
                + ORDERLINE_PKID + " INTEGER PRIMARY KEY," + ORDERLINE_HEADID
                + " TEXT," + ORDERLINE_PRODID + " TEXT," + ORDERLINE_QTY
                + " TEXT," + ORDERLINE_MRP + " TEXT," + ORDERLINE_RATE
                + " TEXT," + ORDERLINE_CREATETIME + " TEXT,"
                + ORDERLINE_DELETEFLAG + " TEXT," + ORDERLINE_PACKAGE_TYPE + " TEXT,"
                /*+ ORDERLINE_UPC + " TEXT,"*/ + ORDERLINE_TAX + " TEXT,"
                + ORDERLINE_PM + " TEXT," + ORDERLINE_BATCH + " TEXT,"
                + ORDERLINE_DISCOUNT + " TEXT," + ORDERLINE_CANCELFLAG + " TEXT,"
                + ORDERLINE_COMPLIMENT + " TEXT," + ORDERLINE_DISCOUNTPER + " TEXT)";
        db.execSQL(CREATE_TABLE_ORDERLINE);

        String CREATE_TABLE_INVENTORY = "CREATE TABLE " + TABLE_INVENTORY + "("
                + INVENTORY_PKID + " INTEGER PRIMARY KEY," + INVENTORY_ID
                + " TEXT," + INVENTORY_PRODUCT + " TEXT," + INVENTORY_QTY
                + " TEXT)";
        db.execSQL(CREATE_TABLE_INVENTORY);

        String CREATE_PRODUCT_TABLE = "CREATE TABLE " + TABLE_PRODUCT + "("
                + PRODUCT_PKID + " INTEGER PRIMARY KEY," + PRODUCT_ID
                + " TEXT," + PRODUCT_NAME + " TEXT," + PRODUCT_CODE + " TEXT,"
                + PRODUCT_CAT + " TEXT," + PRODUCT_BRAND + " TEXT,"
                + PRODUCT_FORM + " TEXT)";
        db.execSQL(CREATE_PRODUCT_TABLE);

        String CREATE_TABLE_MRP = "CREATE TABLE IF NOT EXISTS " + TABLE_MRP
                + "(" + MRP_PKID + " INTEGER PRIMARY KEY," + MRP_ID + " TEXT,"
                + MRP_PRODUCT + " TEXT," + MRP_VALUE + " TEXT," + MRP_FROM
                + " TEXT," + MRP_TO + " TEXT," + MRP_BATCH + " TEXT,"
                + MRP_CONSOFF + " TEXT," + MRP_FLATOFF + " TEXT)";
        db.execSQL(CREATE_TABLE_MRP);

        String CREATE_TABLE_PARTYMARG = "CREATE TABLE IF NOT EXISTS "
                + TABLE_PARTYMARG + "(" + PARTYMARG_PKID
                + " INTEGER PRIMARY KEY," + PARTYMARG_ID + " TEXT,"
                + PARTYMARG_PRODUCT + " TEXT," + PARTYMARG_CAT + " TEXT,"
                + PARTYMARG_VALUE + " TEXT," + PARTYMARG_FROM + " TEXT,"
                + PARTYMARG_CUSTOMER + " TEXT," + PARTYMARG_TO + " TEXT)";
        db.execSQL(CREATE_TABLE_PARTYMARG);

        String CREATE_TABLE_ADDLDISC = "CREATE TABLE IF NOT EXISTS "
                + TABLE_ADDLDISC + "(" + ADDLDISC_PKID
                + " INTEGER PRIMARY KEY," + ADDLDISC_ID + " TEXT,"
                + ADDLDISC_CAT + " TEXT," + ADDLDISC_VALUE + " TEXT,"
                + ADDLDISC_FROM + " TEXT," + ADDLDISC_TO + " TEXT)";
        db.execSQL(CREATE_TABLE_ADDLDISC);

        String CREATE_TABLE_APPUSAGE = "CREATE TABLE " + TABLE_APPUSAGE + "("
                + APPUSAGE_PKID + " INTEGER PRIMARY KEY, " + APPUSAGE_ID
                + " TEXT," + APPUSAGE_PACKAGE + " TEXT," + APPUSAGE_TIME
                + " TEXT," + APPUSAGE_CRTIME + " TEXT," + APPUSAGE_APPNAME
                + " TEXT)";
        db.execSQL(CREATE_TABLE_APPUSAGE);

        String CREATE_TABLE_CHECKLISTINPUT = "CREATE TABLE "
                + TABLE_CHECKLISTINPUT + "(" + CHECKLISTINPUT_PKID
                + " INTEGER PRIMARY KEY," + CHECKLISTINPUT_CODE + " TEXT,"
                + CHECKLISTINPUT_CUSTNAME + " TEXT," + CHECKLISTINPUT_INPUT
                + " TEXT)";
        db.execSQL(CREATE_TABLE_CHECKLISTINPUT);

        String CREATE_TABLE_FOCUSEDPRDS = "CREATE TABLE " + TABLE_FOCUSEDPRDS
                + "(" + FOCUSEDPRDS_PKID + " INTEGER PRIMARY KEY,"
                + FOCUSEDPRDS_ID + " TEXT," + FOCUSEDPRDS_PRODUCT + " TEXT,"
                + FOCUSEDPRDS_FROM + " TEXT," + FOCUSEDPRDS_TO + " TEXT,"
                + FOCUSEDPRDS_CUSTCAT + " TEXT," + FOCUSEDPRDS_FLAG + " TEXT,"
                + FOCUSEDPRDS_MINQTY + " TEXT)";
        db.execSQL(CREATE_TABLE_FOCUSEDPRDS);

        String CREATE_TABLE_PRODUCTCAT = "CREATE TABLE " + TABLE_PRODUCTCAT
                + "(" + PRODUCTCAT_PKID + " INTEGER PRIMARY KEY,"
                + PRODUCTCAT_ID + " TEXT," + PRODUCTCAT_NAME + " TEXT)";
        db.execSQL(CREATE_TABLE_PRODUCTCAT);

        String CREATE_TABLE_BRAND = "CREATE TABLE " + TABLE_BRAND + "("
                + BRAND_PKID + " INTEGER PRIMARY KEY," + BRAND_ID + " TEXT,"
                + BRAND_NAME + " TEXT)";
        db.execSQL(CREATE_TABLE_BRAND);

        String CREATE_TABLE_FORM = "CREATE TABLE " + TABLE_FORM + "("
                + FORM_PKID + " INTEGER PRIMARY KEY," + FORM_ID + " TEXT,"
                + FORM_NAME + " TEXT)";
        db.execSQL(CREATE_TABLE_FORM);

        // END /////////////////////////////////////

        // For saving mobile services
        String CREATE_MOBSERVICES_TABLE = "CREATE TABLE IF NOT EXISTS "
                + TABLE_MOBSERVICES + "(" + MOBILESERVICE_ID
                + " INTEGER PRIMARY KEY," + MOBILESERVICE_NAME + " TEXT,"
                + MOBILESERVICE_DISABLE_INVENTORY + " TEXT,"
                + MOBILESERVICE_CODE + " TEXT)";
        db.execSQL(CREATE_MOBSERVICES_TABLE);

        // String CREATE_JSON_TABLE2 = "CREATE TABLE " + TABLE_JSONS2 + "("
        // + JSONS_ID2 + " INTEGER PRIMARY KEY," + JSONS_TITLE2 + " TEXT,"
        // + JSONS_DATA2 + " TEXT, " + JSONS_DATE2 + " TEXT)";
        // db.execSQL(CREATE_JSON_TABLE2);

        // For Saving Reports data like Coverage, productivity, etc.
        String CREATE_JSON_TABLE = "CREATE TABLE " + TABLE_JSONS + "("
                + JSONS_ID + " INTEGER PRIMARY KEY," + JSONS_TITLE + " TEXT,"
                + JSONS_DATA + " TEXT, " + JSONS_DATE + " TEXT)";
        db.execSQL(CREATE_JSON_TABLE);

        // Navigation table creation
        String CREATE_NAVIGATION_TABLE = "CREATE TABLE " + TABLE_NAVIGATIONDATA
                + "(" + KEY_NAVIGATIONID + " INTEGER PRIMARY KEY," + KEY_PLANID
                + " TEXT," + KEY_VEHICLEID + " TEXT," + KEY_DEVICETIME
                + " TEXT," + KEY_LATITUDE + " TEXT," + KEY_LONGITUDE + " TEXT,"
                + KEY_SPEED + " TEXT," + KEY_PROVIDER + " TEXT," + KEY_BEARING
                + " TEXT," + KEY_ACCURACY + " TEXT," + KEY_DISTANCE + " TEXT,"
                + NAVIGATIONDATA_UPFLAG + " TEXT," + KEY_CUSTOMER + " TEXT,"
                + KEY_TIMEDIFF + " TEXT," + KEY_MONGODT + " TEXT,"
                + KEY_INACTIVE + " TEXT)";
        db.execSQL(CREATE_NAVIGATION_TABLE);

        // schedule header table creation
        String CREATE_SCHEDULEHEADER_TABLE = "CREATE TABLE "
                + TABLE_SCHEDULE_HEADER + "(" + SCHEDULE_HEADER_PKID
                + " INTEGER PRIMARY KEY," + SCHEDULE_ID + " TEXT,"
                + SCHEDULE_NAME + " TEXT," + SALESPERSON_ID + " TEXT,"
                + SALESPERSON_NAME + " TEXT," + SALEROUTE_ID + " TEXT,"
                + SCHEDULE_DATE + " TEXT," + HEADER_STATUS + " TEXT,"
                + ROUTENETWORK_ID + " TEXT," + SCHEDULE_COMPLETION_STATUS
                + " TEXT," + SCHEDULE_BEGIN_TIME + " TEXT,"
                + SCHEDULE_COMPLETION_TIME + " TEXT," + SCHEDULE_EXPIRY_STATUS
                + " TEXT,"

                + SCHEDULE_ODOMETER + " TEXT," + SCHEDULE_DRIVER + " TEXT,"
                + SCHEDULE_VEHICLE + " TEXT," + SCHEDULE_ODOMETER_UUID
                + " TEXT," + SCHEDULE_COMPETE_ODOMETER + " TEXT,"
                + SCHEDULE_COMPETE_UUID + " TEXT,"

                + SCHEDULE_SYNC_TIME + " TEXT)";

        db.execSQL(CREATE_SCHEDULEHEADER_TABLE);

        // schedule detail table creation
        String CREATE_SCHEDULEDETAIL_TABLE = "CREATE TABLE "
                + TABLE_SCHEDULE_DETAIL + "(" + SCHEDULE_DETAIL_PKID
                + " INTEGER PRIMARY KEY," + SCHEDULE_DETAIL_ID + " TEXT,"
                + SCHEDULE_HEADER_ID + " TEXT," + LOCATION_ID + " TEXT,"
                + LOCATION_NAME + " TEXT," + LOCATION_ADRESS + " TEXT," + SEQNO
                + " TEXT," + DETAIL_STATUS + " TEXT," + LATITUDE + " TEXT,"
                + LONGITUDE + " TEXT," + SCHEDULEDETAIL_CREATETIME + " TEXT,"
                + LATITUDE_NEW + " TEXT," + LONGITUDE_NEW + " TEXT,"
                + CUSTOMERCATOGORY + " TEXT," + SCHEDULE_DETAIL_VISITED
                + " TEXT," + RETAILPRICE + " TEXT," + INVOCENUMBER + " TEXT,"
                + PICKLISTNUMBER + " TEXT," + STOREVISIT_REASON + " TEXT,"
                + STOREVISIT_LATTITUDE + " TEXT," + STOREVISIT_LONGITUDE
                + " TEXT," + STOREVISIT_TIME + " TEXT," + SCHED_CUSTOMERCODE
                + " TEXT," + SCHED_LOCUPDATETIME + " TEXT," + LOCATIONLOCK
                + " TEXT," + CLOSING_DAY + " TEXT," + CLOSING_DAY_UPDATETIME
                + " TEXT," + SCHEDULE_DETAIL_PRODUCTIVE + " TEXT,"
                + CUSTOMERCATOGORYID + " TEXT," + SCHEDULEDETAIL_MOBILE
                + " TEXT," + SCHEDULEDETAIL_OTP + " TEXT,"
                + SCHEDULEDETAIL_TINNUMBER + " TEXT," + SCHEDULEDETAIL_PAN
                + " TEXT," + SCHEDULEDETAIL_EDITTIME + " TEXT,"
                + SCHEDULEDETAIL_PHOTOUUID + " TEXT," + SCHEDULEDETAIL_IMGLAT
                + " TEXT," + SCHEDULEDETAIL_IMGLONG + " TEXT,"
                + SCHEDULEDETAIL_LANDMARK + " TEXT," + SCHEDULEDETAIL_TOWN
                + " TEXT," + SCHEDULEDETAIL_CONTACTPERSON + " TEXT,"
                + SCHEDULEDETAIL_OTHERCOMPANY + " TEXT,"
                + SCHEDULEDETAIL_CRATES + " TEXT," + SCHEDULEDETAIL_OTPVERIFIED
                + " TEXT," + SCHEDULEDETAIL_LOCPROVIDER + " TEXT,"
                + SCHEDULEDETAIL_LOCACCURACY + " TEXT," + SCHEDULEDETAIL_EMAIL
                + " TEXT," + SCHEDULEDETAIL_DIRECTCOV + " TEXT,"
                + SCHEDULEDETAIL_SELPRODCAT + " TEXT,"
                + SCHEDULEDETAIL_STORECLOSED + " TEXT,"
                + SCHEDULEDETAIL_PNGCOVERED + " TEXT," + SCHEDULEDETAIL_BRANCH
                + " TEXT," + SCHEDULEDETAIL_PASSBY + " TEXT,"
                + SCHEDULEDETAIL_PASSBYCRTIME + " TEXT,"
                + SCHEDULEDETAIL_ALTERCUSTCODE + " TEXT,"
                + SCHEDULEDETAIL_LATTITUDE + " TEXT,"
                + SCHEDULEDETAIL_TARGET_TYPE + " TEXT,"
                + SCHEDULEDETAIL_TARGET_VALUE + " TEXT,"
                + SCHEDULEDETAIL_CREDIT_LIMIT + " TEXT,"
                + SCHEDULEDETAIL_CREDIT_BALANCE + " TEXT,"
                + SCHEDULEDETAIL_GSTTYPE + " TEXT,"
                + SCHEDULEDETAIL_FSSI + " TEXT,"
                + SCHEDULEDETAIL_LONGITUDE + " TEXT," + SCHEDULEDETAIL_CUSTTYPE
                + " TEXT," + SCHEDULEDETAIL_STREETNAME + " TEXT,"
                + SCHEDULEDETAIL_AREA + " TEXT," + SCHEDULEDETAIL_LANDLINE
                + " TEXT," + SCHEDULEDETAIL_SHOP + " TEXT,"
                + SCHEDULEDETAIL_CITY + " TEXT," + SCHEDULEDETAIL_PIN
                + " TEXT," + SCHEDULEDETAIL_DEL_STATUS + " TEXT,"
                + SCHEDULEDETAIL_DEL_SUBMITTIME + " TEXT,"
                + SCHEDULEDETAIL_DEL_STATUSCRTIME + " TEXT,"
                + SCHEDULEDETAIL_DEL_LATTITUDE + " TEXT,"
                + SCHEDULEDETAIL_DEL_LONGITUDE + " TEXT ," + SCHEDULEDETAIL_PLAN
                + "TEXT," + SCHEDULEDETAIL_DEL_PHOTOUUID + " TEXT,"
                + SCHEDULEDETAIL_DEL_PHOTOLATTITUDE + " TEXT,"
                + SCHEDULEDETAIL_DEL_PHOTOLONGITUDE + " TEXT,"
                + SCHEDULEDETAIL_DEL_PHOTOFLAG + " TEXT,"
                + SCHEDULEDETAIL_DEL_ACCURACY + " TEXT,"
                + SCHEDULEDETAIL_REASON_ACC + " TEXT,"
                + SCHEDULEDETAIL_PHOTO_ACCURACY + " TEXT,"
                + SCHEDULEDETAIL_PHOTO_EDITTIME + " TEXT,"
                + SCHEDULEDETAIL_LATITUDE_ACCURACY + " TEXT,"
                + SCHEDULEDETAIL_STARTTIME + " TEXT," + SCHEDULEDETAIL_ENDTIME
                + " TEXT," + SCHEDULEDETAIL_NEEDOTP + " TEXT,"
                + SCHEDULEDETAIL_OTPSUBTIME + " TEXT,"

                + SCHEDULEDETAIL_OTPLAT + " TEXT," + SCHEDULEDETAIL_OTPLONG + " TEXT,"
                + SCHEDULEDETAIL_UPDATETIME + " TEXT, " + SCHEDULEDETAIL_CUSTIMGURL
                + " TEXT)";
        db.execSQL(CREATE_SCHEDULEDETAIL_TABLE);

        // ORDER HEADER table creation
        String CREATE_ORDERHEADER_TABLE = "CREATE TABLE " + TABLE_ORDER_HEADER
                + "(" + ORDER_HEADER_PKID + " INTEGER PRIMARY KEY,"
                + ORDERBEGIN_TIME + " TEXT," + ORDERSUBMIT_TIME + " TEXT,"
                + ORDER_SCHEDULE_DETAIL_ID + " TEXT," + ORDER_STATUS + " TEXT,"
                + ORDER_LATITUDE + " TEXT," + ORDER_LONGITUDE + " TEXT,"
                + ORDER_SALESPERSONID + " TEXT," + ORDER_ROUTENETWORKID
                + " TEXT," + ORDER_LOCATIONID + " TEXT)";

        db.execSQL(CREATE_ORDERHEADER_TABLE);

        // create categorylist
        String CREATE_CATOGORYTABLE = "CREATE TABLE "
                + TABLE_CUSTOMERCATEGORIES + "(" + CUSTOMERCATEGORIES_PKID
                + " INTEGER PRIMARY KEY," + CUSTOMERCATEGORIES_CODE + " TEXT,"
                + CUSTOMERCATEGORIES_NAME + " TEXT," + CUSTOMERCATEGORIES_TYPE
                + " TEXT)";
        db.execSQL(CREATE_CATOGORYTABLE);

        // ORDER HEADER DETAIL table creation
        String CREATE_TABLE_ORDER_DETAIL = "CREATE TABLE " + TABLE_ORDER_DETAIL
                + "(" + TABLE_ORDERDETAIL_PKID + " INTEGER PRIMARY KEY,"
                + ORDER_HEAD_ID + " TEXT," + PRODUCTID + " TEXT," + QUANTITY
                + " TEXT," + DELIVERYDATE + " TEXT," + CREATETIME + " TEXT,"
                + DELETEFLAG + " TEXT," + RATE + " TEXT," + PRODUCT_TAX
                + " TEXT," + TOTTAL_AMOUNT + " TEXT)";

        db.execSQL(CREATE_TABLE_ORDER_DETAIL);

        // PRODUCT table creation
        // String CREATE_TABLE_PRODUCT = "CREATE TABLE " + PRODUCT_TABLE + "("
        // + PRODUCT_PKID + " INTEGER PRIMARY KEY," + PRODUCT_ID
        // + " TEXT," + PRODUCT_NAME + " TEXT," + PRODUCT_CODE + " TEXT,"
        // + PRODUCT_DISCRIP + " TEXT," + WEIGHT + " TEXT," + VOLUME
        // + " TEXT," + TAX + " TEXT," + UNITRATE + " TEXT)";
        //
        // db.execSQL(CREATE_TABLE_PRODUCT);

        // sync table
        String CREATE_SYNCTABLE = "CREATE TABLE " + TABLE_SYNCTABLE + "("
                + INDEXPKID + " INTEGER PRIMARY KEY," + TABLEINSYNC + " TEXT,"
                + SYNCTIME + " TEXT)";

        db.execSQL(CREATE_SYNCTABLE);

        // location table creation
        String CREATE_LOCATIONTABLE = "CREATE TABLE " + TABLE_LOCATIONS + "("
                + LOCATION_PKID + " INTEGER PRIMARY KEY," + LOCATIONID
                + " TEXT," + NAME + " TEXT," + ADDRESS + " TEXT,"
                + LOCATION_LATITUDE + " TEXT," + LOCATION_LONGITUDE + " TEXT,"
                + LOCATION_CREATETIME + " TEXT," + CUSTOMER_CODE + " TEXT,"
                + CUSTOMER_CATOGORY + " TEXT,"+ TIN_NUMBER + " TEXT,"
                + NEAREST_LANDMARK + " TEXT," + CONTACT_PERSONNAME + " TEXT,"
                + CONTACT_PERSON_NUMBER + " TEXT," + ANT_OTHERCOMPANYVALUE
                + " TEXT," + TOWNCLASS + " TEXT," + SELECTED_PRODUCT_CATEGORIES
                + " TEXT," + CUSTOMERCATEGOY_CODE + " TEXT,"
                + CUSTOMER_SCHEDULELINE_GUID + " TEXT," + LOCATION_PAN
                + " TEXT," + LOCATION_OTPVERIFIED + " TEXT,"
                + LOCATION_LOCPROVIDER + " TEXT," + LOCATION_LOCACCURACY
                + " TEXT," + LOCATION_EMAIL + " TEXT," + LOCATION_PHOTOUUID
                + " TEXT," + LOCATION_DIRECTCOV + " TEXT,"
                + LOCATION_CLOSING_DAY + " TEXT," + LOCATION_STOREISCLOSED
                + " TEXT," + LOCATION_PNGCOVERED + " TEXT," + LOCATION_BRANCH
                + " TEXT," + LOCATION_DRUG + " TEXT," + LOCATION_LOCALITY
                + " TEXT," + LOCATION_CITY + " TEXT," + LOCATION_STATE
                + " TEXT," + LOCATION_PIN + " TEXT," + LOCATION_COVDAY
                + " TEXT," + LOCATION_WEEK1 + " TEXT," + LOCATION_WEEK2
                + " TEXT," + LOCATION_WEEK3 + " TEXT," + LOCATION_WEEK4
                + " TEXT," + LOCATION_VISTFRQ + " TEXT," + LOCATION_TYPE
                + " TEXT," + LOCATION_WHOLSAL + " TEXT," + LOCATION_METRO
                + " TEXT," + LOCATION_CLASSIF + " TEXT,"
                + LOCATION_LATITUDEPHOTO + " TEXT," + LOCATION_LONGITUDEPHOTO
                + " TEXT," + LOCATION_REMARKS + " TEXT," + LOCATION_MARKETNAME
                + " TEXT," + LOCATION_STARTTIME + " TEXT," + LOCATION_ENDTIME
                + " TEXT," + LOCATION_OTPSUBTIME

                + " TEXT," + LOCATION_OTPLAT + " TEXT," + LOCATION_OTPLONG + " TEXT,"
                + LOCATION_FIXTIME
                + " TEXT)";

        db.execSQL(CREATE_LOCATIONTABLE);

        // /previoussorders table ceation

        String CREATE_PREVIOUSORDERTABLE = "CREATE TABLE "
                + TABLE_PREVIOUSORDERS + "(" + PREVIOUSORDERS_PKID
                + " INTEGER PRIMARY KEY," + PREVIOUSORDERS_LOCATIONID
                + " TEXT," + PREVIOUSORDERS_SCHEDULEDETAILID + " TEXT,"
                + ADDRESS + " TEXT," + DATA + " TEXT)";

        db.execSQL(CREATE_PREVIOUSORDERTABLE);

        // create towntable
        String CREATE_TOWNTABLE = "CREATE TABLE " + TABLE_TOWN + "("
                + TOWN_PKID + " INTEGER PRIMARY KEY," + TOWN_CODE + " TEXT,"
                + TOWN_NAME + " TEXT," + TOWN_ORG + " TEXT)";
        db.execSQL(CREATE_TOWNTABLE);

        // create checklist table
        String CREATE_CHECKLISTTABLE = "CREATE TABLE " + TABLE_PRODUCTCHECKLIST
                + "(" + PRODUCTCHECKLIST_PKID + " INTEGER PRIMARY KEY,"
                + PRODUCTCHECKLIST_CODE + " TEXT," + PRODUCTCHECKLIST_NAME
                + " TEXT," + PRODUCTCHECKLIST_SEQNO + " TEXT,"
                + PRODUCTCHECKLIST_TYPE + " TEXT)";
        db.execSQL(CREATE_CHECKLISTTABLE);

        // createpayment table

        String CREATE_PAYMENTTABLE = "CREATE TABLE " + TABLE_PAYMENTS + "("
                + PAYMENT_PKID + " INTEGER PRIMARY KEY,"
                + PAYMENT_SCHEDULEDETAILID + " TEXT," + PAYMENT_METHOD
                + " TEXT," + AMOUNT + " TEXT," + CHEQUENUMBER + " TEXT," + IFSC
                + " TEXT," + PAYMENT_CREATETIME + " TEXT," + DELETE_FLAG
                + " TEXT," + PAYMENTGUID + " TEXT," + SALESPERSONID + " TEXT,"
                + PAYMENT_LOCATIONID + " TEXT," + CHEQE_DATE + " TEXT,"
                + PAYMENT_DATE + " TEXT," + PAYMENT_INVOCENUMBER + " TEXT,"
                + PAYMENT_PICKLISTNUMBER + " TEXT," + PAYMENT_LATTITUDE
                + " TEXT," + PAYMENT_LONGITUDE + " TEXT," + PAYMENT_ACTUALMODE
                + " TEXT," + PAYMENT_BANKCODE + " TEXT," + PAYMENT_APROOVEFLAG
                + " TEXT," + PAYMENT_SIGNING_PERSON + " TEXT, " + PAYMENT_STATUS + " TEXT, "
                + PAYMENT_FILEID + " TEXT," + PAYMENT_VERIFICATIONCODE
                + " TEXT)";

        db.execSQL(CREATE_PAYMENTTABLE);

        // createprice table
        String CREATE_PRICETABLE = "CREATE TABLE " + TABLE_PRICEDETAILS + "("
                + PRICEDETAILS_PKID + " INTEGER PRIMARY KEY,"
                + PRICEDETAILS_PRODUCTID + " TEXT,"
                + PRICEDETAILS_CUSTOMERCATEGORY + " TEXT," + PRICE + " TEXT,"
                + VALIDFROM + " TEXT," + VALIDTO + " TEXT," + MRP + " TEXT)";
        db.execSQL(CREATE_PRICETABLE);

        // create invoice table
        String CREATE_INVOICETABLE = "CREATE TABLE " + TABLE_INVOICE + "("
                + INVOICE_PKID + " INTEGER PRIMARY KEY," + INVOICE_ID
                + " TEXT," + INVOICE_NO + " TEXT," + INVOICE_CUSTOMERID
                + " TEXT," + INVOICE_TOTTALAMOUNT + " TEXT,"
                + INVOICE_BALANCEAMOUNT + " TEXT," + INVOICE_STATUS + " TEXT,"
                + INVOICE_DATE + " TEXT," + INVOICE_STATICBALANCE + " TEXT,"
                + INVOICE_PERSONMOB + " TEXT," + INVOICE_PERSONID + " TEXT,"
                + INVOICE_CUSTCODE + " TEXT," + INVOICE_CUSTNAME + " TEXT,"
                + INVOICE_PERSONCODE + " TEXT," + INVOICE_OUTAMT + " TEXT,"
                + INVOICE_TYPE + " TEXT," + INVOICE_ASOFDATE + " TEXT)";
        db.execSQL(CREATE_INVOICETABLE);

        // create invoice paymenttable

        String CREATE_INVOICEPAYMENT_TABLE = "CREATE TABLE "
                + TABLE_INVOICE_PAYMENTS + "(" + INVOICEPAYMENT_PKID
                + " INTEGER PRIMARY KEY," + INVOICEPAYMENT_HEADERID + " TEXT,"
                + INVOICEPAYMENT_ID + " TEXT," + INVOICEPAYMENT_AMOUNTPAYED
                + " TEXT," + INVOICEPAYMENT_INVOICENO + " TEXT,"
                + INVOICEPAYMENT_DELETEFLAG + " TEXT,"
                + INVOICEPAYMENT_BALANCE + " TEXT)";
        db.execSQL(CREATE_INVOICEPAYMENT_TABLE);

        // create order amount table(capturing amount from store for delivery))

        String CREATE_ORDERAMOUNTTABLE = "CREATE TABLE "
                + TABLE_ORDERAMOUNTDATA + "(" + ORDERAMOUNTDATA_PKID
                + " INTEGER PRIMARY KEY," + ORDERAMOUNTDATA_AMOUNT + " TEXT,"
                + ORDERAMOUNTDATA_SCHEDULEDETAILID + " TEXT,"
                + ORDERAMOUNTDATA_CUSTOMERID + " TEXT,"
                + ORDERAMOUNTDATA_PERSONID + " TEXT," + ORDERAMOUNTDATA_TIME
                + " TEXT," + ORDERAMOUNTDATA_CREATETIME + " TEXT,"
                + ORDERAMOUNTDATA_LATTITUDE + " TEXT,"
                + ORDERAMOUNTDATA_LONGITUDE + " TEXT," + ORDERAMOUNTDATA_GUID
                + " TEXT)";
        db.execSQL(CREATE_ORDERAMOUNTTABLE);

        // create reson table

        String CREATE_REASONTABLE = "CREATE TABLE " + TABLE_REASONS + "("
                + REASONS_PKID + " INTEGER PRIMARY KEY," + REASONS_CODE
                + " TEXT," + REASON_DETAIL + " TEXT," + REASON_SENDBACK
                + " TEXT)";
        db.execSQL(CREATE_REASONTABLE);

        // customer search table
        String CUSTOMER_SEARCHTABLE = "CREATE TABLE " + TABLE_SEARCHCUSTOMERS
                + "(" + SEARCHCUSTOMERS_PKID + " INTEGER PRIMARY KEY,"
                + SEARCHCUSTOMERS_ID + " TEXT," + SEARCHCUSTOMERS_NAME
                + " TEXT," + SEARCHCUSTOMERS_ADRESS + " TEXT,"
                + SEARCHCUSTOMERS_CODE + " TEXT," + SEARCHCUSTOMERS_LATTITUDE
                + " TEXT," + SEARCHCUSTOMERS_LONGITUDE + " TEXT,"
                + SEARCHCUSTOMERS_CATEGORY + " TEXT,"
                + SEARCHCUSTOMERS_LOCATIONLOCK + " TEXT,"
                + SEARCHCUSTOMERS_CLOSINGDAY + " TEXT,"
                + SEARCHCUSTOMERS_MOBILENUMBER + " TEXT,"
                + SEARCHCUSTOMERS_CUSTTYPE + " TEXT," + SEARCHCUSTOMERS_ROUTES + " TEXT,"

                + SEARCHCUSTOMERS_OTPVERIFIED + " TEXT,"
                + SEARCHCUSTOMERS_OTPLAT + " TEXT,"
                + SEARCHCUSTOMERS_OTPLONG + " TEXT)";
        db.execSQL(CUSTOMER_SEARCHTABLE);

        // create table denomination
        String CREATE_DENOMINATIONTABLE = "CREATE TABLE " + TABLE_DENOMINATIONS
                + "(" + DENOMINATIONS_PKID + " INTEGER PRIMARY KEY,"
                + DENOMINATIONS_CODE + " TEXT," + DENOMINATIONS_AMOUNT
                + " TEXT)";
        db.execSQL(CREATE_DENOMINATIONTABLE);

        // create table schedulsummary and cash denomination
        String SCHEDULE_SUMMARY = "CREATE TABLE " + TABLE_SCHEDULESUMMARY + "("
                + SCHEDULESUMMARY_PKID + " INTEGER PRIMARY KEY,"
                + SEARCHCUSTOMERS_ID + " TEXT," + SCHEDULESUMMARY_PERSONID
                + " TEXT," + SCHEDULESUMMARY_CREATETIME + " TEXT,"
                + SCHEDULESUMMARY_SUBMITTIME + " TEXT," + SCHEDULESUMMARY_LAT
                + " TEXT," + SCHEDULESUMMARY_LONGI + " TEXT,"
                + SCHEDULESUMMARY_UNIQUEID + " TEXT,"
                + SCHEDULESUMMARY_DENOMEDATA + " TEXT,"
                + SCHEDULESUMMARY_TOTTALCASH + " TEXT,"
                + SCHEDULESUMMARY_SCHEDULEID + " TEXT,"
                + SCHEDULESUMMARY_SCHEDULEIDLOCAL + " TEXT,"
                + SCHEDULESUMMARY_COMPLETION_TIME + " TEXT,"
                + SCHEDULESUMMARY_BEGIN_TIME + " TEXT,"

                + SCHEDULESUMMARY_BEGINODOMETER + " TEXT,"
                + SCHEDULESUMMARY_BEGINODOMETERUUID + " TEXT,"
                + SCHEDULESUMMARY_VEHICLE + " TEXT," + SCHEDULESUMMARY_DRIVER
                + " TEXT," + SCHEDULESUMMARY_COMPLETIONODOMETER + " TEXT,"
                + SCHEDULESUMMARY_COMPLETIONUUID + " TEXT)";

        db.execSQL(SCHEDULE_SUMMARY);

        // create bank master table

        String CREATE_BANKMASTERTABLE = "CREATE TABLE " + TABLE_BANKMASTER
                + "(" + BANKMASTER_PKID + " INTEGER PRIMARY KEY,"
                + BANKMASTER_CODE + " TEXT," + BANKMASTER_NAME + " TEXT,"
                + BANKMASTER_BRANCHNAME + " TEXT)";
        db.execSQL(CREATE_BANKMASTERTABLE);

        // Sales Return Header Table
        String CREATE_TABLE_SALESRETURN = "CREATE TABLE " + TABLE_SALESRETURN
                + "(" + SALESRETURN_PKID + " INTEGER PRIMARY KEY,"
                + SALESRETURN_SCHLINEID + " TEXT," + SALESRETURN_PERSONID
                + " TEXT," + SALESRETURN_CUSTID + " TEXT,"
                + SALESRETURN_CREATETIME + " TEXT)";
        db.execSQL(CREATE_TABLE_SALESRETURN);

        // SalesReturn Line Table
        String CREATE_TABLE_SALERTNLINE = "CREATE TABLE " + TABLE_SALERTNLINE
                + "(" + SALERTNLINE_PKID + " INTEGER PRIMARY KEY,"
                + SALERTNLINE_HEADID + " TEXT," + SALERTNLINE_PRODID + " TEXT,"
                + SALERTNLINE_QTY + " TEXT," + SALERTNLINE_MRP + " TEXT,"
                + SALERTNLINE_INVOICE + " TEXT," + SALERTNLINE_CREATETIME
                + " TEXT," + SALERTNLINE_DELETEFLAG + " TEXT)";
        db.execSQL(CREATE_TABLE_SALERTNLINE);

        String CREATE_TABLE_CONSTANTS = "CREATE TABLE " + TABLE_CONSTANTS + "("
                + CONSTANTS_PKID + " INTEGER PRIMARY KEY," + CONSTANTS_KEY
                + " TEXT," + CONSTANTS_VALUE + " TEXT)";
        db.execSQL(CREATE_TABLE_CONSTANTS);

        String CREATE_TABLE_CREDITNOTES = "CREATE TABLE " + TABLE_CREDITNOTES
                + "(" + CREDITNOTE_PKID + " INTEGER PRIMARY KEY,"
                + CREDITNOTE_SCHDLELINEID + " TEXT," + CREDITNOTE_AMOUNT
                + " TEXT," + CREDITNOTE_REASON + " TEXT,"
                + CREDITNOTE_DESCRIPTION + " TEXT," + CREDITNOTE_CREATEDTIME
                + " TEXT," + CREDITNOTE_DELETEFLAG + " TEXT)";
        db.execSQL(CREATE_TABLE_CREDITNOTES);

        String CREATE_TABLE_CREDITREASONS = "CREATE TABLE "
                + TABLE_CREDITREASONS + "(" + CREDITREASONS_PKID
                + " INTEGER PRIMARY KEY," + CREDITREASONS_ID + " TEXT,"
                + CREDITREASONS_REASON + " TEXT)";
        db.execSQL(CREATE_TABLE_CREDITREASONS);

        String CREATE_TABLE_CATEGORYPHOTOS = "CREATE TABLE "
                + TABLE_CATEGORYPHOTOS + "(" + CATEGORYPHOTOS_PKID
                + " INTEGER PRIMARY KEY," + CATEGORYPHOTOS_CUSTID + " TEXT,"
                + CATEGORYPHOTOS_CATEGORY + " TEXT," + CATEGORYPHOTOS_UUID
                + " TEXT," + CATEGORYPHOTOS_CREATETIME + " TEXT,"
                + CATEGORYPHOTOS_UPDATEFLAG + " TEXT)";
        db.execSQL(CREATE_TABLE_CATEGORYPHOTOS);

        String CREATE_TABLE_DOCUMENTPHOTOS = "CREATE TABLE "
                + TABLE_DOCUMENTPHOTOS + "(" + DOCUMENTPHOTOS_PKID
                + " INTEGER PRIMARY KEY," + DOCUMENTPHOTOS_CUSTID + " TEXT,"
                + DOCUMENTPHOTOS_DOCUMENT + " TEXT," + DOCUMENTPHOTOS_UUID
                + " TEXT," + DOCUMENTPHOTOS_CREATETIME + " TEXT,"
                + CATEGORYPHOTOS_UPDATEFLAG + " TEXT)";
        db.execSQL(CREATE_TABLE_DOCUMENTPHOTOS);

        String CREATE_TABLE_CRATES = "CREATE TABLE " + TABLE_CRATES + "("
                + CRATES_PKID + " INTEGER PRIMARY KEY," + CRATES_SLINEID
                + " TEXT," + CRATES_IN + " TEXT," + CRATES_OUT + " TEXT,"
                + CREATES_UUID + " TEXT," + CRATES_CRTIME + " TEXT,"
                + CRATES_CRTIMESYNC + " TEXT)";
        db.execSQL(CREATE_TABLE_CRATES);

        String CREATE_TABLE_OTHERCOMPANY = "CREATE TABLE " + TABLE_OTHERCOMPANY
                + "(" + OTHERCOMPANY_PKID + " INTEGER PRIMARY KEY,"
                + OTHERCOMPANY_ID + " TEXT," + OTHERCOMPANY_NAME + " TEXT)";
        db.execSQL(CREATE_TABLE_OTHERCOMPANY);

        String CREATE_TABLE_BRANCHES = "CREATE TABLE " + TABLE_BRANCHES + "("
                + BRANCHES_PKID + " INTEGER PRIMARY KEY," + BRANCHES_ID
                + " TEXT," + BRANCHES_NAME + " TEXT)";
        db.execSQL(CREATE_TABLE_BRANCHES);

        String CREATE_TABLE_UNITS = "CREATE TABLE " + TABLE_UNITS + "("
                + UNITS_PKID + " INTEGER PRIMARY KEY," + UNITS_ID + " TEXT,"
                + UNITS_ABBRV + " TEXT," + UNITS_NAME + " TEXT)";
        db.execSQL(CREATE_TABLE_UNITS);

		/*	String CREATE_TABLE_UNITSMAP = "CREATE TABLE " + TABLE_UNITSMAP + "("
                + UNITSMAP_PKID + " INTEGER PRIMARY KEY," + UNITSMAP_PRODUCT
				+ " TEXT," + UNITSMAP_UOM + " TEXT," + UNITSMAP_UPC + " TEXT)";
		db.execSQL(CREATE_TABLE_UNITSMAP);*/

        String CREATE_TABLE_EXCUST = "CREATE TABLE IF NOT EXISTS "
                + TABLE_EXCUST + "(" + EXCUST_PKID + " INTEGER PRIMARY KEY,"
                + EXCUST_ID + " TEXT," + EXCUST_CODE + " TEXT," + EXCUST_NAME
                + " TEXT," + EXCUST_ADDRESS + " TEXT)";
        db.execSQL(CREATE_TABLE_EXCUST);

        String CREATE_TABLE_MINQTY = "CREATE TABLE IF NOT EXISTS "
                + TABLE_MINQTY + "(" + MINQTY_PKID + " INTEGER PRIMARY KEY,"
                + MINQTY_CUSTCAT + " TEXT," + MINQTY_PRODUCT + " TEXT,"
                + MINQTY_QUANTITY + " TEXT)";
        db.execSQL(CREATE_TABLE_MINQTY);

        String CREATE_TABLE_CUSTOMERTYPE = "CREATE TABLE IF NOT EXISTS "
                + TABLE_CUSTOMERTYPE + "(" + CUSTOMERTYPE_PKID
                + " INTEGER PRIMARY KEY," + CUSTOMERTYPE_ID + " TEXT,"
                + CUSTOMERTYPE_NAME + " TEXT)";
        db.execSQL(CREATE_TABLE_CUSTOMERTYPE);

        String CREATE_TABLE_REGIONS = "CREATE TABLE IF NOT EXISTS "
                + TABLE_REGIONS + "(" + REGIONS_PKID + " INTEGER PRIMARY KEY,"
                + REGIONS_NAME + " TEXT," + REGIONS_TARGT + " TEXT," +
                REGIONS_ACHIVD + " TEXT," + REGIONS_REGIONNAME + " TEXT," + REGIONS_STATUS + " TEXT)";
        db.execSQL(CREATE_TABLE_REGIONS);

        /*
         * Added by Reny For Van Sales Inventory management. May 22 2018
         */

        String CREATE_TABLE_PRODUCTBATCH = "CREATE TABLE IF NOT EXISTS "
                + TABLE_PRODUCTBATCH + "(" + PRODUCTBATCH_PKID
                + " INTEGER PRIMARY KEY," + PRODUCTBATCH_ID + " TEXT,"
                + PRODUCTBATCH_BATCH + " TEXT)";
        db.execSQL(CREATE_TABLE_PRODUCTBATCH);

        String CREATE_TABLE_PRODUCTLINE = "CREATE TABLE IF NOT EXISTS "
                + TABLE_PRODUCTLINE + " ( " + PRODUCTLINE_PKID
                + " INTEGER PRIMARY KEY,  " + PRODUCTLINE_ID + " TEXT,"
                + PRODUCTLINE_PRODUCT + " TEXT,  " + PRODUCTLINE_BATCH
                + " TEXT)";

        db.execSQL(CREATE_TABLE_PRODUCTLINE);
        /*------------sOJU STart----------*/

        String CREATE_TABLE_ASSETS = "CREATE TABLE IF NOT EXISTS "
                + TABLE_ASSETS + "(" + ASSETS_PKID + " INTEGER PRIMARY KEY,"
                + ASSETS_ID + " TEXT," + ASSETS_ASSETQNHDR + " TEXT,"
                + ASSETS_QSTRING + " TEXT," + ASSETS_SLINEID + " TEXT,"
                + ASSETS_QLINEID + " TEXT," + ASSETS_CUSTOMER + " TEXT,"
                + ASSETS_QRCODE + " TEXT," + ASSETS_ANSWER + " TEXT,"
                + ASSETS_PHOTOUUID + " TEXT," + ASSETS_CREATEDTIME + " TEXT,"
                + ASSETS_PHOTOREQ + " TEXT)";

        db.execSQL(CREATE_TABLE_ASSETS);
        String CREATE_TABLE_ORGANIZATION = "CREATE TABLE " + TABLE_ORGANIZATION
                + "(" + ORGANIZATION_PKID + " INTEGER PRIMARY KEY,"
                + ORGANIZATION_ORGLAT + " TEXT," + ORGANIZATION_ORGLONG
                + " TEXT," + ORGANIZATION_ACCURACY + " TEXT,"
                + ORGANIZATION_OSRMPORT + " TEXT,"
                + ORGANIZATION_LOCATIONACCURACY + " TEXT)";
        db.execSQL(CREATE_TABLE_ORGANIZATION);
        String CREATE_TABLE_ORGLEVELSERVICES = "CREATE TABLE IF NOT EXISTS "
                + TABLE_ORGLEVELSERVICES + "(" + ORGLEVELSERVICES_ID
                + " INTEGER PRIMARY KEY," + ORGLEVELSERVICES_NAME + " TEXT,"
                + ORGLEVELSERVICES_CODE + " TEXT)";
        db.execSQL(CREATE_TABLE_ORGLEVELSERVICES);

        String CREATE_TABLE_DRIVER = "CREATE TABLE " + TABLE_DRIVER + "("
                + DRIVER_PKID + " INTEGER PRIMARY KEY," + DRIVER_ID + " TEXT,"
                + DRIVER_CODE + " TEXT," + DRIVER_NAME + " TEXT,"
                + DRIVER_MOBILE + " TEXT)";
        db.execSQL(CREATE_TABLE_DRIVER);
        String CREATE_TABLE_VEHICLE = "CREATE TABLE " + TABLE_VEHICLE + "("
                + VEHICLE_PKID + " INTEGER PRIMARY KEY," + VEHICLE_ID
                + " TEXT," + VEHICLE_REGNO + " TEXT)";
        db.execSQL(CREATE_TABLE_VEHICLE);

        String CREATE_TABLE_ROUTE = "CREATE TABLE " + TABLE_ROUTE + "("
                + ROUTE_PKID + " INTEGER PRIMARY KEY," + ROUTE_CATEGORY
                + " TEXT)";
        db.execSQL(CREATE_TABLE_ROUTE);


        /*------------SOJU END----------*/

        /*
         * String CREATE_TABLE_PRODUCTMRP ="CREATE TABLE IF NOT EXISTS "+
         * TABLE_PRODUCTMRP +" ( "+ PRODUCTMRP_PKID +" INTEGER PRIMARY KEY,  "+
         * PRODUCTMRP_ID +" TEXT,"+ PRODUCTMRP_MRP +" TEXT,"+ PRODUCTMRP_PRODUCT
         * +" TEXT,"+ PRODUCTMRP_BATCH +" TEXT,"+ PRODUCTMRP_KEY +" TEXT,"+
         * PRODUCTMRP_UOM +" TEXT,"+ PRODUCTMRP_UPC +" TEXT,"+ PRODUCTMRP_TAX
         * +" TEXT)" ;
         *
         * db.execSQL(CREATE_TABLE_PRODUCTMRP);
         */

        String CREATE_TABLE_INVENTORYACCOUNT = "CREATE TABLE IF NOT EXISTS "
                + TABLE_INVENTORYACCOUNT + " ( " + INVENTORYACCOUNT_PKID
                + " INTEGER PRIMARY KEY,  " + INVENTORYACCOUNT_ID + " TEXT,"
                + INVENTORYACCOUNT_NAME + " TEXT )";

        db.execSQL(CREATE_TABLE_INVENTORYACCOUNT);

        String CREATE_TABLE_INVENTORYJRNLHDR = "CREATE TABLE IF NOT EXISTS "
                + TABLE_INVENTORYJRNLHDR + " ( " + INVENTORYJRNLHDR_PKID
                + " INTEGER PRIMARY KEY,  " + INVENTORYJRNLHDR_ID + " TEXT,"
                + INVENTORYJRNLHDR_NO + " TEXT," + INVENTORYJRNLHDR_DATE
                + " TEXT," + INVENTORYJRNLHDR_PRODUCT + " TEXT,"
                + INVENTORYJRNLHDR_UOM + " TEXT,"
                + INVENTORYJRNLHDR_ORG + " TEXT," + INVENTORYJRNLHDR_SOURCE
                + " TEXT," + INVENTORYJRNLHDR_SOURCEID + " TEXT," + CREATETIME
                + " TEXT," + INVENTORYJRNLHDR_BATCH + " TEXT )";

        db.execSQL(CREATE_TABLE_INVENTORYJRNLHDR);

        String CREATE_TABLE_INVENTORYJRNLLINE = "CREATE TABLE IF NOT EXISTS "
                + TABLE_INVENTORYJRNLLINE + " ( " + INVENTORYJRNLLINE_PKID
                + " INTEGER PRIMARY KEY,  " + INVENTORYJRNLLINE_ID + " TEXT,"
                + INVENTORYJRNLLINE_HDR + " TEXT,"
                + INVENTORYJRNLLINE_AMNTCREDIT + " TEXT,"
                + INVENTORYJRNLLINE_AMNTDEBIT + " TEXT,"
                + INVENTORYJRNLLINE_ACCOUNT + " TEXT,"
                + INVENTORYJRNLLINE_QTYCREDIT + " TEXT," + CREATETIME
                + " TEXT," + INVENTORYJRNLLINE_QTYDEBIT + " TEXT )";

        db.execSQL(CREATE_TABLE_INVENTORYJRNLLINE);


        String CREATE_TABLE_COMBINGSUMMARY = "CREATE TABLE IF NOT EXISTS " + TABLE_COMBINGSUMMARY
                + " ( " + COMBINGSUMMARY_PKID + " INTEGER PRIMARY KEY,  " +
                COMBINGSUMMARY_CUSTOMERCOUNT + " TEXT," +
                COMBINGSUMMARY_CZONECOUNT + " TEXT," +
                COMBINGSUMMARY_CZONETARGET + " TEXT )";
        db.execSQL(CREATE_TABLE_COMBINGSUMMARY);




        String CREATE_TABLE_COMBINGCOUNT = " CREATE TABLE IF NOT EXISTS " + TABLE_COMBINGCOUNT
                + " ( " + COMBING_COUNTID + " INTEGER PRIMARY KEY, "
                + TOTAL_COMBED + " TEXT,"
                + TOT_DAYS + " TEXT,"
                + TOTAL_COUNT + " TEXT )";
        db.execSQL(CREATE_TABLE_COMBINGCOUNT);

        //Create Nearest Customer Table
        String CREATE_TABLE_NEARESTCUST = "CREATE TABLE IF NOT EXISTS " + TABLE_NEARESTCUST
                + " ( " + NEAREST_ID + " INTEGER PRIMARY KEY,  "
                + NEAREST_CUST_ID + " TEXT, " +
                NEAREST_CUST_CODE + " TEXT, " + NEAREST_CUST_NAME + " TEXT, " +
                NEAREST_CUST_LAT + " TEXT, "+ NEAREST_CUST_LONG + " TEXT, " + NEAREST_CUST_ADDR + " TEXT, "+ NEAREST_CUST_DIST + " TEXT )";
        db.execSQL(CREATE_TABLE_NEARESTCUST);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERHDR);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERLINE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTCAT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BRAND);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORM);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FOCUSEDPRDS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MRP);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PARTYMARG);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CHECKLISTINPUT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDLDISC);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_APPUSAGE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVENTORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SALESRETURN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SALERTNLINE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONSTANTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CREDITNOTES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CREDITREASONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAVIGATIONDATA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCHEDULE_HEADER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCHEDULE_DETAIL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDER_HEADER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDER_DETAIL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SYNCTABLE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PREVIOUSORDERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PAYMENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRICEDETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVOICE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVOICE_PAYMENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERAMOUNTDATA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REASONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SEARCHCUSTOMERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMERCATEGORIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DENOMINATIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCHEDULESUMMARY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BANKMASTER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_JSONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTCHECKLIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TOWN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORYPHOTOS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DOCUMENTPHOTOS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CRATES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OTHERCOMPANY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BRANCHES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_UNITS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MINQTY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMERTYPE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REGIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTBATCH);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTLINE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVENTORYACCOUNT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVENTORYJRNLHDR);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVENTORYJRNLLINE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVOICENOGEN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TAX);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TAXLINE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERTAXLINE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OFFERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OFFERS_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXPENSE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXPENSETYPE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PACKAGE_TYPE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCT_PACKAGE_MAPPING);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORGANIZATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ASSETS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORGLEVELSERVICES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DRIVER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VEHICLE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROUTE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMBINGSUMMARY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMBINGCOUNT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NEARESTCUST);


        if (Global.force) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_MOBSERVICES);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXCUST);
            Global.force = false;
        }
        onCreate(db);


    }

    // intialise sync time at start of aplication
    public void intialisesynctable() {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TABLEINSYNC, TABLE_ORDER_DETAIL);
        values.put(SYNCTIME, "0");
        db.insert(TABLE_SYNCTABLE, null, values);
        locintialise();
        navintialise();
    }

    public void locintialise() {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TABLEINSYNC, TABLE_LOCATIONS);
        values.put(SYNCTIME, "0");
        db.insert(TABLE_SYNCTABLE, null, values);
    }

    public void navintialise() {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TABLEINSYNC, TABLE_NAVIGATIONDATA);
        values.put(SYNCTIME, "0");
        db.insert(TABLE_SYNCTABLE, null, values);
    }

    // /load pricetable
    public void loadpricetable(JSONArray pricedata) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for (int i = 0; i < pricedata.length(); i++) {
            try {
                JSONObject rateobject = pricedata.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(PRICEDETAILS_PRODUCTID,
                        String.valueOf(rateobject.getInt("productId")));
                values.put(PRICEDETAILS_CUSTOMERCATEGORY,
                        rateobject.getString("CustomerCategory"));
                values.put(PRICE, rateobject.getString("Rate"));
                values.put(VALIDFROM, rateobject.getString("ValidFrom"));
                values.put(VALIDTO, rateobject.getString("ValidTo"));
                values.put(MRP, rateobject.optString("mrp", "0.0"));
                db.insert(TABLE_PRICEDETAILS, null, values);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    // ISCATEGORY TABLE EMPTY
    public Boolean categorytableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_CUSTOMERCATEGORIES;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return result;

    }
    // //////////load customer category list

    public void loadcustomercategorys(JSONArray categorydata) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for (int i = 0; i < categorydata.length(); i++) {
            try {
                JSONObject categoryobj = categorydata.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(CUSTOMERCATEGORIES_CODE, categoryobj.getString("id"));
                values.put(CUSTOMERCATEGORIES_NAME,
                        categoryobj.getString("CustCat"));
                values.put(CUSTOMERCATEGORIES_TYPE,
                        categoryobj.optString("Type", ""));
                db.insert(TABLE_CUSTOMERCATEGORIES, null, values);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public Boolean isinvoicetableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_INVOICE;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;

    }

    // load invoice table
    public void loadinvoicetable(JSONArray invoicedata) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for (int i = 0; i < invoicedata.length(); i++) {
            try {
                JSONObject invoiceobject = invoicedata.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(INVOICE_ID,
                        String.valueOf(invoiceobject.getInt("_id")));
                values.put(INVOICE_NO, invoiceobject.getString("InvoiceNo"));
                values.put(INVOICE_CUSTOMERID, invoiceobject.getString("cusid"));
                values.put(INVOICE_TOTTALAMOUNT,
                        invoiceobject.getString("invototal"));
                values.put(INVOICE_BALANCEAMOUNT,
                        invoiceobject.getString("balance"));
                values.put(INVOICE_STATUS, invoiceobject.getString("Status"));
                values.put(INVOICE_DATE, invoiceobject.optString("InvoiceDate"));
                values.put(INVOICE_STATICBALANCE,
                        invoiceobject.getString("balance"));
                values.put(INVOICE_PERSONID,
                        invoiceobject.getString("Persionid"));
                values.put(INVOICE_PERSONMOB,
                        invoiceobject.getString("PersonMobile"));
                values.put(INVOICE_CUSTNAME,
                        invoiceobject.getString("CustName"));
                values.put(INVOICE_CUSTCODE,
                        invoiceobject.optString("CustCode"));
                values.put(INVOICE_PERSONCODE,
                        invoiceobject.getString("PersonCode"));
                values.put(INVOICE_OUTAMT,
                        invoiceobject.getString("outStanding"));
                values.put(INVOICE_TYPE, invoiceobject.getString("Type"));
                values.put(INVOICE_ASOFDATE,
                        invoiceobject.getString("AsOfDate"));
                db.insert(TABLE_INVOICE, null, values);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    // check whether pricetable empty
    public Boolean ispricetableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_PRICEDETAILS;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;

    }

    public void insertintoorgserv(JSONArray orgservicemap) {
        // TODO Auto-generated method stub
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < orgservicemap.length(); i++) {
                ContentValues values = new ContentValues();
                // values.put(MINQTY_PKID,
                // minqty.getJSONObject(i).optString("sid", ""));
                values.put(ORGLEVELSERVICES_NAME, orgservicemap
                        .getJSONObject(i).optString("Services", ""));
                long id = db.insert(TABLE_ORGLEVELSERVICES, null, values);
                System.out.println(id);
            }

            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }

    }



    public void insertservices(JSONArray services) {

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            for (int i = 0; i < services.length(); i++) {
                JSONObject temp = services.optJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(MOBILESERVICE_CODE, temp.optString("_id"));
                values.put(MOBILESERVICE_NAME, temp.optString("srvcname"));
                db.insert(TABLE_MOBSERVICES, null, values);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();

    }

    public void insetintoassets(JSONArray assetqry) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < assetqry.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(ASSETS_ASSETQNHDR, assetqry.getJSONObject(i)
                        .optString("asstqnhdr", ""));
                values.put(ASSETS_QSTRING,
                        assetqry.getJSONObject(i).optString("QString", ""));
                values.put(
                        ASSETS_SLINEID,
                        assetqry.getJSONObject(i).optString("schedulelineid",
                                ""));
                values.put(ASSETS_QLINEID,
                        assetqry.getJSONObject(i).optString("qnlineid", ""));
                values.put(ASSETS_CUSTOMER, assetqry.getJSONObject(i)
                        .optString("customer", ""));
                values.put(ASSETS_QRCODE,
                        assetqry.getJSONObject(i).optString("QRString", ""));
                values.put(ASSETS_ID,
                        assetqry.getJSONObject(i).optString("assetid", ""));
                values.put(ASSETS_ANSWER, "");
                values.put(ASSETS_PHOTOREQ, assetqry.getJSONObject(i)
                        .optString("PhotoReq", ""));
                values.put(ASSETS_CREATEDTIME, "0");
                long id = db.insert(TABLE_ASSETS, null, values);
                // System.out.println("id==>" + id);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }

    }

    // check whether reason table empty
    public Boolean isreasontableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_REASONS;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;

    }

    // IS TOWN TABLE EMPTY
    public Boolean istowntableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_TOWN;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }

        if (cursor != null)
            cursor.close();
        return result;

    }
    // loadtowndata

    public void loadtowns(JSONArray towns) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ;
        for (int i = 0; i < towns.length(); i++) {
            JSONObject obj = towns.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(TOWN_CODE, obj.optString("townid", ""));
            values.put(TOWN_NAME, obj.optString("townname", ""));
            values.put(TOWN_ORG, obj.optString("organization", ""));
            db.insert(TABLE_TOWN, null, values);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public boolean isothercompanytableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_OTHERCOMPANY;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;
    }
    // check whether customer searchtable empty

    public Boolean iscustomersearchtableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_SEARCHCUSTOMERS;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;

    }

    // load customers of dse for search and pickup offline
    public void loadcustomersearchtable(JSONArray customerdata)
            throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        for (int i = 0; i < customerdata.length(); i++) {
            JSONObject custobj = customerdata.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(SEARCHCUSTOMERS_ID, custobj.getString("custid"));
            values.put(SEARCHCUSTOMERS_NAME, custobj.optString("custname", ""));
            values.put(SEARCHCUSTOMERS_ADRESS,
                    custobj.optString("custadress", ""));
            values.put(SEARCHCUSTOMERS_CODE, custobj.optString("custcode", ""));
            values.put(SEARCHCUSTOMERS_LATTITUDE,
                    custobj.optString("custlat", "0"));
            values.put(SEARCHCUSTOMERS_LONGITUDE,
                    custobj.optString("custlong", "0"));
            values.put(SEARCHCUSTOMERS_CATEGORY,
                    custobj.optString("custcategory", ""));
            values.put(SEARCHCUSTOMERS_LOCATIONLOCK,
                    custobj.optString("locationverified", "0"));
            values.put(SEARCHCUSTOMERS_CLOSINGDAY,
                    custobj.optString("Closingday", ""));
            values.put(SEARCHCUSTOMERS_MOBILENUMBER,
                    custobj.optString("mobilenumber", ""));

            String custroute = custobj.optString("custroutes", "");
            if (!custroute.contentEquals(""))
                custroute = "," + custroute + ",";
            values.put(SEARCHCUSTOMERS_ROUTES, custroute);

            values.put(SEARCHCUSTOMERS_OTPVERIFIED, custobj.optString("OtpVerified", ""));
            values.put(SEARCHCUSTOMERS_OTPLAT,
                    custobj.optString("OtpLat", ""));
            values.put(SEARCHCUSTOMERS_OTPLONG,
                    custobj.optString("OtpLong", ""));

            db.insert(TABLE_SEARCHCUSTOMERS, null, values);

        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }
    // check whether denomdata table empty

    public Boolean denomtableableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_DENOMINATIONS;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;

    }
    // load denom data table

    public void loaddenomdata(JSONArray denomdata) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        for (int i = 0; i < denomdata.length(); i++) {
            JSONObject denomobj = denomdata.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(DENOMINATIONS_CODE, denomobj.getString("id"));
            values.put(DENOMINATIONS_AMOUNT, denomobj.optString("amount", "0"));
            db.insert(TABLE_DENOMINATIONS, null, values);

        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public void loadothercompany(JSONArray othercompany) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ;
        for (int i = 0; i < othercompany.length(); i++) {
            JSONObject obj = othercompany.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(OTHERCOMPANY_ID, obj.getString("othercompanyid"));
            values.put(OTHERCOMPANY_NAME, obj.getString("othercompanyname"));
            db.insert(TABLE_OTHERCOMPANY, null, values);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    // inserting schedule details

    public void insertscheduledata(List<Scheduledata> Scheduldat) {
        Long key;
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for (int i = 0; i < Scheduldat.size(); i++) {

            ContentValues values = new ContentValues();
            values.put(SCHEDULE_ID, Scheduldat.get(i).Scheduleid);
            values.put(SCHEDULE_NAME, Scheduldat.get(i).Schedulename);
            values.put(SALESPERSON_ID, Scheduldat.get(i).Salespersonid);
            values.put(SALESPERSON_NAME, Scheduldat.get(i).salespersonname);
            values.put(SALEROUTE_ID, Scheduldat.get(i).Salesrouteid);
            values.put(SCHEDULE_DATE, Scheduldat.get(i).Scheduledate);
            values.put(HEADER_STATUS, Scheduldat.get(i).Headerstatus);
            values.put(ROUTENETWORK_ID, Scheduldat.get(i).Routenetworkid);
            values.put(SCHEDULE_COMPLETION_STATUS,
                    Scheduldat.get(i).completionstatus);
            values.put(SCHEDULE_BEGIN_TIME, Scheduldat.get(i).begintime);
            values.put(SCHEDULE_COMPLETION_TIME,
                    Scheduldat.get(i).completiontime);
            values.put(SCHEDULE_EXPIRY_STATUS, Scheduldat.get(i).expirystatus);

            values.put(SCHEDULE_VEHICLE, Scheduldat.get(i).Vehicle);
            values.put(SCHEDULE_DRIVER, Scheduldat.get(i).Driver);

            key = db.insert(TABLE_SCHEDULE_HEADER, null, values);
            for (int j = 0; j < Scheduldat.get(i).Scheduledetails.size(); j++) {

                ContentValues values2 = new ContentValues();
                values2.put(
                        SCHEDULE_DETAIL_ID,
                        Scheduldat.get(i).Scheduledetails.get(j).Scheduledetailid);
                values2.put(SCHEDULE_HEADER_ID, String.valueOf(key));
                values2.put(LOCATION_ID,
                        Scheduldat.get(i).Scheduledetails.get(j).locationid);
                values2.put(LOCATION_NAME,
                        Scheduldat.get(i).Scheduledetails.get(j).locationname);
                values2.put(LOCATION_ADRESS,
                        Scheduldat.get(i).Scheduledetails.get(j).locationadress);
                values2.put(SEQNO,
                        Scheduldat.get(i).Scheduledetails.get(j).sequencenumber);
                values2.put(DETAIL_STATUS,
                        Scheduldat.get(i).Scheduledetails.get(j).status);
                values2.put(LATITUDE,
                        Scheduldat.get(i).Scheduledetails.get(j).latitude);
                values2.put(LONGITUDE,
                        Scheduldat.get(i).Scheduledetails.get(j).longitude);
                values2.put(LATITUDE_NEW, "0");
                values2.put(LONGITUDE_NEW, "0");
                values2.put(SCHEDULEDETAIL_CREATETIME, "0");
                values2.put(CLOSING_DAY_UPDATETIME, "0");
                values2.put(
                        CUSTOMERCATOGORY,
                        Scheduldat.get(i).Scheduledetails.get(j).Customercatogory);
                values2.put(
                        CUSTOMERCATOGORYID,
                        Scheduldat.get(i).Scheduledetails.get(j).CustomercatogoryId);
                values2.put(RETAILPRICE,
                        Scheduldat.get(i).Scheduledetails.get(j).Rettail);
                values2.put(INVOCENUMBER,
                        Scheduldat.get(i).Scheduledetails.get(j).invoicenumber);
                values2.put(PICKLISTNUMBER,
                        Scheduldat.get(i).Scheduledetails.get(j).picklistnumber);
                values2.put(SCHED_CUSTOMERCODE,
                        Scheduldat.get(i).Scheduledetails.get(j).customercode);
                values2.put(LOCATIONLOCK,
                        Scheduldat.get(i).Scheduledetails.get(j).locationlock);
                values2.put(CLOSING_DAY,
                        Scheduldat.get(i).Scheduledetails.get(j).Closingday);
                values2.put(SCHEDULEDETAIL_MOBILE,
                        Scheduldat.get(i).Scheduledetails.get(j).mobilenumber);
                values2.put(SCHEDULEDETAIL_OTP,
                        Scheduldat.get(i).Scheduledetails.get(j).OTP);
                values2.put(SCHEDULEDETAIL_TINNUMBER,
                        Scheduldat.get(i).Scheduledetails.get(j).tinnumber);
                values2.put(SCHEDULEDETAIL_PAN,
                        Scheduldat.get(i).Scheduledetails.get(j).pan);
                values2.put(SCHEDULEDETAIL_PHOTOUUID,
                        Scheduldat.get(i).Scheduledetails.get(j).photoUUID);
                values2.put(SCHEDULEDETAIL_LANDMARK,
                        Scheduldat.get(i).Scheduledetails.get(j).landmark);
                values2.put(SCHEDULEDETAIL_TOWN,
                        Scheduldat.get(i).Scheduledetails.get(j).town);
                values2.put(SCHEDULEDETAIL_CONTACTPERSON,
                        Scheduldat.get(i).Scheduledetails.get(j).contactperson);
                values2.put(SCHEDULEDETAIL_OTHERCOMPANY,
                        Scheduldat.get(i).Scheduledetails.get(j).othercompany);
                values2.put(SCHEDULEDETAIL_CRATES,
                        Scheduldat.get(i).Scheduledetails.get(j).crates);
                values2.put(
                        SCHEDULEDETAIL_ALTERCUSTCODE,
                        Scheduldat.get(i).Scheduledetails.get(j).alternatecustcode);
                values2.put(SCHEDULEDETAIL_CUSTTYPE,
                        Scheduldat.get(i).Scheduledetails.get(j).custtype);

                values2.put(SCHEDULEDETAIL_STREETNAME,
                        Scheduldat.get(i).Scheduledetails.get(j).street);
                values2.put(SCHEDULEDETAIL_AREA,
                        Scheduldat.get(i).Scheduledetails.get(j).area);
                values2.put(SCHEDULEDETAIL_LANDLINE,
                        Scheduldat.get(i).Scheduledetails.get(j).landLine);
                values2.put(SCHEDULEDETAIL_SHOP,
                        Scheduldat.get(i).Scheduledetails.get(j).shopname);
                values2.put(SCHEDULEDETAIL_CITY,
                        Scheduldat.get(i).Scheduledetails.get(j).city);
                values2.put(SCHEDULEDETAIL_PIN,
                        Scheduldat.get(i).Scheduledetails.get(j).pin);
                values2.put(SCHEDULEDETAIL_EMAIL,
                        Scheduldat.get(i).Scheduledetails.get(j).email);
//                values2.put(SCHEDULEDETAIL_PLAN,
//                        Scheduldat.get(i).Scheduledetails.get(j).plan);
                values2.put(SCHEDULEDETAIL_DEL_PHOTOFLAG,
                        Scheduldat.get(i).Scheduledetails.get(j).DeliveryImgFlg);

                values2.put(SCHEDULEDETAIL_STARTTIME,
                        Scheduldat.get(i).Scheduledetails.get(j).StartTime);
                values2.put(SCHEDULEDETAIL_ENDTIME,
                        Scheduldat.get(i).Scheduledetails.get(j).EndTime);

                values2.put(SCHEDULEDETAIL_OTPVERIFIED,
                        Scheduldat.get(i).Scheduledetails.get(j).otpverified);

                values2.put(SCHEDULEDETAIL_OTPLAT,
                        Scheduldat.get(i).Scheduledetails.get(j).otplat);
                values2.put(SCHEDULEDETAIL_OTPLONG,
                        Scheduldat.get(i).Scheduledetails.get(j).otplong);
                values2.put(SCHEDULEDETAIL_CUSTIMGURL, Scheduldat.get(i).Scheduledetails.get(j).custimgurl);
                values2.put(SCHEDULE_DETAIL_VISITED,Scheduldat.get(i).Scheduledetails.get(j).visited);

                /*
                 * values2.put( SCHEDULEDETAIL_ALTERCUSTCODE,
                 * Scheduldat.get(i).Scheduledetails.get(j).alternatecustcode);
                 * values2.put( SCHEDULEDETAIL_ALTERCUSTCODE,
                 * Scheduldat.get(i).Scheduledetails.get(j).alternatecustcode);
                 */

                db.insert(TABLE_SCHEDULE_DETAIL, null, values2);

            }

        }

        // 2nd argument is String containing nullColumnHack
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public void Deletetabledata() {

        SQLiteDatabase db = this.getWritableDatabase();
        onUpgrade(db, 1, 1);
    }

    public boolean excustneeded() {
        boolean temp = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "Select " + EXCUST_ID + " from " + TABLE_EXCUST;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            temp = false;
        }
        return temp;
    }

    public void insertintoregions(JSONArray customertypes) throws Exception {
        SQLiteDatabase db = this.getWritableDatabase();
        for (int i = 0; i < customertypes.length(); i++) {
            JSONObject categoryobj = customertypes.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(REGIONS_NAME, categoryobj.optString("Polygon", ""));
            values.put(REGIONS_TARGT, categoryobj.optString("Target", ""));
            values.put(REGIONS_ACHIVD, categoryobj.optString("Achieved", ""));
            values.put(REGIONS_REGIONNAME, categoryobj.optString("name", ""));
            values.put(REGIONS_STATUS, categoryobj.optString("Status", ""));
            db.insert(TABLE_REGIONS, null, values);
        }
        db.close();
    }

    public void insertintocutomertype(JSONArray customertypes) throws Exception {
        SQLiteDatabase db = this.getWritableDatabase();
        for (int i = 0; i < customertypes.length(); i++) {
            JSONObject categoryobj = customertypes.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(CUSTOMERTYPE_ID, categoryobj.getString("sid"));
            values.put(CUSTOMERTYPE_NAME, categoryobj.getString("name"));
            db.insert(TABLE_CUSTOMERTYPE, null, values);
        }
        db.close();
    }

    public void insertintoproductcats(JSONArray productcats) throws Exception {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < productcats.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(PRODUCTCAT_ID, productcats.getJSONObject(i)
                        .optString("sid", ""));
                values.put(PRODUCTCAT_NAME, productcats.getJSONObject(i)
                        .optString("ProdCat", ""));
                long id = db.insert(TABLE_PRODUCTCAT, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }

        db.close();
    }

    public void insertintobrands(JSONArray brands) {
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            db.beginTransaction();
            for (int i = 0; i < brands.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(BRAND_ID,
                        brands.getJSONObject(i).optString("sid", ""));
                values.put(BRAND_NAME,
                        brands.getJSONObject(i).optString("Name", ""));
                long id = db.insert(TABLE_BRAND, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }

        db.close();

    }


    public void insertintoforms(JSONArray forms) {
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            db.beginTransaction();
            for (int i = 0; i < forms.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(FORM_ID, forms.getJSONObject(i).optString("sid", ""));
                values.put(FORM_NAME,
                        forms.getJSONObject(i).optString("Name", ""));
                long id = db.insert(TABLE_FORM, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }

        db.close();
    }

    public void insertintoproducts(JSONArray products) throws Exception {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + PRODUCT_ID + " FROM " + TABLE_PRODUCT;
        Cursor cursor = db.rawQuery(query, null);
        HashSet<String> ids = new HashSet<String>();
        if (cursor.moveToFirst()) {
            do {
                ids.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        try {
            db.beginTransaction();
            for (int i = 0; i < products.length(); i++) {
                if (ids.contains(products.getJSONObject(i).optString("sid", "")))
                    continue;
                ContentValues values = new ContentValues();
                values.put(PRODUCT_ID,
                        products.getJSONObject(i).optString("sid", ""));
                values.put(PRODUCT_NAME,
                        products.getJSONObject(i).optString("ProductName", ""));
                values.put(PRODUCT_CODE,
                        products.getJSONObject(i).optString("ProductCode", ""));
                values.put(
                        PRODUCT_CAT,
                        products.getJSONObject(i).optString("ProductCat",
                                "Other"));
                values.put(PRODUCT_BRAND,
                        products.getJSONObject(i).optString("Brand", "Other"));
                values.put(PRODUCT_FORM,
                        products.getJSONObject(i).optString("Form", "Other"));
                long id = db.insert(TABLE_PRODUCT, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        db.close();
    }

    public void insertintofocsdproducts(JSONArray focusedprds) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < focusedprds.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(FOCUSEDPRDS_ID, focusedprds.getJSONObject(i)
                        .optString("sid", ""));
                values.put(FOCUSEDPRDS_PRODUCT, focusedprds.getJSONObject(i)
                        .optString("product", ""));

                values.put(FOCUSEDPRDS_CUSTCAT, focusedprds.getJSONObject(i)
                        .optString("CustCategory", ""));
                values.put(FOCUSEDPRDS_FLAG, focusedprds.getJSONObject(i)
                        .optString("FocussedProduct", ""));
                values.put(FOCUSEDPRDS_MINQTY, focusedprds.getJSONObject(i)
                        .optString("MinQuantity", ""));

                String from = focusedprds.getJSONObject(i).optString(
                        "StartDate", "");
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date bt = df.parse(from);
                    SimpleDateFormat df2 = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    from = df2.format(bt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String to = focusedprds.getJSONObject(i).optString("EndDate",
                        "");
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date bt = df.parse(to);
                    SimpleDateFormat df2 = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    to = df2.format(bt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                values.put(FOCUSEDPRDS_FROM, from);
                values.put(FOCUSEDPRDS_TO, to);
                long id = db.insert(TABLE_FOCUSEDPRDS, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }

        db.close();
    }


    //--------------------



//    public void updatecombingsummary()throws JSONException{
//        SQLiteDatabase db = this.getWritableDatabase();
//        String query="SELECT COUNT( CASE WHEN " +  DETAIL_STATUS + " == \"Completed\" THEN 1 ELSE NULL END ) AS STAT , COUNT ( "
//                + LOCATION_ID + ") AS TOTALCOUNT FROM " + TABLE_SCHEDULE_DETAIL;
//
//        JSONObject ob=new JSONObject();
//        Cursor cursor = db.rawQuery(query, null);
//        if (cursor.moveToFirst()) {
//            do {
//                ob.put("CombingCount", cursor.getString(cursor.getColumnIndex("STAT")));
//                ob.put("TotalCustomers",cursor.getString(cursor.getColumnIndex("TOTALCOUNT")));
//            } while (cursor.moveToNext());
//        }
//        String
//
//    }


    //---------------------



    public void insertintocombingsummary(JSONArray combingsummary) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < combingsummary.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(COMBINGSUMMARY_CUSTOMERCOUNT,
                        combingsummary.getJSONObject(i).getString("ccount"));
                values.put(COMBINGSUMMARY_CZONECOUNT, combingsummary.getJSONObject(i)
                        .getString("czoncount"));
                values.put(COMBINGSUMMARY_CZONETARGET,
                        combingsummary.getJSONObject(i).getString("target"));
                long id = db.insert(TABLE_COMBINGSUMMARY, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        db.close();
    }


    public void insertintoinventory(JSONArray inventory) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < inventory.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(INVENTORY_ID,
                        inventory.getJSONObject(i).getString("sid"));
                values.put(INVENTORY_PRODUCT, inventory.getJSONObject(i)
                        .getString("product"));
                values.put(INVENTORY_QTY,
                        inventory.getJSONObject(i).getString("qty"));
                long id = db.insert(TABLE_INVENTORY, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        db.close();
    }

    public boolean isbranchestableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_BRANCHES;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }

        if (cursor != null)
            cursor.close();
        return result;

    }

    public void loadbranches(JSONArray branches) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (int i = 0; i < branches.length(); i++) {
                JSONObject branchesobj = branches.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(BRANCHES_ID, branchesobj.optString("sid", ""));
                values.put(BRANCHES_NAME, branchesobj.optString("orgname", ""));
                db.insert(TABLE_BRANCHES, null, values);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.setTransactionSuccessful();
            db.endTransaction();
            db.close();
        }
    }

    // ////////////////loading bankmaster table///////////////////////
    public Boolean bankmastertableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_BANKMASTER;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }

        if (cursor != null)
            cursor.close();
        return result;

    }

    // /check status of producttable
    public Boolean isproducttableempty() {
        Boolean result = true;
        // SQLiteDatabase db = this.getWritableDatabase();
        // String count = "SELECT * FROM " + PRODUCT_TABLE;
        // Cursor cursor = db.rawQuery(count, null);
        // if (cursor.moveToFirst()) {
        // result = false;
        // }

        return result;

    }

    // IS CHECKLIST TABLE EMPTY
    public Boolean ischecklisttableempty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_PRODUCTCHECKLIST;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }

        if (cursor != null)
            cursor.close();
        return result;

    }
    // //loadchecklist

    public void loadchecklist(JSONArray checklist) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for (int i = 0; i < checklist.length(); i++) {
            JSONObject checklistobj = checklist.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(PRODUCTCHECKLIST_CODE, checklistobj.getString("id"));
            values.put(PRODUCTCHECKLIST_NAME,
                    checklistobj.getString("CustomerChecklist"));
            values.put(PRODUCTCHECKLIST_SEQNO, checklistobj.getString("SeqNo"));
            values.put(PRODUCTCHECKLIST_TYPE, checklistobj.getString("Type"));
            db.insert(TABLE_PRODUCTCHECKLIST, null, values);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    // ///////////load bankmaster to cache/////////////////////////
    public void loadbankmaster(JSONArray bankmasterdata) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for (int i = 0; i < bankmasterdata.length(); i++) {
            JSONObject reasonobj = bankmasterdata.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(BANKMASTER_CODE, reasonobj.getString("id"));
            values.put(BANKMASTER_NAME, reasonobj.getString("bankname"));
            values.put(BANKMASTER_BRANCHNAME,
                    reasonobj.getString("baranchname"));
            db.insert(TABLE_BANKMASTER, null, values);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public void insertintoparty(JSONArray party) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + PARTYMARG_ID + " FROM " + TABLE_PARTYMARG;
        Cursor cursor = db.rawQuery(query, null);
        HashSet<String> ids = new HashSet<String>();
        if (cursor.moveToFirst()) {
            do {
                ids.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        try {
            db.beginTransaction();
            for (int i = 0; i < party.length(); i++) {
                if (ids.contains(party.getJSONObject(i).optString("sid", "")))
                    continue;
                ContentValues values = new ContentValues();
                values.put(PARTYMARG_ID,
                        party.getJSONObject(i).optString("sid", ""));
                values.put(PARTYMARG_PRODUCT,
                        party.getJSONObject(i).optString("Product", ""));
                values.put(PARTYMARG_VALUE,
                        party.getJSONObject(i).optString("Value", ""));
                values.put(PARTYMARG_CAT,
                        party.getJSONObject(i).optString("Cat", ""));
                values.put(PARTYMARG_CUSTOMER,
                        party.getJSONObject(i).optString("Customer", ""));

                String from = party.getJSONObject(i).optString("From", "");
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date bt = df.parse(from);
                    SimpleDateFormat df2 = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    from = df2.format(bt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String to = party.getJSONObject(i).optString("To", "");
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date bt = df.parse(to);
                    SimpleDateFormat df2 = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    to = df2.format(bt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                values.put(PARTYMARG_FROM, from);
                values.put(PARTYMARG_TO, to);

                long id = db.insert(TABLE_PARTYMARG, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        db.close();

    }

    public void insertintoaddis(JSONArray adldisc) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + ADDLDISC_ID + " FROM " + TABLE_ADDLDISC;
        Cursor cursor = db.rawQuery(query, null);
        HashSet<String> ids = new HashSet<String>();
        if (cursor.moveToFirst()) {
            do {
                ids.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        try {
            db.beginTransaction();
            for (int i = 0; i < adldisc.length(); i++) {
                if (ids.contains(adldisc.getJSONObject(i).optString("sid", "")))
                    continue;
                ContentValues values = new ContentValues();
                values.put(ADDLDISC_ID,
                        adldisc.getJSONObject(i).optString("sid", ""));
                values.put(ADDLDISC_VALUE,
                        adldisc.getJSONObject(i).optString("Value", ""));
                values.put(ADDLDISC_CAT,
                        adldisc.getJSONObject(i).optString("Cat", ""));

                String from = adldisc.getJSONObject(i).optString("From", "");
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date bt = df.parse(from);
                    SimpleDateFormat df2 = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    from = df2.format(bt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String to = adldisc.getJSONObject(i).optString("To", "");
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date bt = df.parse(to);
                    SimpleDateFormat df2 = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    to = df2.format(bt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                values.put(ADDLDISC_FROM, from);
                values.put(ADDLDISC_TO, to);
                long id = db.insert(TABLE_ADDLDISC, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        db.close();

    }

    public void insertintodrivertable(JSONArray orgservicemap) {
        // TODO Auto-generated method stub
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < orgservicemap.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(DRIVER_ID,
                        orgservicemap.getJSONObject(i).optString("sid", ""));
                values.put(DRIVER_CODE, orgservicemap.getJSONObject(i)
                        .optString("PCode", ""));
                values.put(DRIVER_NAME, orgservicemap.getJSONObject(i)
                        .optString("PName", ""));
                values.put(DRIVER_MOBILE, orgservicemap.getJSONObject(i)
                        .optString("Mobile", ""));
                long id = db.insert(TABLE_DRIVER, null, values);
                System.out.println(id);
            }

            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }

    }


    public void insertintomrp(JSONArray mrp) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + MRP_ID + " FROM " + TABLE_MRP;
        Cursor cursor = db.rawQuery(query, null);
        HashSet<String> ids = new HashSet<String>();
        if (cursor.moveToFirst()) {
            do {
                ids.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        try {
            db.beginTransaction();
            for (int i = 0; i < mrp.length(); i++) {
                if (ids.contains(mrp.getJSONObject(i).optString("sid", "")))
                    continue;
                ContentValues values = new ContentValues();
                values.put(MRP_ID, mrp.getJSONObject(i).optString("sid", ""));
                values.put(MRP_PRODUCT,
                        mrp.getJSONObject(i).optString("Product", ""));
                values.put(MRP_VALUE,
                        mrp.getJSONObject(i).optString("Value", ""));
                String from = mrp.getJSONObject(i).optString("From", "");
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date bt = df.parse(from);
                    SimpleDateFormat df2 = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    from = df2.format(bt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String to = mrp.getJSONObject(i).optString("To", "");
                try {
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date bt = df.parse(to);
                    SimpleDateFormat df2 = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    to = df2.format(bt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                values.put(MRP_FROM, from);
                values.put(MRP_TO, to);
                values.put(MRP_BATCH,
                        mrp.getJSONObject(i).optString("ProductBatch", ""));
                values.put(MRP_CONSOFF,
                        mrp.getJSONObject(i).optString("SellingCO", ""));
                values.put(MRP_FLATOFF,
                        mrp.getJSONObject(i).optString("SellingFD", ""));
                values.put(MRP_TAX, mrp.getJSONObject(i).optString("TAX", ""));
                long id = db.insert(TABLE_MRP, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        db.close();

    }

    public void insertintounits(JSONArray units) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {

            db.beginTransaction();
            for (int i = 0; i < units.length(); i++) {
                JSONObject obj = units.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(UNITS_ID, obj.optString("sid", ""));
                values.put(UNITS_NAME, obj.optString("UnitName", ""));
                values.put(UNITS_ABBRV, obj.optString("Abbreviation", ""));
                long id = db.insert(TABLE_UNITS, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        db.close();

    }

    public void insertintocreditnotereasons(JSONArray creditnotereasons) {

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < creditnotereasons.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(CREDITREASONS_ID, creditnotereasons.getJSONObject(i)
                        .optString("sid", ""));
                values.put(CREDITREASONS_REASON, creditnotereasons
                        .getJSONObject(i).optString("reason", ""));
                long id = db.insert(TABLE_CREDITREASONS, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        db.close();

    }

    public void insertintoconstants(JSONArray getMapUrl) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < getMapUrl.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(CONSTANTS_KEY,
                        getMapUrl.getJSONObject(i).optString("keys", ""));
                values.put(CONSTANTS_VALUE, getMapUrl.getJSONObject(i)
                        .optString("value", ""));
                long id = db.insert(TABLE_CONSTANTS, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        db.close();
    }

    public void insertintoexcust(JSONArray getMapUrl) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < getMapUrl.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(EXCUST_ID,
                        getMapUrl.getJSONObject(i).optString("sid", ""));
                values.put(EXCUST_CODE,
                        getMapUrl.getJSONObject(i).optString("CustCode", ""));
                values.put(EXCUST_NAME,
                        getMapUrl.getJSONObject(i).optString("CustName", ""));
                values.put(EXCUST_ADDRESS, getMapUrl.getJSONObject(i)
                        .optString("Address", ""));
                long id = db.insert(TABLE_EXCUST, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        db.close();
    }

    public void insertintorouteofperson(JSONArray routeofperson) {
        // TODO Auto-generated method stub
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < routeofperson.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(ROUTE_CATEGORY, routeofperson.getJSONObject(i)
                        .optString("category", ""));
                long id = db.insert(TABLE_ROUTE, null, values);
                // System.out.println(id);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }

    }

    public void insertintovehicletable(JSONArray orgservicemap) {
        // TODO Auto-generated method stubdsf
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < orgservicemap.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(VEHICLE_ID, orgservicemap.getJSONObject(i)
                        .optString("sid", ""));
                values.put(VEHICLE_REGNO, orgservicemap.getJSONObject(i)
                        .optString("RegistrationNo", ""));
                long id = db.insert(TABLE_VEHICLE, null, values);
                System.out.println(id);
            }

            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }

    }

    public void insertintominqty(JSONArray minqty) {
        // TODO Auto-generated method stub
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < minqty.length(); i++) {
                ContentValues values = new ContentValues();
                // values.put(MINQTY_PKID,
                // minqty.getJSONObject(i).optString("sid", ""));
                values.put(MINQTY_PRODUCT,
                        minqty.getJSONObject(i).optString("product", ""));
                values.put(MINQTY_CUSTCAT,
                        minqty.getJSONObject(i).optString("CustCategory", ""));
                values.put(MINQTY_QUANTITY,
                        minqty.getJSONObject(i).optString("MinQuantity", ""));
                long id = db.insert(TABLE_MINQTY, null, values);
                System.out.println(id);
            }

            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }

    }

    public boolean isProductBatchTableEmpty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_PRODUCTBATCH;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;
    }

    public boolean isInventoryAccountTableEmpty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_INVENTORYACCOUNT;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;
    }

    public void loadInventoryAccount(JSONArray input) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        for (int i = 0; i < input.length(); i++) {
            JSONObject obj = input.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(INVENTORYACCOUNT_ID,
                    obj.optString("inventoryaccountid", ""));
            values.put(INVENTORYACCOUNT_NAME, obj.optString("AccountName", ""));

            db.insert(TABLE_INVENTORYACCOUNT, null, values);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public boolean isInvoiceNoGenTableEmpty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_INVOICENOGEN;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;
    }

    public boolean isTaxTableEmpty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_TAX;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;
    }

    public void loadProductBatch(JSONArray batches) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        for (int i = 0; i < batches.length(); i++) {
            JSONObject obj = batches.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put(PRODUCTBATCH_ID, obj.optString("productbatchid", ""));
            values.put(PRODUCTBATCH_BATCH, obj.optString("Batch", ""));
            db.insert(TABLE_PRODUCTBATCH, null, values);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public void loadInvoiceNoGen(JSONArray input) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        ;
        for (int i = 0; i < input.length(); i++) {
            JSONObject obj = input.getJSONObject(i);
            ContentValues values = new ContentValues();
            // values.put(INVOICENOGEN_ACTIVE, obj.optString("Active", ""));
            values.put(INVOICENOGEN_CURRVAL, obj.optString("CurrVal", ""));
            values.put(INVOICENOGEN_MAXVAL, obj.optString("MaxVal", ""));
            values.put(INVOICENOGEN_MINVAL, obj.optString("MinVal", ""));
            values.put(INVOICENOGEN_ORGCODE, obj.optString("OrgCode", ""));

            db.insert(TABLE_INVOICENOGEN, null, values);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public void loadAvailableStock(JSONArray input) throws JSONException {

        JSONObject journalData = new JSONObject();
        JSONArray journalDataArray = new JSONArray();

        String Source = "SYNC";
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -1);
        DateFormat dateTimeFormat = new SimpleDateFormat("M/d/yyyy");

        String currenttime = dateTimeFormat.format(c.getTime());


        for (int i = 0; i < input.length(); i++) {

            String jrnlHdr = UUID.randomUUID().toString();
            JSONObject invObject = input.getJSONObject(i);
            String product = invObject.getString("ProductId");
            String batch = invObject.getString("BatchId");
            double qty = Double.parseDouble(invObject
                    .getString("Available  Quantity"));
            double mrp = Double.parseDouble(invObject.getString("MRP"));

            double amount = qty * mrp;

            JSONObject invJrnlHdr = new JSONObject().put("JrnlHdr", jrnlHdr)
                    .put("JournalNo", Source + currenttime)
                    .put("JournalDate", currenttime).put("Product", product)
                    .put("Source", Source).put("SourceID", currenttime)
                    .put("BatchNo", batch).put("UOM", invObject.optString("UOM", "")); // .put("Organization", value);
            invObject.put("InvJrnlHdr", invJrnlHdr);

            JSONArray InvJournalLines = new JSONArray();

            JSONObject availableStockObject = new JSONObject()
                    .put("JournalHdr", jrnlHdr)
                    .put("AmountCredited", "0")
                    .put("AmountDebited", amount + "")
                    .put("InventoryAccount",
                            getInventoryAccountId("Available Stock"))
                    .put("QuantityCredited", "0")
                    .put("QuantityDebited", qty + "");
            InvJournalLines.put(availableStockObject);

            /*
             * JSONObject goodsIssueObject = new JSONObject() .put("JournalHdr",
             * jrnlHdr) .put("AmountCredited","0") .put("AmountDebited",
             * amount+"" ) .put("InventoryAccount",
             * getInventoryAccountId("Goods Issued"))
             * .put("QuantityCredited","0") .put("QuantityDebited", qty+"");
             * InvJournalLines.put(goodsIssueObject);
             */
            invObject.put("InvJournalLines", InvJournalLines);

            journalDataArray.put(invObject);
        }

        journalData.put("JournalData", journalDataArray);

        insertInventoryJrnl(journalData);

    }

    public Boolean insertInventoryJrnl(JSONObject webresp) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();

        // SharedPreferences pref
        // =getApplicationContext().getSharedPreferences("Config",
        // MODE_PRIVATE);

        try {
            JSONArray JournalData = webresp.getJSONArray("JournalData");

            for (int i = 0; i < JournalData.length(); i++) {
                ContentValues InvJrnlHdrval = new ContentValues();
                JSONObject jdobj = JournalData.getJSONObject(i);
                JSONObject jhobj = jdobj.getJSONObject("InvJrnlHdr");

                InvJrnlHdrval.put(INVENTORYJRNLHDR_ID,
                        jhobj.getString("JrnlHdr"));
                InvJrnlHdrval.put(INVENTORYJRNLHDR_NO,
                        jhobj.getString("JournalNo"));
                InvJrnlHdrval.put(INVENTORYJRNLHDR_DATE,
                        jhobj.getString("JournalDate"));
                InvJrnlHdrval.put(INVENTORYJRNLHDR_PRODUCT,
                        jhobj.getString("Product"));
                InvJrnlHdrval.put(INVENTORYJRNLHDR_ORG,
                        jhobj.optString("Organization", "0"));
                InvJrnlHdrval.put(INVENTORYJRNLHDR_SOURCE,
                        jhobj.getString("Source"));
                InvJrnlHdrval.put(INVENTORYJRNLHDR_SOURCEID,
                        jhobj.getString("SourceID"));
                InvJrnlHdrval.put(INVENTORYJRNLHDR_BATCH,
                        jhobj.getString("BatchNo"));

                InvJrnlHdrval.put(INVENTORYJRNLHDR_UOM,
                        jhobj.getString("UOM"));

                long id = db
                        .insert(TABLE_INVENTORYJRNLHDR, null, InvJrnlHdrval);

                Log.d("inserted1", "insert=" + id);

                JSONArray JournaLine = jdobj.getJSONArray("InvJournalLines");
                for (int j = 0; j < JournaLine.length(); j++) {
                    ContentValues InvJrnlLineval = new ContentValues();

                    JSONObject jlobj = JournaLine.getJSONObject(j);
                    InvJrnlLineval.put(INVENTORYJRNLLINE_ID, "0");
                    InvJrnlLineval.put(INVENTORYJRNLLINE_HDR,
                            jlobj.getString("JournalHdr"));
                    InvJrnlLineval.put(INVENTORYJRNLLINE_AMNTCREDIT,
                            jlobj.getString("AmountCredited"));
                    InvJrnlLineval.put(INVENTORYJRNLLINE_AMNTDEBIT,
                            jlobj.getString("AmountDebited"));
                    // InvJrnlLineval.put(INVENTORYJRNLLINE_ACCOUNT
                    // ,getInventoryAccountId(jlobj.getString("InventoryAccount")));

                    InvJrnlLineval.put(INVENTORYJRNLLINE_ACCOUNT,
                            jlobj.getString("InventoryAccount"));
                    InvJrnlLineval.put(INVENTORYJRNLLINE_QTYCREDIT,
                            jlobj.getString("QuantityCredited"));
                    InvJrnlLineval.put(INVENTORYJRNLLINE_QTYDEBIT,
                            jlobj.getString("QuantityDebited"));

                    id = db.insert(TABLE_INVENTORYJRNLLINE, null,
                            InvJrnlLineval);
                    Log.d("inserted2", "insert=" + id);
                }

            }
        } catch (Exception e) {
            e.getStackTrace();
            throw e;
        }
        // return null;
        Boolean result = true;
        // String count = "SELECT * FROM " + TABLE_INVENTORYJRNLHDR;
        String count = "SELECT * FROM " + TABLE_INVENTORYJRNLLINE;

        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null) {
            cursor.close();
        }
        return result;
    }

    public void insertproducts(List<product> products) {


    }

    public void loadTaxTables(JSONArray input) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        HashMap<String, String> taxMap = new HashMap<>();

        for (int i = 0; i < input.length(); i++) {

            JSONObject obj = input.getJSONObject(i);
            String taxId = obj.getString("taxid");
            String taxName = obj.getString("taxname");

            String taxlinename = obj.getString("taxlinename");
            String description = obj.getString("description");
            String rate = obj.getString("rate");
            String type = obj.getString("taxtype");
            String taxlineid = obj.getString("taxlineid");

            if (!taxMap.isEmpty()) {

                if (!taxMap.containsKey(taxId)) {
                    insertTax(taxId, taxName, db);
                    taxMap.put(taxId, taxName);
                }

            } else {// 1st row

                insertTax(taxId, taxName, db);
                taxMap.put(taxId, taxName);

            }

            insertTaxLine(taxId, taxlineid, taxlinename, description, rate,
                    type, db);

        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public void insertTax(String taxId, String taxName, SQLiteDatabase db)
            throws JSONException {

        ContentValues values = new ContentValues();
        values.put(TAX_ID, taxId);
        values.put(TAX_NAME, taxName);

        db.insert(TABLE_TAX, null, values);

    }

    public void insertTaxLine(String taxId, String taxlineId,
                              String taxlinename, String description, String rate,
                              String taxtype, SQLiteDatabase db) throws JSONException {

        ContentValues values = new ContentValues();
        values.put(TAXLINE_ID, taxlineId);
        values.put(TAXLINE_DESCRIP, description);
        values.put(TAXLINE_RATE, rate);
        values.put(TAXLINE_TAX, taxId);
        values.put(TAXLINE_NAME, taxlinename);
        values.put(TAXLINE_TYPE, taxtype);

        db.insert(TABLE_TAXLINE, null, values);

    }

    public boolean isInvJrnlTableEmpty() {
        Boolean result = true;
        SQLiteDatabase db = this.getWritableDatabase();
        String count = "SELECT * FROM " + TABLE_INVENTORYJRNLLINE;
        Cursor cursor = db.rawQuery(count, null);
        if (cursor.moveToFirst()) {
            result = false;
        }
        if (cursor != null)
            cursor.close();
        return result;
    }

    public void loadOffers(JSONArray offersArray) throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();

        HashMap<String, String> hashMap = new HashMap<>();

        for (int i = 0; i < offersArray.length(); i++) {

            JSONObject obj = offersArray.getJSONObject(i);
            String offerId = obj.optString(OFFERS_ID, "");
            String NoOfFreeItems = obj.optString(OFFERS_FREEITEMSNO, "");
            String FreeProduct = obj.optString(OFFERS_FREEPRODUCT, "");
            String MinQty = obj.optString(OFFERS_MINQTY, "");
            String OffType = obj.optString(OFFERS_OFFTYPE, "");
            String OffValue = obj.optString(OFFERS_OFFVALUE, "");
            String RateType = obj.optString(OFFERS_RATETYPE, "");
            String UOM = obj.optString(OFFERS_UOM, "");
            String individualFlag = obj.optString("ApplyOnIndividualItem", "");
            String custCat = obj.optString("CustomerCategory", "0");

            String offerDtlId = obj.optString(OFFERS_DETAILS_ID, "");
            String product = obj.optString("ProductName", "");
            String productCategory = obj.optString("ProductCategory", "");

            if (!hashMap.isEmpty()) {

                if (!hashMap.containsKey(offerId)) {

                    insertOffer(offerId, NoOfFreeItems, FreeProduct, MinQty, OffType,
                            OffValue, RateType, UOM, individualFlag, custCat, db);
                    hashMap.put(offerId, MinQty);
                }

            } else {// 1st row

                insertOffer(offerId, NoOfFreeItems, FreeProduct, MinQty, OffType,
                        OffValue, RateType, UOM, individualFlag, custCat, db);
                hashMap.put(offerId, MinQty);
            }


            insertOfferDtls(offerId, offerDtlId, product, productCategory, db);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public void insertorglatlngs(JSONArray assetqry) {

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < assetqry.length(); i++) {
                ContentValues values = new ContentValues();
                values.put(ORGANIZATION_ORGLAT, assetqry.getJSONObject(i)
                        .optString("OrgLat", ""));
                values.put(ORGANIZATION_ORGLONG, assetqry.getJSONObject(i)
                        .optString("OrgLong", ""));
                values.put(ORGANIZATION_ACCURACY, assetqry.getJSONObject(i)
                        .optString("Accuracy", ""));
                values.put(ORGANIZATION_OSRMPORT, assetqry.getJSONObject(i)
                        .optString("OSRMPORT", ""));

                values.put(ORGANIZATION_LOCATIONACCURACY, assetqry
                        .getJSONObject(i).optString("locationaccuracy", ""));

                long id = db.insert(TABLE_ORGANIZATION, null, values);
                System.out.println("id==>" + id);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }

    }

    public void insertOfferDtls(String offerId, String offerDtlId,
                                String product, String productCategory, SQLiteDatabase db) throws JSONException {

        ContentValues values = new ContentValues();
        values.put(OFFERS_DETAILS_ID, offerDtlId);
        values.put(OFFERS_DETAILS_OFFER, offerId);
        values.put(OFFERS_DETAILS_PRODUCT, product);
        values.put(OFFERS_DETAILS_PRODUCTCATEGORY, productCategory);

        db.insert(TABLE_OFFERS_DETAILS, null, values);

    }

    public void loadPackageTypes(JSONArray input) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.beginTransaction();
            for (int i = 0; i < input.length(); i++) {
                JSONObject obj = input.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(PACKAGE_TYPE_ID, obj.optString("TypeId", ""));
                values.put(PACKAGE_TYPE_CODE, obj.optString("Code", ""));
                values.put(PACKAGE_TYPE_NAME, obj.optString("Name", ""));
                long id = db.insert(TABLE_PACKAGE_TYPE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        db.close();

    }

    public void deleteOrdertakingService(String services) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String count = "DELETE FROM " + TABLE_MOBSERVICES + " WHERE "
                    + MOBILESERVICE_NAME + "='" + services + "'";
            db.execSQL(count);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteService(String services) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String count = "DELETE FROM " + TABLE_MOBSERVICES + " WHERE "
                    + MOBILESERVICE_NAME + "='" + services + "'";
            db.execSQL(count);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createNewSchedule(Scheduledata header) {

        long key = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SCHEDULE_ID, header.Scheduleid);
        values.put(SCHEDULE_NAME, header.Schedulename);
        values.put(SALESPERSON_ID, header.Salespersonid);
        values.put(SALESPERSON_NAME, header.salespersonname);
        values.put(SALEROUTE_ID, header.Salesrouteid);
        values.put(SCHEDULE_DATE, header.Scheduledate);
        values.put(HEADER_STATUS, header.Headerstatus);
        values.put(ROUTENETWORK_ID, header.Routenetworkid);
        values.put(SCHEDULE_BEGIN_TIME, header.begintime);
        key = db.insert(TABLE_SCHEDULE_HEADER, null, values);
    }

    // load resons to cache
    public void loadresons(JSONArray resondata) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for (int i = 0; i < resondata.length(); i++) {
            try {
                JSONObject reasonobj = resondata.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(REASONS_CODE, reasonobj.getString("Id"));
                values.put(REASON_DETAIL, reasonobj.getString("Reason"));
                values.put(REASON_SENDBACK, reasonobj.getString("SendBackFlag"));
                db.insert(TABLE_REASONS, null, values);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }

    public void insertOffer(String offerId, String NoOfFreeItems, String FreeProduct, String MinQty,
                            String OffType, String OffValue, String RateType, String UOM, String individualFlag, String custCategory, SQLiteDatabase db)
            throws JSONException {

        ContentValues values = new ContentValues();
        values.put(OFFERS_ID, offerId);
        values.put(OFFERS_FREEITEMSNO, NoOfFreeItems);
        values.put(OFFERS_FREEPRODUCT, FreeProduct);
        values.put(OFFERS_MINQTY, MinQty);
        values.put(OFFERS_OFFTYPE, OffType);
        values.put(OFFERS_OFFVALUE, OffValue);
        values.put(OFFERS_RATETYPE, RateType);
        values.put(OFFERS_UOM, UOM);
        values.put(OFFERS_INDIVIDUAL_FLAG, individualFlag);
        values.put(OFFERS_CUST_CATEGORY, custCategory);
        long id = db.insert(TABLE_OFFERS, null, values);

    }

    public String getInventoryAccountId(String accountName) {

        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + INVENTORYACCOUNT_ID + " FROM "
                + TABLE_INVENTORYACCOUNT + " WHERE " + INVENTORYACCOUNT_NAME
                + " = '" + accountName + "'";

        String id = "0";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            id = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();

        // db.close();
        return id;

    }

    public void loadExpenseTypes(JSONArray inputArray) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + EXPENSETYPE_ID + " FROM " + TABLE_EXPENSETYPE;
        Cursor cursor = db.rawQuery(query, null);
        HashSet<String> ids = new HashSet<String>();
        if (cursor.moveToFirst()) {
            do {
                ids.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        try {
            db.beginTransaction();
            for (int i = 0; i < inputArray.length(); i++) {
                if (ids.contains(inputArray.getJSONObject(i).optString("_id", "")))
                    continue;
                ContentValues values = new ContentValues();
                values.put(EXPENSETYPE_ID, inputArray.getJSONObject(i).optString("_id", ""));
                values.put(EXPENSETYPE_NAME, inputArray.getJSONObject(i).optString("Expense Type", ""));

                long id = db.insert(TABLE_EXPENSETYPE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        db.close();

    }

    public boolean isorgserviceexist(String servcname) {
        Boolean result = false;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String count = "SELECT * FROM " + TABLE_ORGLEVELSERVICES
                    + " WHERE " + ORGLEVELSERVICES_NAME + "='" + servcname
                    + "'";
            Cursor cursor = db.rawQuery(count, null);
            if (cursor.moveToFirst()) {
                result = true;
            }
            if (cursor != null)
                cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getConstantValue(String key) throws Exception {
        String temp = "";
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + CONSTANTS_VALUE + " FROM " + TABLE_CONSTANTS
                + " WHERE " + CONSTANTS_KEY + "='" + key + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            temp = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        return temp.contentEquals("") && key.contentEquals("passbyRange") ? "25"
                : temp;
    }

    public String getmapurloptions() {
        String temp = null;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + CONSTANTS_VALUE + " FROM " + TABLE_CONSTANTS
                + " WHERE " + CONSTANTS_KEY + "='mapURLOptions'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            temp = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public String getmapurl() {
        String temp = null;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + CONSTANTS_VALUE + " FROM " + TABLE_CONSTANTS
                + " WHERE " + CONSTANTS_KEY + "='mapURL'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            temp = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public long AddNavigationdata(Vehicledata vehdata) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PLANID, vehdata.PlanId);
        values.put(KEY_VEHICLEID, vehdata.VehicleId);
        values.put(KEY_DEVICETIME, vehdata.DeviceTime);
        values.put(KEY_LATITUDE, vehdata.Latitude);
        values.put(KEY_LONGITUDE, vehdata.Longitude);
        values.put(KEY_SPEED, vehdata.Speed);
        values.put(KEY_PROVIDER, vehdata.provider);
        values.put(KEY_BEARING, vehdata.bearing);
        values.put(KEY_ACCURACY, vehdata.accuracy);
        values.put(KEY_DISTANCE, vehdata.distance);
        values.put(KEY_CUSTOMER, vehdata.customer);
        values.put(NAVIGATIONDATA_UPFLAG, "0");
        values.put(KEY_TIMEDIFF, vehdata.timediff);
        values.put(KEY_MONGODT, vehdata.mongoDate);
        values.put(KEY_INACTIVE, vehdata.inactive);

        // Inserting Row
        long id = db.insert(TABLE_NAVIGATIONDATA, null, values);

        // 2nd argument is String containing nullColumnHack
        db.close();

        return id;
    }

    public JSONArray getallcustomerlatlongs() throws Exception {
        JSONArray array = new JSONArray();
        String sql = "select " + LOCATION_ID + "," + LATITUDE + "," + LONGITUDE
                + " from " + TABLE_SCHEDULE_DETAIL;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                if (!cursor.getString(0).contains("-")
                        && Double.parseDouble(cursor.getString(1)) > 0) {
                    JSONObject obj = new JSONObject();
                    obj.put("customer", cursor.getString(0));
                    obj.put("lat", cursor.getString(1));
                    obj.put("long", cursor.getString(2));
                    array.put(obj);
                }
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return array;
    }

    public JSONArray getallcustomerlatlongsnotpass() throws Exception {
        JSONArray array = new JSONArray();
        String sql = "select " + SCHEDULE_DETAIL_PKID + "," + LATITUDE + ","
                + LONGITUDE + " from " + TABLE_SCHEDULE_DETAIL + " where "
                + SCHEDULEDETAIL_PASSBY + "!='1' or " + SCHEDULEDETAIL_PASSBY
                + " is null";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                if (!cursor.getString(0).contains("-")
                        && Double.parseDouble(cursor.getString(1)) > 0) {
                    JSONObject obj = new JSONObject();
                    obj.put("slineid", cursor.getString(0));
                    obj.put("lat", cursor.getString(1));
                    obj.put("long", cursor.getString(2));
                    array.put(obj);
                }
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return array;
    }

    public int NavDatacounttoSync() {
        int temp = 0;
        String countQuery = "SELECT count(1) FROM " + TABLE_NAVIGATIONDATA
                + " where " + NAVIGATIONDATA_UPFLAG + "!='1' or "
                + NAVIGATIONDATA_UPFLAG + " is null";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        if (cursor.moveToFirst()) {
            temp = cursor.getInt(0);
        }
        if (cursor != null)
            cursor.close();

        return temp;
    }

    public List<Vehicledata> Getallnavigationdata() {

        List<Vehicledata> vehicledatalist = new ArrayList<Vehicledata>();
        try {
            // Select All Query
            String selectQuery = "SELECT * FROM " + TABLE_NAVIGATIONDATA
                    + " where " + NAVIGATIONDATA_UPFLAG + "!='1' or "
                    + NAVIGATIONDATA_UPFLAG + " is null";
            Log.e("In DB", selectQuery);
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            // looping through all rows and adding to list

            if (cursor.moveToFirst()) {
                do {
                    Vehicledata vehdata = new Vehicledata();
                    vehdata.pkid = cursor.getString(0);
                    vehdata.PlanId = cursor.getString(1);
                    vehdata.VehicleId = cursor.getString(2);
                    vehdata.DeviceTime = cursor.getString(3);
                    vehdata.Latitude = cursor.getString(4);
                    vehdata.Longitude = cursor.getString(5);
                    vehdata.Speed = cursor.getString(6);
                    vehdata.provider = cursor.getString(7);
                    vehdata.bearing = cursor.getString(8);
                    vehdata.accuracy = cursor.getString(9);
                    vehdata.distance = cursor.getString(10);
                    vehdata.customer = cursor.getString(cursor
                            .getColumnIndex(KEY_CUSTOMER));
                    vehdata.timediff = cursor.getString(cursor
                            .getColumnIndex(KEY_TIMEDIFF));
                    vehdata.mongoDate = cursor.getString(cursor
                            .getColumnIndex(KEY_MONGODT));
                    vehdata.inactive = cursor.getString(cursor
                            .getColumnIndex(KEY_INACTIVE));
                    vehicledatalist.add(vehdata);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vehicledatalist;

    }

    public void cleartable(String pkid) {
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "update " + TABLE_NAVIGATIONDATA + " set "
                + NAVIGATIONDATA_UPFLAG + "='1' where " + KEY_NAVIGATIONID
                + " in " + pkid;
        db.execSQL(query);

    }

    public void updateSlinePassby(String slineid, String crtime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SCHEDULEDETAIL_PASSBY, "1");
        values.put(SCHEDULEDETAIL_PASSBYCRTIME, crtime);
        db.update(TABLE_SCHEDULE_DETAIL, values, SCHEDULE_DETAIL_PKID + "=?",
                new String[]{slineid});
    }

    public String getactualscheduleid() {

        String ServerScheduleid = "0";

        String selectQuery = "SELECT " + SCHEDULE_ID + " FROM "
                + TABLE_SCHEDULE_HEADER + " ORDER BY " + SCHEDULE_HEADER_PKID
                + " DESC LIMIT 1";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                ServerScheduleid = cursor.getString(0);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return ServerScheduleid;

    }

    public String getscheduleheaderbegintime(String scheduleid) {
        // TODO Auto-generated method stub
        String Schedulebegintime = "";
        String get_schedulebegin = "SELECT " + SCHEDULE_BEGIN_TIME + " FROM "
                + TABLE_SCHEDULE_HEADER + " WHERE  " + SCHEDULE_ID + "='"
                + scheduleid + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(get_schedulebegin, null);
        if (cursor.moveToFirst()) {

            Schedulebegintime = cursor.getString(0);
        }

        // if (Schedulebegintime == null)
        // Schedulebegintime = "";
        if (cursor != null)
            cursor.close();
        return Schedulebegintime;

    }

    // get unvisited stores to sync
    public JSONArray getunvisitedstores(boolean backup) throws JSONException {

        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }


        //		String lastupdate = getrecentsynctime();
        String salespersonid = String.valueOf(getsalespersonid());
        JSONArray unvisitedstores = new JSONArray();
        try {
            String selectQuery = new StringBuilder().append("SELECT ")
                    .append(STOREVISIT_REASON).append(" AS REASON ,").append(STOREVISIT_TIME).append(",")
                    .append(STOREVISIT_LATTITUDE).append(",").append(STOREVISIT_LONGITUDE).append(",")
                    .append(SCHEDULE_DETAIL_ID).append(",").append(LOCATION_ID).append(",")
                    .append(SCHEDULEDETAIL_LATTITUDE).append(",").append(SCHEDULEDETAIL_LONGITUDE).append(",")
                    .append(SCHEDULE_DETAIL_VISITED).append(" AS VISITED ")
                    .append(" FROM ").append(TABLE_SCHEDULE_DETAIL).append(" WHERE ")
                    .append(SCHEDULEDETAIL_CREATETIME).append(" > ")
                    .append(lastupdate).toString();


            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    JSONObject obj = new JSONObject();
                    String reason = cursor.getString(0);
                    obj.put("reason", cursor.getString(cursor.getColumnIndex("REASON")));
                    obj.put("visittime", cursor.getString(1));
                    obj.put("lattitude", cursor.getString(2));
                    obj.put("longitude", cursor.getString(3));
                    obj.put("scheduledetailid", cursor.getString(4));
                    obj.put("customerid", cursor.getString(5));
                    obj.put("personid", salespersonid);
                    String lat = cursor.getString(cursor
                            .getColumnIndex(SCHEDULEDETAIL_LATTITUDE));
                    String lng = cursor.getString(cursor
                            .getColumnIndex(SCHEDULEDETAIL_LONGITUDE));

                    obj.put("reasonlatitude", lat == null ? "" : lat);
                    obj.put("reasonlongitude", lng == null ? "" : lng);
                    obj.put("visited",cursor.getString(cursor.getColumnIndex("VISITED")));

                    if (reason != null && !reason.equals("null")
                            && !reason.equals("")) {
                        unvisitedstores.put(obj);
                    }

                } while (cursor.moveToNext());

            }
            if (cursor != null)
                cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return unvisitedstores;
    }

    public JSONArray getlocationdata() throws Exception {


        String lastsynctime = lastlocatiosynctime();
        JSONArray loclist = new JSONArray();

        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_LOCATIONS + " WHERE "
                + LOCATION_CREATETIME + ">" + lastsynctime;
        ;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {

                JSONObject obj = new JSONObject();

                obj.put("Locationid", cursor.getString(1));
                obj.put("Locationname", cursor.getString(2));
                obj.put("Locationadress", cursor.getString(3));
                obj.put("Latitude", cursor.getString(4));
                obj.put("Longitude", cursor.getString(5));
                obj.put("CustCode", cursor.getString(7));
                obj.put("Customercategory", cursor.getString(8));
                obj.put("Tinnumber", cursor.getString(9));
                obj.put("nearestlandmark", cursor.getString(cursor.getColumnIndex(NEAREST_LANDMARK)));
                obj.put("contactpersonname", cursor.getString(11));
                obj.put("contactnumber", cursor.getString(12));
                obj.put("anyothercompany", cursor.getString(cursor.getColumnIndex(ANT_OTHERCOMPANYVALUE)));
                obj.put("townclass", cursor.getString(14));
                obj.put("selectedproductcategories", cursor.getString(15));
                obj.put("customercategorycode", cursor.getString(16));
                obj.put("schlineguid", cursor.getString(17));
                obj.put("Salespersonid", getsalespersonid());
                obj.put("Pan",
                        cursor.getString(cursor.getColumnIndex(LOCATION_PAN)));
                obj.put("category",
                        getProductCats(db, cursor.getString(1), lastsynctime));
                obj.put("document",
                        getDocuments(db, cursor.getString(1), lastsynctime));
                obj.put("email",
                        cursor.getString(cursor.getColumnIndex(LOCATION_EMAIL)));
                obj.put("photouuid", cursor.getString(cursor
                        .getColumnIndex(LOCATION_PHOTOUUID)));
                obj.put("directcov", cursor.getString(cursor
                        .getColumnIndex(LOCATION_DIRECTCOV)));
                obj.put("locprovider", cursor.getString(cursor
                        .getColumnIndex(LOCATION_LOCPROVIDER)));
                obj.put("locaccuracy", cursor.getString(cursor
                        .getColumnIndex(LOCATION_LOCACCURACY)));
                obj.put("otpverified", cursor.getString(cursor
                        .getColumnIndex(LOCATION_OTPVERIFIED)));
                obj.put("closingday", cursor.getString(cursor
                        .getColumnIndex(LOCATION_CLOSING_DAY)));
                obj.put("storeclosed", cursor.getString(cursor
                        .getColumnIndex(LOCATION_STOREISCLOSED)));
                obj.put("pngcovered", cursor.getString(cursor
                        .getColumnIndex(LOCATION_PNGCOVERED)));
                obj.put("branch", cursor.getString(cursor
                        .getColumnIndex(LOCATION_BRANCH)));

                obj.put("drugLicienceNo",
                        cursor.getString(cursor.getColumnIndex(LOCATION_DRUG)));
                obj.put("locality", cursor.getString(cursor
                        .getColumnIndex(LOCATION_LOCALITY)));
                obj.put("city",
                        cursor.getString(cursor.getColumnIndex(LOCATION_CITY)));
                obj.put("state",
                        cursor.getString(cursor.getColumnIndex(LOCATION_STATE)));
                obj.put("pin",
                        cursor.getString(cursor.getColumnIndex(LOCATION_PIN)));
                obj.put("coverageDay", cursor.getString(cursor
                        .getColumnIndex(LOCATION_COVDAY)));
                obj.put("week1",
                        cursor.getString(cursor.getColumnIndex(LOCATION_WEEK1)));
                obj.put("week2",
                        cursor.getString(cursor.getColumnIndex(LOCATION_WEEK2)));
                obj.put("week3",
                        cursor.getString(cursor.getColumnIndex(LOCATION_WEEK3)));
                obj.put("week4",
                        cursor.getString(cursor.getColumnIndex(LOCATION_WEEK4)));
                obj.put("visitFrequency", cursor.getString(cursor
                        .getColumnIndex(LOCATION_VISTFRQ)));
                obj.put("type",
                        cursor.getString(cursor.getColumnIndex(LOCATION_TYPE)));
                obj.put("wholeSale", cursor.getString(cursor
                        .getColumnIndex(LOCATION_WHOLSAL)));
                obj.put("metro",
                        cursor.getString(cursor.getColumnIndex(LOCATION_METRO)));
                obj.put("classification", cursor.getString(cursor
                        .getColumnIndex(LOCATION_CLASSIF)));

                obj.put("latitudePhoto", cursor.getString(cursor
                        .getColumnIndex(LOCATION_LATITUDEPHOTO)));
                obj.put("longitudePhoto", cursor.getString(cursor
                        .getColumnIndex(LOCATION_LONGITUDEPHOTO)));
                obj.put("remarks", cursor.getString(cursor
                        .getColumnIndex(LOCATION_REMARKS)));
                obj.put("marketname", cursor.getString(cursor
                        .getColumnIndex(LOCATION_MARKETNAME)));

                obj.put("startTime", cursor.getString(cursor
                        .getColumnIndex(LOCATION_STARTTIME)));
                obj.put("endTime", cursor.getString(cursor
                        .getColumnIndex(LOCATION_ENDTIME)));
                obj.put("submitedtime", cursor.getString(cursor
                        .getColumnIndex(LOCATION_CREATETIME)));

                obj.put("otpSubmitTime", cursor.getString(cursor
                        .getColumnIndex(LOCATION_OTPSUBTIME)));

                obj.put("GPSCapTime", cursor.getString(cursor
                        .getColumnIndex(LOCATION_FIXTIME)));

                String selectSurrogates = "SELECT * FROM "
                        + TABLE_CHECKLISTINPUT + " WHERE "
                        + CHECKLISTINPUT_CUSTNAME + " = '"
                        + cursor.getString(17) + "'";
                Cursor cursorsurrogates = db.rawQuery(selectSurrogates, null);
                JSONArray surrogates = new JSONArray();
                if (cursorsurrogates.moveToFirst()) {
                    do {
                        JSONObject objects = new JSONObject();
                        objects.put("id", cursorsurrogates.getString(1));
                        objects.put("input", cursorsurrogates.getString(3));
                        surrogates.put(objects);
                    } while (cursorsurrogates.moveToNext());
                }
                obj.put("surrogates", surrogates);
                loclist.put(obj);

            } while (cursor.moveToNext());

        }
        for (int i = 0; i < loclist.length(); i++) {
            String filename = loclist.getJSONObject(i).getString("Locationid")
                    + ".png";
//            loclist.getJSONObject(i).put("idproof",
//                    readFromFile("idproof_" + filename));// .replace("\n",
            // "").replace("\r",
            // ""));
            loclist.getJSONObject(i).put("photo",
                    readFromFile("photo_" + filename));
            loclist.getJSONObject(i).put("sales",
                    readFromFile("sales_" + filename));
        }
        if (cursor != null)
            cursor.close();
        return loclist;


    }

    // /////////reading customer proof image from local file
    private String readFromFile(String filename) {

        String stringifiedbitmap = "";
        try {
            File rootsd = Environment.getExternalStorageDirectory();
            String path = rootsd.getAbsolutePath() + "/Dhirouter/" + filename;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(path, options);

            stringifiedbitmap = getStringFromBitmap(bitmap);
        } catch (Exception e) {
            stringifiedbitmap = "";
            e.printStackTrace();

        }
        return stringifiedbitmap;
        // Bitmap decodedbitmap=getBitmapFromString(stringifiedbitmap);

    }

    // convert bitmap image to string
    private String getStringFromBitmap(Bitmap bitmapPicture) {
        /*
         * This functions converts Bitmap picture to a string which can be
         * JSONified.
         */
        final int COMPRESSION_QUALITY = 75;
        String encodedImage;
        ByteArrayOutputStream byteArrayBitmapStream = new ByteArrayOutputStream();
        bitmapPicture.compress(Bitmap.CompressFormat.JPEG, COMPRESSION_QUALITY,
                byteArrayBitmapStream);
        byte[] b = byteArrayBitmapStream.toByteArray();
        encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }

    // get closing day update data for sync
    // get unvisited stores to sync
    public JSONArray getclosingdayupdates(boolean backup) throws JSONException {


        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }


        //		String lastupdate = getrecentsynctime();
        String salespersonid = String.valueOf(getsalespersonid());
        JSONArray closingdayupdates = new JSONArray();
        String selectQuery = new StringBuilder().append("SELECT ").append(CLOSING_DAY).append(",").append(SCHEDULE_DETAIL_ID).append(",").append(LOCATION_ID).append(" FROM ").append(TABLE_SCHEDULE_DETAIL).append(" WHERE ").append(CLOSING_DAY_UPDATETIME).append(" > ").append(lastupdate).toString();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("closingday", cursor.getString(0));
                obj.put("scheduledetailid", cursor.getString(1));
                obj.put("customerid", cursor.getString(2));
                obj.put("personid", salespersonid);
                closingdayupdates.put(obj);
            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return closingdayupdates;
    }

    // get all denomination data to sync
    public JSONArray getdenominationdatatatosync(boolean backup) throws JSONException {
        String lastsyc = "";
        if (backup) {
            lastsyc = "0";
        } else {
            lastsyc = getrecentsynctime();
        }

        //		String lastsyc = getrecentsynctime();

        JSONArray denomdata = new JSONArray();

        String selectQuery = new StringBuilder().append("SELECT ").append(SCHEDULESUMMARY_SUBMITTIME).append(",").append(SCHEDULESUMMARY_PERSONID).append(",").append(SCHEDULESUMMARY_LAT).append(",").append(SCHEDULESUMMARY_LONGI).append(",").append(SCHEDULESUMMARY_UNIQUEID).append(",").append(SCHEDULESUMMARY_DENOMEDATA).append(",").append(SCHEDULESUMMARY_TOTTALCASH).append(",").append(SCHEDULESUMMARY_SCHEDULEID).append(",").append(SCHEDULESUMMARY_COMPLETION_TIME).append(",").append(SCHEDULESUMMARY_BEGIN_TIME).append(" FROM ").append(TABLE_SCHEDULESUMMARY).append(" WHERE ").append(SCHEDULESUMMARY_CREATETIME).append(" > ").append(lastsyc).toString();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();

                obj.put("Submittime", cursor.getString(0));
                obj.put("personid", cursor.getString(1));
                obj.put("lattitude", cursor.getString(2));
                obj.put("longitude", cursor.getString(3));
                obj.put("uniqueid", cursor.getString(4));
                obj.put("denomedata", cursor.getString(5));
                obj.put("totalcash", cursor.getString(6));
                obj.put("scheduleid", cursor.getString(7));
                obj.put("completiontime", cursor.getString(8));
                obj.put("begintime", cursor.getString(9));

                denomdata.put(obj);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return denomdata;

    }

    public String getrecentsynctime() {
        String lastsynctime = "0";
        String selectQuery = "SELECT * FROM " + TABLE_SYNCTABLE + " WHERE "
                + TABLEINSYNC + "=" + "'orderdetail'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                lastsynctime = cursor.getString(2);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return lastsynctime;

    }

    // ///getsalesperson name
    public int getsalespersonid() {

        int salepersonid = 0;
        String selectQuery = "SELECT * FROM " + TABLE_SCHEDULE_HEADER;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                String salepid = cursor.getString(3);
                salepersonid = Integer.parseInt(salepid);
                return salepersonid;

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return salepersonid;

    }

    // /get the last synctime for location
    public String lastlocatiosynctime() throws Exception {

        String lastsynctime = "0";
        String selectQuery = "SELECT * FROM " + TABLE_SYNCTABLE + " WHERE "
                + TABLEINSYNC + "=" + "'loactiontable'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                lastsynctime = cursor.getString(2);
            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return lastsynctime;
    }

    private JSONArray getProductCats(SQLiteDatabase db, String string,
                                     String lastsynctime) throws JSONException {
        JSONArray temp = new JSONArray();
        String sql = "Select " + CATEGORYPHOTOS_CATEGORY + ","
                + CATEGORYPHOTOS_UUID + " from " + TABLE_CATEGORYPHOTOS
                + " where " + CATEGORYPHOTOS_CUSTID + "='" + string + "' AND "
                + CATEGORYPHOTOS_CREATETIME + ">" + lastsynctime + " AND "
                + CATEGORYPHOTOS_UPDATEFLAG + "!='1'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("name", cursor.getString(0));
                obj.put("uuid", cursor.getString(1));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    private JSONArray getDocuments(SQLiteDatabase db, String string,
                                   String lastsynctime) throws JSONException {
        JSONArray temp = new JSONArray();
        String sql = "Select " + DOCUMENTPHOTOS_DOCUMENT + ","
                + DOCUMENTPHOTOS_UUID + " from " + TABLE_DOCUMENTPHOTOS
                + " where " + DOCUMENTPHOTOS_CUSTID + "='" + string + "'  AND "
                + DOCUMENTPHOTOS_CREATETIME + ">" + lastsynctime + " AND "
                + DOCUMENTPHOTOS_UPDATEFLAG + "!='1'";
        ;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("name", cursor.getString(0));
                obj.put("uuid", cursor.getString(1));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public JSONArray getupdatecustomerstosync(boolean backup) throws JSONException {

        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }

        //		String lastupdate = getrecentsynctime();
        JSONArray temp = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = new StringBuilder().append("select * from ").append(TABLE_SCHEDULE_DETAIL).append(" where ").append(SCHEDULEDETAIL_EDITTIME).append(">").append(lastupdate).toString();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("detailid", cursor.getString(cursor
                        .getColumnIndex(SCHEDULE_DETAIL_ID)));
                obj.put("Address", cursor.getString(cursor
                        .getColumnIndex(LOCATION_ADRESS)));
                obj.put("Status", cursor.getString(cursor
                        .getColumnIndex(DETAIL_STATUS)));         //Added by yadhu on 21.11.2019
                obj.put("Phone", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_MOBILE)));
                obj.put("Gst", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_TINNUMBER)));
                obj.put("Pan", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_PAN)));
                obj.put("PhotoUUID", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_PHOTOUUID)));
//                obj.put("ImgLat", cursor.getString(cursor
//                        .getColumnIndex(SCHEDULEDETAIL_IMGLAT)));
//                obj.put("ImgLong", cursor.getString(cursor
//                        .getColumnIndex(SCHEDULEDETAIL_IMGLONG)));
                obj.put("landmark", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_LANDMARK)));
                obj.put("town", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_TOWN)));
                obj.put("contactperson", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_CONTACTPERSON)));
                obj.put("othercompany", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_OTHERCOMPANY)));
                obj.put("closingday",
                        cursor.getString(cursor.getColumnIndex(CLOSING_DAY)));
                obj.put("selectedproductcat", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_SELPRODCAT)));
                obj.put("AlternateCustCode", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_ALTERCUSTCODE)));

                obj.put("GSTType", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_GSTTYPE)));
                obj.put("FSSINumber", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_FSSI)));

                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public JSONArray getphotostosync(boolean backup) throws JSONException {

        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }

        //		String lastupdate = getrecentsynctime();
        JSONArray temp = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = new StringBuilder().append("select * from ").append(TABLE_DOCUMENTPHOTOS).append(" where ").append(DOCUMENTPHOTOS_CREATETIME).append(">").append(lastupdate).append(" and ").append(DOCUMENTPHOTOS_UPDATEFLAG).append("='1'").toString();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("type", "document");
                obj.put("slineid", cursor.getString(cursor
                        .getColumnIndex(DOCUMENTPHOTOS_CUSTID)));
                obj.put("name", cursor.getString(cursor
                        .getColumnIndex(DOCUMENTPHOTOS_DOCUMENT)));
                obj.put("catid", "");
                obj.put("uuid", cursor.getString(cursor
                        .getColumnIndex(DOCUMENTPHOTOS_UUID)));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        sql = new StringBuilder().append("select * from ").append(TABLE_CATEGORYPHOTOS).append(" where ").append(CATEGORYPHOTOS_CREATETIME).append(">").append(lastupdate).append(" and ").append(CATEGORYPHOTOS_UPDATEFLAG).append("='1'").toString();
        cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("type", "category");
                obj.put("slineid", cursor.getString(cursor
                        .getColumnIndex(CATEGORYPHOTOS_CUSTID)));
                String cat = cursor.getString(cursor
                        .getColumnIndex(CATEGORYPHOTOS_CATEGORY));
                obj.put("name", cat);
                String catid = getchecklistids(cat, db);
                obj.put("catid", catid);
                obj.put("uuid", cursor.getString(cursor
                        .getColumnIndex(CATEGORYPHOTOS_UUID)));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public String getchecklistids(String names, SQLiteDatabase db) {
        String temp = "";
        String selectQuery = "SELECT " + PRODUCTCHECKLIST_CODE + " FROM "
                + TABLE_PRODUCTCHECKLIST + " WHERE " + PRODUCTCHECKLIST_NAME
                + "='" + names + "'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            temp = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public JSONArray getpassbyupdatedsync(boolean backup) throws JSONException {

        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }

        //		String lastupdate = getrecentsynctime();
        JSONArray temp = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = new StringBuilder().append("select ").append(SCHEDULE_DETAIL_ID).append(",").append(LOCATION_ID).append(",").append(SCHEDULEDETAIL_IMGLAT).append(",").append(SCHEDULEDETAIL_IMGLONG).append(" from ").append(TABLE_SCHEDULE_DETAIL).append(" where ").append(SCHEDULEDETAIL_PASSBYCRTIME).append(">").append(lastupdate).toString();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("detailid", cursor.getString(cursor
                        .getColumnIndex(SCHEDULE_DETAIL_ID)));
                obj.put("custid",
                        cursor.getString(cursor.getColumnIndex(LOCATION_ID)));
                obj.put("ImgLat", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_IMGLAT)));
                obj.put("ImgLong", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_IMGLONG)));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    // //getupdatedlatlongs

    public JSONArray getupdatedlaatlong(boolean backup) throws JSONException {
        String lastsyc = "";
        if (backup) {
            lastsyc = "0";
        } else {
            lastsyc = getrecentsynctime();
        }
        //		String lastsyc = getrecentsynctime();

        JSONArray childs = new JSONArray();

        String selectQuery = new StringBuilder().append("SELECT ").append(LOCATION_ID).append(",").append(LATITUDE_NEW).append(",").append(LONGITUDE_NEW).append(",").append(SCHED_LOCUPDATETIME).append(" FROM ").append(TABLE_SCHEDULE_DETAIL).append(" WHERE ").append(SCHEDULEDETAIL_CREATETIME).append(" > ").append(lastsyc).toString();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                String lat = cursor.getString(1);
                obj.put("locationid", cursor.getString(0));
                obj.put("latitude", cursor.getString(1));
                obj.put("longitude", cursor.getString(2));
                obj.put("Salespersonid", getsalespersonid());
                obj.put("updatetime", cursor.getString(3));
                if (lat != null && !lat.equals("0")) {
                    childs.put(obj);
                }

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return childs;

    }


    public boolean getpendingNotApproved() throws JSONException {
        boolean temp = false;
        String lastupdate = getrecentsynctime();

        String selectQuery = "SELECT * FROM " + TABLE_PAYMENTS + " WHERE "
                + PAYMENT_CREATETIME + " > " + lastupdate + " AND "
                + DELETE_FLAG + "=0" + " AND " + PAYMENT_APROOVEFLAG
                + "!=1 ORDER BY " + PAYMENT_CREATETIME + " DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            temp = true;
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public boolean getPendingOrderPayment() throws JSONException {
        boolean temp = false;
        String lastupdate = getrecentsynctime();
        String selectQuery = "SELECT ol.id FROM " + TABLE_ORDERLINE
                + " ol join " + TABLE_ORDERHDR + " orh on orh." + ORDERHDR_PKID
                + "=ol." + ORDERLINE_HEADID + " WHERE ol."
                + ORDERLINE_CREATETIME + " > " + lastupdate + " AND ol."
                + ORDERLINE_DELETEFLAG + "=0" + " and (orh."
                + ORDERHDR_APPROVEFLAG + "= '0' or orh." + ORDERHDR_APPROVEFLAG
                + " is null)";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            temp = true;
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public boolean isWithoutInventory() {
        boolean result = false;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String count = "SELECT " + MOBILESERVICE_DISABLE_INVENTORY + " FROM " + TABLE_MOBSERVICES;
            Cursor cursor = db.rawQuery(count, null);

            if (cursor.moveToFirst()) {

                if (Integer.parseInt(cursor.getString(0)) == 1) {
                    result = true;
                }

            }
            if (cursor != null)
                cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public Boolean anypendingorderexist() {

        Boolean reuslt = false;
        SQLiteDatabase db = this.getWritableDatabase();

        String lastupdate = getrecentsynctime();

        String selectQuery = "SELECT * FROM " + TABLE_ORDER_DETAIL + " WHERE "
                + CREATETIME + ">" + lastupdate;

        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            reuslt = true;
            return reuslt;
        }
        if (cursor != null)
            cursor.close();

        return reuslt;
    }

    public boolean validateCreateTime(String createTime) {


        if (checkWithSyncTime) {

            String lastSyncTime = getrecentsynctime();

            SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");

            Date lastSync = new Date();
            Date createDateTime = new Date();
            try {
                lastSync = df.parse(lastSyncTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            try {
                createDateTime = df.parse(createTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (lastSync.before(createDateTime)) {

                long diff = createDateTime.getTime() - lastSync.getTime();
                long diffInHours = TimeUnit.MILLISECONDS.toHours(diff);

                if (diffInHours > 48) {//two days
                    return false;
                } else {
                    return true;
                }

            } else {
                return false;
            }

            //return lastSync.before(createDateTime);

        } else {

            return true;
        }

    }

    public String getcurrinvoiceval() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + INVOICENOGEN_CURRVAL + " FROM "
                + TABLE_INVOICENOGEN;

        String currval = new String();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            currval = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();

        // db.close();
        return currval;

    }

    public void updatesynctable(String synctime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SYNCTIME, synctime);
        db.update(TABLE_SYNCTABLE, values, TABLEINSYNC + "=?",
                new String[]{TABLE_ORDER_DETAIL});

    }

    // /update synctable for locations
    public void updatesynctablelocationsync(String synctime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SYNCTIME, synctime);
        db.update(TABLE_SYNCTABLE, values, TABLEINSYNC + "=?",
                new String[]{TABLE_LOCATIONS});


    }

    public void updatescheduleheaderbegintime(String begintime, String synctime) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SCHEDULE_BEGIN_TIME, begintime);
        values.put(SCHEDULE_SYNC_TIME, synctime);
        db.update(TABLE_SCHEDULE_HEADER, values, "", new String[]{});

    }

    public void intialisesynctable(String scheduleBeginTime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TABLEINSYNC, TABLE_ORDER_DETAIL);
        values.put(SYNCTIME, scheduleBeginTime);
        db.insert(TABLE_SYNCTABLE, null, values);
        locintialise(scheduleBeginTime);
        expenseSyncInitialise(scheduleBeginTime);
    }

    public void expenseSyncInitialise(String scheduleBeginTime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TABLEINSYNC, TABLE_EXPENSE);
        values.put(SYNCTIME, scheduleBeginTime);
        db.insert(TABLE_SYNCTABLE, null, values);
    }

    public void locintialise(String scheduleBeginTime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TABLEINSYNC, TABLE_LOCATIONS);
        values.put(SYNCTIME, scheduleBeginTime);
        db.insert(TABLE_SYNCTABLE, null, values);
    }

    public boolean getpendingNotApprovedOrders() throws Exception {
        boolean temp = false;
        String lastupdate = getrecentsynctime();
        String selectQuery = "SELECT ol.id FROM " + TABLE_ORDERLINE
                + " ol join " + TABLE_ORDERHDR + " orh on orh." + ORDERHDR_PKID
                + "=ol." + ORDERLINE_HEADID + " WHERE "
                //			    + "ol." + ORDERLINE_CREATETIME + " > " + lastupdate + " AND "
                + "ol." + ORDERLINE_DELETEFLAG + "=0" + " and (orh."
                + ORDERHDR_APPROVEFLAG + "!= '1'  or orh." + ORDERHDR_APPROVEFLAG
                + " is null )";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            temp = true;
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public boolean anyoperation() {
        boolean temp = false;
        if (anypayment())
            temp = true;
        if (anymarketorder())
            temp = true;
        if (anyorder())
            temp = true;

        if (simpledelivery())
            temp = true;

        return temp;
    }

    public boolean simpledelivery() {
        boolean temp = false;
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select " + SCHEDULE_DETAIL_PKID + " from "
                + TABLE_SCHEDULE_DETAIL + " where "
                + SCHEDULE_DETAIL_PRODUCTIVE + " =1 ";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            temp = true;
        }
        return temp;
    }

    public boolean isanycompleteschedule() {
        boolean temp = false;
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select " + SCHEDULE_HEADER_PKID + " from "
                + TABLE_SCHEDULE_HEADER + " where "
                + SCHEDULE_COMPLETION_STATUS + "='Completed'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            temp = true;
        }
        return temp;
    }

    public boolean anyorder() {
        boolean temp = false;
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select " + ORDERHDR_PKID + " from " + TABLE_ORDERHDR;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            temp = true;
        }
        return temp;
    }

    public boolean anymarketorder() {
        boolean temp = false;
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select " + ORDERAMOUNTDATA_PKID + " from "
                + TABLE_ORDERAMOUNTDATA;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            temp = true;
        }
        return temp;
    }

    public boolean anypayment() {
        boolean temp = false;
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select " + PAYMENT_PKID + " from " + TABLE_PAYMENTS;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            temp = true;
        }
        return temp;
    }

    // get schedule status
    public String getschedulestatus() {

        String Schedulsttatus = "";
        String get_schedulestatus = "SELECT " + SCHEDULE_COMPLETION_STATUS
                + " FROM " + TABLE_SCHEDULE_HEADER + " WHERE  "
                + SCHEDULE_COMPLETION_STATUS + " ='Completed'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(get_schedulestatus, null);
        if (cursor.moveToFirst()) {

            Schedulsttatus = cursor.getString(0);
        }

        if (Schedulsttatus == null)
            Schedulsttatus = "";
        if (cursor != null)
            cursor.close();
        return Schedulsttatus;
    }

    // /get order amount to sync
    public JSONArray getorderpaymentstosync(boolean backup) throws JSONException {

        String lastupdate = " ";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }

        //		String lastupdate = getrecentsynctime();
        JSONArray orederamountdata = new JSONArray();
        String selectQuery = "SELECT * FROM " + TABLE_ORDERAMOUNTDATA
                + " WHERE " + ORDERAMOUNTDATA_CREATETIME + " > " + lastupdate;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();

                obj.put("amount", cursor.getString(1));
                obj.put("scheduledetailid", cursor.getString(2));
                obj.put("customerid", cursor.getString(3));
                obj.put("personid", cursor.getString(4));
                obj.put("time", cursor.getString(5));
                obj.put("lattitude", cursor.getString(7));
                obj.put("longitude", cursor.getString(8));
                obj.put("guid", cursor.getString(9));
                orederamountdata.put(obj);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return orederamountdata;

    }

    public JSONArray getorderdatatosync(boolean backup) throws JSONException {

        String lastsyc = "";
        if (backup) {
            lastsyc = "0";
        } else {
            lastsyc = getrecentsynctime();
        }

        //		String lastsyc = getrecentsynctime();
        JSONArray orderdata = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT oln.*,hrd." + ORDERHDR_SCHLINEID + ", hrd."
                + ORDERHDR_CUSTOMER + ", hrd." + ORDERHDR_LATITUDE + ", hrd."
                + ORDERHDR_LONGITUDE + " , hrd." + ORDERHDR_NOTE + " FROM "
                + TABLE_ORDERLINE + " oln JOIN " + TABLE_ORDERHDR
                + " hrd on hrd." + ORDERHDR_PKID + "=oln." + ORDERLINE_HEADID
                + " JOIN " + TABLE_SCHEDULE_DETAIL + " sln on hrd."
                + ORDERHDR_SCHLINEID + "=sln." + SCHEDULE_DETAIL_ID
                + " WHERE oln." + ORDERLINE_CREATETIME + ">'" + lastsyc
                + "' AND " + ORDERHDR_APPROVEFLAG + "='1'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject tempObj = new JSONObject();
                tempObj.put(
                        "schedulelineid",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_SCHLINEID)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_SCHLINEID)));
                tempObj.put(
                        "product",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_PRODID)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_PRODID)));
                tempObj.put(
                        "qty",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_QTY)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_QTY)));
                tempObj.put(
                        "mrp",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_MRP)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_MRP)));
                tempObj.put(
                        "rate",
                        cursor.getString(cursor.getColumnIndex(ORDERLINE_RATE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_RATE)));
                tempObj.put(
                        "deleteflag",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_DELETEFLAG)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_DELETEFLAG)));
                tempObj.put(
                        "createtime",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_CREATETIME)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERLINE_CREATETIME)));
                tempObj.put(
                        "latitude",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_LATITUDE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_LATITUDE)));
                tempObj.put(
                        "longitude",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_LONGITUDE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_LONGITUDE)));
                tempObj.put(
                        "customer",
                        cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_CUSTOMER)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_CUSTOMER)));

                String product = cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PRODID)) == null ? ""
                        : cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PRODID));
                String partymarg = cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PM)) == null ? "" : cursor
                        .getString(cursor.getColumnIndex(ORDERLINE_PM));
                /*String upc = cursor.getString(cursor
						.getColumnIndex(ORDERLINE_PRODID)) == null ? ""
								: cursor.getString(cursor.getColumnIndex(ORDERLINE_UPC));
				 	tempObj.put("uom", upc);
				double valupc = getupcofprod(db, product, upc);
				tempObj.put("upc", String.valueOf(valupc));
				 */
                String packagetype = cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PACKAGE_TYPE)) == null ? ""
                        : cursor.getString(cursor.getColumnIndex(ORDERLINE_PACKAGE_TYPE));

                tempObj.put("PackageType", packagetype);
                tempObj.put("pm", partymarg.contentEquals("") ? "0" : partymarg);
                String batch = cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_BATCH)) == null ? "" : cursor
                        .getString(cursor.getColumnIndex(ORDERLINE_BATCH));
                tempObj.put("batch", batch);
                tempObj.put(
                        "note",
                        cursor.getString(cursor.getColumnIndex(ORDERHDR_NOTE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(ORDERHDR_NOTE)));

                orderdata.put(tempObj);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return orderdata;
    }

    public JSONArray getpaymentstosync(boolean backup) throws JSONException {
        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }
        //		String lastupdate = getrecentsynctime();
        JSONArray paymentlist = new JSONArray();
        boolean enabledigital_signature = isserviceexist("DigitalSignature");
        JSONArray fileData = new JSONArray();
        HashSet<String> unique_fileid = new HashSet<String>();

        String selectQuery = "SELECT * FROM " + TABLE_PAYMENTS + " WHERE "
                + PAYMENT_CREATETIME + " >" + lastupdate + " AND "
                + DELETE_FLAG + "=0" + " AND " + PAYMENT_APROOVEFLAG
                + "=1 ORDER BY " + PAYMENT_CREATETIME + " DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                String paymentid = cursor.getString(0);
                String schduleline_id = cursor.getString(1);
                obj.put("scheduledetailid", schduleline_id);
                obj.put("payentmethode", cursor.getString(17));// 17 for pdc/cdc
                obj.put("amount", cursor.getString(3));
                obj.put("chequenumber", cursor.getString(4));
                obj.put("ifsc", cursor.getString(5));
                obj.put("deleteflag", cursor.getString(7));
                obj.put("paymentid", cursor.getString(8));
                obj.put("salespersonid", cursor.getString(9));
                obj.put("locationid", cursor.getString(10));
                obj.put("chequedate", cursor.getString(11));
                obj.put("paymentdate", cursor.getString(12));
                obj.put("invoicenumber", cursor.getString(13));
                obj.put("picklistnumber", cursor.getString(14));
                obj.put("latitude", cursor.getString(15));
                obj.put("longitude", cursor.getString(16));
                obj.put("banknamecode", cursor.getString(18));
                obj.put("verificationcode", cursor.getString(22));
                if (enabledigital_signature) {
                    obj.put("signingperson", cursor.getString(20));
                    String fileid = cursor.getString(21);
                    obj.put("signaturefileid", fileid);

                    if (!unique_fileid.contains(fileid)) {
                        JSONObject imageobject = new JSONObject();
                        imageobject.put("signmappingid", fileid);
                        imageobject.put("signmappingimage", readFromFile(fileid
                                + "_signature.png"));
                        fileData.put(imageobject);
                        unique_fileid.add(fileid);
                    }
                }

                obj.put("invoicedata", getinvoicedatatosync(paymentid));
                paymentlist.put(obj);

            } while (cursor.moveToNext());

            if (paymentlist.length() > 0 && enabledigital_signature) {
                paymentlist.getJSONObject(0).put("signaturedata", fileData);
            }

        }

        return paymentlist;

    }

    // getting invoicedata on sync

    public JSONArray getinvoicedatatosync(String paymentheaderid)
            throws JSONException {

        JSONArray allinvoicedata = new JSONArray();

        String selectQuery = "SELECT " + INVOICEPAYMENT_INVOICENO + ","
                + INVOICEPAYMENT_AMOUNTPAYED + "," + INVOICEPAYMENT_BALANCE + "," + INVOICEPAYMENT_DELETEFLAG
                + " FROM " + TABLE_INVOICE_PAYMENTS + " WHERE "
                + INVOICEPAYMENT_HEADERID + "='" + paymentheaderid + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {

                String status = "";

                if (cursor.getString(3).equals("1")) {
                    status = "Cancelled";
                }

                JSONObject invoiceobject = new JSONObject();
                invoiceobject.put("invoiceid", cursor.getString(0));
                invoiceobject.put("amount", cursor.getString(1));
                invoiceobject.put("balance", cursor.getString(2));
                invoiceobject.put("Status", status);
                allinvoicedata.put(invoiceobject);
            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return allinvoicedata;

    }

    public boolean isserviceexist(String servcname) {
        Boolean result = false;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String count = "SELECT * FROM " + TABLE_MOBSERVICES + " WHERE "
                    + MOBILESERVICE_NAME + "='" + servcname + "'";
            Cursor cursor = db.rawQuery(count, null);
            if (cursor.moveToFirst()) {
                result = true;
            }
            if (cursor != null)
                cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    public JSONArray getsalesreturndatatosync(boolean backup) throws JSONException {

        String lastsyc = "";
        if (backup) {
            lastsyc = "0";
        } else {
            lastsyc = getrecentsynctime();
        }
        //		String lastsyc = getrecentsynctime();
        JSONArray orderdata = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT srn.*,hrd." + SALESRETURN_SCHLINEID + " FROM "
                + TABLE_SALERTNLINE + " srn JOIN " + TABLE_SALESRETURN
                + " hrd on hrd." + SALESRETURN_PKID + "=srn."
                + SALERTNLINE_HEADID + " JOIN " + TABLE_SCHEDULE_DETAIL
                + " sln on hrd." + SALESRETURN_SCHLINEID + "=sln."
                + SCHEDULE_DETAIL_ID + " WHERE srn." + SALERTNLINE_CREATETIME
                + ">'" + lastsyc + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject tempObj = new JSONObject();
                tempObj.put(
                        "schedulelineid",
                        cursor.getString(cursor
                                .getColumnIndex(SALESRETURN_SCHLINEID)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALESRETURN_SCHLINEID)));
                tempObj.put(
                        "product",
                        cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_PRODID)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_PRODID)));
                tempObj.put(
                        "qty",
                        cursor.getString(cursor.getColumnIndex(SALERTNLINE_QTY)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_QTY)));
                tempObj.put(
                        "mrp",
                        cursor.getString(cursor.getColumnIndex(SALERTNLINE_MRP)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_MRP)));
                tempObj.put(
                        "invoice",
                        cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_INVOICE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_INVOICE)));
                tempObj.put(
                        "deleteflag",
                        cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_DELETEFLAG)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_DELETEFLAG)));
                tempObj.put(
                        "createtime",
                        cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_CREATETIME)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_CREATETIME)));
                orderdata.put(tempObj);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return orderdata;
    }

    public JSONArray getcreditnotestosync(boolean backup) throws JSONException {

        String lastsyc = "";
        if (backup) {
            lastsyc = "0";
        } else {
            lastsyc = getrecentsynctime();
        }

        //		String lastsyc = getrecentsynctime();
        JSONArray orderdata = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_CREDITNOTES + " WHERE "
                + CREDITNOTE_CREATEDTIME + ">'" + lastsyc + "'";

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject tempObj = new JSONObject();
                tempObj.put(
                        "schedulelineid",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_SCHDLELINEID)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_SCHDLELINEID)));
                tempObj.put(
                        "amount",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_AMOUNT)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_AMOUNT)));
                tempObj.put(
                        "reason",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_REASON)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_REASON)));
                tempObj.put(
                        "description",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_DESCRIPTION)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_DESCRIPTION)));
                tempObj.put(
                        "deleteflag",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_DELETEFLAG)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_DELETEFLAG)));
                tempObj.put(
                        "createtime",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_CREATEDTIME)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_CREATEDTIME)));
                orderdata.put(tempObj);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return orderdata;
    }

    public JSONArray getcratestosync(boolean backup) throws JSONException {

        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }

        //		String lastupdate = getrecentsynctime();
        JSONArray temp = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = new StringBuilder().append("select * from ").append(TABLE_CRATES).append(" where ").append(CRATES_CRTIMESYNC).append(">").append(lastupdate).toString();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("detailid",
                        cursor.getString(cursor.getColumnIndex(CRATES_SLINEID)));
                obj.put("inval",
                        cursor.getString(cursor.getColumnIndex(CRATES_IN)));
                obj.put("outval",
                        cursor.getString(cursor.getColumnIndex(CRATES_OUT)));
                obj.put("crtime",
                        cursor.getString(cursor.getColumnIndex(CRATES_CRTIME)));
                obj.put("uuid",
                        cursor.getString(cursor.getColumnIndex(CREATES_UUID)));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    // /geting order data for upload
    public JSONArray getorders(boolean backup) throws JSONException {
        String lastupdate = "";
        if (backup) {
            lastupdate = "0";
        } else {
            lastupdate = getrecentsynctime();
        }

        //		String lastupdate = getrecentsynctime();
        JSONArray orders = new JSONArray();
        String selectQuery = "SELECT * FROM " + TABLE_ORDER_HEADER + " WHERE "
                + ORDERSUBMIT_TIME + " IS NOT NULL ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                String orderheadid = cursor.getString(0);
                obj.put("orderbegintime", cursor.getString(1));
                obj.put("ordersubmittime", cursor.getString(2));
                obj.put("scheduledetailid", cursor.getString(3));
                obj.put("latitude", cursor.getString(5));
                obj.put("longitude", cursor.getString(6));
                obj.put("salespersonid", cursor.getString(7));
                obj.put("routenetworkid", cursor.getString(8));
                obj.put("locationid", cursor.getString(9));
                obj.put("products", getorderdetails(orderheadid, lastupdate));
                orders.put(obj);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return orders;

    }


    public JSONArray getorderdetails(String orderhead, String lastupdate) throws JSONException {

        JSONArray orderdetail = new JSONArray();
        String selectQuery = "SELECT * FROM " + TABLE_ORDER_DETAIL + " WHERE "
                + ORDER_HEAD_ID + "=" + orderhead + " AND " + CREATETIME + ">"
                + lastupdate;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();

                obj.put("productid", cursor.getString(2));
                obj.put("quantity", cursor.getString(3));
                obj.put("deliverydate", cursor.getString(4));
                obj.put("DeleteFlag", cursor.getString(6));
                obj.put("rate", cursor.getString(7));
                obj.put("PackageType", cursor.getString(cursor
                        .getColumnIndex(ORDERLINE_PACKAGE_TYPE)));
                String product = cursor.getString(2);
				/*	String upc = cursor.getString(cursor
						.getColumnIndex(ORDERLINE_UPC));
				obj.put("uom", upc);
				double valupc = getupcofprod(db, product, upc);
				obj.put("upc", String.valueOf(valupc));*/
                orderdetail.put(obj);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return orderdetail;
    }

    public String GetCurrentScheduledate() {
        boolean temp = false;
        String schdate = "";

        try {
            String sql = "select " + SCHEDULE_DATE + " from "
                    + TABLE_SCHEDULE_HEADER;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                do {
                    schdate = cursor.getString(0);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return schdate;
    }

    public JSONArray getExpenseDataForSync(boolean backup) {


        String expenseDt = "";
        JSONArray expensesArray = new JSONArray();


        try {

            HashMap<String, FourStrings> expHashMap = getExpensesForSync(backup);

            if (expHashMap.size() > 0) {

                JSONObject expenses = new JSONObject();
                expenses.put("PaymentMode", "Cash Payment");

                String scheduleId = getactualscheduleid();
                expenses.put("scheduleId", scheduleId);
                JSONArray expenseDtls = new JSONArray();

                for (Map.Entry<String, FourStrings> entry : expHashMap.entrySet()) {

                    String expenseId = entry.getKey();
                    FourStrings value = entry.getValue();

                    JSONObject expObject = new JSONObject()
                            .put("amount", value.mrp)
                            .put("expenseTypeId", value.qty)
                            .put("description", value.rate);
					/*if (value.upc == null) {

					}*/
                    expenseDt = value.taxid;
                    expenseDtls.put(expObject);
                }

                expenses.put("expenseDtls", expenseDtls).put("expenseDate", expenseDt);
                expensesArray.put(expenses);
            }

        } catch (JSONException e) {

            e.printStackTrace();
        }
        return expensesArray;

    }


    public HashMap<String, FourStrings> getExpensesForSync(boolean backup) {


        String lastsyc = "";
        if (backup) {
            lastsyc = "0";
        } else {
            lastsyc = getrecentsynctime();
        }

        HashMap<String, FourStrings> temp = new HashMap<>();
        SQLiteDatabase db = this.getWritableDatabase();

        //		String lastsyc = getrecentsynctime();
        String sql = "select e." + EXPENSE_CREATETIME + ",e." + EXPENSE_EXPENSETYPE + ",e." + EXPENSE_AMOUNT + ",e." +
                EXPENSE_DESCRIPTION + ",e." + EXPENSE_PKID + ",et." + EXPENSETYPE_NAME + " from " + TABLE_EXPENSE + " e " +
                "JOIN " + TABLE_EXPENSETYPE + " et ON et." + EXPENSETYPE_ID + " = e." + EXPENSE_EXPENSETYPE
                + " WHERE e." + EXPENSE_CREATETIME + ">'" + lastsyc + "'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                String c = cursor.getString(cursor.getColumnIndex(EXPENSE_CREATETIME));
                FourStrings strings =
                        new FourStrings(cursor.getString(cursor.getColumnIndex(EXPENSE_EXPENSETYPE)),
                                cursor.getString(cursor.getColumnIndex(EXPENSE_AMOUNT)),
                                cursor.getString(cursor.getColumnIndex(EXPENSE_DESCRIPTION)) == null ? ""
                                        : cursor.getString(cursor.getColumnIndex(EXPENSE_DESCRIPTION)),
                                //	cursor.getString(cursor.getColumnIndex(EXPENSE_DESCRIPTION)),
                                cursor.getString(cursor.getColumnIndex(EXPENSETYPE_NAME)),
                                cursor.getString(cursor.getColumnIndex(EXPENSE_CREATETIME)) == null ? ""
                                        : cursor.getString(cursor.getColumnIndex(EXPENSE_CREATETIME)), "", "");
                temp.put(cursor.getString(cursor.getColumnIndex(EXPENSE_PKID)), strings);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;

    }

    public boolean orderDataExist() {

        boolean result = false;
        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_ORDERLINE;

        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            result = true;
            return result;
        }
        if (cursor != null)
            cursor.close();

        return result;
    }

    public JSONArray getscheduledata(String text, String id) throws Exception {

        JSONArray group = new JSONArray();
        String selectQuery = "SELECT * FROM " + TABLE_SCHEDULE_HEADER + " where (" + SCHEDULE_NAME + "='" + id + "' OR 'SELECT'='" + id + "')";
        //String selectQuery = "SELECT * FROM " + TABLE_SCHEDULE_HEADER ;//+" where ("+SCHEDULE_NAME+"='"+id+"' OR 'SELECT'='"+id+"')";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                String schedulepkid = cursor.getString(0);
                obj.put("schedulepkid", schedulepkid);
                obj.put("scheduleid", cursor.getString(1));
                obj.put("schedulename", cursor.getString(2));
                obj.put("salespersonid", cursor.getString(3));
                obj.put("salespersonname", cursor.getString(4));
                obj.put("salesrouteid", cursor.getString(5));
                obj.put("scheduledate", cursor.getString(6));
                obj.put("headerstatus", cursor.getString(7));
                obj.put("routenetworkid", cursor.getString(8));
                obj.put("schedulecopletionstatus", cursor.getString(9));
                obj.put("coustemerlist", getchid(schedulepkid, text));
                group.put(obj);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return group;
    }


    public JSONArray getchid(String schedulepkid, String text) throws Exception {
        text = text.replaceAll("'", "\'");
        JSONArray childs = new JSONArray();
        String selectQuery = "SELECT sl.*,count(pt." + PAYMENT_PKID
                + ") as Approve,count(oln." + ORDERLINE_PKID
                + ") as ApproveOrder FROM " + TABLE_SCHEDULE_DETAIL
                + " sl left join " + TABLE_PAYMENTS + " pt on pt."
                + PAYMENT_SCHEDULEDETAILID + "=sl." + SCHEDULE_DETAIL_ID
                //+" and pt."+PAYMENT_STATUS+" != 'Cancelled' "
                + " and pt." + PAYMENT_APROOVEFLAG + "=0 and pt." + DELETE_FLAG

                + "=0  left join " + TABLE_ORDERHDR + " ot on ot."
                + ORDERHDR_SCHLINEID + "=sl." + SCHEDULE_DETAIL_ID + " and ( ot."
                + ORDERHDR_APPROVEFLAG + " is null OR " + ORDERHDR_APPROVEFLAG + " = '0' )  left join "
                + TABLE_ORDERLINE + " oln on oln."
                + ORDERLINE_HEADID
                + "=ot."
                + ORDERHDR_PKID
                // NA Changes
                + " and oln." + ORDERLINE_DELETEFLAG + "=0 "

                + " WHERE " + SCHEDULE_HEADER_ID
                + "="
                + schedulepkid

                // + " AND oln." + ORDERLINE_DELETEFLAG + "=0"

                + " AND (" + LOCATION_NAME + " like '%" + text + "%' OR "
                + SCHED_CUSTOMERCODE + " LIKE '%" + text + "%' )"
                + " group by sl." + SCHEDULE_DETAIL_ID + " ORDER BY " + SEQNO
                + "* 1 ASC ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("scheduledetailpkid", cursor.getString(0));
                obj.put("scheduledetailid", cursor.getString(1));
                obj.put("scheduleheaderid", cursor.getString(2));
                obj.put("locationid", cursor.getString(3));
                obj.put("locationname", cursor.getString(4));
                obj.put("locationadress", cursor.getString(5));
                obj.put("seqno", cursor.getString(6));
                obj.put("status", cursor.getString(7));
                obj.put("latitude", cursor.getString(8));
                obj.put("longitude", cursor.getString(9));
                obj.put("Customercategory", cursor.getString(13));
                obj.put("customercode", cursor.getString(cursor
                        .getColumnIndex(SCHED_CUSTOMERCODE)));
                obj.put("otpverified", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_OTPVERIFIED)));
                obj.put("locationlock", cursor.getString(24));
                obj.put("Approve",
                        cursor.getString(cursor.getColumnIndex("Approve")));
                obj.put("ApproveOrder",
                        cursor.getString(cursor.getColumnIndex("ApproveOrder")));

                childs.put(obj);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return childs;
    }


    public HashMap<String, String> getCustInvoice() {
        HashMap<String, String> temp = new HashMap<>();
        String query = "Select " + INVOICE_CUSTOMERID +
                ", " + INVOICE_NO + ", " + INVOICE_TOTTALAMOUNT + " from "
                + TABLE_INVOICE + " where " + INVOICE_STATUS + " != 'Cancelled' group by " + INVOICE_CUSTOMERID;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp.put(cursor.getString(0), "Invoice: " + cursor.getString(1)
                        + ", Amount: " + cursor.getDouble(2));
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }


    public JSONArray getalllatlongs() throws JSONException {
        String lastupdate = getrecentsynctime();
        SQLiteDatabase db = this.getWritableDatabase();
        String id = largetVistedSeq(db);
        JSONArray alldata = new JSONArray();

        //TODO verifychange
        //commented sajin for getting all data
        /*String selectQuery = "SELECT * FROM " + TABLE_SCHEDULE_DETAIL +" where "+
                //" case when "+SCHEDULEDETAIL_EDITTIME+">0 then "+SCHEDULEDETAIL_EDITTIME+">"+lastupdate+" else "+LOCATION_CREATETIME+">"+lastsynctime+" end";
                SCHEDULEDETAIL_UPDATETIME + ">" + lastupdate ; //+ " or "+ LOCATION_CREATETIME + ">" + lastsynctime;*/

        //get all data
        String selectQuery = "";
        boolean select_all=Global.schedule.equals("") || Global.schedule.equals("SELECT");

        if (select_all) {
            selectQuery = "SELECT SD.*,IFNULL(TC." + CHECKLISTINPUT_CODE +",0) AS listcount  FROM " + TABLE_SCHEDULE_DETAIL + " SD LEFT JOIN "+  TABLE_CHECKLISTINPUT +" TC on TC."+CHECKLISTINPUT_CUSTNAME+"=SD."+SCHEDULE_DETAIL_ID +" GROUP BY SD." + SCHEDULE_DETAIL_ID;
        } else {
            selectQuery = "SELECT SD." + SCHEDULE_DETAIL_PKID + ",SD." + DETAIL_STATUS + ",SD." + LATITUDE + ",SD." + LONGITUDE + ", IFNULL(TC." + CHECKLISTINPUT_CODE +",0) AS listcount ,SD."
                    + LOCATION_NAME + ",SD." + LOCATION_ADRESS + ",SD." + SEQNO + ",SD." + CUSTOMERCATOGORY +",SD." + SCHEDULE_DETAIL_VISITED + ",SD. " + SCHEDULE_HEADER_ID + " ,SD. "
                    + SCHEDULE_DETAIL_ID + " ,SD." + STOREVISIT_REASON + " FROM " + TABLE_SCHEDULE_HEADER + " SH JOIN " + TABLE_SCHEDULE_DETAIL + " SD on SD. " + SCHEDULE_HEADER_ID + " = SH."
                    + SCHEDULE_HEADER_PKID + " LEFT JOIN "+  TABLE_CHECKLISTINPUT +" TC on TC."+CHECKLISTINPUT_CUSTNAME+"=SD."+SCHEDULE_DETAIL_ID +" WHERE SH." + SCHEDULE_ID + "="+ Global.schedule
                    +" GROUP BY SD." + SCHEDULE_DETAIL_ID ;

        }


        // +" where "+
        //" case when "+SCHEDULEDETAIL_EDITTIME+">0 then "+SCHEDULEDETAIL_EDITTIME+">"+lastupdate+" else "+LOCATION_CREATETIME+">"+lastsynctime+" end";
        //SCHEDULEDETAIL_UPDATETIME + ">" + lastupdate ; //+ " or "+ LOCATION_CREATETIME + ">" + lastsynctime;


        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                JSONObject latlongdata = new JSONObject();
                // if (cursor.getString(cursor.getColumnIndex(SCHEDULE_DETAIL_PKID)).contentEquals(id))
                //    latlongdata.put("status", "Next");
                // else
                latlongdata.put("status", cursor.getString(cursor.getColumnIndex(DETAIL_STATUS)));
                latlongdata.put("latitude_current", cursor.getString(cursor.getColumnIndex(LATITUDE)));
                latlongdata.put("longitude_current", cursor.getString(cursor.getColumnIndex(LONGITUDE)));
                latlongdata.put("storename", cursor.getString(cursor.getColumnIndex(LOCATION_NAME)));
                latlongdata.put("adress", cursor.getString(cursor.getColumnIndex(LOCATION_ADRESS)));
                latlongdata.put("seq", cursor.getString(cursor.getColumnIndex(SEQNO)));
                latlongdata.put("provider", "gps");
                latlongdata.put("cat", cursor.getString(cursor.getColumnIndex(CUSTOMERCATOGORY)));
                latlongdata.put("scheduleid",cursor.getString(cursor.getColumnIndex(SCHEDULE_DETAIL_ID)));
                latlongdata.put("visited",cursor.getString(cursor.getColumnIndex("listcount")));
                alldata.put(latlongdata);
            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();

        return alldata;

    }

    private String largetVistedSeq(SQLiteDatabase db) {
        String id = "0";
        String sql = "select " + SCHEDULE_DETAIL_PKID + " from "
                + TABLE_SCHEDULE_DETAIL + " where " + SEQNO + "*1>(select "
                + SEQNO + " from " + TABLE_SCHEDULE_DETAIL + " where "
                + DETAIL_STATUS + "='Completed' order by " + SEQNO
                + "*1 desc limit 1)*1 order by " + SEQNO + "*1 asc limit 1";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            id = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        if (id.contentEquals("0")) {
            sql = "select " + SCHEDULE_DETAIL_PKID + " from "
                    + TABLE_SCHEDULE_DETAIL + " order by " + SEQNO
                    + "*1 limit 1";
            Cursor cursor1 = db.rawQuery(sql, null);
            if (cursor1.moveToFirst()) {
                id = cursor1.getString(0);
            }
            if (cursor1 != null)
                cursor1.close();
        }
        return id;
    }

    // schedule begin status
    public String getschedulebegintime() {

        String Schedulsttatus = "";
        String get_schedulestatus = "SELECT " + SCHEDULE_BEGIN_TIME + " FROM "
                + TABLE_SCHEDULE_HEADER + " WHERE  " + SCHEDULE_BEGIN_TIME
                + " IS NOT NULL";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(get_schedulestatus, null);
        if (cursor.moveToFirst()) {

            Schedulsttatus = cursor.getString(0);
        }

        if (Schedulsttatus == null)
            Schedulsttatus = "";
        if (cursor != null)
            cursor.close();
        return Schedulsttatus;
    }


    // //adding single location
    public void addnewlocation(Locations loc) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LOCATIONID, loc.Locationid);
        values.put(NAME, loc.Locationname);
        values.put(ADDRESS, loc.Locationadress);
        values.put(LOCATION_LATITUDE, loc.Latitude);
        values.put(LOCATION_LONGITUDE, loc.Longitude);
        values.put(LOCATION_CREATETIME, loc.createtime);
        values.put(CUSTOMER_CODE, loc.costomercode);
        values.put(CUSTOMER_CATOGORY, loc.customercategory);
        //values.put(TIN_NUMBER, loc.tinnumber);
        values.put(NEAREST_LANDMARK, loc.nearestlandmarkvalue);
        values.put(CONTACT_PERSONNAME, loc.contactpersonvalue);
        values.put(CONTACT_PERSON_NUMBER, loc.Contactpersonnumbervalue);
        values.put(ANT_OTHERCOMPANYVALUE, loc.anyothercompanyvalue);
        values.put(TOWNCLASS, loc.townclassvalue);
        values.put(SELECTED_PRODUCT_CATEGORIES, loc.selectedproductcategories);
        values.put(CUSTOMERCATEGOY_CODE, loc.customercatogorycode);
        values.put(CUSTOMER_SCHEDULELINE_GUID, loc.schdulineguid);
        values.put(LOCATION_PAN, loc.pan);
        values.put(LOCATION_OTPVERIFIED, loc.otpverified);
        values.put(LOCATION_LOCPROVIDER, loc.locprovider);
        values.put(LOCATION_LOCACCURACY, loc.locaccuracy);
        values.put(LOCATION_EMAIL, loc.email);
        values.put(LOCATION_PHOTOUUID, loc.photouuid);
        values.put(LOCATION_DIRECTCOV, loc.directcov);
        values.put(LOCATION_CLOSING_DAY, loc.closingday);
        values.put(LOCATION_STOREISCLOSED, loc.storeclosed);
        values.put(LOCATION_PNGCOVERED, loc.pngcovered);
        values.put(LOCATION_BRANCH, loc.branch);

        values.put(LOCATION_DRUG, loc.drugLicienceNo);
        values.put(LOCATION_LOCALITY, loc.locality);
        values.put(LOCATION_CITY, loc.city);
        values.put(LOCATION_STATE, loc.state);
        values.put(LOCATION_PIN, loc.pin);
        values.put(LOCATION_COVDAY, loc.coverageDay);
        values.put(LOCATION_WEEK1, loc.week1);
        values.put(LOCATION_WEEK2, loc.week2);
        values.put(LOCATION_WEEK3, loc.week3);
        values.put(LOCATION_WEEK4, loc.week4);
        values.put(LOCATION_VISTFRQ, loc.visitFrequency);
        values.put(LOCATION_WHOLSAL, loc.wholeSale);
        values.put(LOCATION_METRO, loc.metro);
        values.put(LOCATION_CLASSIF, loc.classification);
        values.put(LOCATION_TYPE, loc.type);

        values.put(LOCATION_LATITUDEPHOTO, loc.latitudePhoto);
        values.put(LOCATION_LONGITUDEPHOTO, loc.longitudePhoto);

        values.put(LOCATION_REMARKS, loc.remarks);
        values.put(LOCATION_MARKETNAME, loc.marketname);

        values.put(LOCATION_STARTTIME, loc.startTime);
        values.put(LOCATION_ENDTIME, loc.endTime);

        values.put(SCHEDULEDETAIL_CITY, loc.city);
        values.put(SCHEDULEDETAIL_PIN, loc.pin);
        values.put(LOCATION_OTPSUBTIME, loc.otpsubtime);

        values.put(LOCATION_OTPVERIFIED, loc.otpverified);
        values.put(LOCATION_OTPLAT, loc.otplat);
        values.put(LOCATION_OTPLONG, loc.otplong);

        values.put(LOCATION_FIXTIME, loc.fixtime);

        db.insert(TABLE_LOCATIONS, null, values);

    }


    // new locationscheduleexist
    public String isnewlocationsexist() throws JSONException {

        String result = "0";

        String selectQuery = "SELECT * FROM " + TABLE_SCHEDULE_HEADER
                + " WHERE " + SCHEDULE_NAME + "=" + "'New Locations'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            result = cursor.getString(0);

        }
        if (cursor != null)
            cursor.close();
        return result;
    }


    public String getcustomercategoriesId(String string) {
        String temp = "";
        string = string.replaceAll("'", "\'");
        String selectQuery = "SELECT " + CUSTOMERCATEGORIES_CODE + " FROM "
                + TABLE_CUSTOMERCATEGORIES + " WHERE "
                + CUSTOMERCATEGORIES_NAME + "= '" + string + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public void firstlocationadd(Scheduledata header, Scheduledetail dettail, String crtime) {


        Long key;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SCHEDULE_ID, header.Scheduleid);
        values.put(SCHEDULE_NAME, header.Schedulename);
        values.put(SALESPERSON_ID, header.Salespersonid);
        values.put(SALESPERSON_NAME, header.salespersonname);
        values.put(SALEROUTE_ID, header.Salesrouteid);
        values.put(SCHEDULE_DATE, header.Scheduledate);
        values.put(HEADER_STATUS, header.Headerstatus);
        values.put(ROUTENETWORK_ID, header.Routenetworkid);
        values.put(SCHEDULE_BEGIN_TIME, header.begintime);
        key = db.insert(TABLE_SCHEDULE_HEADER, null, values);

        ContentValues values2 = new ContentValues();
        values2.put(SCHEDULE_DETAIL_ID, dettail.Scheduledetailid);
        values2.put(SCHEDULE_HEADER_ID, String.valueOf(key));
        values2.put(LOCATION_ID, dettail.locationid);
        values2.put(LOCATION_NAME, dettail.locationname);
        values2.put(LOCATION_ADRESS, dettail.locationadress);
        values2.put(SEQNO, dettail.sequencenumber);
        values2.put(DETAIL_STATUS, dettail.status);
        values2.put(LATITUDE, dettail.latitude);
        values2.put(LONGITUDE, dettail.longitude);
        values2.put(LATITUDE_NEW, "0");
        values2.put(LONGITUDE_NEW, "0");
        values2.put(SCHEDULEDETAIL_CREATETIME, "0");
        values2.put(CLOSING_DAY_UPDATETIME, "0");
        values2.put(CUSTOMER_CATOGORY, dettail.Customercatogory);
        values2.put(CUSTOMERCATOGORYID, dettail.CustomercatogoryId);
        values2.put(INVOCENUMBER, dettail.invoicenumber);
        values2.put(PICKLISTNUMBER, dettail.picklistnumber);
        values2.put(SCHED_CUSTOMERCODE, dettail.customercode);
        values2.put(LOCATIONLOCK, dettail.locationlock);
        values2.put(CLOSING_DAY, dettail.Closingday);
        values2.put(SCHEDULEDETAIL_MOBILE, dettail.mobilenumber);
        //values2.put(SCHEDULEDETAIL_TINNUMBER, dettail.tinnumber);
        values2.put(SCHEDULEDETAIL_PAN, dettail.pan);
        values2.put(SCHEDULEDETAIL_LANDMARK, dettail.landmark);
        values2.put(SCHEDULEDETAIL_TOWN, dettail.town);
        values2.put(SCHEDULEDETAIL_CONTACTPERSON, dettail.contactperson);
        values2.put(SCHEDULEDETAIL_OTHERCOMPANY, dettail.othercompany);
        values2.put(SCHEDULEDETAIL_OTPVERIFIED, dettail.otpverified);
        values2.put(SCHEDULEDETAIL_LOCPROVIDER, dettail.locprovider);
        values2.put(SCHEDULEDETAIL_LOCACCURACY, dettail.locaccuracy);
        values2.put(SCHEDULEDETAIL_PHOTOUUID, dettail.photoUUID);
        values2.put(SCHEDULEDETAIL_EMAIL, dettail.email);
        values2.put(SCHEDULEDETAIL_DIRECTCOV, dettail.directcov);
        values2.put(SCHEDULEDETAIL_SELPRODCAT,
                dettail.selectedproductcategories);
        values2.put(CLOSING_DAY, dettail.Closingday);
        values2.put(SCHEDULEDETAIL_STORECLOSED, dettail.storeclosed);
        values2.put(SCHEDULEDETAIL_PNGCOVERED, dettail.pngcovered);
        values2.put(SCHEDULEDETAIL_BRANCH, dettail.branch);
        values2.put(SCHEDULEDETAIL_CUSTTYPE, dettail.custtype);
        values2.put(SCHEDULEDETAIL_CITY, dettail.city);
        values2.put(SCHEDULEDETAIL_PIN, dettail.pin);

        values2.put(SCHEDULEDETAIL_OTPSUBTIME, dettail.otpsubtime);

        values2.put(SCHEDULEDETAIL_OTPLAT, dettail.otplat);
        values2.put(SCHEDULEDETAIL_OTPLONG, dettail.otplong);
        values2.put(SCHEDULEDETAIL_UPDATETIME, crtime);

        db.insert(TABLE_SCHEDULE_DETAIL, null, values2);


    }

    public void addtoexistingnewlocations(String scheduleheadid,
                                          Scheduledetail dettail, String crtime) {


        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values2 = new ContentValues();
        values2.put(SCHEDULE_DETAIL_ID, dettail.Scheduledetailid);
        values2.put(SCHEDULE_HEADER_ID, scheduleheadid);
        values2.put(LOCATION_ID, dettail.locationid);
        values2.put(LOCATION_NAME, dettail.locationname);
        values2.put(LOCATION_ADRESS, dettail.locationadress);
        values2.put(SEQNO, dettail.sequencenumber);
        values2.put(DETAIL_STATUS, dettail.status);
        values2.put(LATITUDE, dettail.latitude);
        values2.put(LONGITUDE, dettail.longitude);
        values2.put(LATITUDE_NEW, "0");
        values2.put(LONGITUDE_NEW, "0");
        values2.put(SCHEDULEDETAIL_CREATETIME, "0");
        values2.put(CLOSING_DAY_UPDATETIME, "0");
        values2.put(CUSTOMER_CATOGORY, dettail.Customercatogory);
        values2.put(CUSTOMERCATOGORYID, dettail.CustomercatogoryId);
        values2.put(INVOCENUMBER, dettail.invoicenumber);
        values2.put(PICKLISTNUMBER, dettail.picklistnumber);
        values2.put(SCHED_CUSTOMERCODE, dettail.customercode);
        values2.put(LOCATIONLOCK, dettail.locationlock);
        values2.put(CLOSING_DAY, dettail.Closingday);
        values2.put(SCHEDULEDETAIL_MOBILE, dettail.mobilenumber);
        values2.put(SCHEDULEDETAIL_TINNUMBER, dettail.tinnumber);
        values2.put(SCHEDULEDETAIL_PAN, dettail.pan);
        values2.put(SCHEDULEDETAIL_LANDMARK, dettail.landmark);
        values2.put(SCHEDULEDETAIL_TOWN, dettail.town);
        values2.put(SCHEDULEDETAIL_CONTACTPERSON, dettail.contactperson);
        values2.put(SCHEDULEDETAIL_OTHERCOMPANY, dettail.othercompany);
        values2.put(SCHEDULEDETAIL_OTPVERIFIED, dettail.otpverified);
        values2.put(SCHEDULEDETAIL_LOCPROVIDER, dettail.locprovider);
        values2.put(SCHEDULEDETAIL_LOCACCURACY, dettail.locaccuracy);
        values2.put(SCHEDULEDETAIL_PHOTOUUID, dettail.photoUUID);
        values2.put(SCHEDULEDETAIL_EMAIL, dettail.email);
        values2.put(SCHEDULEDETAIL_DIRECTCOV, dettail.directcov);
        values2.put(SCHEDULEDETAIL_SELPRODCAT,
                dettail.selectedproductcategories);
        values2.put(CLOSING_DAY, dettail.Closingday);
        values2.put(SCHEDULEDETAIL_STORECLOSED, dettail.storeclosed);
        values2.put(SCHEDULEDETAIL_PNGCOVERED, dettail.pngcovered);
        values2.put(SCHEDULEDETAIL_BRANCH, dettail.branch);
        values2.put(SCHEDULEDETAIL_CUSTTYPE, dettail.custtype);
        values2.put(SCHEDULEDETAIL_CITY, dettail.city);
        values2.put(SCHEDULEDETAIL_PIN, dettail.pin);
        values2.put(SCHEDULEDETAIL_OTPSUBTIME, dettail.otpsubtime);

        values2.put(SCHEDULEDETAIL_OTPLAT, dettail.otplat);
        values2.put(SCHEDULEDETAIL_OTPLONG, dettail.otplong);
        values2.put(SCHEDULEDETAIL_UPDATETIME, crtime);

        db.insert(TABLE_SCHEDULE_DETAIL, null, values2);


    }

    public void insertDocumentPhotos(String customerid,
                                     HashMap<String, Bitmap> docmap, String createtime, String upflag) {
        boolean need = false;
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "INSERT INTO " + TABLE_DOCUMENTPHOTOS + " ("
                + DOCUMENTPHOTOS_CUSTID + "," + DOCUMENTPHOTOS_DOCUMENT + ","
                + DOCUMENTPHOTOS_UUID + ", " + DOCUMENTPHOTOS_CREATETIME + ", "
                + DOCUMENTPHOTOS_UPDATEFLAG + ") VALUES ";
        for (String key : docmap.keySet()) {
            need = true;
            sql += "('" + customerid + "','PHOTOS','" + key + "','"
                    + createtime + "','" + upflag + "'),";
        }
        if (need) {
            sql = sql.substring(0, sql.length() - 1);
            db.execSQL(sql);
        }
    }


    // get checklist data
    public JSONArray getchecklist() throws JSONException {
        JSONArray resons = new JSONArray();
        String selectQuery = "SELECT " + PRODUCTCHECKLIST_CODE + ","
                + PRODUCTCHECKLIST_NAME + "," + PRODUCTCHECKLIST_TYPE
                + " FROM " + TABLE_PRODUCTCHECKLIST + " order by "
                + PRODUCTCHECKLIST_SEQNO;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("catid", cursor.getString(0));
                obj.put("catname", cursor.getString(1));
                obj.put("type", cursor.getString(2));
                resons.put(obj);
            } while (cursor.moveToNext());

        }

        if (cursor != null)
            cursor.close();
        return resons;
    }

    public void insertproductinputs(String customerid,
                                    String checklistinputcode, String checklistinput) {

        String custid=customerid;
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "INSERT INTO " + TABLE_CHECKLISTINPUT + " ("
                + CHECKLISTINPUT_CODE + "," + CHECKLISTINPUT_CUSTNAME + ","
                + CHECKLISTINPUT_INPUT + ") VALUES ";
        sql += "('" + checklistinputcode + "','" + customerid + "','"
                + checklistinput + "')";
        db.execSQL(sql);

    }

    // get closing day update data for sync
    // get unvisited stores to sync
    public JSONArray getclosingdayupdates() throws JSONException {

        String lastupdate = getrecentsynctime();
        String salespersonid = String.valueOf(getsalespersonid());
        JSONArray closingdayupdates = new JSONArray();
        String selectQuery = "SELECT " + CLOSING_DAY + "," + SCHEDULE_DETAIL_ID
                + "," + LOCATION_ID + " FROM " + TABLE_SCHEDULE_DETAIL
                + " WHERE " + CLOSING_DAY_UPDATETIME + " > " + lastupdate;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("closingday", cursor.getString(0));
                obj.put("scheduledetailid", cursor.getString(1));
                obj.put("customerid", cursor.getString(2));
                obj.put("personid", salespersonid);
                closingdayupdates.put(obj);
            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return closingdayupdates;
    }

    // get unvisited stores to sync
    public JSONArray getunvisitedstores() throws JSONException {

        String lastupdate = getrecentsynctime();
        String salespersonid = String.valueOf(getsalespersonid());
        JSONArray unvisitedstores = new JSONArray();
        try {
            String selectQuery = "SELECT " + STOREVISIT_REASON + ","
                    + STOREVISIT_TIME + "," + STOREVISIT_LATTITUDE + ","
                    + STOREVISIT_LONGITUDE + "," + SCHEDULE_DETAIL_ID + ","
                    + LOCATION_ID + "," + SCHEDULEDETAIL_LATTITUDE + ","
                    + SCHEDULEDETAIL_LONGITUDE + ","
                    + SCHEDULEDETAIL_REASON_ACC + " FROM "
                    + TABLE_SCHEDULE_DETAIL + " WHERE "
                    + SCHEDULEDETAIL_CREATETIME + " > " + lastupdate;
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    JSONObject obj = new JSONObject();
                    String reason = cursor.getString(0);
                    obj.put("reason", cursor.getString(0));
                    obj.put("visittime", cursor.getString(1));
                    obj.put("lattitude", cursor.getString(2));
                    obj.put("longitude", cursor.getString(3));
                    obj.put("scheduledetailid", cursor.getString(4));
                    obj.put("customerid", cursor.getString(5));
                    obj.put("personid", salespersonid);
                    String lat = cursor.getString(cursor
                            .getColumnIndex(SCHEDULEDETAIL_LATTITUDE));
                    String lng = cursor.getString(cursor
                            .getColumnIndex(SCHEDULEDETAIL_LONGITUDE));

                    obj.put("reasonlatitude", lat == null ? "" : lat);
                    obj.put("reasonlongitude", lng == null ? "" : lng);
                    obj.put("reasonAccuracy", cursor.getString(cursor
                            .getColumnIndex(SCHEDULEDETAIL_REASON_ACC)));

                    if (reason != null && !reason.equals("null")
                            && !reason.equals("")) {
                        unvisitedstores.put(obj);
                    }

                } while (cursor.moveToNext());

            }
            if (cursor != null)
                cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return unvisitedstores;
    }

    // /get order amount to sync

    public JSONArray getorderpaymentstosync() throws JSONException {
        String lastupdate = getrecentsynctime();
        JSONArray orederamountdata = new JSONArray();
        String selectQuery = "SELECT * FROM " + TABLE_ORDERAMOUNTDATA
                + " WHERE " + ORDERAMOUNTDATA_CREATETIME + " > " + lastupdate;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();

                obj.put("amount", cursor.getString(1));
                obj.put("scheduledetailid", cursor.getString(2));
                obj.put("customerid", cursor.getString(3));
                obj.put("personid", cursor.getString(4));
                obj.put("time", cursor.getString(5));
                obj.put("lattitude", cursor.getString(7));
                obj.put("longitude", cursor.getString(8));
                obj.put("guid", cursor.getString(9));
                orederamountdata.put(obj);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return orederamountdata;

    }

    public JSONArray getpaymentstosync() throws JSONException {

        String lastupdate = getrecentsynctime();
        JSONArray paymentlist = new JSONArray();
        boolean enabledigital_signature = isserviceexist("DigitalSignature");
        JSONArray fileData = new JSONArray();
        HashSet<String> unique_fileid = new HashSet<String>();

        String selectQuery = "SELECT * FROM " + TABLE_PAYMENTS + " WHERE "
                + PAYMENT_CREATETIME + " > " + lastupdate + " AND "
                + DELETE_FLAG + "=0" + " AND " + PAYMENT_APROOVEFLAG
                + "=1 ORDER BY " + PAYMENT_CREATETIME + " DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                String paymentid = cursor.getString(0);
                String schduleline_id = cursor.getString(1);
                obj.put("scheduledetailid", schduleline_id);
                obj.put("payentmethode", cursor.getString(17));// 17 for pdc/cdc
                obj.put("amount", cursor.getString(3));
                obj.put("chequenumber", cursor.getString(4));
                obj.put("ifsc", cursor.getString(5));
                obj.put("deleteflag", cursor.getString(7));
                obj.put("paymentid", cursor.getString(8));
                obj.put("salespersonid", cursor.getString(9));
                obj.put("locationid", cursor.getString(10));
                obj.put("chequedate", cursor.getString(11));
                obj.put("paymentdate", cursor.getString(12));
                obj.put("invoicenumber", cursor.getString(13));
                obj.put("picklistnumber", cursor.getString(14));
                obj.put("latitude", cursor.getString(15));
                obj.put("longitude", cursor.getString(16));
                obj.put("banknamecode", cursor.getString(18));
                obj.put("verificationcode", cursor.getString(22));
                if (enabledigital_signature) {
                    obj.put("signingperson", cursor.getString(20));
                    String fileid = cursor.getString(21);
                    obj.put("signaturefileid", fileid);

                    if (!unique_fileid.contains(fileid)) {
                        JSONObject imageobject = new JSONObject();
                        imageobject.put("signmappingid", fileid);
                        imageobject.put("signmappingimage", readFromFile(fileid
                                + "_signature.png"));
                        fileData.put(imageobject);
                        unique_fileid.add(fileid);
                    }
                }

                obj.put("invoicedata", getinvoicedatatosync(paymentid));
                paymentlist.put(obj);

            } while (cursor.moveToNext());

            if (paymentlist.length() > 0 && enabledigital_signature) {
                paymentlist.getJSONObject(0).put("signaturedata", fileData);
            }

        }

        return paymentlist;

    }

    // get all denomination data to sync
    public JSONArray getdenominationdatatatosync() throws JSONException {

        String lastsyc = getrecentsynctime();

        JSONArray denomdata = new JSONArray();

        String selectQuery = "SELECT " + SCHEDULESUMMARY_SUBMITTIME + ","
                + SCHEDULESUMMARY_PERSONID + "," + SCHEDULESUMMARY_LAT + ","
                + SCHEDULESUMMARY_LONGI + "," + SCHEDULESUMMARY_UNIQUEID + ","
                + SCHEDULESUMMARY_DENOMEDATA + "," + SCHEDULESUMMARY_TOTTALCASH
                + "," + SCHEDULESUMMARY_SCHEDULEID + ","
                + SCHEDULESUMMARY_COMPLETION_TIME + ","
                + SCHEDULESUMMARY_BEGIN_TIME + ","

                + SCHEDULESUMMARY_BEGINODOMETER + ","
                + SCHEDULESUMMARY_BEGINODOMETERUUID + ","
                + SCHEDULESUMMARY_VEHICLE + "," + SCHEDULESUMMARY_DRIVER + ","
                + SCHEDULESUMMARY_COMPLETIONODOMETER + ","
                + SCHEDULESUMMARY_COMPLETIONUUID

                + " FROM " + TABLE_SCHEDULESUMMARY + " WHERE "
                + SCHEDULESUMMARY_CREATETIME + " > " + lastsyc;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();

                obj.put("Submittime", cursor.getString(0));
                obj.put("personid", cursor.getString(1));
                obj.put("lattitude", cursor.getString(2));
                obj.put("longitude", cursor.getString(3));
                obj.put("uniqueid", cursor.getString(4));
                obj.put("denomedata", cursor.getString(5));
                obj.put("totalcash", cursor.getString(6));
                obj.put("scheduleid", cursor.getString(7));
                obj.put("completiontime", cursor.getString(8));
                obj.put("begintime", cursor.getString(9));

                obj.put("StartOdometer", cursor.getString(10));
                obj.put("StartOdometerPic", cursor.getString(11));
                obj.put("Vehicle", cursor.getString(12));
                obj.put("Driver", cursor.getString(13));
                obj.put("EndOdometer", cursor.getString(14));
                obj.put("EndOdometerPic", cursor.getString(15));

                denomdata.put(obj);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return denomdata;

    }

    public JSONArray appusagedatatosync() throws JSONException {
        String lastupdate = getrecentsynctime();
        JSONArray temp = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select * from " + TABLE_APPUSAGE + " where "
                + APPUSAGE_CRTIME + ">" + lastupdate;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("packagename", cursor.getString(cursor
                        .getColumnIndex(APPUSAGE_PACKAGE)));
                obj.put("usagetime",
                        cursor.getString(cursor.getColumnIndex(APPUSAGE_TIME)));
                obj.put("submitteddate", cursor.getString(cursor
                        .getColumnIndex(APPUSAGE_CRTIME)));
                obj.put("appname", cursor.getString(cursor
                        .getColumnIndex(APPUSAGE_APPNAME)));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public JSONArray getschedulebeginsync() throws JSONException {
        // TODO Auto-generated method stub

        String lastsyc = getrecentsynctime();
        Log.e("LastSyncTime", lastsyc);
        JSONArray schbegindata = new JSONArray();
        /*
         * String selectQuery = "SELECT " + SCHEDULESUMMARY_SUBMITTIME + "," +
         * SCHEDULESUMMARY_PERSONID + "," + SCHEDULESUMMARY_LAT + "," +
         * SCHEDULESUMMARY_LONGI + "," + SCHEDULESUMMARY_UNIQUEID + "," +
         * SCHEDULESUMMARY_DENOMEDATA + "," + SCHEDULESUMMARY_TOTTALCASH + "," +
         * SCHEDULESUMMARY_SCHEDULEID + "," + SCHEDULESUMMARY_COMPLETION_TIME +
         * "," + SCHEDULESUMMARY_BEGIN_TIME + ","
         *
         * + SCHEDULESUMMARY_BEGINODOMETER + "," +
         * SCHEDULESUMMARY_BEGINODOMETERUUID + "," + SCHEDULESUMMARY_VEHICLE +
         * "," + SCHEDULESUMMARY_DRIVER + "," +
         * SCHEDULESUMMARY_COMPLETIONODOMETER + "," +
         * SCHEDULESUMMARY_COMPLETIONUUID
         *
         * + " FROM " + TABLE_SCHEDULESUMMARY + " WHERE " +
         * SCHEDULESUMMARY_CREATETIME + " > " + lastsyc;
         */

        String selectQuery = "SELECT " + SCHEDULE_ID + "," + SALESPERSON_ID
                + "," + SCHEDULE_BEGIN_TIME + "," + SCHEDULE_ODOMETER + ","
                + SCHEDULE_ODOMETER_UUID + "," + SCHEDULE_VEHICLE + ","
                + SCHEDULE_DRIVER + " FROM " + TABLE_SCHEDULE_HEADER
                + " WHERE " + SCHEDULE_SYNC_TIME + " > " + lastsyc;
        Log.e("selectQuery", selectQuery);
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("scheduleid", cursor.getString(0));
                obj.put("personid", cursor.getString(1));
                obj.put("begintime", cursor.getString(2));
                obj.put("StartOdometer", cursor.getString(3));
                obj.put("StartOdometerPic", cursor.getString(4));
                obj.put("Vehicle", cursor.getString(5));
                obj.put("Driver", cursor.getString(6));
                schbegindata.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        Log.e("schbegindata", schbegindata.toString());
        return schbegindata;

    }


    public JSONArray getsalesreturndatatosync() throws JSONException {
        String lastsyc = getrecentsynctime();
        JSONArray orderdata = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT srn.*,hrd." + SALESRETURN_SCHLINEID + " FROM "
                + TABLE_SALERTNLINE + " srn JOIN " + TABLE_SALESRETURN
                + " hrd on hrd." + SALESRETURN_PKID + "=srn."
                + SALERTNLINE_HEADID + " JOIN " + TABLE_SCHEDULE_DETAIL
                + " sln on hrd." + SALESRETURN_SCHLINEID + "=sln."
                + SCHEDULE_DETAIL_ID + " WHERE srn." + SALERTNLINE_CREATETIME
                + ">'" + lastsyc + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject tempObj = new JSONObject();
                tempObj.put(
                        "schedulelineid",
                        cursor.getString(cursor
                                .getColumnIndex(SALESRETURN_SCHLINEID)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALESRETURN_SCHLINEID)));
                tempObj.put(
                        "product",
                        cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_PRODID)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_PRODID)));
                tempObj.put(
                        "qty",
                        cursor.getString(cursor.getColumnIndex(SALERTNLINE_QTY)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_QTY)));
                tempObj.put(
                        "mrp",
                        cursor.getString(cursor.getColumnIndex(SALERTNLINE_MRP)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_MRP)));
                tempObj.put(
                        "invoice",
                        cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_INVOICE)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_INVOICE)));
                tempObj.put(
                        "deleteflag",
                        cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_DELETEFLAG)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_DELETEFLAG)));
                tempObj.put(
                        "createtime",
                        cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_CREATETIME)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(SALERTNLINE_CREATETIME)));
                orderdata.put(tempObj);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return orderdata;
    }

    public JSONArray getcreditnotestosync() throws JSONException {
        String lastsyc = getrecentsynctime();
        JSONArray orderdata = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_CREDITNOTES + " WHERE "
                + CREDITNOTE_CREATEDTIME + ">'" + lastsyc + "'";

        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject tempObj = new JSONObject();
                tempObj.put(
                        "schedulelineid",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_SCHDLELINEID)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_SCHDLELINEID)));
                tempObj.put(
                        "amount",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_AMOUNT)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_AMOUNT)));
                tempObj.put(
                        "reason",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_REASON)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_REASON)));
                tempObj.put(
                        "description",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_DESCRIPTION)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_DESCRIPTION)));
                tempObj.put(
                        "deleteflag",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_DELETEFLAG)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_DELETEFLAG)));
                tempObj.put(
                        "createtime",
                        cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_CREATEDTIME)) == null ? ""
                                : cursor.getString(cursor
                                .getColumnIndex(CREDITNOTE_CREATEDTIME)));
                orderdata.put(tempObj);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return orderdata;
    }

    public JSONArray getphotostosync() throws JSONException {
        String lastupdate = getrecentsynctime();
        JSONArray temp = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select * from " + TABLE_DOCUMENTPHOTOS + " where "
                + DOCUMENTPHOTOS_CREATETIME + ">" + lastupdate + " and "
                + DOCUMENTPHOTOS_UPDATEFLAG + "='1'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("type", "document");
                obj.put("slineid", cursor.getString(cursor
                        .getColumnIndex(DOCUMENTPHOTOS_CUSTID)));
                obj.put("name", cursor.getString(cursor
                        .getColumnIndex(DOCUMENTPHOTOS_DOCUMENT)));
                obj.put("catid", "");
                obj.put("uuid", cursor.getString(cursor
                        .getColumnIndex(DOCUMENTPHOTOS_UUID)));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        sql = "select * from " + TABLE_CATEGORYPHOTOS + " where "
                + CATEGORYPHOTOS_CREATETIME + ">" + lastupdate + " and "
                + CATEGORYPHOTOS_UPDATEFLAG + "='1'";
        cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("type", "category");
                obj.put("slineid", cursor.getString(cursor
                        .getColumnIndex(CATEGORYPHOTOS_CUSTID)));
                String cat = cursor.getString(cursor
                        .getColumnIndex(CATEGORYPHOTOS_CATEGORY));
                obj.put("name", cat);
                String catid = getchecklistids(cat, db);
                obj.put("catid", catid);
                obj.put("uuid", cursor.getString(cursor
                        .getColumnIndex(CATEGORYPHOTOS_UUID)));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public JSONArray getupdatecustomerstosync() throws JSONException {
        String lastupdate = getrecentsynctime();
        JSONArray temp = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select * from " + TABLE_SCHEDULE_DETAIL + " where "
                + SCHEDULEDETAIL_EDITTIME + ">" + lastupdate;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("detailid", cursor.getString(cursor
                        .getColumnIndex(SCHEDULE_DETAIL_ID)));
                obj.put("Address", cursor.getString(cursor
                        .getColumnIndex(LOCATION_ADRESS)));
                obj.put("Status", cursor.getString(cursor
                        .getColumnIndex(DETAIL_STATUS))); // added by yadhu on 21.11.2019
                obj.put("Phone", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_MOBILE)));
                obj.put("Gst", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_TINNUMBER)));
                obj.put("Pan", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_PAN)));
                obj.put("PhotoUUID", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_PHOTOUUID)));
                obj.put("ImgLat", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_IMGLAT)));
                obj.put("ImgLong", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_IMGLONG)));
                obj.put("landmark", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_LANDMARK)));
                obj.put("town", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_TOWN)));
                obj.put("contactperson", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_CONTACTPERSON)));
                obj.put("othercompany", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_OTHERCOMPANY)));
                obj.put("closingday",
                        cursor.getString(cursor.getColumnIndex(CLOSING_DAY)));
                obj.put("selectedproductcat", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_SELPRODCAT)));
                obj.put("AlternateCustCode", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_ALTERCUSTCODE)));
                obj.put("Customercategory", cursor.getString(cursor
                        .getColumnIndex(CUSTOMERCATOGORYID)));

                obj.put("street", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_STREETNAME)));
                obj.put("area", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_AREA)));
                obj.put("landline", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_LANDLINE)));
                obj.put("shopname", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_SHOP)));
                obj.put("city", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_CITY)));
                obj.put("pincode", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_PIN)));
                obj.put("email", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_EMAIL)));
                obj.put("custname",
                        cursor.getString(cursor.getColumnIndex(LOCATION_NAME)));
                obj.put("imgAcc", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_PHOTO_ACCURACY)));
                obj.put("imgTime", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_PHOTO_EDITTIME)));
                obj.put("startTime", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_STARTTIME)));
                obj.put("EndTime", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_ENDTIME)));
                obj.put("OtpVerified", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_OTPVERIFIED)));
                obj.put("needotp", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_NEEDOTP)));

                obj.put("OtpSubTime", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_OTPSUBTIME)));

                obj.put("OtpLat", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_OTPLAT)));
                obj.put("OtpLong", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_OTPLONG)));

                String selectSurrogates = "SELECT * FROM "
                        + TABLE_CHECKLISTINPUT + " WHERE "
                        + CHECKLISTINPUT_CUSTNAME + " = '"
                        + cursor.getString(cursor.getColumnIndex(SCHEDULE_DETAIL_ID)) + "'";
                Cursor cursorsurrogates = db.rawQuery(selectSurrogates, null);
                JSONArray surrogates = new JSONArray();
                if (cursorsurrogates.moveToFirst()) {
                    do {
                        JSONObject objects = new JSONObject();
                        objects.put("id", cursorsurrogates.getString(1));
                        objects.put("input", cursorsurrogates.getString(3));
                        surrogates.put(objects);
                    } while (cursorsurrogates.moveToNext());
                }
                obj.put("surrogates", surrogates);
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public JSONArray getassetstosync() throws JSONException {
        String lastupdate = getrecentsynctime();
        JSONArray temp = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select * from " + TABLE_ASSETS + " where "
                + ASSETS_CREATEDTIME + ">" + lastupdate;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("schedulelineid",
                        cursor.getString(cursor.getColumnIndex(ASSETS_SLINEID)));
                obj.put("customer", cursor.getString(cursor
                        .getColumnIndex(ASSETS_CUSTOMER)));
                obj.put("createdon", cursor.getString(cursor
                        .getColumnIndex(ASSETS_CREATEDTIME)));
                obj.put("qnlineid",
                        cursor.getString(cursor.getColumnIndex(ASSETS_QLINEID)));
                obj.put("answer",
                        cursor.getString(cursor.getColumnIndex(ASSETS_ANSWER)));
                obj.put("photouuid",
                        cursor.getString(cursor
                                .getColumnIndex(ASSETS_PHOTOUUID)) + "");
                obj.put("asstqnhdr", cursor.getString(cursor
                        .getColumnIndex(ASSETS_ASSETQNHDR)));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public JSONArray getpassbyupdatedsync() throws JSONException {
        String lastupdate = getrecentsynctime();
        JSONArray temp = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select " + SCHEDULE_DETAIL_ID + "," + LOCATION_ID
                + " from " + TABLE_SCHEDULE_DETAIL + " where "
                + SCHEDULEDETAIL_PASSBYCRTIME + ">" + lastupdate;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("detailid", cursor.getString(cursor
                        .getColumnIndex(SCHEDULE_DETAIL_ID)));
                obj.put("custid",
                        cursor.getString(cursor.getColumnIndex(LOCATION_ID)));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public JSONArray getcratestosync() throws JSONException {
        String lastupdate = getrecentsynctime();
        JSONArray temp = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select * from " + TABLE_CRATES + " where "
                + CRATES_CRTIMESYNC + ">" + lastupdate;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("detailid",
                        cursor.getString(cursor.getColumnIndex(CRATES_SLINEID)));
                obj.put("inval",
                        cursor.getString(cursor.getColumnIndex(CRATES_IN)));
                obj.put("outval",
                        cursor.getString(cursor.getColumnIndex(CRATES_OUT)));
                obj.put("crtime",
                        cursor.getString(cursor.getColumnIndex(CRATES_CRTIME)));
                obj.put("uuid",
                        cursor.getString(cursor.getColumnIndex(CREATES_UUID)));
                temp.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    // /geting order data for upload
    public JSONArray getorders() throws JSONException {
        String lastupdate = getrecentsynctime();
        JSONArray orders = new JSONArray();
        String selectQuery = "SELECT * FROM " + TABLE_ORDER_HEADER + " WHERE "
                + ORDERSUBMIT_TIME + " IS NOT NULL ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                String orderheadid = cursor.getString(0);
                obj.put("orderbegintime", cursor.getString(1));
                obj.put("ordersubmittime", cursor.getString(2));
                obj.put("scheduledetailid", cursor.getString(3));
                obj.put("latitude", cursor.getString(5));
                obj.put("longitude", cursor.getString(6));
                obj.put("salespersonid", cursor.getString(7));
                obj.put("routenetworkid", cursor.getString(8));
                obj.put("locationid", cursor.getString(9));
                obj.put("products", getorderdetails(orderheadid, lastupdate));
                orders.put(obj);

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return orders;

    }

    // //getupdatedlatlongs

    public JSONArray getupdatedlaatlong() throws JSONException {

        String lastsyc = getrecentsynctime();

        JSONArray childs = new JSONArray();

        String selectQuery = "SELECT " + LOCATION_ID + "," + LATITUDE_NEW + ","
                + LONGITUDE_NEW + "," + SCHED_LOCUPDATETIME + ","
                + SCHEDULEDETAIL_LATITUDE_ACCURACY + " FROM "
                + TABLE_SCHEDULE_DETAIL + " WHERE " + SCHEDULEDETAIL_CREATETIME
                + " > " + lastsyc;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                String lat = cursor.getString(1);
                obj.put("locationid", cursor.getString(0));
                obj.put("latitude", cursor.getString(1));
                obj.put("longitude", cursor.getString(2));
                obj.put("Salespersonid", getsalespersonid());
                obj.put("updatetime", cursor.getString(3));

                obj.put("accuracy", cursor.getString(4));

                if (lat != null && !lat.equals("0")) {
                    childs.put(obj);
                }

            } while (cursor.moveToNext());

        }
        if (cursor != null)
            cursor.close();
        return childs;

    }

    public String getchecklistids(String names) {
        String temp = "";
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT " + PRODUCTCHECKLIST_CODE + " FROM "
                + TABLE_PRODUCTCHECKLIST + " WHERE " + PRODUCTCHECKLIST_NAME
                + " IN (" + names + ")";

        // SQLiteStatement stmt = db.compileStatement("SELECT "
        // + PRODUCTCHECKLIST_CODE + " FROM " + TABLE_PRODUCTCHECKLIST
        // + " WHERE " + PRODUCTCHECKLIST_NAME + " IN (?)");
        // stmt.bindString(1, names);
        // stmt.execute();

        // Cursor cursor = db.rawQuery(selectQuery, new String[] { names });
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                temp += cursor.getString(0) + ",";
            } while (cursor.moveToNext());

        }
        if (!temp.equals("")) {
            temp = temp.substring(0, temp.length() - 1);
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public JSONObject getworksummary() throws Exception {

        int numberofstores = 0;
        int numberofvisitedstores = 0;
        int numberofproductivestores = 0;
        double artarget = 0.0;

        Double amount_collected = 0.0;
        Double ordeamount_enterred = 0.0;
        int geo_tagged = 0;

        int orderstorescount = 0;
        JSONObject summarydata = new JSONObject();

        SQLiteDatabase db = this.getWritableDatabase();
        String getstore_count = "SELECT COUNT(*) FROM " + TABLE_SCHEDULE_DETAIL;
        Cursor cursor = db.rawQuery(getstore_count, null);
        if (cursor.moveToFirst()) {

            numberofstores = cursor.getInt(0);
        }

        String get_visited_store_count = "SELECT COUNT(CASE WHEN " + DETAIL_STATUS + " == \"Completed\" THEN 1 ELSE NULL END )FROM "
                + TABLE_SCHEDULE_DETAIL ;
        cursor = db.rawQuery(get_visited_store_count, null);
        if (cursor.moveToFirst()) {

            numberofvisitedstores = cursor.getInt(0);
        }

        /*
         * String get_productive_store_count = "SELECT COUNT(*) FROM " +
         * TABLE_SCHEDULE_DETAIL + " WHERE " + SCHEDULE_DETAIL_PRODUCTIVE + "="
         * + "1";
         */

        // String get_productive_store_count = "SELECT COUNT(*) FROM "
        String get_productive_store_count = "SELECT count(DISTINCT CASE WHEN "
                + PAYMENT_PKID + " is not null or " + ORDERHDR_PKID
                + " is not null or " + ORDERAMOUNTDATA_PKID + " is not null"
                + " or " + SCHEDULE_DETAIL_PRODUCTIVE + " is not null "
                + " THEN sl." + SCHEDULE_DETAIL_PKID + " ELSE null END) FROM "
                + TABLE_SCHEDULE_DETAIL + " sl LEFT JOIN " + TABLE_PAYMENTS
                + " pt ON pt. " + PAYMENT_SCHEDULEDETAILID + " =sl."
                + SCHEDULE_DETAIL_ID + " AND pt. " + PAYMENT_APROOVEFLAG
                + " =1 " + " LEFT JOIN " + TABLE_ORDERHDR + " ot ON ot. "
                + ORDERHDR_SCHLINEID + " =sl. " + SCHEDULE_DETAIL_ID
                + " AND ot. " + ORDERHDR_APPROVEFLAG + " =1 " + " LEFT JOIN "
                + TABLE_ORDERAMOUNTDATA + " oamt ON oamt. "
                + ORDERAMOUNTDATA_SCHEDULEDETAILID + " =sl. "
                + SCHEDULE_DETAIL_ID + " WHERE sl."
                + SCHEDULE_DETAIL_PRODUCTIVE + " =1";
        cursor = db.rawQuery(get_productive_store_count, null);
        if (cursor.moveToFirst()) {

            numberofproductivestores = cursor.getInt(0);
        }

        String get_tottalcollectedamount = "SELECT SUM(AMOUNT) FROM "
                + TABLE_PAYMENTS + " WHERE " + PAYMENT_METHOD
                + "!= 'Return' AND " + PAYMENT_METHOD + "!= 'Credit'" + " AND "
                + DELETE_FLAG + "=0" + " AND " + PAYMENT_APROOVEFLAG + "=1";
        ;
        cursor = db.rawQuery(get_tottalcollectedamount, null);
        if (cursor.moveToFirst()) {

            amount_collected = cursor.getDouble(0);
        }

        String get_tottal_orderamount = "SELECT SUM(" + ORDERAMOUNTDATA_AMOUNT
                + ") FROM " + TABLE_ORDERAMOUNTDATA;
        cursor = db.rawQuery(get_tottal_orderamount, null);
        if (cursor.moveToFirst()) {

            ordeamount_enterred = cursor.getDouble(0);
        }

        String marketorder_count = "SELECT COUNT(*) FROM "
                + TABLE_ORDERAMOUNTDATA;
        cursor = db.rawQuery(marketorder_count, null);
        if (cursor.moveToFirst()) {
            orderstorescount = cursor.getInt(0);
        }

        // ar target

        // getting AR target for the schedule
        String get_artarget = "select sum (inv.`staticbalance`) from invoice inv join scheduledetail schdtl on  schdtl.`locationid`=inv.`customerid`";

        cursor = db.rawQuery(get_artarget, null);
        if (cursor.moveToFirst()) {
            artarget = cursor.getDouble(0);
        }

        String get_geo_tagged = "SELECT COUNT(1) FROM " + TABLE_SCHEDULE_DETAIL
                + " WHERE " + LATITUDE_NEW + " > " + "0";
        cursor = db.rawQuery(get_geo_tagged, null);
        if (cursor.moveToFirst()) {
            geo_tagged = cursor.getInt(0);
        }

        summarydata.put("tottalnumberofstores", numberofstores);
        summarydata.put("visitedstores", numberofvisitedstores);
        summarydata.put("Tottalamountcollected", amount_collected);
        summarydata.put("Tottalorderamount", ordeamount_enterred);
        summarydata.put("tottalmarketorders", orderstorescount);
        summarydata.put("productivestores", numberofproductivestores);
        summarydata.put("artarget", artarget);
        summarydata.put("geotagged", geo_tagged);

        if (cursor != null)
            cursor.close();
        return summarydata;

    }

    // get category data from cache
    public JSONArray getcustomercategories(String personrole)
            throws JSONException {
        JSONArray resons = new JSONArray();
        String selectQuery = "";
        if (personrole.contentEquals("Combing User"))
            selectQuery = "SELECT " + CUSTOMERCATEGORIES_CODE + ","
                    + CUSTOMERCATEGORIES_NAME + " FROM "
                    + TABLE_CUSTOMERCATEGORIES + " where "
                    + CUSTOMERCATEGORIES_TYPE + "='Combing'";
        else
            selectQuery = "SELECT " + CUSTOMERCATEGORIES_CODE + ","
                    + CUSTOMERCATEGORIES_NAME + " FROM "
                    + TABLE_CUSTOMERCATEGORIES + " where "
                    + CUSTOMERCATEGORIES_TYPE + "!='Combing'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("catcode", cursor.getString(0));
                obj.put("catname", cursor.getString(1));
                resons.put(obj);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        db.close();
        return resons;
    }


    public List<FourStrings> getregionlatlngsNew() throws Exception {
        List<FourStrings> temp = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + REGIONS_NAME + ", " + REGIONS_REGIONNAME + ", " + REGIONS_TARGT + " ," + REGIONS_ACHIVD
                + "," + REGIONS_PKID + ", " + REGIONS_STATUS + " FROM " + TABLE_REGIONS;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                String qty = cursor.getString(cursor.getColumnIndex(REGIONS_NAME));
                String mrp = cursor.getString(cursor.getColumnIndex(REGIONS_REGIONNAME));
                String rate = cursor.getString(cursor.getColumnIndex(REGIONS_TARGT));
                String packageType = cursor.getString(cursor.getColumnIndex(REGIONS_ACHIVD));
                String taxid = cursor.getString(cursor.getColumnIndex(REGIONS_PKID));
                String discount = cursor.getString(cursor.getColumnIndex(REGIONS_STATUS));

                FourStrings obj = new FourStrings(qty, mrp, rate, packageType,
                        taxid, discount, "");
                temp.add(obj);
                //Log.e("latlongreg",cursor.getString(0));
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public String getTodayCollectionCount() {
        String temp = "0";
        String sql = "select count(1) from " + TABLE_LOCATIONS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            temp = cursor.getString(0);
        }
        return temp;
    }

    public JSONObject csummary() {
        String temp = "0";
        JSONObject summarydata = new JSONObject();
        String sql = "select * from " + TABLE_COMBINGSUMMARY;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                try {
                    summarydata.put("ccount", cursor.getString(cursor.getColumnIndex(COMBINGSUMMARY_CUSTOMERCOUNT)));
                    summarydata.put("zonecount", cursor.getString(cursor.getColumnIndex(COMBINGSUMMARY_CZONECOUNT)));
                    summarydata.put("czonext", cursor.getString(cursor.getColumnIndex(COMBINGSUMMARY_CZONETARGET)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }
        return summarydata;
    }

    public JSONObject getallcustdata(String scheduledetailid) throws Exception {
        JSONObject temp = new JSONObject();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "select * from " + TABLE_SCHEDULE_DETAIL + " where "
                + SCHEDULE_DETAIL_ID + "='" + scheduledetailid + "'";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                temp.put("CustCode", cursor.getString(cursor
                        .getColumnIndex(SCHED_CUSTOMERCODE)));
                temp.put("AlternateCustcode", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_ALTERCUSTCODE)));
                temp.put("AlternateCustCode", cursor.getString(cursor
                        .getColumnIndex(SCHED_CUSTOMERCODE)));
                temp.put("CustName",
                        cursor.getString(cursor.getColumnIndex(LOCATION_NAME)));
                temp.put("CustAddress", cursor.getString(cursor
                        .getColumnIndex(LOCATION_ADRESS)));
                temp.put("CustPhone", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_MOBILE)));
                temp.put("CustGst", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_TINNUMBER)));
                temp.put("CustCat", cursor.getString(cursor
                        .getColumnIndex(CUSTOMERCATOGORY)));
                temp.put("CustPan", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_PAN)));
                temp.put("PhotoUUID", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_PHOTOUUID)));
                temp.put("landmark", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_LANDMARK)));
                temp.put("town", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_TOWN)));
                temp.put("contactperson", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_CONTACTPERSON)));
                temp.put("othercompany", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_OTHERCOMPANY)));
                temp.put("closingday",
                        cursor.getString(cursor.getColumnIndex(CLOSING_DAY)));

                temp.put("CustImg", cursor.getString(cursor.getColumnIndex(SCHEDULEDETAIL_CUSTIMGURL)));

                temp.put("street", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_STREETNAME)));
                temp.put("area", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_AREA)));
                temp.put("landline", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_LANDLINE)));
                temp.put("shopname", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_SHOP)));
                temp.put("city", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_CITY)));
                temp.put("pin", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_PIN)));
                temp.put("email", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_EMAIL)));


                temp.put("starttime", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_STARTTIME)));
                temp.put("endtime", cursor.getString(cursor
                        .getColumnIndex(SCHEDULEDETAIL_ENDTIME)));

                // temp.put("begintime",cursor.getString(cursor.getColumnIndex(SCHEDULE_BEGIN_TIME)));
                // temp.put("cmpletiontime",cursor.getString(cursor.getColumnIndex(SCHEDULE_COMPLETION_TIME)));
                // temp.put("productcats", cursor.getString(cursor
                // .getColumnIndex(SCHEDULEDETAIL_SELPRODCAT)));
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public String getcustomerid(String schedulelineid) {
        String temp = "0";
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + LOCATION_ID + " FROM "
                + TABLE_SCHEDULE_DETAIL + " WHERE " + SCHEDULE_DETAIL_ID
                + "= '" + schedulelineid + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                temp = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return temp;
    }

    public JSONObject getchecklists(String cid, String typeid) throws JSONException {
        JSONObject result = new JSONObject();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + CHECKLISTINPUT_INPUT + "," + CHECKLISTINPUT_CODE + " FROM "
                + TABLE_CHECKLISTINPUT + " WHERE " + CHECKLISTINPUT_CUSTNAME
                + "= '" + cid + "' AND " + CHECKLISTINPUT_CODE + "= '" + typeid + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                result.put("code", cursor.getString(cursor.getColumnIndex(CHECKLISTINPUT_CODE)));
                result.put("input", cursor.getString(cursor.getColumnIndex(CHECKLISTINPUT_INPUT)));
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return result;
    }

    public void deletesurrogates(String custid) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String count = "DELETE FROM " + TABLE_CHECKLISTINPUT + " WHERE "
                    + CHECKLISTINPUT_CUSTNAME + "='" + custid + "'";
            db.execSQL(count);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String schedulelineVerified(String scheduledetailid) {
        // TODO Auto-generated method stub
        String otpverified = "";
        String selectQuery = "SELECT " + SCHEDULEDETAIL_OTPVERIFIED + " FROM "
                + TABLE_SCHEDULE_DETAIL + " WHERE " + SCHEDULE_DETAIL_ID + // " = "
                " = '" + scheduledetailid + "'";
        //+ scheduledetailid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            otpverified = cursor.getString(0);
        }
        if (cursor != null)
            cursor.close();
        if (otpverified == null)
            otpverified = "";

        return otpverified;

    }

    public void editCustomer(String scheduledetailid, Scheduledetail loc,
                             String crtime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LOCATION_ADRESS, loc.locationadress);
        values.put(SCHEDULEDETAIL_MOBILE, loc.mobilenumber);
        values.put(SCHEDULEDETAIL_TINNUMBER, loc.tinnumber);
        values.put(SCHEDULEDETAIL_PAN, loc.pan);
        values.put(SCHEDULEDETAIL_LANDMARK, loc.landmark);
        values.put(SCHEDULEDETAIL_TOWN, loc.town);
        values.put(SCHEDULEDETAIL_CONTACTPERSON, loc.contactperson);
        values.put(SCHEDULEDETAIL_OTHERCOMPANY, loc.othercompany);
        values.put(CLOSING_DAY, loc.Closingday);
        values.put(SCHEDULEDETAIL_SELPRODCAT, loc.selectedproductcategories);
        values.put(SCHEDULEDETAIL_ALTERCUSTCODE, loc.alternatecustcode);
        values.put(SCHEDULEDETAIL_EDITTIME, crtime);

        values.put(SCHEDULEDETAIL_SHOP, loc.shopname);
        values.put(CUSTOMER_CATOGORY, loc.Customercatogory);
        values.put(CUSTOMERCATOGORYID, loc.CustomercatogoryId);
        values.put(SCHEDULEDETAIL_STREETNAME, loc.street);
        values.put(SCHEDULEDETAIL_AREA, loc.area);
        values.put(SCHEDULEDETAIL_LANDLINE, loc.landLine);
        values.put(SCHEDULEDETAIL_CITY, loc.city);
        values.put(SCHEDULEDETAIL_PIN, loc.pin);
        values.put(SCHEDULEDETAIL_EMAIL, loc.email);
        values.put(LOCATION_NAME, loc.locationname);
        values.put(SCHEDULEDETAIL_STARTTIME, loc.StartTime);
        values.put(SCHEDULEDETAIL_ENDTIME, loc.EndTime);
        values.put(SCHEDULEDETAIL_OTPVERIFIED, loc.otpverified);
        values.put(SCHEDULEDETAIL_NEEDOTP, loc.needotp);

        values.put(DETAIL_STATUS, loc.status);  //for updating status as "completed"

        values.put(SCHEDULEDETAIL_OTPSUBTIME, loc.otpsubtime);
        values.put(SCHEDULEDETAIL_UPDATETIME, crtime);

        db.update(TABLE_SCHEDULE_DETAIL, values, SCHEDULE_DETAIL_ID + "=?",
                new String[]{scheduledetailid});
    }

    public void editCustomerPhoto(String scheduledetailid, String uuid,
                                  String time, String imglat, String imglong, String acc,
                                  String edittime) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SCHEDULEDETAIL_EDITTIME, time);
        values.put(SCHEDULEDETAIL_PHOTOUUID, uuid);
        values.put(SCHEDULEDETAIL_IMGLAT, imglat);
        values.put(SCHEDULEDETAIL_IMGLONG, imglong);

        values.put(SCHEDULEDETAIL_PHOTO_ACCURACY, acc);
        values.put(SCHEDULEDETAIL_PHOTO_EDITTIME, edittime);

        db.update(TABLE_SCHEDULE_DETAIL, values, SCHEDULE_DETAIL_ID + "=?",
                new String[]{scheduledetailid});
    }

    public void otplatlongupdate(String scheduledetailid, String crtime,
                                 String otplat, String otplong) {
        // TODO Auto-generated method stub
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SCHEDULEDETAIL_OTPLAT, otplat);
        values.put(SCHEDULEDETAIL_OTPLONG, otplong);
        db.update(TABLE_SCHEDULE_DETAIL, values, SCHEDULE_DETAIL_ID + "=?",
                new String[]{scheduledetailid});

    }

    public ArrayList<FourStrings> getSchedulesIdsForSpinner() {
        ArrayList<FourStrings> list = new ArrayList<FourStrings>();
        list.add(new FourStrings("SELECT", "SELECT", "", ""));
        String selectQuery = "select " + SCHEDULE_ID + ", " + SCHEDULE_NAME + " from " + TABLE_SCHEDULE_HEADER + " group by " + SCHEDULE_ID;
        //+ scheduledetailid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getString(0);
                String name = cursor.getString(1);
                list.add(new FourStrings(id, name, "", ""));
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();

        return list;
    }
    //-------------------------yadhu----------------------



    public void updatenovisitstatus(String scheduledetailid) {

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues values = new ContentValues();

            values.put(STOREVISIT_REASON,"visited");
            values.put(SCHEDULE_DETAIL_VISITED, "1");
            db.update(TABLE_SCHEDULE_DETAIL, values, SCHEDULE_DETAIL_ID + "=?",
                    new String[]{scheduledetailid});
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }
    public JSONArray CustCount() throws JSONException {
        JSONArray count = new JSONArray();
        String count_query = "SELECT count(SD. " + LOCATION_ID + " ) AS CUST_COUNT , count( case when SD."
                + DETAIL_STATUS + " == \"Completed\" then 1 else null end ) AS VISITED ,SH."
                + SCHEDULE_NAME + " FROM " + TABLE_SCHEDULE_HEADER + " SH JOIN " + TABLE_SCHEDULE_DETAIL
                + " SD on SD." + SCHEDULE_HEADER_ID + " = SH." + SCHEDULE_HEADER_PKID + " GROUP BY SH."
                + SCHEDULE_HEADER_PKID;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(count_query, null);
        if (cursor.moveToFirst()) {
            do {
                JSONObject obj = new JSONObject();
                obj.put("ScheduleName", cursor.getString(cursor.getColumnIndex(SCHEDULE_NAME)));
                obj.put("custCount", cursor.getString(cursor.getColumnIndex("CUST_COUNT")));
                obj.put("visited", cursor.getString(cursor.getColumnIndex("VISITED")));
                count.put(obj);
            } while (cursor.moveToNext());
        }
        return count;
    }

    public void insertintocombingcount(JSONArray count){
        SQLiteDatabase db= this.getWritableDatabase();
        try {
            for (int i = 0; i < count.length(); i++) {
                JSONObject temp = count.optJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(TOTAL_COMBED, temp.optString("Pending"));
                values.put(TOTAL_COUNT, temp.optString("Total"));
                values.put(TOT_DAYS, temp.optString("Days"));
                db.insert(TABLE_COMBINGCOUNT, null, values);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public JSONObject getcombingcount() {
        String temp = "0";
        JSONObject combingcount = new JSONObject();
        String sql = "select " + TOTAL_COMBED + " , " + TOTAL_COUNT + " , " + TOT_DAYS + " from " + TABLE_COMBINGCOUNT ;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                try {
                    combingcount.put("combed", cursor.getString(cursor.getColumnIndex(TOTAL_COMBED)));
                    combingcount.put("count", cursor.getString(cursor.getColumnIndex(TOTAL_COUNT)));
                    combingcount.put("Days", cursor.getString(cursor.getColumnIndex(TOT_DAYS)));
                    //combingcount.put("date",cursor.getString(cursor.getColumnIndex(CMB_DATE)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }
        return combingcount;
    }


    //-------------------yadhu------------------------


    public JSONArray selectNearcust() throws JSONException {
        SQLiteDatabase db = this.getWritableDatabase();
        JSONArray arr = new JSONArray();
        String query = "SELECT " + NEAREST_CUST_CODE + "," + NEAREST_CUST_NAME + "," + NEAREST_CUST_DIST + " FROM " + TABLE_NEARESTCUST + " WHERE "
                + NEAREST_CUST_DIST + "<51";

        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            do{
                JSONObject obj = new JSONObject();
                int dist = cursor.getInt(Integer.valueOf(cursor.getColumnIndex(NEAREST_CUST_DIST)));
                if(dist<51){
                    obj.put("CustCode", cursor.getString(cursor
                            .getColumnIndex(NEAREST_CUST_CODE)));
                    obj.put("CustName",cursor.getString(cursor.getColumnIndex(NEAREST_CUST_NAME)));
                    obj.put("Distance",cursor.getInt(Integer.parseInt(String.valueOf(cursor.getColumnIndex(NEAREST_CUST_DIST)))));
                    arr.put(obj);
                }

            }while(cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return arr;
    }

    public void insert_nearestcust(JSONArray nearestArray){
        deleteNearCust();
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for (int i = 0; i < nearestArray.length(); i++) {
            try {
                JSONObject rateobject = nearestArray.getJSONObject(i);
                ContentValues values = new ContentValues();
                values.put(NEAREST_CUST_ID,
                        rateobject.getString("_id"));
                values.put(NEAREST_CUST_CODE,
                        rateobject.getString("CustCode"));
                values.put(NEAREST_CUST_NAME, rateobject.getString("CustName"));
                values.put(NEAREST_CUST_LAT, rateobject.getString("CustLat"));
                values.put(NEAREST_CUST_LONG, rateobject.getString("CustLong"));
                values.put(NEAREST_CUST_ADDR, rateobject.getString("Address"));
                values.put(NEAREST_CUST_DIST,rateobject.getString("Distance"));
                long t =  db.insert(TABLE_NEARESTCUST, null, values);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
    }


       /*public String getCustLatitudeForNearstCust(int n){
           SQLiteDatabase db = this.getWritableDatabase();
           JSONArray arr = new JSONArray();
           String query = "SELECT " + NEAREST_CUST_CODE  + " FROM " + TABLE_NEARESTCUST + " WHERE " + NEAREST_CUST_ID +
                   " = '" + n + "'";
           Cursor cursor = db.rawQuery(query, null);

       }*/

    /*public JSONObject NearestFunction(String custCode)throws JSONException{

        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + NEAREST_CUST_LAT  + "," + NEAREST_CUST_LONG + " FROM " + TABLE_NEARESTCUST + " WHERE " + NEAREST_CUST_CODE +
                " = '" + custCode + "'";
        JSONObject obj = new JSONObject();
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            do{

                obj.put("Lati", cursor.getString(cursor
                        .getColumnIndex(NEAREST_CUST_LAT)));
                obj.put("Longi",cursor.getString(cursor.getColumnIndex(NEAREST_CUST_LONG)));

            }while(cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
        return obj;




    }*/

    public void deleteNearCust() {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String count = "DELETE FROM " + TABLE_NEARESTCUST;
            db.execSQL(count);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//-------------------Sreelakshmi------------------------

}