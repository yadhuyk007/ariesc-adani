package combing.sales.adani.objects;

import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;

public class WriteFile {


    static String filename;

    public void writeFile(String process,String createtime,String data,String directory){

        try{

            filename=process+createtime+".txt";

            int asci=0;

            //			String key =salespersoncode;
            //			char[] code=key.toCharArray();
            File tempDir = Environment.getExternalStorageDirectory();
            //			tempDir = new File(tempDir.getAbsolutePath() + "/AriesBackup");

            tempDir = new File(tempDir.getAbsolutePath() + "/"+directory);

            if (!tempDir.exists()) {
                tempDir.mkdirs();
            }

            File myeFile = new File(tempDir,filename);
            FileOutputStream efos = new FileOutputStream(myeFile);

			/*for(int k=0;k<code.length;k++){
				asci=asci+(int)code[k];
			}*/

            for(int i = 0; i < data.length(); i++) {

                char ch= data.charAt(i);
//				ch=(char) (ch+3);//Encoding with 3.
                efos.write(ch);

            }
            efos.close();

//			decryptFileData(process, createtime, directory);
          //  Log.d("File creation","success");
        }catch(Exception e){
            e.printStackTrace();
         //   Log.d("File creation","failed");
        }


    }

    /**
     * Decryption code..
     */
    public void decryptFileData(String process,String createtime,String directory){


        try{
            String dfilename ="d_"+ process+createtime+".txt";
            filename=process+createtime+".txt";

            File tempDir = Environment.getExternalStorageDirectory();
            tempDir = new File(tempDir.getAbsolutePath() + "/"+directory);
            if (!tempDir.exists()) {
                tempDir.mkdirs();
            }
            File myeFile = new File(tempDir,dfilename);
            FileOutputStream efos = new FileOutputStream(myeFile);

            StringBuffer buffer=new StringBuffer();
            String line;

            File efilePath = new File(tempDir,filename);
            BufferedReader ereader = new BufferedReader(new FileReader(efilePath));
            while ((line = ereader.readLine()) != null){
                buffer.append(line);


            }ereader.close();
            String decrypted=buffer.toString();
            for(int i=0;i<decrypted.length();i++){
                char ch= decrypted.charAt(i);
                ch=(char) (ch-3);
                efos.write(ch);
            }

            efos.close();

        }catch(Exception e){
            e.printStackTrace();
           // Log.d("File creation","failed");
        }

    }



}
