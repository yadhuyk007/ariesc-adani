package combing.sales.adani.gps;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import combing.sales.adani.ariesc.R;
import combing.sales.adani.database.DatabaseHandler;
import combing.sales.adani.objects.Font;
import combing.sales.adani.objects.Stacklogger;


public class    GPSAccuracy extends AppCompatActivity {

    private TextView mTextField;
    private DatabaseHandler db;
    private String mapurl;
    private String mapurloptions;
    private ProgressBar progressBar;
    private Font a;
    private TextView acc;
    private List<Location> loclist;
    private String operation;
    private String LocatioSetFlag = "";
    private JSONObject movedloc = new JSONObject();

    public static float distance(double lat1, double lng1, double lat2,
                                 double lng2) {
        double earthRadius = 6371000; // meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2)
                * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);
        return dist;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gpsaccuracy);
        Thread.setDefaultUncaughtExceptionHandler(new Stacklogger(this));

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},200);
        }
        startTimerService();
        //Bundle bundle = getIntent().getExtras();
        // operation = bundle.getString("operation");
        loclist = new ArrayList<Location>();

        db = new DatabaseHandler(getApplicationContext());
        mapurl = db.getmapurl();
        mapurloptions = db.getmapurloptions();

        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        WebView mWebView = (WebView) findViewById(R.id.map);
        // ActionBar act = getActionBar();
        // act.show();
        // act.setDisplayShowTitleEnabled(false);
        // act.setHomeButtonEnabled(true);
        // act.setDisplayHomeAsUpEnabled(false);
        // act.setIcon(R.drawable.ic_dhi_logo_pad);
        // ColorDrawable colorDrawable = new ColorDrawable(
        // Color.parseColor("#F55A26"));
        // act.setBackgroundDrawable(colorDrawable);

        mWebView.setWebChromeClient(new WebChromeClient() {

            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
                if (progress == 100)
                    progressBar.setVisibility(View.GONE);
            }
        });
        mWebView.setWebViewClient(new WebViewClient());
        String url = "file:///android_asset/mymap.html";
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(url);
        mWebView.addJavascriptInterface(new StringGetter(GPSAccuracy.this),
                "Route");
        a = new Font(GPSAccuracy.this,
                (ViewGroup) findViewById(android.R.id.content), Font.Regular);

        final JSONObject latarray = new JSONObject();
        setcountdown();
        if (locationaccuracy() != 3) {
            /*
             * AlertDialog.Builder builder = Global.Alert(GPSAccuracy.this,
             * "Enable High Location Accuracy",
             * "Currently location accuracy low.");
             * builder.setPositiveButton("OK", new
             * DialogInterface.OnClickListener() { public void
             * onClick(DialogInterface dialog, int which) { startActivity(new
             * Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)); } });
             * AlertDialog dialog = builder.create();
             * dialog.setCanceledOnTouchOutside(false); dialog.show();
             * Global.setDialogFont(GPSAccuracy.this, dialog); return;
             */
            finish();

        }
    }

    //Testing for mock location
    /*public static boolean isMockSettingsON(Context c) {
        // returns true if mock location enabled, false if not enabled.
        if (Settings.Secure.getString(c.getContentResolver(),
                Settings.Secure.ALLOW_MOCK_LOCATION).equals("0"))
            return false;
        else
            return true;
    }*/

    public static boolean isMockLocation(Location location) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2 && location != null && location.isFromMockProvider();
    }

   /* public static boolean areThereMockPermissionApps(Context context) {
        int count = 0;

        PackageManager pm = context.getPackageManager();
        List<ApplicationInfo> packages =
                pm.getInstalledApplications(PackageManager.GET_META_DATA);

        for (ApplicationInfo applicationInfo : packages) {
            try {
                PackageInfo packageInfo = pm.getPackageInfo(applicationInfo.packageName,
                        PackageManager.GET_PERMISSIONS);

                // Get Permissions
                String[] requestedPermissions = packageInfo.requestedPermissions;

                if (requestedPermissions != null) {
                    for (int i = 0; i < requestedPermissions.length; i++) {
                        if (requestedPermissions[i]
                                .equals("android.permission.ACCESS_MOCK_LOCATION")
                                && !applicationInfo.packageName.equals(context.getPackageName())) {
                            count++;
                        }
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                Log.e("Got exception " , e.getMessage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (count > 0)
            return true;
        return false;
    }*/

    private void setcountdown() {
        startTimerService();
        mTextField = (TextView) findViewById(R.id.message);
        acc = (TextView) findViewById(R.id.title);
        // acc.setText("Fetching Location!");
        ((LinearLayout) findViewById(R.id.buttons)).setVisibility(View.GONE);
        ((LinearLayout) findViewById(R.id.buttonsT)).setVisibility(View.GONE);
        ((RelativeLayout) findViewById(R.id.timer)).setVisibility(View.VISIBLE);

        new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
                mTextField.setText("" + millisUntilFinished / 1000);

                LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                boolean statusOfGPS = manager
                        .isProviderEnabled(LocationManager.GPS_PROVIDER);
                if (!statusOfGPS)
                    acc.setText("Turn ON your GPS!");
                else if (locationaccuracy() != 3) {
                    acc.setText("Enable High Location Accuracy!");
                }
                if (locationaccuracy() != 3) {
                    finish();
                }
            }

            public void onFinish() {
                Location loc = findBestLoc();
                Boolean str=false;
                if(loc!=null)
                    str = isMockLocation(loc);
                //Boolean s = areThereMockPermissionApps(getApplicationContext());
                if(str){
                    LinearLayout layout = (LinearLayout) findViewById(R.id.parent);
                    LayoutInflater layoutInflater = (LayoutInflater)GPSAccuracy.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View customView = layoutInflater.inflate(R.layout.alert_for_fake_location, null);

                    TextView msg = (TextView) customView.findViewById(R.id.message);
                    msg.setText("Please disable Mock Location option from Settings and Try Again!");
                    Button closePopupBtn = (Button) customView.findViewById(R.id.close);

                    int width = LinearLayout.LayoutParams.MATCH_PARENT;
                    int height = LinearLayout.LayoutParams.MATCH_PARENT;
                    final PopupWindow popupWindow = new PopupWindow(customView, width, height);
                    popupWindow.setFocusable(true);

                    //display the popup window
                    popupWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
                    popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                    //close the popup window on button click
                    closePopupBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow.dismiss();
                            //super.onBackPressed();
                            acc = (TextView) findViewById(R.id.title);
                            acc.setText("Please Try Again");
                            ((LinearLayout) findViewById(R.id.buttonsT))
                                    .setVisibility(View.VISIBLE);

                            overridePendingTransition(0, 0);
                        }
                    });


                }

                else {
                    mTextField.setText("0");

                    Location locs = findBestLoc();
                    double x =0;
                    if(loc!=null)
                        x = locs.getSpeed();
                    ((RelativeLayout) findViewById(R.id.timer))
                            .setVisibility(View.GONE);
                    if (loc == null) {
                        acc = (TextView) findViewById(R.id.title);
                        acc.setText("Location Not Found!");
                        ((LinearLayout) findViewById(R.id.buttonsT))
                                .setVisibility(View.VISIBLE);
                    }/*
                    else if(x>2){

                        acc = (TextView) findViewById(R.id.title);
                        acc.setText("Retry!");
                        ((LinearLayout) findViewById(R.id.buttonsT))
                                .setVisibility(View.VISIBLE);

                    } */else {
                        ((LinearLayout) findViewById(R.id.buttons))
                                .setVisibility(View.VISIBLE);
                        LocatioSetFlag = "true";
                    }
                }
            }

        }.start();
    }


    private int locationaccuracy() {
        int locationMode = -1;
        try {
            locationMode = Settings.Secure.getInt(this.getContentResolver(),
                    Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return locationMode;
    }

    private Location findBestLoc() {
        Location temp = null;
        if (!(loclist.size() > 0))
            return null;
        HashMap<Integer, Double> hashloc = new HashMap<Integer, Double>();
        for (int i = 0; i < loclist.size(); i++) {
            Location mainloc = loclist.get(i);
            double dist = 0;
            for (int j = 0; j < loclist.size(); j++) {
                Location curloc = loclist.get(j);
                dist += distance(mainloc.getLatitude(), mainloc.getLongitude(),
                        curloc.getLatitude(), curloc.getLongitude());
            }
            hashloc.put(i, dist);
        }
        int minkey = 0;
        double mindistance = 0;
        for (int key : hashloc.keySet()) {
            double dist = hashloc.get(key);
            if (key == 1 || dist < mindistance) {
                mindistance = dist;
                minkey = key;
            }
        }
        return loclist.get(minkey);
    }

    public void tryagain(View v) {

        LocatioSetFlag = "";
        setcountdown();
    }

    public void save(View v) {
        if(movedloc.length() > 0) {
            try {
                String lt = movedloc.getString("lat");
                String lng = movedloc.getString("long");
                Intent returnIntent = new Intent();
                returnIntent.putExtra("latit",  Double.parseDouble(lt));
                returnIntent.putExtra("longit", Double.parseDouble(lng));
                returnIntent.putExtra("minacc", "");
                DateFormat dateTimeFormat = new SimpleDateFormat(
                        "yyyyMMddHHmmss");
                //Date d = new Date("");
                //String gpsTime = dateTimeFormat.format(d);
                returnIntent.putExtra("fixtime", "");
                returnIntent.putExtra("gpsTime", "");
                //Log.e("loc.getTime()", gpsTime);
                returnIntent.putExtra("prov", "gps");
                returnIntent.putExtra("operation", operation);
                setResult(Activity.RESULT_OK, returnIntent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            Location loc = findBestLoc();
            if (loc != null) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("latit", loc.getLatitude());
                returnIntent.putExtra("longit", loc.getLongitude());
                returnIntent.putExtra("minacc", loc.getAccuracy());
                DateFormat dateTimeFormat = new SimpleDateFormat(
                        "yyyyMMddHHmmss");
                Date d = new Date(loc.getTime());
                String gpsTime = dateTimeFormat.format(d);
                returnIntent.putExtra("fixtime", loc.getTime());
                returnIntent.putExtra("gpsTime", gpsTime);
                Log.e("loc.getTime()", gpsTime);
                returnIntent.putExtra("prov", "gps");
                returnIntent.putExtra("operation", operation);
                setResult(Activity.RESULT_OK, returnIntent);
            }
        }
        finish();
    }

    public class StringGetter {
        Context jContext;

        StringGetter(Context context) {
            jContext = context;
        }

        @JavascriptInterface
        public String mapURL() {
            return mapurl;// == null ?
            // "https://maps.dhisigma.com/osm_tiles/{z}/{x}/{y}.png"
            // : mapurl;
        }

        @JavascriptInterface
        public String locationsetfn() {
            return LocatioSetFlag;
        }

        @JavascriptInterface
        public void setLeafLatLong(String lat,String lng) {
            String latlo=lat;
            try {
                movedloc.put("lat",lat);
                movedloc.put("long",lng);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            // return LocatioSetFlag;
        }

        @JavascriptInterface
        public String mapURLOptions() {
            return mapurloptions;// == null ?
            // "{minZoom: 4, maxZoom:18,attribution: '&copy <a href=\"http://openstreetmap.org\">OpenStreetMap</a>'}"
            // : mapurloptions;
        }

        @JavascriptInterface
        public String myLoc() {
            String myloc = "";
            String val = mTextField.getText().toString();
            if (val.contentEquals("0")) {
                return bestlocation();
            }
            Location loc = null;
            if (PlayTracker.getLastLocation() != null) {
                loc = PlayTracker.getLastLocation();
                if (loc.getAccuracy() < 30) {
                    loclist.add(loc);
                }

                myloc = "{\"lat\":\"" + String.valueOf(loc.getLatitude())
                        + "\",\"long\":\"" + String.valueOf(loc.getLongitude())
                        + "\",\"accuracy\":\""
                        + String.valueOf(loc.getAccuracy()) + "\",\"val\":\""
                        + val + "\"}";
                // else
                final float ac = loc.getAccuracy() / 2;
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        // Global.Toast(
                        // RouteMapActivity.this,
                        // "Current location is not set. Please try again.",
                        // Toast.LENGTH_SHORT, Font.Regular);
                        if (ac > 0)
                            acc.setText("You are within: " + ac
                                    + " m.");
                        else
                            acc.setText("Fetching Location!");
                    }
                });
                // Toast.makeText(getApplicationContext(), "myloc" + myloc,
                // Toast.LENGTH_SHORT).show();
            } else {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        acc.setText("Fetching Location!");
                        // Global.Toast(
                        // GPSAccuracy.this,
                        // "Location Unavailable. Check your gps and try again!",
                        // Toast.LENGTH_SHORT, Font.Regular);
                        // finish();
                        // overridePendingTransition(0, 0);

                    }
                });
            }
            return myloc;
        }

        private String bestlocation() {
            Location loc = findBestLoc();
            String myloc = "";
            if (loc != null) {
                myloc = "{\"lat\":\"" + String.valueOf(loc.getLatitude())
                        + "\",\"long\":\"" + String.valueOf(loc.getLongitude())
                        + "\",\"accuracy\":\""
                        + String.valueOf(loc.getAccuracy()) + "\",\"val\":\""
                        + 0 + "\"}";
                // else
                final float ac = loc.getAccuracy() / 2;
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        // Global.Toast(
                        // RouteMapActivity.this,
                        // "Current location is not set. Please try again.",
                        // Toast.LENGTH_SHORT, Font.Regular);
                        if (ac > 0)
                            acc.setText("Please wait. You are within: " + ac
                                    + " m.");
                        else
                            acc.setText("Please wait. Fetching Location!");
                    }
                });

            }
            return myloc;
        }
    }
    private void startTimerService() {
        // Toast.makeText(getApplicationContext(), "startTimerService on sync",
        // Toast.LENGTH_LONG).show();
        // startService(new Intent(this, CopyOfVehicletracker.class));
        // startService(new Intent(this, Vehicletracker.class));
        Calendar cal = Calendar.getInstance();
//        Intent intent = new Intent(CustomerListActivity.this,
//                CopyOfVehicletracker.class);
//        PendingIntent pintent = PendingIntent.getService(
//                CustomerListActivity.this, 0, intent, 0);
//        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
//        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
//                5 * 1000, pintent);

        Intent intentn = new Intent(GPSAccuracy.this, PlayTracker.class);
        PendingIntent pintentn = PendingIntent.getService(
                GPSAccuracy.this, 0, intentn, 0);
        AlarmManager alarmn = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmn.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                1000, pintentn);
    }
}
