package combing.sales.adani.objects;

public class product {

	public String productid;
	public String productname;
	public String productcode;
	public String productdescrip;
	public String weight;
	public String volume;
	public String tax;
	public String unitprice;
	public String mrp;
	public String partyMargin ;

	public String batch;
	public String minQty;
	public String flag ;

	public product() {

	}

	public product(String id, String name, String focused, String mrp,
				   String rate, String partyMargin, String batch, String minQty,String flag,String tax) {
		this.productid = id;
		this.productname = name;
		//this.focused = focused;
		this.mrp = mrp;
		this.unitprice = rate;
		this.partyMargin = partyMargin;
		this.batch = batch;
		this.minQty = minQty;
		this.flag = flag;
		this.tax=tax;
	}

}
