package combing.sales.adani.customer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;

import combing.sales.adani.ariesc.R;
import combing.sales.adani.customerlist.NearestCustomerAdapter;
import combing.sales.adani.database.DatabaseHandler;
//import combing.sales.dhi.ariesc.R;
//import combing.sales.dhi.customerlist.NearestCustomerAdapter;
//import combing.sales.dhi.database.DatabaseHandler;

public class NearestCustomerActivity extends AppCompatActivity {

    String[] custName;
    String[] custCode;
    int[] custDist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearestcustomerview);
        FrameLayout linearLayout = (FrameLayout) findViewById(R.id.custlistlayout);
        /*linearLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent iobj = new Intent(getApplicationContext(), NearestCustomerActivity.class);
                finish();


            }
        });*/
        ImageView close = (ImageView) findViewById(R.id.clse);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent iobj = new Intent(getApplicationContext(), NearestCustomerActivity.class);
                finish();
            }
        });

        /*ActionBar actionBar = getSupportActionBar();
        actionBar.hide();*/
        DatabaseHandler db;
        db = new DatabaseHandler(getApplicationContext());
        RecyclerView recvw = (RecyclerView) findViewById(R.id.NearCustRecycler);

        JSONArray srrlist = new JSONArray();

        try {
            srrlist = db.selectNearcust();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(srrlist != null){
            custName = new String[srrlist.length()];
            custCode = new String[srrlist.length()];
            custDist = new int[srrlist.length()];
            for (int i = 0; i < srrlist.length(); i++) {

                try {


                    custName[i] = srrlist.getJSONObject(i).getString("CustName");
                    custCode[i] = srrlist.getJSONObject(i).getString("CustCode");
                    custDist[i] = srrlist.getJSONObject(i).getInt("Distance");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        NearestCustomerAdapter adapter = new NearestCustomerAdapter(custName,custCode,custDist);
        recvw.setAdapter(adapter);

        recvw.setLayoutManager(new LinearLayoutManager(this));

    }
}
