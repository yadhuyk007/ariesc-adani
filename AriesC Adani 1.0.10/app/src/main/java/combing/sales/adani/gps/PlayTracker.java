package combing.sales.adani.gps;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import combing.sales.adani.database.DatabaseHandler;
import combing.sales.adani.objects.Global;
import combing.sales.adani.objects.Vehicledata;


public class PlayTracker extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    static GoogleApiClient mGoogleApiClient;
    private static DatabaseHandler db;
    LocationRequest mLocationRequest;
    private SharedPreferences pref;

    public PlayTracker() {
        buildGoogleApiClient();
    }

    public static float distance(double lat1, double lng1, double lat2,
                                 double lng2) {
        double earthRadius = 6371000; // meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2)
                * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);
        return dist;
    }

    public static Location getLastLocation() {
        Location loc = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);
        return loc == null ? null : loc;
    }

    public static boolean intime() {
        try {
            Calendar c1 = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String current = sdf.format(c1.getTime());
            Date today = sdf.parse(current);
            long time = PlayTracker.getLastLocation().getTime();
            Date lasts = new Date(time);

            Global.lastSetOn=sdf.format(lasts);

            int secs = (int) ((today.getTime() - lasts.getTime()) / (1000));

            String gpsFixTime = db.getConstantValue("gpsFixTime");
            if (gpsFixTime.contentEquals(""))
                return true;
            else {
                int ft = Integer.parseInt(gpsFixTime);
                //ft=2;
                if (secs < ft)
                    return true;
                else {
                    return false;
                }
            }
        } catch (Exception e) {

            return true;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        //  throw new UnsupportedOperationException("Not yet implemented");
        return null;
    }

    public void buildGoogleApiClient() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {

            @Override
            public void run() {
                //  Toast.makeText(PlayTracker.this.getApplicationContext(), "My Awesome service toast...", Toast.LENGTH_SHORT).show();
                mGoogleApiClient = new GoogleApiClient.Builder(PlayTracker.this.getApplicationContext())
                        .addConnectionCallbacks(PlayTracker.this)
                        .addOnConnectionFailedListener(PlayTracker.this)
                        .addApi(LocationServices.API).build();
                try {
                    mGoogleApiClient.connect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public void onConnected(Bundle bundle) {
        try {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(10000);
            mLocationRequest
                    // .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            pref = getApplicationContext()
                    .getSharedPreferences("Config", MODE_PRIVATE);
            db = new DatabaseHandler(getApplicationContext());
            Calendar c1 = Calendar.getInstance();
            DateFormat sdf2 = new SimpleDateFormat("yyyyMMddHHmmss");
            String cmpDate = sdf2.format(c1.getTime());
            DateFormat dateTimeFormat2 = new SimpleDateFormat("HH:mm");
            String checkDate = dateTimeFormat2.format(c1.getTime());
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String formattedDateT = sdf.format(c1.getTime());
            if (PlayLocation.reallocation == null) {
                PlayLocation.reallocation = location;
                PlayLocation.fixedtime = formattedDateT;
                String PlanId = Integer.parseInt(pref
                        .getString("personid", "0")) + "";
                String VehicleId = "";
                String Latitude = location.getLatitude() + "";
                String Longitude = location.getLongitude() + "";
                String Speed = location.getSpeed() + "";
                String provider = "gps";
                try {
                    float accuracy = location.getAccuracy();
                    String provt = "gps";
                    if (accuracy > 30) {
                        provt = "network";
                    }
                    provider = provt;
                } catch (Exception e) {
                    provider = "network";
                }
                String accuracy = location.getAccuracy() + "";
                String bearing = location.getBearing() + "";
                float distance = 0;
                String customer = "";
                String timediff = "";
                String mongoDate = "";
                boolean foreground = true;
                int inminutes = 0;
                try {
                    if (Global.lastacttime.contentEquals(""))
                        inminutes = timeDiff(cmpDate, cmpDate);
                    else
                        inminutes = timeDiff(cmpDate, Global.lastacttime);
                    if (inminutes > 14)
                        foreground = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                float speed = location.getSpeed();
                Vehicledata Vehdata = new Vehicledata(PlanId, VehicleId,
                        formattedDateT, Latitude, Longitude, Speed, provider,
                        accuracy, bearing, distance + "", customer, timediff,
                        mongoDate, foreground);
                try {
                    if (speed == 0)
                        Vehdata = checkWithCustomerdata(Vehdata);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    checkpassby(Vehdata.Latitude, Vehdata.Longitude);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                long id = db.AddNavigationdata(Vehdata);
                // Toast.makeText(getApplicationContext(), id + "inplaynew",
                // Toast.LENGTH_SHORT).show();
            } else if (PlayLocation.reallocation.getLatitude() != location
                    .getLatitude()
                    || PlayLocation.reallocation.getLongitude() != location
                    .getLongitude()) {
                SharedPreferences pref = getApplicationContext()
                        .getSharedPreferences("Config", MODE_PRIVATE);
                String PlanId = Integer.parseInt(pref
                        .getString("personid", "0")) + "";
                String VehicleId = "";
                String Latitude = location.getLatitude() + "";
                String Longitude = location.getLongitude() + "";
                String Speed = location.getSpeed() + "";
                String provider = "gps";
                try {
                    float accuracy = location.getAccuracy();
                    String provt = "gps";
                    if (accuracy > 30) {
                        provt = "network";
                    }
                    provider = provt;
                } catch (Exception e) {
                    provider = "network";
                }
                String accuracy = location.getAccuracy() + "";
                String bearing = location.getBearing() + "";
                float distance = 0;
                try {
                    distance = distance(
                            PlayLocation.reallocation.getLatitude(),
                            PlayLocation.reallocation.getLongitude(),
                            location.getLatitude(), location.getLongitude());
                } catch (Exception d) {
                }
                String customer = "";
                String timediff = "";
                String mongoDate = "";
                boolean foreground = true;
                int inminutes = 0;
                try {
                    if (Global.lastacttime.contentEquals(""))
                        inminutes = timeDiff(cmpDate, cmpDate);
                    else
                        inminutes = timeDiff(cmpDate, Global.lastacttime);
                    if (inminutes > 14)
                        foreground = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                float speed = location.getSpeed();
                Vehicledata Vehdata = new Vehicledata(PlanId, VehicleId,
                        formattedDateT, Latitude, Longitude, Speed, provider,
                        accuracy, bearing, distance + "", customer, timediff,
                        mongoDate, foreground);
                try {
                    if (speed == 0)
                        Vehdata = checkWithCustomerdata(Vehdata);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    checkpassby(Vehdata.Latitude, Vehdata.Longitude);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                long id = db.AddNavigationdata(Vehdata);
                // Toast.makeText(getApplicationContext(), id + "inplayold",
                // Toast.LENGTH_SHORT).show();
                PlayLocation.reallocation = location;
                PlayLocation.fixedtime = formattedDateT;

            }
            final int navcnt = db.NavDatacounttoSync();
            //Toast.makeText(PlayTracker.this.getApplicationContext(), "service toast..."+navcnt, Toast.LENGTH_SHORT).show();

            if (navcnt > 5)
                SyncNavData.syncnavdata(getApplicationContext());
        } catch (Exception main) {

        }
    }

    private int timeDiff(String current, String prev) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date today = sdf.parse(current);
        Date lasts = sdf.parse(prev);
        int inminutes = (int) ((today.getTime() - lasts.getTime()) / (1000 * 60));
        return inminutes;
    }

    private Vehicledata checkWithCustomerdata(Vehicledata vehdata)
            throws Exception {
        JSONArray custlatlongs = db.getallcustomerlatlongs();
        for (int i = 0; i < custlatlongs.length(); i++) {
            String lat = custlatlongs.getJSONObject(i).optString("lat", "0");
            String lng = custlatlongs.getJSONObject(i).optString("long", "0");
            String customer = custlatlongs.getJSONObject(i).optString(
                    "customer", "");
            double lat1 = Double.parseDouble(vehdata.Latitude);
            double lng1 = Double.parseDouble(vehdata.Longitude);
            double lat2 = Double.parseDouble(lat);
            double lng2 = Double.parseDouble(lng);
            float distance = distance(lat1, lng1, lat2, lng2);
            if (distance < 20)
                vehdata.customer = customer;
            else
                vehdata.customer = "0";
        }
        return vehdata;
    }

    private void checkpassby(String latit, String lngit) throws Exception {
        JSONArray custlatlongs = db.getallcustomerlatlongsnotpass();
        for (int i = 0; i < custlatlongs.length(); i++) {
            String lat = custlatlongs.getJSONObject(i).optString("lat", "0");
            String lng = custlatlongs.getJSONObject(i).optString("long", "0");
            String slineid = custlatlongs.getJSONObject(i).optString("slineid",
                    "");
            double lat1 = Double.parseDouble(latit);
            double lng1 = Double.parseDouble(lngit);
            double lat2 = Double.parseDouble(lat);
            double lng2 = Double.parseDouble(lng);
            float distance = distance(lat1, lng1, lat2, lng2);
            int range = 0;
            try {
                range = Integer.parseInt(db.getConstantValue("passbyRange"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (distance < range) {
                Calendar c = Calendar.getInstance();
                DateFormat dateTimeFormat = new SimpleDateFormat(
                        "yyyyMMddHHmmss");
                String time = dateTimeFormat.format(c.getTime());
                db.updateSlinePassby(slineid, time);
            }
        }
    }
}
