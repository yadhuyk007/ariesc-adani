package combing.sales.adani.objects;

public class Vehicledata {

    public String pkid;
    public String PlanId;
    public String VehicleId;
    public String DeviceTime;
    public String Latitude;
    public String Longitude;
    public String Speed;
    public String provider;
    public String accuracy;
    public String bearing;
    public String distance;
    public String customer;
    public String timediff;
    public String mongoDate;
    public String inactive;

    public Vehicledata() {

    }

    public Vehicledata(String PlanId, String VehicleId, String DeviceTime,
                       String Latitude, String Longitude, String Speed, String provider,
                       String accuracy, String bearing, String distance, String customer,
                       String timediff, String mongoDate, boolean forground) {
	this.PlanId = PlanId;
	this.VehicleId = VehicleId;
	this.DeviceTime = DeviceTime;
	this.Latitude = Latitude;
	this.Longitude = Longitude;
	this.Speed = Speed;
	this.provider = provider;
	this.accuracy = accuracy;
	this.bearing = bearing;
	this.distance = distance;
	this.customer = customer;
	this.timediff = timediff;
	this.mongoDate = mongoDate;
	if (!forground)
	    this.inactive = "1";
	else
	    this.inactive = "0";
    }
}
