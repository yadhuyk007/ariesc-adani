package combing.sales.adani.objects;

import android.app.Activity;
import android.graphics.Typeface;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class Font {
    public static String Italic = "It";
    public static String Bold = "Bold";
    public static String Regular = "Regular";
    public static String Semibold = "Semibold";
    public static String BoldItalic = "BoldIt";
    private SparseArray<TextView> arrays;

    public Font(Activity mAct, ViewGroup mView, String style) {
	arrays = new SparseArray<>();
	findAllEdittexts(mView);
	setfont(mAct, arrays, style);
    }

    public Font(Activity mAct, TextView edittext, String style) {
	try {
	    String fontPath = "fonts/MyriadPro-Regular.otf";
	    switch (style) {
	    case "It":
		fontPath = "fonts/MyriadPro-It.otf";
		break;
	    case "Bold":
		fontPath = "fonts/MyriadPro-Bold.otf";
		break;
	    case "BoldIt":
		fontPath = "fonts/MyriadPro-BoldIt.otf";
		break;
	    case "Semibold":
		fontPath = "fonts/MyriadPro-Semibold.otf";
		break;
	    default:
		fontPath = "fonts/MyriadPro-Regular.otf";
		break;
	    }
	    Typeface m_typeFace = Typeface.createFromAsset(mAct.getAssets(),
		    fontPath);
	    edittext.setTypeface(m_typeFace);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }


    private void findAllEdittexts(ViewGroup viewGroup) {
	int count = viewGroup.getChildCount();
	for (int i = 0; i < count; i++) {
	    View view = viewGroup.getChildAt(i);
	    if (view instanceof ViewGroup)
		findAllEdittexts((ViewGroup) view);
	    else if ((view instanceof TextView) || (view instanceof EditText)) {
		TextView edittext = (TextView) view;
		arrays.put(edittext.getId(), edittext);
	    }
	}

    }

    public static void setfont(Activity mAct, SparseArray<TextView> array,
	    String style) {
	for (int i = 0; i < array.size(); i++) {
	    try {
		TextView edittext = array.valueAt(i);
		String fontPath = "fonts/MyriadPro-Regular.otf";
		switch (style) {
		case "It":
		    fontPath = "fonts/MyriadPro-It.otf";
		    break;
		case "Bold":
		    fontPath = "fonts/MyriadPro-Bold.otf";
		    break;
		case "BoldIt":
		    fontPath = "fonts/MyriadPro-BoldIt.otf";
		    break;
		case "Semibold":
		    fontPath = "fonts/MyriadPro-Semibold.otf";
		    break;
		default:
		    fontPath = "fonts/MyriadPro-Regular.otf";
		    break;
		}

		Typeface m_typeFace = Typeface.createFromAsset(
			mAct.getAssets(), fontPath);
		edittext.setTypeface(m_typeFace);
	    } catch (Exception e) {
		e.printStackTrace();
	    }

	}
    }
}
