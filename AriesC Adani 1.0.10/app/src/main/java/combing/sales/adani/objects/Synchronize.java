package combing.sales.adani.objects;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import combing.sales.adani.database.DatabaseHandler;
import combing.sales.adani.landing.LandingActivity;

import static com.amazonaws.mobile.auth.core.internal.util.ThreadUtils.runOnUiThread;


public class Synchronize {

    private static final int REQUEST_CODE_DATE_SETTING = 0;

    private Context context;

    private boolean refreshCache;

    private DatabaseHandler db;

    private boolean backup;

    private boolean autoSync;


    public Synchronize(Context context, boolean refreshCache,boolean autoSync) {
        this.context = context;
        this.refreshCache = refreshCache;
        db = new DatabaseHandler(context);
        this.autoSync =autoSync;
    }

    public void syncData() throws Exception { //boolean autoSync, Context context
        syncphotos();
        try {
            syncerrors();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        backup = false;

        JSONArray uplatlong = new JSONArray();
        JSONArray newcostomerdata = db.getlocationdata();
        Log.e("newcostomerdata",newcostomerdata.toString());

        JSONArray datadb = new JSONArray();
        JSONArray paymentlist = new JSONArray();
        JSONArray orderpayments = new JSONArray();
        JSONArray unvisitedstores = db.getunvisitedstores(backup);
        JSONArray closingdayupdates = db.getclosingdayupdates(backup);
        JSONArray denominationdata = db.getdenominationdatatatosync(backup);
        JSONArray orderdata = new JSONArray(); // db.getorderdatatosync();
        JSONArray salesreturn = new JSONArray(); // db.getsalesreturndatatosync();
        JSONArray creditnote = new JSONArray(); // db.getcreditnotestosync();
//        JSONArray updatecustomers = db.getupdatecustomerstosync(backup);
        JSONArray updatecustomers = db.getupdatecustomerstosync();
//        JSONArray updatephotos = db.getphotostosync(backup);
        JSONArray cratesdata = new JSONArray();//db.getcratestosync(backup);
        JSONArray updatephotos = db.getphotostosync();
        JSONArray passbyupdated = db.getpassbyupdatedsync(backup);
        JSONArray schedulebegin = db.getschedulebeginsync();
        JSONArray expenseData = new JSONArray();//db.getExpenseDataForSync(backup);

        JSONArray orderDataVansales = new JSONArray();//db.getvansalesdatatosync(backup);
        JSONArray paymentDataVansales = new JSONArray();//db.getvansalespaymentstosync(backup);


        uplatlong = db.getupdatedlaatlong(backup);


        webconfigurration C = new webconfigurration(context);

        String inputParamsStr = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"SalesproOperations\","
                + "\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\","
                + "\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";

        JSONObject inputParamObj = new JSONObject(inputParamsStr);

        JSONObject updatestatus = null;
        try {
            webconfigurration.status = "Sync";
            updatestatus = getstatusobject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        inputParamObj.put("updatestatus", updatestatus);
        SharedPreferences pref = context.getSharedPreferences(
                "Config", Context.MODE_PRIVATE);
        JSONObject data = new JSONObject();
        data.put("operation", "PutOrders").put("data", datadb)
                .put("customers", newcostomerdata)
                .put("updatelatlong", uplatlong).put("payments", paymentlist)
                .put("reasons", unvisitedstores)
                .put("orderamountfromstores", orderpayments)
                .put("closingdayupdates", closingdayupdates)
                .put("Denominationdata", denominationdata)
                .put("OrderTaking", orderdata).put("SalesReturn", salesreturn)
                .put("CreditNotes", creditnote)
                .put("updatecustomers", updatecustomers)
                .put("updatephotos", updatephotos)
                .put("passbyupdated", passbyupdated)
                .put("schedulebegin", schedulebegin);


        inputParamObj.getJSONObject("data").getJSONArray("data").put(data);
        inputParamObj.getJSONObject("data")
                .put("personid", pref.getString("personid", ""))
                .put("PersonOrg", pref.getString("tmsOrgId", ""));
        // .put("InvNoForUpdate",db.getcurrinvoiceval());
        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        inputParamObj.put("auth", auth);


        if (db.getpendingNotApproved()) {
            if (!autoSync) {
                Toast.makeText(context, "Payment Approvals Pending!", Toast.LENGTH_SHORT).show();


            }
            throw new Exception("Payment Approvals Pending!");
         /*   Global .Toast(context,
                    "Payment Approvals Pending!", Toast.LENGTH_SHORT,
                    Font.Regular);*/
           /* if (syncbutton || refreshbutton)
                Global.Toast(activity,
                        "Payment Approvals Pending!", Toast.LENGTH_SHORT,
                        Font.Regular);
            enablesyncbutton();*/

        } else if (db.getPendingOrderPayment() && (!db.isWithoutInventory())) {
            if (!autoSync)
                Toast.makeText(context, "Payments are pending!", Toast.LENGTH_SHORT).show();

            throw new Exception("Payments are pending!");
           /* Global.Toast(context, "Payments are pending!",
                    Toast.LENGTH_SHORT, Font.Regular);*/

            /*if (syncbutton || refreshbutton)
                Global.Toast(DashboardActivity.this, "Payments are pending!",
                        Toast.LENGTH_SHORT, Font.Regular);
            enablesyncbutton();*/

        } else if (db.anypendingorderexist() || uplatlong.length() > 0
                || newcostomerdata.length() > 0 || unvisitedstores.length() > 0
                || closingdayupdates.length() > 0
                || denominationdata.length() > 0
                || denominationdata.length() > 0
                || updatecustomers.length() > 0 || updatephotos.length() > 0
                || cratesdata.length() > 0 || passbyupdated.length() > 0
                || expenseData.length() > 0 || orderDataVansales.length() > 0
                || paymentDataVansales.length() > 0 || schedulebegin.length() > 0) {

            /*if (syncbutton || refreshbutton)
                Global.Toast(DashboardActivity.this, "Syncing...",
                        Toast.LENGTH_SHORT, Font.Regular);*/
            if (!autoSync)
                Toast.makeText(context, "Syncing !!", Toast.LENGTH_SHORT).show();
            uploadOrders(inputParamObj.toString(), refreshCache, C.url);

        } else {
            if (!autoSync) {
                Toast.makeText(context, "All data are synced !!", Toast.LENGTH_SHORT).show();
                if (context instanceof LandingActivity) {

                    ((LandingActivity) context).completeSync();
                }

            }
          /*  Global.Toast(activity, "All data are synced",
                    Toast.LENGTH_SHORT, Font.Regular);*/


            if (refreshCache) {
                pref = context.getSharedPreferences("Config",
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putFloat("distancetravelled", 0);
                editor.commit();
                // stopService(new Intent(this, Vehicletracker.class));
                db.Deletetabledata();
                //   reloaddata(); must be written in separate class.
            }

        }

    }

    private void uploadOrders(String data, boolean refreshCache, String url) {

        // write to sync file in phone memory

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        String synctime = df.format(c.getTime());

        //    if (db.validateCreateTime(synctime)) {

        WriteFile w = new WriteFile();
        w.writeFile("Sync", synctime, data, "AriesBackup");

        SyncThread myRunnable = new SyncThread(data, refreshCache, url, context);
        Thread t = new Thread(myRunnable);
        t.start();
//        } else {
//            Toast.makeText(context,
//                    "Please correct Mobile Date and Time!!", Toast.LENGTH_LONG)
//                    .show();
//
//           /* Intent dateSetttingIntent = new Intent(
//                    android.provider.Settings.ACTION_DATE_SETTINGS);
//            activity.startActivityForResult(dateSetttingIntent,
//                    REQUEST_CODE_DATE_SETTING);*/
//        }


    }


    private JSONObject getstatusobject() throws Exception {
        JSONObject temp = new JSONObject();
        SharedPreferences pref = context.getSharedPreferences(
                "Config", Context.MODE_PRIVATE);

        String person = "";
        String lat = "";
        String lng = "";
        String operation = "";
        String version = "";
        String createdon = "";
        String uuid = "";
        String appname = "";
        String versioncode = "";
        String imei = "";
        String androidId = "";

        person = pref.getString("personid", "0");

        operation = webconfigurration.status;
        appname = webconfigurration.appname;
        lat = webconfigurration.lat;
        lng = webconfigurration.lng;

        PackageInfo pInfo = context.getPackageManager().getPackageInfo(
                context.getPackageName(), 0);
        version = pInfo.versionName;
        versioncode = String.valueOf(pInfo.versionCode);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        createdon = df.format(c.getTime());

        uuid = UUID.randomUUID().toString();
        String battery = batteryLevel();

        try {
            androidId = Settings.Secure.getString(context
                    .getContentResolver(), Settings.Secure.ANDROID_ID);

            TelephonyManager telephonyManager =
                    (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

                imei = "";
                //   return TODO;
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                imei = telephonyManager.getImei();
            }

        } catch (Exception e) {
            if (androidId != null && androidId.equals(""))
                androidId = "NA";

            if (imei != null && imei.equals(""))
                imei = "NA";

            e.printStackTrace();
        }

        temp.put("person", person).put("lat", lat).put("lng", lng)
                .put("operation", operation).put("version", version)
                .put("versioncode", versioncode).put("appname", appname)
                .put("createdon", createdon).put("uuid", uuid)
                .put("battery", battery);

        temp.put("AndroidId", androidId);
        temp.put("IMEI", imei);
        return temp;
    }

    private JSONObject getVanSalesDataForSync(JSONArray orderDataVansales,
                                              JSONArray paymentDataVansales) throws JSONException {
        JSONObject returnObject = new JSONObject();

        webconfigurration C = new webconfigurration(context);

        String inputParamsStr = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"OrderTakingForVanSales\",\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";

        returnObject = new JSONObject(inputParamsStr);

        /*
         * JSONArray orderDataVansales = db.getvansalesdatatosync(); JSONArray
         * paymentDataVansales =db.getvansalespaymentstosync();
         */

        SharedPreferences pref = context.getSharedPreferences(
                "Config", Context.MODE_PRIVATE);
        JSONObject data = new JSONObject();

        data.put("orderData", orderDataVansales).put("payments",
                paymentDataVansales);
        returnObject.getJSONObject("data").getJSONArray("data").put(data);
        returnObject.getJSONObject("data")
                .put("personId", pref.getString("personid", ""))
                .put("personOrg", pref.getString("tmsOrgId", ""))
                .put("InvNoForUpdate", db.getcurrinvoiceval());

        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        returnObject.put("auth", auth);

        Log.d("Data Input:", returnObject.toString());

        return returnObject;
    }


    private String batteryLevel() {
        Intent batteryIntent = context.registerReceiver(null, new IntentFilter(
                Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        if (level == -1 || scale == -1) {
            return String.valueOf(50.0f);
        }

        return String.valueOf(((float) level / (float) scale) * 100.0f);
    }


    private void syncphotos() throws Exception {
        String path = Environment.getExternalStorageDirectory().toString() + "/Aries";
        Log.e("Files", "Path: " + path);
        final File directory = new File(path);

        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                // foreTest();
                final File[] filesRoot = directory.listFiles();
                if (filesRoot != null) {
                    for (int i = 0; i < filesRoot.length; i++) {
                        if (filesRoot[i].getName().contains("draft"))
                            filesRoot[i].delete();
                    }
                    final File[] files = directory.listFiles();
                    final int size = (files == null) ? 0 : files.length;
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            try {
                                if (size > 0) {

                                    JSONArray uploadimages = new JSONArray();
                                    String file = Global.readFromFile(files[0]
                                            .getPath());
                                    String name = files[0].getName();
                                    Log.e("filename",name);
                                    uploadimages.put(new JSONObject().put(
                                            "file", file).put("name", name));
                                    webconfigurration C = new webconfigurration(
                                            context);

                                    String inputParamsStr = "{\"data\": {\"User\": \""
                                            + C.user
                                            + "\",\"DBname\": \""
                                            + C.dbname
                                            + "\", \"BussinessUnit\": \""
                                            + C.bu
                                            + "\","
                                            + "\"Operation\": \"Custom\","
                                            + "\"columnlist\": [],"
                                            + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"SalesproOperations\",\"operationtype\": \"custom\",\"Value\": \"\","
                                            + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                                            + "\"appname\": \""
                                            + C.appid
                                            + "\","
                                            + "\"data\": [],"
                                            + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                                            + C.orgid
                                            + "\"},"
                                            + "\"meta\": []}";

                                    JSONObject inputParamObj = new JSONObject(
                                            inputParamsStr);

                                    JSONObject updatestatus = null;
                                    try {
                                        webconfigurration.status = "Sync";
                                        updatestatus = getstatusobject();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    inputParamObj.put("updatestatus",
                                            updatestatus);
                                    SharedPreferences pref = context
                                            .getSharedPreferences("Config",
                                                    Context.MODE_PRIVATE);
                                    JSONObject data = new JSONObject();
                                    data.put("operation", "UploadImages").put(
                                            "uploadimages", uploadimages);
                                    inputParamObj.getJSONObject("data")
                                            .getJSONArray("data").put(data);
                                    inputParamObj
                                            .getJSONObject("data")
                                            .put("personid",
                                                    pref.getString("personid",
                                                            ""))
                                            .put("PersonOrg",
                                                    pref.getString("tmsOrgId",
                                                            ""));
                                    String auth = webconfigurration.auth;
                                    auth = Base64.encodeToString(
                                            auth.getBytes(), Base64.NO_WRAP);
                                    inputParamObj.put("auth", auth);
                                    new ImageSyncOperation()
                                            .execute(inputParamObj);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

        }, 0, 10000);

        // Log.d("Files", "Size: " + files.length);
        // for (int i = 0; i < files.length; i++) {
        // Log.d("Files", "FileName:" + files[i].getName());
        // }

    }public class SyncThread implements Runnable {

        private String dataString;
        private boolean refreshCache = false;
        private String url = "";
        private Context context;

        public SyncThread(String dataString, boolean refreshCache, String url, Context context) {
            this.dataString = dataString;
            this.refreshCache = refreshCache;
            this.url = url;
            this.context = context;
        }

        @Override
        public void run() {


            try {
                URL connUrl = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-16");
                conn.setRequestProperty("Accept-Encoding", "identity");

                // conn.setReadTimeout(60000);
                conn.setConnectTimeout(10000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);

                conn.setRequestMethod("POST");
                // conn.setRequestProperty("Connection", "Keep-Alive");
                // conn.setRequestProperty("Cache-Control", "no-cache");

                // getting syncevent trigger time
                Calendar c = Calendar.getInstance();
                DateFormat dateTimeFormat = new SimpleDateFormat(
                        "yyyyMMddHHmmss");
                String formattedDate = dateTimeFormat.format(c.getTime());

                int totalLength = (dataString.getBytes("utf-16").length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.write(dataString.getBytes("utf-16"));
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                String responsefrom = response.toString();
                JSONObject resp = new JSONObject(responsefrom);

                responsefrom = resp.optString("data", "No response data");


                rd.close();

                if (responsefrom.equals("Success")) {

                    DatabaseHandler db = new DatabaseHandler(
                            context);
                    db.updatesynctable(formattedDate);
                    db.updatesynctablelocationsync(formattedDate);

                    if (refreshCache) {

                        db.Deletetabledata();
                        //  reloaddata();
                    }

                    ((Activity) context).runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            Toast.makeText(((Activity) context), " Syncing Completed !!",
                                    Toast.LENGTH_SHORT).show();

                            if (context instanceof LandingActivity) {

                                ((LandingActivity) context).completeSync();
                            }

                        }
                    });

                }

                if (conn != null) {
                    conn.disconnect();
                }


            } catch (Exception e) {
                Log.d("post", "Cannot create connection");
                e.printStackTrace();

                //enablesyncbutton();
                ((Activity) context).runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        Toast.makeText(((Activity) context), "Cannot connect to server !!",
                                Toast.LENGTH_SHORT).show();


                        if (context instanceof LandingActivity) {

                            ((LandingActivity) context).completeSync();
                        }

                    }
                });

            }


        }
    }

    private class ImageSyncOperation extends
            AsyncTask<JSONObject, Void, String> {
        protected void onPreExecute() {
            Global.onSync = true;
        }

        ;

        @Override
        protected String doInBackground(JSONObject... credentials) {
            String responsefrom = null;
            webconfigurration C = new webconfigurration(context);
            String targetURL = C.url;
            try {

                Log.e("req",credentials[0].toString());
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-16");
                conn.setRequestProperty("Accept-Encoding", "identity");

                conn.setConnectTimeout(100000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setReadTimeout(200000);
                conn.setRequestMethod("POST");

                int totalLength = (credentials[0].toString().getBytes("utf-16").length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.write(credentials[0].toString().getBytes("utf-16"));
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                responsefrom = response.toString();
                Log.e("respns",responsefrom);
                rd.close();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("ImgError",e.getMessage());

            }
            return responsefrom;
        }

        @Override
        protected void onPostExecute(final String result) {
            super.onPostExecute(result);
            Global.onSync = false;
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    try {
                        Log.e("reply", result);
                        JSONObject reply = new JSONObject(result);
                        if (reply.getString("Status").contentEquals("Success")) {
                            JSONArray ids = reply.getJSONArray("ids");
                            for (int i = 0; i < ids.length(); i++) {
                                String filename = ids.getString(i);
                                String path = Environment
                                        .getExternalStorageDirectory()
                                        .toString()
                                        + "/Aries";
                                File file = new File(path, filename);
                                boolean deleted = file.delete();
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

    }
    private void syncerrors() throws Exception {

        String path = Environment.getExternalStorageDirectory().toString()
                + "/AriesErrors";
        Log.d("Files", "Path: " + path);
        final File directory = new File(path);

        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                final File[] filesRoot = directory.listFiles();
                if (filesRoot != null) {
                    for (int i = 0; i < filesRoot.length; i++) {
                        if (filesRoot[i].getName().contains("draft"))
                            filesRoot[i].delete();
                    }
                    final File[] files = directory.listFiles();
                    final int size = (files == null) ? 0 : files.length;
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            try {
                                if (size > 0 && !Global.onSync) {

                                    JSONArray uploadimages = new JSONArray();
                                    String file = Global
                                            .readFromTextFile(files[0]
                                                    .getPath());
                                    String name = files[0].getName();
                                    uploadimages.put(new JSONObject().put(
                                            "file", file).put("name", name));
                                    webconfigurration C = new webconfigurration(
                                            context);

                                    String inputParamsStr = "{\"data\": {\"User\": \""
                                            + C.user
                                            + "\",\"DBname\": \""
                                            + C.dbname
                                            + "\", \"BussinessUnit\": \""
                                            + C.bu
                                            + "\","
                                            + "\"Operation\": \"Custom\","
                                            + "\"columnlist\": [],"
                                            + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"SalesproOperations\",\"operationtype\": \"custom\",\"Value\": \"\","
                                            + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                                            + "\"appname\": \""
                                            + C.appid
                                            + "\","
                                            + "\"data\": [],"
                                            + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                                            + C.orgid
                                            + "\"},"
                                            + "\"meta\": []}";

                                    JSONObject inputParamObj = new JSONObject(
                                            inputParamsStr);

                                    JSONObject updatestatus = null;
                                    try {
                                        webconfigurration.status = "Sync";
                                        updatestatus = getstatusobject();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    inputParamObj.put("updatestatus",
                                            updatestatus);
                                    SharedPreferences pref = context
                                            .getSharedPreferences("Config",
                                                    Context.MODE_PRIVATE);
                                    JSONObject data = new JSONObject();
                                    data.put("operation", "ErrorLog").put(
                                            "uploadimages", uploadimages);
                                    inputParamObj.getJSONObject("data")
                                            .getJSONArray("data").put(data);
                                    inputParamObj
                                            .getJSONObject("data")
                                            .put("personid",
                                                    pref.getString("personid",
                                                            ""))
                                            .put("PersonOrg",
                                                    pref.getString("tmsOrgId",
                                                            ""));
                                    String auth = webconfigurration.auth;
                                    auth = Base64.encodeToString(
                                            auth.getBytes(), Base64.NO_WRAP);
                                    inputParamObj.put("auth", auth);
                                    new ErrorSyncOperation()
                                            .execute(inputParamObj);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }

        }, 0, 90000);

    }
    private class ErrorSyncOperation extends
            AsyncTask<JSONObject, Void, String> {
        protected void onPreExecute() {
            Global.onSync = true;
        };

        @Override
        protected String doInBackground(JSONObject... credentials) {
            String responsefrom = null;
            webconfigurration C = new webconfigurration(context);
            String targetURL = C.url;
            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-16");
                conn.setRequestProperty("Accept-Encoding", "identity");

                conn.setConnectTimeout(100000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setReadTimeout(200000);
                conn.setRequestMethod("POST");

                int totalLength = (credentials[0].toString().getBytes("utf-16").length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.write(credentials[0].toString().getBytes("utf-16"));
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                responsefrom = response.toString();
                rd.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responsefrom;
        }

        @Override
        protected void onPostExecute(final String result) {
            super.onPostExecute(result);
            Global.onSync = false;
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    try {
                        Log.e("reply", result);
                        JSONObject reply = new JSONObject(result);
                        if (reply.getString("Status").contentEquals("Success")) {
                            JSONArray ids = reply.getJSONArray("ids");
                            for (int i = 0; i < ids.length(); i++) {
                                String filename = ids.getString(i);
                                String path = Environment
                                        .getExternalStorageDirectory()
                                        .toString()
                                        + "/AriesErrors";
                                File file = new File(path, filename);
                                boolean deleted = file.delete();
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

    }
}
