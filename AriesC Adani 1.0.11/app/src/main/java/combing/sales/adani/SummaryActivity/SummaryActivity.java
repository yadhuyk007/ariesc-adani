package combing.sales.adani.SummaryActivity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import combing.sales.adani.ariesc.R;
import combing.sales.adani.database.DatabaseHandler;
import combing.sales.adani.landing.LandingActivity;
import combing.sales.adani.objects.webconfigurration;

public class SummaryActivity extends AppCompatActivity {

    private DatabaseHandler db;
    private String arcollected;
    private SharedPreferences pref;
    TextView masalaper;
    TextView stpowper;
    TextView otherper;
    TextView distancetraveled;
    TextView qtytarget;
    TextView achieved;
    LinearLayout layout_masalaPer;
    LinearLayout layout_stpowPer;
    LinearLayout layout_otherPer;
    String Rupeessymbol = "\u20B9";
    private JSONArray custCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);

        db = new DatabaseHandler(getApplicationContext());
        pref = getApplicationContext()
                .getSharedPreferences("Config", MODE_PRIVATE);
        try {
            setvalues();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void setvalues() throws ParseException {
        JSONObject combingcount =new JSONObject();
        JSONObject summarydata = new JSONObject();
        JSONObject custsummarydata = new JSONObject();
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        webconfigurration w = new webconfigurration(getApplicationContext());
        try {
            summarydata = db.getworksummary();
        } catch (Exception e) {
            e.printStackTrace();
            summarydata = new JSONObject();
        }
        try {
            custsummarydata = db.csummary();
        } catch (Exception e) {
            e.printStackTrace();
            custsummarydata = new JSONObject();
        }
        combingcount=db.getcombingcount();





        int totalstores = summarydata
                .optInt("tottalnumberofstores", 0);
        int visited = summarydata
                .optInt("visitedstores", 0);
        int rejected=totalstores-visited;

        int totalcount=combingcount.optInt("count",0);
        int totalcombed=combingcount.optInt("combed",0);
        int totaldays=combingcount.optInt("Days",0);

//        String TotalCombed=String.valueOf(Integer.parseInt(totalcombed)-Integer.parseInt(rejected));
        if(totalcount>0 && totaldays>0){
            int TotalRejected= totalcount-totalcombed;
            ((TextView) findViewById(R.id.totalrejected)).setText(String.valueOf(TotalRejected));
            int avgcust=totalcount/totaldays;
            ((TextView) findViewById(R.id.avgcustcount)).setText(String.valueOf(avgcust));

        }

        ((TextView) findViewById(R.id.rejectedtoday)).setText(String.valueOf(rejected));
        ((TextView) findViewById(R.id.combedtoday)).setText(String.valueOf(visited));
        ((TextView) findViewById(R.id.totalcombedcount)).setText(String.valueOf(totalcombed));
        ((TextView) findViewById(R.id.daysworked)).setText(String.valueOf(totaldays));


        String scheduledate = pref.getString("Schbegintime", "");
        Date datsched = new Date();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date curr = new Date();
        long diff = 0;
        long hours_tottal = 0;
        long mins_tottal = 0;
        int progress1;
        ProgressBar mProgress1=(ProgressBar) findViewById(R.id.performance_progress_bar);
        if (!scheduledate.contentEquals("")) {
            datsched = dateTimeFormat.parse(scheduledate);
            diff = curr.getTime() - datsched.getTime();
            hours_tottal = diff / (60 * 60 * 1000);
            mins_tottal = diff / (60 * 1000) % 60;
        }
        String sched_time = String.valueOf(hours_tottal) + "h "
                + String.valueOf(mins_tottal) + "min";
        ((TextView) findViewById(R.id.worktime)).setText(sched_time);

        // updating working hours in progrss bar and textview
        final String scheduledate_final = pref
                .getString("Schbegintime", "");
        double worktimemax = (((hours_tottal * 60) + mins_tottal) / (8.0 * 60.0)) * 100.0;
        progress1 = (int) worktimemax;
        animateProgress(mProgress1, progress1);
    }

    private void animateProgress(ProgressBar mProgress, int i) {
        ObjectAnimator animation = ObjectAnimator.ofInt(mProgress, "progress",
                0, i);
        animation.setDuration(800);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
    }

    @Override
    public void onBackPressed() {

        Intent intent=new Intent(SummaryActivity.this, LandingActivity.class);
        finish();
        startActivity(intent);
        super.onBackPressed();
    }

    public static double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}
