package combing.sales.adani.customerlist;


import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import combing.sales.adani.SummaryActivity.SummaryActivity;
import combing.sales.adani.about.AboutActivity;
import combing.sales.adani.ariesc.LoginActivity;
import combing.sales.adani.ariesc.R;
import combing.sales.adani.customer.NewCustomerActivity;
import combing.sales.adani.database.DatabaseHandler;
import combing.sales.adani.download.DownloadActivity;
import combing.sales.adani.gps.CopyOfVehicletracker;
import combing.sales.adani.gps.PlayTracker;
import combing.sales.adani.landing.LandingActivity;
import combing.sales.adani.objects.Font;
import combing.sales.adani.objects.FourStrings;
import combing.sales.adani.objects.Global;
import combing.sales.adani.objects.Schedule;
import combing.sales.adani.objects.Stacklogger;
import combing.sales.adani.objects.SupportFunctions;
import combing.sales.adani.objects.webconfigurration;
import combing.sales.adani.resetpassword.ResetPasswordActivity;


public class CustomerListActivity extends AppCompatActivity implements CustListRecyclerViewAdapter.ItemClickListener, View.OnClickListener  {
    private Menu menu;
	private static String salespersonid = "";
    private static String salespersonname = "";
    private static String Scheduledate = "0";
    CustListRecyclerViewAdapter adapter;
    FloatingActionButton fab1;
    LinearLayout fabLayoutNewCust;
    LinearLayout fabLayoutCustSrch;
    private boolean isFABOpen = false;
    private ScheduleAdapter mAdapter;
    private RecyclerView recyclerView;
    private ScheduledRouteObject[] elemts;
    private ScheduledRouteObject[] scheduledata;
    private DatabaseHandler db;
    private TextView searchText;
    private ImageView srchbtn;
    private SharedPreferences pref;
    private Spinner schedulespinner;
    ArrayList<FourStrings> schdlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule_customerlist_main);

        Thread.setDefaultUncaughtExceptionHandler(new Stacklogger(this));

//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        menu = navigationView.getMenu();
//        View header = navigationView.getHeaderView(0);
        db = new DatabaseHandler(getApplicationContext());
        pref = getApplicationContext()
                .getSharedPreferences("Config",
                        MODE_PRIVATE);
        String salespersonsync = db.getrecentsynctime();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            Date temp = dateTimeFormat.parse(salespersonsync);
            dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            salespersonsync = dateTimeFormat.format(temp);
        } catch (ParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
//        TextView txt = header.findViewById(R.id.textdsename);
//        txt.setText(pref.getString("personname", ""));
//        txt = header.findViewById(R.id.texttype);
//        txt.setText(pref.getString("personrole", ""));
//        txt = header.findViewById(R.id.textcode);
//        txt.setText(pref.getString("pesronorg", ""));
//        txt = header.findViewById(R.id.textdate);
//        txt.setText(salespersonsync);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);


//        if (ContextCompat.checkSelfPermission(CustomerListActivity.this,
//                Manifest.permission.ACCESS_FINE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            ActivityCompat.requestPermissions(CustomerListActivity.this,
//                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 200);
//        }
//
//
//
//        if (ContextCompat.checkSelfPermission(CustomerListActivity.this,
//                Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            ActivityCompat.requestPermissions(CustomerListActivity.this,
//                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1888);
//        }
//
//        if (ContextCompat.checkSelfPermission(CustomerListActivity.this,
//                Manifest.permission.READ_PHONE_STATE)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            ActivityCompat.requestPermissions(CustomerListActivity.this,
//                    new String[]{Manifest.permission.READ_PHONE_STATE},112);
//        }



        // The request code used in ActivityCompat.requestPermissions()
// and returned in the Activity's onRequestPermissionsResult()
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.CAMERA
        };

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }


        startTimerService();
        schedulespinner=(Spinner)findViewById(R.id.scheduledropdown);
        searchText = (TextView) findViewById(R.id.searchText);
        initializeschedulespinner();
        schedulespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                searchText.setText("");
                Global.spinnerpos=position;
                setList(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        schedulespinner.setSelection(Global.spinnerpos);
        String scheduleid = db.getactualscheduleid();
        String beginTime = db.getscheduleheaderbegintime(scheduleid);
        try {
            //Toast.makeText(CustomerListActivity.this, searchText.getText().toString()+schedulespinner.getSelectedItem().toString()+" three", Toast.LENGTH_SHORT).show();
            elemts = getScheduledRoutes("",schedulespinner.getSelectedItem().toString());
            //elemts = getScheduledRoutes("","SELECT");
            // mExpListAdapter = new ExpListAdapter(getApplicationContext(),
            // elemts, mArrChildelements, this);
            loadCustList(elemts);

            //check gps on
            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            boolean statusOfGPS = manager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!statusOfGPS) {

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        CustomerListActivity.this);
                builder.setTitle(R.string.switch_on_gps);
                builder.setMessage(R.string.gps_off_alert_message);
                builder.setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                startActivityForResult(
                                        new Intent(
                                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                        0);
                            }
                        });
                builder.show();
                return;
            }


            if (beginTime == null) {
                String schDate = db.GetCurrentScheduledate();
                Calendar c = Calendar.getInstance();
                dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd",
                        Locale.US);
                String currenttime = dateTimeFormat.format(c.getTime());
//                if (currenttime.contentEquals(schDate)) {
//                    //final LinearLayout schbgnpopup= (LinearLayout) findViewById(R.id.testid);
//                    final DrawerLayout schbgnpopup = (DrawerLayout) findViewById(R.id.drawer_layout);
//                    LayoutInflater layoutInflater = (LayoutInflater) CustomerListActivity.this.getSystemService(LAYOUT_INFLATER_SERVICE);
//                    View customView = layoutInflater.inflate(R.layout.popuplayout, null);
//                    Button closePopupBtn = (Button) customView.findViewById(R.id.close);
//                    Button confirm = (Button) customView.findViewById(R.id.yes);
//                    TextView title = (TextView) customView.findViewById(R.id.header);
//                    title.setText(R.string.begin_schedule);
//                    TextView message = (TextView) customView.findViewById(R.id.message);
//                    message.setText(R.string.begin_schedule_message);

                //instantiate popup window

//                    int width = LinearLayout.LayoutParams.MATCH_PARENT;
//                    int height = LinearLayout.LayoutParams.MATCH_PARENT;
//                    final PopupWindow popupWindow = new PopupWindow(customView, width, height);
//                    // popupWindow.showAtLocation(customView , Gravity.CENTER, 0, 0);
//
//                    //display the popup window
//                    // popupWindow.showAtLocation(schbgnpopup, Gravity.CENTER, 0, 0);
//
//                    new Handler().postDelayed(new Runnable() {
//
//                        public void run() {
//                            //popupWindow.showAtLocation(CustomerListActivity.this.getWindow().getDecorView(), Gravity.CENTER,0,0);
//                            popupWindow.showAtLocation(schbgnpopup, Gravity.CENTER, 0, 0);
//                        }
//
//                    }, 600L);
//                    popupWindow.setFocusable(true);
//                    popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//                    //schbgnpopup.setAlpha(0.5F);
//                    //close the popup window on button click
//                    closePopupBtn.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            popupWindow.dismiss();
//                            return;
//                        }
//                    });
//                    confirm.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            popupWindow.dismiss();
//                            Calendar c = Calendar.getInstance();
//                            DateFormat dateTimeFormat = new SimpleDateFormat(
//                                    "yyyyMMddHHmmss", Locale.US);
//                            DateFormat dateTimeFormat_readable = new SimpleDateFormat(
//                                    "dd/MM/yyyy HH:mm:ss", Locale.US);
//                            String currenttime = dateTimeFormat.format(c
//                                    .getTime());
//                            String readabletime = dateTimeFormat_readable
//                                    .format(c.getTime());
//
//                            db.updatescheduleheaderbegintime(currenttime);
//
//
//                            db.intialisesynctable(currenttime);
//
//                            SharedPreferences.Editor editor = pref.edit();
//                            editor.putString("Schbegintime", currenttime);
//                            editor.commit();
//                            Toast.makeText(CustomerListActivity.this,R.string.schedule_started, Toast.LENGTH_SHORT).show();
//                            /*Global.Toast(CustomerListActivity.this,
//                                    "Schedule Started!",
//                                    Toast.LENGTH_SHORT, Font.Regular);*/
//                            finish();
//                            startActivity(getIntent());
//                            overridePendingTransition(0,0);
//                            return;
//
//
//
//                        }
//                    });
//
//                }
//            }//beginTime == null end

            }
        } catch (Exception e2) {
            // TODO Auto-generated catch block

            e2.printStackTrace();
        }
//        NavigationView mNavigationView = (NavigationView) findViewById(R.id.nav_view);
//        if (mNavigationView != null) {
//            mNavigationView.setNavigationItemSelectedListener(this);
//        }


        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;


        fabLayoutNewCust = (LinearLayout) findViewById(R.id.fabLayoutNewCust);
        fabLayoutCustSrch = (LinearLayout) findViewById(R.id.fabLayoutCustSrch);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseHandler db = new DatabaseHandler(
                        getApplicationContext());
                String status = db.getschedulestatus();
                if (status.equals("Completed")) {
                    Toast.makeText(getApplicationContext(), R.string.schedules_completed, Toast.LENGTH_SHORT).show();
                    return;
                }
                String scheduleid = db.getactualscheduleid();
                String begintime = db.getscheduleheaderbegintime(scheduleid);
                if (begintime == null) {
                    String schDate = db.GetCurrentScheduledate();
                    Calendar c = Calendar.getInstance();
                    DateFormat dateTimeFormat = new SimpleDateFormat(
                            "yyyy-MM-dd", Locale.US);
                    String currenttime = dateTimeFormat.format(c.getTime());
                    if (!currenttime.contentEquals(schDate)) {
                        Toast.makeText(CustomerListActivity.this,
                                R.string.not_allowed_to_start_future_date_schedule, Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                if (!isFABOpen) {
                    showFABMenu();
                } else {
                    closeFABMenu();
                }

            }
        });
        FloatingActionButton fabNewCust = (FloatingActionButton) findViewById(R.id.fabNewCust);
        fabNewCust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CustomerListActivity.this,
                        NewCustomerActivity.class);
                startActivity(i);
                finish();
            }
        });
//        FloatingActionButton fabCustSrch = (FloatingActionButton) findViewById(R.id.fabCustSrch);
//        fabCustSrch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(CustomerListActivity.this,
//                        SearchCustomerOffRouteActivity.class);
//                startActivity(i);
//                finish();
//            }
//        });
//search fn
        searchText = (TextView) findViewById(R.id.searchText);

        srchbtn = (ImageView) findViewById(R.id.srchbtn);
        srchbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchText.setText("");

            }
        });

        searchText.addTextChangedListener(new TextWatcher() {


            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (s.length() > 0) {
                    srchbtn.setImageResource(R.drawable.ic_dhi_deletetextn);
                } else {
                    srchbtn.setImageResource(R.drawable.ic_dhi_new_search);
                }
                //populateCustList(s.toString());
                try {
                    //schedulespinner.setSelection(0);
                    String spintext=schedulespinner.getSelectedItem().toString();
                    //Toast.makeText(CustomerListActivity.this, s.toString()+spintext+" one", Toast.LENGTH_SHORT).show();
                    elemts = getScheduledRoutes(s.toString(),spintext);
                    loadCustList(elemts);
                } catch (Exception e2) {
                    e2.printStackTrace();

                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }
    private void setList(int position){
        String pos=schdlist.get(position).mrp;
        Global.schedule=schdlist.get(position).qty;
        try {
            //Toast.makeText(CustomerListActivity.this, searchText.getText().toString()+pos+" two", Toast.LENGTH_SHORT).show();
            elemts = getScheduledRoutes("",pos);
            loadCustList(elemts);
        }catch (Exception e){
            e.printStackTrace();
        }
        //schdlist = db.getSchedulesIdsForSpinner();

    }

    private  void initializeschedulespinner(){

        schdlist = db.getSchedulesIdsForSpinner();
        ArrayList<String> schdlnames = new ArrayList<String>();
        for(int i=0;i<schdlist.size();i++){
            schdlnames.add(schdlist.get(i).mrp);//second string name
        }
//        ArrayAdapter<String> spinadapter =
//                new ArrayAdapter<String>(getApplicationContext(),  R.layout.simple_spinner_dropdown_item, schdlnames);
//        spinadapter.setDropDownViewResource( R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<String> spinadapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item, schdlnames) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView tv = (TextView) super.getView(position, convertView,
                        parent);
                try {
                    tv.setTextColor(Color.parseColor("#000000"));
                    tv.setTextSize(9);

                    String fontPath = "fonts/segoeui.ttf";
                    Typeface m_typeFace = Typeface.createFromAsset(CustomerListActivity.this.getAssets(), fontPath);
                    tv.setTypeface(m_typeFace);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return tv;
            }

        };
        spinadapter.setDropDownViewResource(R.layout.spinner_additem_dropdown);
        schedulespinner.setAdapter(spinadapter);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu, menu);
//        return super.onCreateOptionsMenu(menu);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case R.id.action_create_alarm:
                openorclosenavmenu();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openorclosenavmenu() {

        DrawerLayout DrawerLayout = (android.support.v4.widget.DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView nav = (NavigationView) findViewById(R.id.nav_view);
        if (nav.isShown())
            DrawerLayout.closeDrawers();
        else
            DrawerLayout.openDrawer(nav);
    }

    private void showFABMenu() {
        isFABOpen = true;
        fabLayoutNewCust.setVisibility(View.VISIBLE);
        fabLayoutNewCust.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        fabLayoutCustSrch.setVisibility(View.VISIBLE);
        fabLayoutCustSrch.animate().translationY(-getResources().getDimension(R.dimen.standard_105));
    }

    private void closeFABMenu() {
        isFABOpen = false;
        fabLayoutNewCust.animate().translationY(0);
        fabLayoutNewCust.setVisibility(View.GONE);
        fabLayoutCustSrch.animate().translationY(0);
        fabLayoutCustSrch.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(View view, int position) {
    //    Toast.makeText(this, "You clicked " + adapter.getItem(position) + " on row number " + position, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {


    }



    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        DrawerLayout schbgnpopup = (DrawerLayout) findViewById(R.id.drawer_layout);
        Intent intent;
        switch (id) {
//            case R.id.nav_inoutward:
//                Intent intent = new Intent(CustomerListActivity.this, InwardOutwardActivity.class);
//                startActivity(intent);
//                finish();
//                schbgnpopup.closeDrawers();
//                return true;
//            case R.id.nav_expense:
//                schbgnpopup.closeDrawers();
//                intent = new Intent(CustomerListActivity.this, ExpenseActivity.class);
//                finish();
//                startActivity(intent);
//                overridePendingTransition(0, 0);
//                return true;
//            case R.id.nav_dsalesrpt:
//                schbgnpopup.closeDrawers();
//                intent = new Intent(CustomerListActivity.this, DailySalesReportActivity.class);
//                finish();
//                startActivity(intent);
                //Toast.makeText(getApplicationContext(), "nav_dsalesrpt", Toast.LENGTH_SHORT).show();
//                return true;
//            case R.id.nav_stockrpt:
//                intent = new Intent(CustomerListActivity.this, StockReportActivity.class);
//                startActivity(intent);
//                finish();
                //Toast.makeText(getApplicationContext(), "nav_stockrpt", Toast.LENGTH_SHORT).show();
//                return true;
//            case R.id.nav_prdctsalesrpt:
//                intent = new Intent(CustomerListActivity.this, ProductSalesReportActivity.class);
//                startActivity(intent);
//                finish();
                //Toast.makeText(getApplicationContext(), "nav_prdctsalesrpt", Toast.LENGTH_SHORT).show();
//                return true;
            case R.id.nav_dashboard:
                intent = new Intent(CustomerListActivity.this, SummaryActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
                return true;
            case R.id.nav_refreshcache:
                //refreshcachedata();
                return true;
            case R.id.nav_fclrdata:
                Global.force = true;
                forcecleardata();
                return true;
            case R.id.nav_chkupdate:
                Toast.makeText(getApplicationContext(), R.string.checking_new_version_available, Toast.LENGTH_SHORT).show();
               /* intent = new Intent(
                        CustomerListActivity.this,
                        DownloadActivity.class);
                startActivity(intent);*/
                checkversion();
                return true;
            case R.id.nav_resetpswd:
                intent = new Intent(CustomerListActivity.this, ResetPasswordActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
//                Toast.makeText(getApplicationContext(), "nav_resetpswd", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.nav_about:
                Intent intent2 = new Intent(CustomerListActivity.this, AboutActivity.class);
                startActivity(intent2);
                finish();
                overridePendingTransition(0, 0);
                return true;
            case R.id.nav_logout:
                confirnlogout();
                return true;
//            case R.id.nav_completeschedl:
//                try {
//                    if (scheduledata.length <= 0) {
//                        Toast.makeText(CustomerListActivity.this,R.string.no_schedules_available, Toast.LENGTH_SHORT).show();
//                        return true;
//                    }
//                    completeschedule();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                return true;
            case R.id.nav_sync:
                //syncData(menuItem);
                return true;

//            case R.id.nav_multilingual:
////                intent = new Intent(CustomerListActivity.this, MultiLanguage.class);
////                startActivity(intent);
////                finish();
////                overridePendingTransition(0, 0);
////                Toast.makeText(getApplicationContext(), "nav_resetpswd", Toast.LENGTH_SHORT).show();
//                return true;

        }
        return false;
    }

    public void forcecleardata() {


        final AlertDialog.Builder builder = new AlertDialog.Builder(
                CustomerListActivity.this);

        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input
        // as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT
                | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        builder.setView(input);
        builder.setTitle("Are you sure you want to clear all data?");
        builder.setMessage("All unsynced data will be lost these data cannot be recovered."
                + "please provide your password");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String userinput = input.getText().toString();
                SharedPreferences pref = getApplicationContext()
                        .getSharedPreferences("Config", MODE_PRIVATE);
                String orginalpassword = pref.getString("passcode", "");

                if (userinput.equals(orginalpassword)) {
                    confirnlogout();

                } else {
                    Global.Toast(CustomerListActivity.this, "Wrong Password",
                            Toast.LENGTH_SHORT, Font.Regular);

                }

            }
        });

        builder.setNeutralButton("CANCEL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                });

        builder.show();


    }

    public void backupData() {

        JSONObject data = new JSONObject();

        try {

            DatabaseHandler db = new DatabaseHandler(getApplicationContext());

            if (db.isserviceexist("Van Sales")) {

                data = getBackUpDataForVanSales();

            } else {

                data = getBackUpForOrderTaking();

            }

            //			confirmlogout();

           // new BackupOperation().execute(data);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();

        }
    }

    // /upload locations
    public JSONArray uploadlocations() {

        String data = "";
        db = new DatabaseHandler(getApplicationContext());

        JSONArray datatoupload = new JSONArray();
        try {
            datatoupload = db.getlocationdata();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return datatoupload;
        // uploadorders(data);

    }

    private JSONObject getBackUpForOrderTaking() throws JSONException {

        boolean backup = true;


        JSONArray uplatlong = new JSONArray();
        JSONArray newcostomerdata = uploadlocations();
        JSONArray datadb = new JSONArray();
        db = new DatabaseHandler(getApplicationContext());

        JSONArray datatoupload = new JSONArray();
        JSONArray paymentlist = db.getpaymentstosync(backup);
        JSONArray orderpayments = db.getorderpaymentstosync(backup);
        JSONArray unvisitedstores = db.getunvisitedstores(backup);
        JSONArray closingdayupdates = db.getclosingdayupdates(backup);
        JSONArray denominationdata = db.getdenominationdatatatosync(backup);
        JSONArray orderdata = db.getorderdatatosync(backup);
        JSONArray salesreturn = db.getsalesreturndatatosync(backup);
        JSONArray creditnote = db.getcreditnotestosync(backup);
        JSONArray updatecustomers = db.getupdatecustomerstosync(backup);
        JSONArray updatephotos = db.getphotostosync(backup);
        JSONArray cratesdata = db.getcratestosync(backup);
        JSONArray passbyupdated = db.getpassbyupdatedsync(backup);

        datatoupload = db.getorders(backup);
        uplatlong = db.getupdatedlaatlong(backup);

        datadb = datatoupload;

        webconfigurration C = new webconfigurration(getApplicationContext());

        String inputParamsStr = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"SalesproOperations\",\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";

        JSONObject inputParamObj = new JSONObject(inputParamsStr);

        JSONObject updatestatus = null;
        try {
            webconfigurration.status = "ClearCache";
            updatestatus = getstatusobject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        inputParamObj.put("updatestatus", updatestatus);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", Context.MODE_PRIVATE);
        JSONObject data = new JSONObject();
        data.put("operation", "PutMobileLog").put("data", datadb)
                .put("customers", newcostomerdata)
                .put("updatelatlong", uplatlong).put("payments", paymentlist)
                .put("reasons", unvisitedstores)
                .put("orderamountfromstores", orderpayments)
                .put("closingdayupdates", closingdayupdates)
                .put("Denominationdata", denominationdata)
                .put("OrderTaking", orderdata).put("SalesReturn", salesreturn)
                .put("CreditNotes", creditnote)
                .put("updatecustomers", updatecustomers)
                .put("updatephotos", updatephotos)
                .put("cratesdata", cratesdata)
                .put("passbyupdated", passbyupdated)
                .put("expenses", db.getExpenseDataForSync(backup));
        inputParamObj.getJSONObject("data").getJSONArray("data").put(data);
        inputParamObj.getJSONObject("data")
                .put("personid", pref.getString("personid", ""))
                .put("PersonOrg", pref.getString("tmsOrgId", ""));
        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        inputParamObj.put("auth", auth);

        return inputParamObj;

    }

    //===============================================For vansales backup
    private JSONObject getBackUpDataForVanSales() throws JSONException {
        boolean backup = true;
        JSONArray uplatlong = new JSONArray();
        JSONArray newcostomerdata = uploadlocations();
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        JSONArray datadb = new JSONArray();
        JSONArray paymentlist = new JSONArray();
        JSONArray orderpayments = new JSONArray();
        JSONArray unvisitedstores = db.getunvisitedstores(backup);
        JSONArray closingdayupdates = db.getclosingdayupdates(backup);
        JSONArray denominationdata = db.getdenominationdatatatosync(backup);
        JSONArray orderdata = new JSONArray(); //db.getorderdatatosync();
        JSONArray salesreturn = new JSONArray(); //db.getsalesreturndatatosync();
        JSONArray creditnote = new JSONArray(); // db.getcreditnotestosync();
        JSONArray updatecustomers = db.getupdatecustomerstosync(backup);
        JSONArray updatephotos = db.getphotostosync(backup);
        JSONArray cratesdata = db.getcratestosync(backup);
        JSONArray passbyupdated = db.getpassbyupdatedsync(backup);

        JSONArray expenseData = db.getExpenseDataForSync(backup);

        JSONArray orderDataVansales = new JSONArray();//db.getvansalesdatatosync(backup);
        JSONArray paymentDataVansales = new JSONArray();//db.getvansalespaymentstosync(backup);
        try {
            //			datatoupload = db.getorders();
            uplatlong = db.getupdatedlaatlong(backup);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        webconfigurration C = new webconfigurration(getApplicationContext());

        String inputParamsStr = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"SalesproOperations\"," +
                "\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\"," +
                "\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";

        JSONObject inputParamObj = new JSONObject(inputParamsStr);

        JSONObject updatestatus = null;
        try {
            webconfigurration.status = "ClearCache";
            updatestatus = getstatusobject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        inputParamObj.put("updatestatus", updatestatus);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", Context.MODE_PRIVATE);
        JSONObject data = new JSONObject();
        data.put("operation", "PutMobileLog").put("data", datadb)
                .put("customers", newcostomerdata)
                .put("updatelatlong", uplatlong).put("payments", paymentlist)
                .put("reasons", unvisitedstores)
                .put("orderamountfromstores", orderpayments)
                .put("closingdayupdates", closingdayupdates)
                .put("Denominationdata", denominationdata)
                .put("OrderTaking", orderdata).put("SalesReturn", salesreturn)
                .put("CreditNotes", creditnote)
                .put("updatecustomers", updatecustomers)
                .put("updatephotos", updatephotos)
                .put("cratesdata", cratesdata)
                .put("passbyupdated", passbyupdated)
                .put("expenses", expenseData);

        if (orderDataVansales.length() > 0 || paymentDataVansales.length() > 0) {
            data.put("VanSalesData", getVanSalesDataForSync(orderDataVansales, paymentDataVansales));
        }

        inputParamObj.getJSONObject("data").getJSONArray("data").put(data);
        inputParamObj.getJSONObject("data")
                .put("personid", pref.getString("personid", ""))
                .put("PersonOrg", pref.getString("tmsOrgId", ""));
        //		.put("InvNoForUpdate",db.getcurrinvoiceval());
        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        inputParamObj.put("auth", auth);

        return inputParamObj;

    }

    private JSONObject getstatusobject() throws Exception {
        JSONObject temp = new JSONObject();
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);

        String person = "";
        String lat = "";
        String lng = "";
        String operation = "";
        String version = "";
        String createdon = "";
        String uuid = "";
        String appname = "";
        String versioncode = "";
        person = pref.getString("personid", "0");

        operation = webconfigurration.status;
        appname = webconfigurration.appname;
        lat = webconfigurration.lat;
        lng = webconfigurration.lng;

        PackageInfo pInfo = getPackageManager().getPackageInfo(
                getPackageName(), 0);
        version = pInfo.versionName;
        versioncode = String.valueOf(pInfo.versionCode);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        createdon = df.format(c.getTime());

        uuid = UUID.randomUUID().toString();

        temp.put("person", person).put("lat", lat).put("lng", lng)
                .put("operation", operation).put("version", version)
                .put("versioncode", versioncode).put("appname", appname)
                .put("createdon", createdon).put("uuid", uuid);
        return temp;
    }

    private JSONObject getVanSalesDataForSync(JSONArray orderDataVansales, JSONArray paymentDataVansales) throws JSONException {
        JSONObject returnObject = new JSONObject();


        webconfigurration C = new webconfigurration(getApplicationContext());

        String inputParamsStr = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"OrderTakingForVanSales\",\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";

        returnObject = new JSONObject(inputParamsStr);

		/*JSONArray orderDataVansales = db.getvansalesdatatosync();
        JSONArray paymentDataVansales =db.getvansalespaymentstosync();*/

        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", Context.MODE_PRIVATE);
        JSONObject data = new JSONObject();

        data.put("orderData", orderDataVansales)
                .put("payments", paymentDataVansales);
        returnObject.getJSONObject("data").getJSONArray("data").put(data);
        returnObject.getJSONObject("data")
                .put("personId", pref.getString("personid", ""))
                .put("personOrg", pref.getString("tmsOrgId", ""))
                .put("InvNoForUpdate", db.getcurrinvoiceval());

        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        returnObject.put("auth", auth);

        Log.d("Data Input:", returnObject.toString());

        return returnObject;
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mAdapter.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mAdapter.onRestoreInstanceState(savedInstanceState);
    }



    public ScheduledRouteObject[] getScheduledRoutes(String text,String headid)
            throws Exception {

        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        JSONArray scheduleDetailDataArry = db.getscheduledata(text,headid);
        //Toast.makeText(getApplicationContext(),scheduleDetailDataArry.toString(),Toast.LENGTH_LONG).show();
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);
        salespersonid = pref.getString("personid", "");
        salespersonname = pref.getString("personname", "");

        //turnGPSOn();



		/*if (!isServiceRunning()) {
//			startService(new Intent(this, Vehicletracker.class));
			Log.d("service", "servicestarted");

		}*/

        if (scheduleDetailDataArry.length() > 0) {

            String Scheduledate_temp = scheduleDetailDataArry.getJSONObject(0)
                    .getString("scheduledate");

            DateFormat dateTimeFormat1 = new SimpleDateFormat("MM/dd/yyyy");

            Date temp = new Date();
            try {
                temp = dateTimeFormat1.parse(Scheduledate_temp);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            DateFormat dateTimeFormat2 = new SimpleDateFormat("dd MMM  yyyy");

            salespersonid = pref.getString("personid", "");
            salespersonname = pref.getString("personname", "");
            Scheduledate = dateTimeFormat2.format(temp);

        }

        // /////////////////////////////////////

        /*
         * ScheduledRouteObject[] ObjectItemData = new ScheduledRouteObject[5];
         * JSONArray customerArray=new JSONArray(); JSONObject CustomerObj=new
         * JSONObject(); CustomerObj.put("CustName", "Kavitha Store");
         * CustomerObj.put("CustAddress", "Kaloor,Near Pavakulam temple");
         * CustomerObj.put("Status", "Completed"); CustomerObj.put("Time",
         * "10:00 AM"); customerArray.put(CustomerObj); CustomerObj=new
         * JSONObject(); CustomerObj.put("CustName", "KR Bakers");
         * CustomerObj.put("CustAddress", "Pottakuzhy");
         * CustomerObj.put("Status", "Pending"); CustomerObj.put("Time",
         * "10:30 AM"); customerArray.put(CustomerObj); CustomerObj=new
         * JSONObject(); CustomerObj.put("CustName", "Best Bakers");
         * CustomerObj.put("CustAddress", "Near Jawans Pump");
         * CustomerObj.put("Status", "Pending"); CustomerObj.put("Time",
         * "11:15 AM"); customerArray.put(CustomerObj); ObjectItemData[0] = new
         * ScheduledRouteObject("R001",
         * "Kaloor Route","10:00 AM",customerArray);
         *
         *
         * customerArray=new JSONArray(); CustomerObj=new JSONObject();
         * CustomerObj.put("CustName", "Super Bakery");
         * CustomerObj.put("CustAddress", "Near Stadium");
         * CustomerObj.put("Status", "Pending"); CustomerObj.put("Time",
         * "11:30 AM");
         *
         * customerArray.put(CustomerObj); CustomerObj=new JSONObject();
         * CustomerObj.put("CustName", "Al Maray");
         * CustomerObj.put("CustAddress", "Near Church");
         * CustomerObj.put("Status", "Pending"); CustomerObj.put("Time",
         * "11:45 AM"); customerArray.put(CustomerObj);
         *
         *
         * ObjectItemData[1] = new ScheduledRouteObject("R002",
         * "Deshabhimani Route","11:00 AM",customerArray); ObjectItemData[2] =
         * new ScheduledRouteObject("R003", "Palarivattom Route","2:00 PM",new
         * JSONArray()); ObjectItemData[3] = new ScheduledRouteObject("R004",
         * "PipeLine Route","4:00 PM",new JSONArray()); ObjectItemData[4] = new
         * ScheduledRouteObject("R005", "Vazhakkulam Route","5:00 PM",new
         * JSONArray());
         */

        return getFormattedScheduleRouteData(scheduleDetailDataArry);
    }

    public ScheduledRouteObject[] getFormattedScheduleRouteData(
            JSONArray inputJsonArray) throws JSONException {
        JSONArray OutputJsonArr = new JSONArray();
        JSONObject inputJsonObject = null;
        JSONObject oJsonObject = null;
        ScheduledRouteObject[] ObjectItemData = new ScheduledRouteObject[inputJsonArray
                .length()];
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        HashMap<String, String> invoicehashmap = db.getCustInvoice();

        for (int i = 0; i < inputJsonArray.length(); i++) {
            inputJsonObject = inputJsonArray.getJSONObject(i);

            JSONArray inCustomerListArray = inputJsonObject
                    .getJSONArray("coustemerlist");
            JSONArray outCustomerListArray = new JSONArray();
            for (int j = 0; j < inCustomerListArray.length(); j++) {

                JSONObject CustJSONObj = inCustomerListArray.getJSONObject(j);
                JSONObject CustomerObj = new JSONObject();
                CustomerObj.put("CustName",
                        CustJSONObj.getString("locationname"));
                CustomerObj.put("CustAddress",
                        CustJSONObj.getString("locationadress"));
                CustomerObj.put("Status", CustJSONObj.getString("status"));
                CustomerObj.put("Time", "");
                String custid = CustJSONObj.getString("locationid");
                CustomerObj.put("CustId", custid);
                CustomerObj.put("scheduledetailid",
                        CustJSONObj.getString("scheduledetailid"));
                CustomerObj.put("latitude", CustJSONObj.optString("latitude"));
                CustomerObj
                        .put("longitude", CustJSONObj.optString("longitude"));
                CustomerObj.put("Customercategory",
                        CustJSONObj.optString("Customercategory"));
                CustomerObj.put("customercode",
                        CustJSONObj.optString("customercode"));
                CustomerObj.put("locationlock",
                        CustJSONObj.optString("locationlock", "0"));
                CustomerObj.put("Approve",
                        CustJSONObj.optString("Approve", "0"));
                CustomerObj.put("ApproveOrder",
                        CustJSONObj.optString("ApproveOrder", "0"));
                if (invoicehashmap.containsKey(custid))
                    CustomerObj.put("Invoice", invoicehashmap.get(custid));
                else
                    CustomerObj.put("Invoice", "Invoice: NIL, Amount: 0");
                CustomerObj.put("otpverify",
                        CustJSONObj.optString("otpverified", "0"));
                outCustomerListArray.put(CustomerObj);
            }

            Calendar c = Calendar.getInstance();
            DateFormat dateTimeFormat = new SimpleDateFormat("hh:mm a");
            String formattedDate = dateTimeFormat.format(c.getTime());

            String temp = inputJsonObject.getString("headerstatus");
            ObjectItemData[i] = new ScheduledRouteObject(
                    inputJsonObject.getString("schedulepkid"),
                    inputJsonObject.getString("schedulename"), "",
                    outCustomerListArray,
                    inputJsonObject.getString("headerstatus"),
                    inputJsonObject.optString("schedulecopletionstatus",
                            "incomplete"));
        }
        scheduledata = ObjectItemData;

        return ObjectItemData;

    }

   /* private void turnGPSOn() {

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!statusOfGPS) {

            AlertDialog.Builder builder = new AlertDialog.Builder(
                    ListSalesLocationActivity.this);
            builder.setTitle("Switch On GPS");
            builder.setMessage("Currently gps of the device is off");
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            startActivityForResult(
                                    new Intent(
                                            android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                    0);
                        }
                    });
            builder.show();

        }

    }*/

    public void loadCustList(ScheduledRouteObject[] elemts)
            throws Exception {
        final List<Schedule> schedules = new ArrayList<>();
        for (int i = 0; i < elemts.length; i++) {
            List<CustomerList> custlist = new ArrayList<>();
            ;

            try {

                String RouteId = elemts[i].RouteId;
                String RouteName = elemts[i].RouteName;
                String RouteTime = elemts[i].RouteTime;
                //child array list
                JSONArray custtemparr = elemts[i].CustomerList;
                for (int j = 0; j < custtemparr.length(); j++) {
                    JSONObject customerObject = new JSONObject();
                    customerObject = elemts[i].CustomerList
                            .getJSONObject(j);

                    String name = customerObject.getString("CustName");
                    String customercode = customerObject.getString("customercode");
                    String category = customerObject.getString("Customercategory");
                    String adr = customerObject.getString("CustAddress");
                    String invoice = customerObject.optString("otpverified","");
//                    String invoice = customerObject.getString("Invoice");
                    String scheduledetailid = customerObject.getString("scheduledetailid");
                    String latitude = customerObject.getString("latitude");
                    String longitude = customerObject.getString("longitude");
					String approve = customerObject.getString("Approve");
                    String approveorder = customerObject.getString("ApproveOrder");
                    String status = customerObject.getString("Status");
                    String locationlock = customerObject.getString("locationlock");
                    String otpverify = customerObject.getString("otpverify");
                    CustomerList custitem = new CustomerList(name, customercode, category, adr, invoice, scheduledetailid, latitude, longitude,approve,approveorder,status,locationlock,otpverify);
                    custlist.add(custitem);

                }
                Schedule schObject = new Schedule(RouteName, custlist, RouteId, RouteTime);
                schedules.add(schObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        // set up the RecyclerView
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        mAdapter = new ScheduleAdapter(CustomerListActivity.this, schedules);

        mAdapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
            @Override
            public void onListItemExpanded(int position) {
                Schedule expandedCustList = schedules.get(position);

                //// String toastMsg = getResources().getString(R.string.expanded, expandedMovieCategory.getName());
                //Toast.makeText(MainActivity.this,
                //    toastMsg,
                //   Toast.LENGTH_SHORT)
                //  .show();
            }

            @Override
            public void onListItemCollapsed(int position) {
                Schedule collapsedCustList = schedules.get(position);

                // String toastMsg = getResources().getString(R.string.collapsed, collapsedMovieCategory.getName());
                // Toast.makeText(MainActivity.this,
                //         toastMsg,
                //        Toast.LENGTH_SHORT)
                //        .show();
            }
        });
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (elemts.length == 1) {
            mAdapter.expandParent(0);

        }
    }

    public void confirnlogout() {
        //====================================================logout data
        Global.force = true;
        JSONArray uplatlong = new JSONArray();
        JSONArray newcostomerdata = uploadlocations();
        JSONArray datadb = new JSONArray();
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        try {
            JSONArray closingdayupdates = db.getclosingdayupdates(false);
            JSONArray unvisitedstores = db.getunvisitedstores(false);
            JSONArray orderpayments = db.getorderpaymentstosync(false);
            JSONArray datatoupload = new JSONArray();

            JSONArray denominationdata = db.getdenominationdatatatosync(false);
            JSONArray orderdata = db.getorderdatatosync(false);
            JSONArray paymentlist = db.getpaymentstosync(false);
            JSONArray salesreturn = db.getsalesreturndatatosync(false);
            JSONArray creditnote = db.getcreditnotestosync(false);

            JSONArray updatecustomers = db.getupdatecustomerstosync(false);
            JSONArray updatephotos = db.getphotostosync(false);
            JSONArray cratesdata = db.getcratestosync(false);
            JSONArray passbyupdated = db.getpassbyupdatedsync(false);

            JSONArray expenses =  new JSONArray();//db.getExpenseDataForSync(false) ;
            if(db.isserviceexist("Van Sales")){
                orderdata =  new JSONArray();//db.getvansalesdatatosync(false);
                paymentlist =  new JSONArray();//db.getvansalespaymentstosync(false);
            }
            datatoupload = db.getorders(false);
            uplatlong = db.getupdatedlaatlong(false);

            datadb = datatoupload;

            if (db.getpendingNotApproved()) {
               /* Global.Toast(CustomerListActivity.this, R.string.payment_approval_pending,
                        Toast.LENGTH_SHORT, Font.Regular);*/
                Toast.makeText(CustomerListActivity.this, R.string.payment_approval_pending,
                        Toast.LENGTH_SHORT).show();
                return;
            }
            if (db.getpendingNotApprovedOrders()) {
                /*Global.Toast(CustomerListActivity.this, "Order Approvals Pending!",
                        Toast.LENGTH_SHORT, Font.Regular);*/

                Toast.makeText(CustomerListActivity.this, R.string.order_approval_pending,
                        Toast.LENGTH_SHORT).show();
                return;
            }
            if (expenses.length() > 0 ||db.anypendingorderexist() || uplatlong.length() > 0
                    || newcostomerdata.length() > 0 || paymentlist.length() > 0
                    || orderpayments.length() > 0 || unvisitedstores.length() > 0
                    || closingdayupdates.length() > 0
                    || denominationdata.length() > 0 || orderdata.length() > 0
                    || salesreturn.length() > 0 || creditnote.length() > 0
                    || updatecustomers.length() > 0 || cratesdata.length() > 0
                    || updatephotos.length() > 0 || passbyupdated.length() > 0) {
                // AlertDialog.Builder builder = new AlertDialog.Builder(
                // DashboardActivity.this);
                // builder.setTitle("Sync Data");
                // builder.setMessage("You need to sync your data before logout");
                AlertDialog builder = new AlertDialog.Builder(CustomerListActivity.this)
                        .setTitle(R.string.sync_data)
                        .setMessage( R.string.logout_sync_data_warning_message )
                        .setPositiveButton(
                                android.R.string.yes,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(
                                            DialogInterface dialog,
                                            int which) {
                                    }
                                })
                        .setNegativeButton(
                                android.R.string.cancel,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(
                                            DialogInterface dialog,
                                            int which) {
                                    }
                                })
                        .show();

            }
            else {
                String status = db.getschedulestatus();
                if (status.equals("Completed")) {
                    NavigationView nav = (NavigationView) findViewById(R.id.nav_view);
                    LayoutInflater layoutInflater = (LayoutInflater) CustomerListActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View customView = layoutInflater.inflate(R.layout.popuplayout, null);
                    TextView header = (TextView) customView.findViewById(R.id.header);
                    header.setText(R.string.logout_confirmation_title);
                    TextView msg = (TextView) customView.findViewById(R.id.message);
                    msg.setText(R.string.logout_confirmation_message);
                    Button closePopupBtn = (Button) customView.findViewById(R.id.close);
                    Button confirm = (Button) customView.findViewById(R.id.yes);

                    //instantiate popup window

                    int width = LinearLayout.LayoutParams.MATCH_PARENT;
                    int height = LinearLayout.LayoutParams.MATCH_PARENT;
                    final PopupWindow popupWindow = new PopupWindow(customView, width, height);
                    popupWindow.setFocusable(true);

                    //display the popup window
                    popupWindow.showAtLocation(nav, Gravity.CENTER, 0, 0);
                    popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                    //close the popup window on button click
                    closePopupBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow.dismiss();
                        }
                    });
                    confirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow.dismiss();
                            logout();
                        }
                    });

                }
                else{
                    Toast.makeText(getApplicationContext(), R.string.complete_schedule_before_Logout, Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void logout() {

        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = dateTimeFormat.format(c.getTime());

        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);
        String personid = pref.getString("personid", "0");

        webconfigurration C = new webconfigurration(getApplicationContext());

        String inputParamsStr = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"SalesproOperations\",\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";

        JSONObject inputParamObj = null;
        try {
            inputParamObj = new JSONObject(inputParamsStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject updatestatus = null;
        try {

            webconfigurration.status = "Logout";//getstatusobject();//
            updatestatus = SupportFunctions.getstatusobject(getApplicationContext(), "", personid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            inputParamObj.put("updatestatus", updatestatus);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject data = new JSONObject();
        try {
            data.put("operation", "PutMobileLog");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            inputParamObj.getJSONObject("data").getJSONArray("data").put(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        try {
            inputParamObj.put("auth", auth);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new LongOperation2().execute(inputParamObj);

    }

    private void startTimerService() {
        // Toast.makeText(getApplicationContext(), "startTimerService on sync",
        // Toast.LENGTH_LONG).show();
        // startService(new Intent(this, CopyOfVehicletracker.class));
        // startService(new Intent(this, Vehicletracker.class));
        Calendar cal = Calendar.getInstance();
        Intent intent = new Intent(CustomerListActivity.this,
                CopyOfVehicletracker.class);
        PendingIntent pintent = PendingIntent.getService(
                CustomerListActivity.this, 0, intent, 0);
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                1000, pintent);

        Intent intentn = new Intent(CustomerListActivity.this, PlayTracker.class);
        PendingIntent pintentn = PendingIntent.getService(
                CustomerListActivity.this, 0, intentn, 0);
        AlarmManager alarmn = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmn.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                1000, pintentn);
    }

    private void checkversion() {
        JSONObject params = new JSONObject();
        try {
            params.put("AppName", "dsale");
            params.put("Version", "1");
            params.put("email", "");
            params.put("password", "");
            String auth = webconfigurration.auth;
            auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
            params.put("auth", auth);
            params.put("Operation", "VersionCheck");

            new LongOperation().execute(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void completeSync() {

        MenuItem menuItem = menu.findItem(R.id.nav_sync);

        menuItem.getActionView().clearAnimation();
        menuItem.setActionView(null);
    }



    private class LongOperation2 extends AsyncTask<JSONObject, Void, String> {
        private ProgressDialog mProgress;

        protected void onPreExecute() {
            try {
                mProgress = new ProgressDialog(CustomerListActivity.this);
                mProgress.setTitle("");
                mProgress.setContentView(R.layout.adapter_loading);
                mProgress.setMessage(getString(R.string.loading));
                mProgress.setCancelable(false);
                mProgress.setIndeterminate(true);
                mProgress.show();
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... credentials) {
            String responsefrom = null;
            webconfigurration C = new webconfigurration(getApplicationContext());
            String targetURL = C.url;
            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-16");
                conn.setRequestProperty("Accept-Encoding", "identity");

                conn.setConnectTimeout(100000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setReadTimeout(200000);
                conn.setRequestMethod("POST");

                int totalLength = (credentials[0].toString().getBytes("utf-16").length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.write(credentials[0].toString().getBytes("utf-16"));
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                responsefrom = response.toString();
                rd.close();
            } catch (Exception e) {
                e.printStackTrace();

            }
            return responsefrom;
        }

        protected void onPostExecute(final String response) {
            try {
                if (response != null) {

                    JSONObject resp = new JSONObject(response);
                    if (resp.optString("status").contentEquals("success")) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                SharedPreferences pref = getApplicationContext()
                                        .getSharedPreferences("Config",
                                                MODE_PRIVATE);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.clear();
                                editor.commit();
                                DatabaseHandler db = new DatabaseHandler(
                                        getApplicationContext());
                                Global.force = true;
                                db.Deletetabledata();
                                Intent intent = new Intent(
                                        CustomerListActivity.this,
                                        LoginActivity.class);
                                finish();
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                                overridePendingTransition(0, 0);
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), R.string.cannot_connect_to_server, Toast.LENGTH_SHORT).show();
                                if (mProgress != null) {
                                    mProgress.dismiss();
                                }
                            }
                        });
                    }
                } else {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), R.string.cannot_connect_to_server, Toast.LENGTH_SHORT).show();
                            if (mProgress != null) {
                                mProgress.dismiss();
                            }
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), R.string.cannot_connect_to_server, Toast.LENGTH_SHORT).show();
                        if (mProgress != null) {
                            mProgress.dismiss();
                        }
                    }
                });
            }
        }
    }

    private class LongOperation extends AsyncTask<JSONObject, Void, String> {
        // final Dialog dialog = new Dialog(MainActivity.this);
        protected void onPreExecute() {
            // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            // dialog.getWindow().clearFlags(
            // WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            // dialog.setContentView(R.layout.adapter_progressdialog);
            // TextView txt = (TextView) dialog.findViewById(R.id.text);
            // txt.setText("Please wait..");
            // dialog.getWindow().setBackgroundDrawable(
            // new ColorDrawable(android.graphics.Color.TRANSPARENT));
            // dialog.show();
            // dialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(JSONObject... credentials) {
            String responsefrom = null;
            webconfigurration C = new webconfigurration(getApplicationContext());
            String targetURL = C.loginurl;
            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-16");
                conn.setRequestProperty("Accept-Encoding", "identity");

                conn.setConnectTimeout(10000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setReadTimeout(10000);
                conn.setRequestMethod("POST");

                int totalLength = (credentials[0].toString().getBytes("utf-16").length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.write(credentials[0].toString().getBytes("utf-16"));
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                responsefrom = response.toString();
                rd.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responsefrom;
        }

        protected void onPostExecute(final String response) {
            // dialog.dismiss();
            if (response != null) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        String replay = "";
                        try {
                            JSONObject resp = new JSONObject(response);
                            if (resp.optString("status").contentEquals(
                                    "success")) {
                                replay = resp.optString("Version");
                                int vcode = Integer.parseInt(replay);
                                int currnt = 0;
                                try {
                                    PackageManager manager = getApplicationContext()
                                            .getPackageManager();
                                    PackageInfo info = manager.getPackageInfo(
                                            getApplicationContext()
                                                    .getPackageName(), 0);
                                    currnt = info.versionCode;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                if (vcode > currnt) {
                                    AlertDialog builder = new AlertDialog.Builder(
                                            CustomerListActivity.this)
                                            .setTitle(getString(R.string.alert))
                                            .setMessage(getString(R.string.new_version) )
                                            .setPositiveButton(
                                                    android.R.string.yes,
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(
                                                                DialogInterface dialog,
                                                                int which) {
                                                            updateApp();
                                                        }

                                                        private void updateApp() {
                                                            Intent intent = new Intent(
                                                                    CustomerListActivity.this,
                                                                    DownloadActivity.class);
                                                            startActivity(intent);
                                                        }
                                                    })
                                            .setNegativeButton(
                                                    android.R.string.no,
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(
                                                                DialogInterface dialog,
                                                                int which) {
                                                        }
                                                    })
                                            .setIcon(
                                                    android.R.drawable.ic_dialog_alert)
                                            .show();
                                }
                            } else
                                replay = resp.optString("Error");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(getApplicationContext(), replay,
                        // Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                // Toast.makeText(getApplicationContext(),
                // "Cannot connect to server", Toast.LENGTH_LONG).show();
            }
        }
    }



    public void completeschedule() throws Exception {
        try {
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            if (db.getpendingNotApproved()) {
                Toast.makeText(CustomerListActivity.this,
                        R.string.payment_approval_pending, Toast.LENGTH_SHORT).show();
            } else if (db.getPendingOrderPayment()) {
                Toast.makeText(CustomerListActivity.this,
                        R.string.pending_payments , Toast.LENGTH_SHORT).show();
            } else {
                /*Intent intent = new Intent(this, CompleteScheduleActivity.class);
                finish();
                startActivity(intent);
                overridePendingTransition(0,0);*/
            }
        }catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(CustomerListActivity.this, LandingActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(0, 0);
    }

}



