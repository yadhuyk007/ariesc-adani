package combing.sales.adani.customerlist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import combing.sales.adani.ariesc.R;
//import combing.sales.dhi.ariesc.R;

public class NearestCustomerAdapter extends RecyclerView.Adapter<NearestCustomerAdapter.ViewHolder> {


    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView custName;
        public ImageView fabtn;
        public TextView distance;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            custName = (TextView) itemView.findViewById(R.id.CustomerName);
            fabtn = (ImageView) itemView.findViewById(R.id.imgNavigatordynamic);
            distance = (TextView) itemView.findViewById(R.id.dist);
        }


    }

    String[] names;
    String[] codes;
    int[] distances;
    String latit;
    String longi;



    public NearestCustomerAdapter(String[] arr1, String[] arr2, int[] arr3) {
        names = arr1;
        codes = arr2;
        distances = arr3;
    }

    @NonNull
    @Override
    public NearestCustomerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.recyclerview_row_nearestcustomerlist, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }



    @Override
    public void onBindViewHolder(NearestCustomerAdapter.ViewHolder viewHolder, int position) {
            //Contact contact = mContacts.get(position);
            String nm = names[position];
            final String cd = codes[position];
            final int d = distances[position];

            //if(d<51){
                TextView custName = viewHolder.custName;
                custName.setText(nm);
                TextView distance = viewHolder.distance;
                distance.setText(d+" m away");
            //}



            /*ImageView iv = viewHolder.fabtn;
            iv.setClickable(true);
            iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    JSONObject objct = new JSONObject();
                    DatabaseHandler db;
                    db = new DatabaseHandler(view.getContext());
                    try {

                        objct = db.NearestFunction(cd);
                        latit = objct.getString("Lati");
                        longi = objct.getString("Longi");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try{
                        if (!latit.toString().equals("")
                                && !latit.toString().equals("0")
                                && !latit.toString().equals("0.0") && !longi.toString().equals("")
                                && !longi.toString().equals("0")
                                && !longi.toString().equals("0.0")) {

                            Uri gmmIntentUri = Uri.parse("google.navigation:q="
                                    + latit + "," + longi);
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW,
                                    gmmIntentUri);
                            mapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mapIntent
                                    .setPackage("com.google.android.apps.maps");
                            view.getContext().startActivity(mapIntent);

                        }

                        else{
                            Toast.makeText(view.getContext(),
                             "No Location Data Found..!",
                            Toast.LENGTH_LONG).show();
                        }
                    }

                    catch (Exception e){
                        //e.printStackTrace();
                        Toast.makeText(view.getContext(),
                        "Install Google Maps or Check Internet Connectivity!", Toast.LENGTH_LONG)
                         .show();
                    }
                }
            });*/
    }

    @Override
    public int getItemCount() {

        return names.length;
    }


}