package combing.sales.adani.navmap;

import android.content.Context;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import combing.sales.adani.ariesc.R;
import combing.sales.adani.database.DatabaseHandler;
import combing.sales.adani.gps.PlayLocation;
import combing.sales.adani.objects.Font;
import combing.sales.adani.objects.Global;
import combing.sales.adani.objects.Stacklogger;


public class NavMapActivity extends AppCompatActivity {
    WebView mWebView;
    ProgressBar progressBar;
    String key1;
    DatabaseHandler db;
    JSONArray locations;
    Bundle bundle;
    String storename;
    String storeaddress;
    double latt2;
    double longi2;
    double accuracy;
    private String mapurl;
    private String mapurloptions;
    private boolean offroute;
    private String Lat;
    private String Long;
    private Location loc;
    private Font a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav);

        Thread.setDefaultUncaughtExceptionHandler(new Stacklogger(this));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
        bundle = getIntent().getExtras();
        try {
            storename = bundle.getString("storename");
            storeaddress = bundle.getString("storeaddress");
            offroute = bundle.getBoolean("offroute", false);
            Lat = bundle.getString("Lat", "0");
            Long = bundle.getString("Long", "0");
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        db = new DatabaseHandler(getApplicationContext());
        mapurl = db.getmapurl();
        mapurloptions = db.getmapurloptions();

//	Vehicletracker tracker = new Vehicletracker();
        PlayLocation tracker  = new PlayLocation();
        latt2 = tracker.getlattitude();
        longi2 = tracker.getlongitude();
        accuracy = tracker.getaccuracy();
        String myloc = "{\"lat\":\"0\",\"long\":\"0\"}";
        loc = tracker.getMyLocation();
        try {
            if (offroute) {
                locations = new JSONArray();
                JSONObject latlongdata = new JSONObject();
                latlongdata.put("latitude_current", Lat);
                latlongdata.put("longitude_current", Long);
                latlongdata.put("storename", storename);
                latlongdata.put("adress", storeaddress);
                latlongdata.put("seq", "");
                latlongdata.put("status", "");
                locations.put(latlongdata);
            } else
                locations = db.getalllatlongs();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (locations.length() > 0) {
            key1 = locations.toString();
        }
        //Toast.makeText(getApplicationContext(),locations.toString(),Toast.LENGTH_SHORT).show();
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        WebView mWebView = (WebView) findViewById(R.id.map);


        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
                if (progress == 100)
                    progressBar.setVisibility(View.GONE);
            }
        });
        mWebView.setWebViewClient(new WebViewClient());
        String url = "file:///android_asset/navmap.html";
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(url);
        mWebView.addJavascriptInterface(new StringGetter(NavMapActivity.this),
                "Route");
        a = new Font(NavMapActivity.this,
                (ViewGroup) findViewById(android.R.id.content), Font.Regular);
    }

    public class StringGetter {
        Context jContext;

        StringGetter(Context context) {
            jContext = context;
        }

        @JavascriptInterface
        public String getString() {
            return key1;
        }

        @JavascriptInterface
        public String StoreName() {
            return storename;
        }

        @JavascriptInterface
        public String StoreAddress() {
            return storeaddress;
        }

        @JavascriptInterface
        public String myLoc() {
            String myloc = "{\"lat\":\"0\",\"long\":\"0\",\"accuracy\":\"0\"}";
            if (loc != null)
                myloc = "{\"lat\":\"" + String.valueOf(loc.getLatitude())
                        + "\",\"long\":\"" + String.valueOf(loc.getLongitude())
                        + "\",\"accuracy\":\""
                        + String.valueOf(loc.getAccuracy()) + "\"}";
            else
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Global.Toast(
                                NavMapActivity.this,
                                "Current location is not set. Please try again.",
                                Toast.LENGTH_SHORT, Font.Regular);
                    }
                });
            return myloc;
        }

        @JavascriptInterface
        public String mapURL() {
            return mapurl;
            // == null ? "https://maps.dhisigma.com/osm_tiles/{z}/{x}/{y}.png" :
            // mapurl;
        }

        @JavascriptInterface
        public String mapURLOptions() {
            return mapurloptions;
            // == null ?
            // "{minZoom: 4, maxZoom:18,attribution: '&copy <a href=\"http://openstreetmap.org\">OpenStreetMap</a>'}"
            // : mapurloptions;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.bookmark_menu:
                startActivity(getIntent());
                finish();
                overridePendingTransition(0, 0);
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dhi_action_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
