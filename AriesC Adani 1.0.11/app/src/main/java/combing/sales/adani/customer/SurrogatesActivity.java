package combing.sales.adani.customer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import combing.sales.adani.ariesc.R;
import combing.sales.adani.database.DatabaseHandler;
import combing.sales.adani.objects.Stacklogger;

public class SurrogatesActivity extends AppCompatActivity {

    //private static final int REQUEST_CODE_GPS_ACCURACY = 1;
    public static String[] productcategories;
    public static boolean[] selectedarray;
    public static String[] categorycode;
    public static String[] typearray;
    public static String[] selectedarraytext;
    private String pin;
    private String phone;
    private String address;
    private String custcategory;
    private String custname;
    private DatabaseHandler db;
    private SharedPreferences pref;
    private String customerid = "";
    private String createtime = "";
    //private Bitmap customerphoto;
    //private Bitmap insidephoto;
    //private boolean OTP_verificationstatus;
    //private String OTP_submitTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_surrogates);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
        Thread.setDefaultUncaughtExceptionHandler(new Stacklogger(this));

        db = new DatabaseHandler(getApplicationContext());
        pref = getApplicationContext()
                .getSharedPreferences("Config",
                        MODE_PRIVATE);

        //Bundle bundle = getIntent().getExtras();
        // custname = bundle.getString("custname", "");
        // address = bundle.getString("address", "");
        // phone = bundle.getString("phone", "");
        // pin = bundle.getString("pin", "");
        // custcategory = bundle.getString("category","");
        // OTP_verificationstatus = bundle.getBoolean("OTP_verificationstatus", false);
        // OTP_submitTime = bundle.getString("OTP_submitTime", "");
        // customerphoto = Global.sharedcustimg;
        // insidephoto = Global.sharedinsideimg;



        try {
            intialisedialoguedata();
        } catch (Exception e) {
            e.printStackTrace();
        }


        LinearLayout group = (LinearLayout)
                findViewById(R.id.outlinear);

        TextView customTitleView = new TextView(SurrogatesActivity.this);
        customTitleView.setText("SURROGATES");
        customTitleView.setTextSize(20);
        customTitleView.setPadding(20, 20, 20, 20);
        customTitleView.setBackgroundResource(R.color.gray2);
//        Font a = new Font(mAct, customTitleView, Font.Semibold);
//        builder.setCustomTitle(customTitleView);
        for (int ct = 0; ct < productcategories.length; ct++) {
            final int which = ct;
            if (typearray[ct] == null
                    || typearray[ct].contentEquals("")) {
                LayoutInflater inflater=getLayoutInflater();
                View child=inflater.inflate(R.layout.surrogate_checkbox,null);
                //final TextView first = new TextView(SurrogatesActivity.this);
                final TextView first=(TextView)child.findViewById(R.id.txt);
                first.setText(productcategories[ct]);
                //group.addView(first);
                //CheckBox firstCheck = new CheckBox(SurrogatesActivity.this);
                CheckBox firstCheck = (CheckBox)child.findViewById(R.id.chkbox);
                if (selectedarray[ct])
                    firstCheck.setChecked(true);

                // firstCheck.setGravity(Gravity.RIGHT);

                group.addView(child);
                // group.addView(firstCheck);
                firstCheck
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                CheckBox c = (CheckBox) v;
                                if (c.isChecked()) {
                                    // Toast.makeText(getActivity(),
                                    // "True", Toast.LENGTH_LONG)
                                    // .show();
                                    selectedarray[which] = true;
                                } else {
                                    selectedarray[which] = false;
                                }
                            }
                        });

            } else {
                LayoutInflater inflater=getLayoutInflater();
                View child=inflater.inflate(R.layout.surrogate_inputbox,null);
                //final TextView first = new TextView(SurrogatesActivity.this);
                final TextView first=(TextView)child.findViewById(R.id.txt);
                first.setText(productcategories[ct]);

                //group.addView(first);
                //final EditText firstEdit = new EditText(SurrogatesActivity.this);
                EditText firstEdit = (EditText)child.findViewById(R.id.chkbox);
                firstEdit.setText(selectedarraytext[ct]);
                if (typearray[ct].equalsIgnoreCase("Number"))
                    firstEdit.setInputType(InputType.TYPE_CLASS_PHONE);
                //group.addView(firstEdit);
                group.addView(child);
                firstEdit.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s,
                                              int start, int before, int count) {
                        // Toast.makeText(getActivity(), s + "",
                        // Toast.LENGTH_LONG).show();
                        selectedarray[which] = true;
                        selectedarraytext[which] = s + "";
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s,
                                                  int start, int count, int after) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        // TODO Auto-generated method stub
                    }
                });
            }
        }
        ((Button) findViewById(R.id.next)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SurrogatesActivity.this, NewCustomerActivity.class);
                intent.putExtra("selectedarray", selectedarray);
                intent.putExtra("selectedarraytext", selectedarraytext);
                intent.putExtra("categorycode",categorycode);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);

            }
        });
    }
    public void intialisedialoguedata() throws JSONException {
        JSONArray data = db.getchecklist();
        productcategories = new String[data.length()];
        categorycode = new String[data.length()];
        selectedarray = new boolean[data.length()];
        typearray = new String[data.length()];
        selectedarraytext = new String[data.length()];

        for (int i = 0; i < data.length(); i++) {
            JSONObject dataobj = data.getJSONObject(i);
            productcategories[i] = dataobj.getString("catname");
            categorycode[i] = dataobj.getString("catid");
            selectedarray[i] = false;
            typearray[i] = dataobj.getString("type");
            // selectedarraytext[i]="";
        }
    }
}