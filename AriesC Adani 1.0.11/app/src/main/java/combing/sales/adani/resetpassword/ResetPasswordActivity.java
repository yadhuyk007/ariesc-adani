package combing.sales.adani.resetpassword;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import combing.sales.adani.ariesc.R;
import combing.sales.adani.landing.LandingActivity;
import combing.sales.adani.objects.webconfigurration;

public class ResetPasswordActivity extends AppCompatActivity {

    EditText oldpassword;
    EditText newpassword;
    EditText repassword;
    Button save;

    String uname;
    String opassword;
    String npassword;
    String rpassword;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_reset_password);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);


        oldpassword = (EditText) findViewById(R.id.oldpassword);
        newpassword = (EditText) findViewById(R.id.newpassword);
        repassword = (EditText) findViewById(R.id.repassword);
        save = (Button) findViewById(R.id.button1);
        pref = getApplicationContext().getSharedPreferences("Config",
                MODE_PRIVATE);
        save.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                uname = pref.getString("usernamefor", "");
                opassword = oldpassword.getText().toString();
                npassword = newpassword.getText().toString();
                rpassword = repassword.getText().toString();
//                Log.d("usernamefor",uname);
//                Log.d("opassword",opassword);
//                Log.d("npassword",npassword);
//                Log.d("rpassword",rpassword);
                if (opassword.contentEquals("") || npassword.contentEquals("")
                        || rpassword.contentEquals("")) {
                    Toast.makeText(getApplicationContext(),
                            "All fields required", Toast.LENGTH_LONG).show();
                } else if (!npassword.contentEquals(rpassword)) {
                    Toast.makeText(getApplicationContext(),
                            "Passwords not matching", Toast.LENGTH_LONG).show();
                } else {
                    JSONObject params = new JSONObject();
                    try {
                        String cText = Base64.encodeToString(uname.getBytes(),
                                Base64.NO_WRAP);
                        params.put("email", cText);
                        cText = Base64.encodeToString(opassword.getBytes(),
                                Base64.NO_WRAP);
                        params.put("password", cText);
                        cText = Base64.encodeToString(npassword.getBytes(),
                                Base64.NO_WRAP);
                        params.put("newpassword", cText);
                        params.put("Operation", "ResetPassword");

                        String auth = webconfigurration.auth;
                        auth = Base64.encodeToString(auth.getBytes(),
                                Base64.NO_WRAP);
                        params.put("auth", auth);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    new LongOperation().execute(params);
                }
            }
        });
    }

    //Added by Jithu on 17/05/2019 for back press redirect to customer list
    @Override
    public void onBackPressed() {

        Intent intent = new Intent(ResetPasswordActivity.this, LandingActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

    private class LongOperation extends AsyncTask<JSONObject, Void, String> {

        final ProgressDialog mProgress = new ProgressDialog(ResetPasswordActivity.this);

        protected void onPreExecute() {
            mProgress.setTitle("");
            mProgress.setContentView(R.layout.adapter_loading);
            mProgress.setMessage("Loading...");
            mProgress.setCancelable(false);
            mProgress.setIndeterminate(true);
            mProgress.show();
        }

        @Override
        protected String doInBackground(JSONObject... credentials) {
            String responsefrom = null;
            webconfigurration C = new webconfigurration(getApplicationContext());
            String targetURL = C.loginurl;
            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-16");
                conn.setRequestProperty("Accept-Encoding", "identity");

                conn.setConnectTimeout(10000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setReadTimeout(200000);
                conn.setRequestMethod("POST");

                int totalLength = (credentials[0].toString().getBytes("utf-16").length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.write(credentials[0].toString().getBytes("utf-16"));
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                responsefrom = response.toString();
                rd.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responsefrom;
        }

        protected void onPostExecute(final String response) {
            mProgress.dismiss();
            if (response != null) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        String replay = "";
                        try {
                            JSONObject resp = new JSONObject(response);
                            if (resp.optString("status").contentEquals(
                                    "success")) {
                                replay = "Password changed";
                                if (mProgress != null) {
                                    mProgress.dismiss();
                                }
                                Intent intent = new Intent(ResetPasswordActivity.this, LandingActivity.class);
                                startActivity(intent);
                                finish();
                            } else
                                replay = resp.optString("Error");
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        if (mProgress != null) {
                            mProgress.dismiss();
                        }
                        Toast.makeText(getApplicationContext(), replay,
                                Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                if (mProgress != null) {
                    mProgress.dismiss();
                }
                Toast.makeText(getApplicationContext(),
                        "Cannot connect to server", Toast.LENGTH_LONG).show();
            }
        }
    }


}
