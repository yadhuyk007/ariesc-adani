package combing.sales.adani.objects;
import java.util.List;

import combing.sales.adani.customerlist.CustomerList;
import combing.sales.adani.customerlist.ParentListItem;


public class Schedule implements ParentListItem {
    private String mRouteName;
    private String mRouteId;
    private String mRouteStartTime;
    private List<CustomerList> mcustlist;

    public Schedule(String name, List<CustomerList> custlist, String RouteId, String RouteStartTime) {
        mRouteName = name;
        mcustlist = custlist;
        mRouteId = RouteId;
        mRouteStartTime = RouteStartTime;
    }

    public String getRouteName() {
        return mRouteName;
    }
    public String getRouteId() {
        return mRouteId;
    }
    public String getRouteStartTime() {
        return mRouteStartTime;
    }

    @Override
    public List<?> getChildItemList() {
        return mcustlist;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
