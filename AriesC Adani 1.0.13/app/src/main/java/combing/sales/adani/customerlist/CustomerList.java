package combing.sales.adani.customerlist;

public class CustomerList {
    private String mName;
    private String mCustomercode;
    private String mCategory;
    private String mCustomerAddress;
    private String mInvoice;
    private String mScheduledetailid;
    private String latitude;
    private String longitude;
    private String otpvery;

    private String mApprove;
    private String mApproveOrder;
    private String mStatus;
    private String mlocationlock;




    public CustomerList(String name, String customercode, String category, String customeraddress, String invoice, String scheduledetailid,
                        String lati, String longi,String approve, String approveorder,String status,String locationlock,String otpverify) {
        mName = name;
        mCustomercode = customercode;
        mCategory = category;
        mCustomerAddress = customeraddress;
        mInvoice = invoice;
        mScheduledetailid = scheduledetailid;
        latitude = lati;
        longitude = longi;
        otpvery= otpverify;

        mApprove = approve;
        mApproveOrder = approveorder;
        mStatus = status;
        mlocationlock=locationlock;

    }

    public String getName() {
        return mName;
    }

    public String getCustomercode() {
        return mCustomercode;
    }

    public String getCustomerAddress() {
        return mCustomerAddress;
    }

    public String getCategory() {
        return mCategory;
    }

    public String getInvoice() {
        return mInvoice;
    }

    public String getotp() {
        return otpvery;
    }

    public String getScheduledetailid() {
        return mScheduledetailid;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getApprove() {
        return mApprove;
    }

    public String getApproveOrder() {
        return mApproveOrder;
    }

    public String getStatus() {
        return mStatus;
    }
    public String getLocationLock() {
        return mlocationlock;
    }
}
