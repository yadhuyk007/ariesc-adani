package combing.sales.adani.landing;

import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import combing.sales.adani.SummaryActivity.SummaryActivity;
import combing.sales.adani.about.AboutActivity;
import combing.sales.adani.ariesc.LoginActivity;
import combing.sales.adani.ariesc.R;
import combing.sales.adani.customer.NearestCustomerActivity;
import combing.sales.adani.customer.SurrogateImageActivity;
import combing.sales.adani.customer.SurrogatesActivity;
import combing.sales.adani.customer.SurrogatesEditActivity;
import combing.sales.adani.customerlist.CustomerListActivity;
import combing.sales.adani.database.DatabaseHandler;
import combing.sales.adani.gps.CopyOfVehicletracker;
import combing.sales.adani.gps.PlayLocation;
import combing.sales.adani.gps.PlayTracker;
import combing.sales.adani.objects.Font;
import combing.sales.adani.objects.FourStrings;
import combing.sales.adani.objects.Global;
import combing.sales.adani.objects.Stacklogger;
import combing.sales.adani.objects.Synchronize;
import combing.sales.adani.objects.Vehicledata;
import combing.sales.adani.objects.webconfigurration;
import combing.sales.adani.reports.MarketActivities;
import combing.sales.adani.resetpassword.ResetPasswordActivity;

import static combing.sales.adani.gps.SyncNavData.notifyReturn;

public class LandingActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    JSONArray locations;
    boolean doubleBackToExitPressedOnce = false;
    private Menu menu;
    private LinearLayout fabLayoutNewCust;
    private LinearLayout fabLayoutCustSrch;
    private LinearLayout fabLayoutCustSrchRefresh;
    private boolean isFABOpen = false;
    private DatabaseHandler db;
    private SharedPreferences pref;
    private String mapurl;
    private String path;
    private String mapurloptions;
    private double latt2;
    private double longi2;
    private float accuracy;
    private Location loc;
    private ProgressBar progressBar;
    private String key1;
    private List<FourStrings> regions;
    private String livedata;
    private boolean refreshbutton = false;
    private boolean syncbutton = false;
    private LinearLayout popupsummary;
    private static String mapstatus = "Streets";
    private String scheduleid;
    private String OrgID;

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static String getpkidstring(List<Vehicledata> Navigationdata) {
        // (1, 4, 6, 7)
        String pkdata = "(";
        int last = Navigationdata.size() - 1;
        for (int i = 0; i < (Navigationdata.size() - 1); i++) {
            pkdata = pkdata + String.valueOf(Navigationdata.get(i).pkid) + ",";

        }
        pkdata = pkdata + String.valueOf(Navigationdata.get(last).pkid) + ")";
        return pkdata;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_main);
        Thread.setDefaultUncaughtExceptionHandler(new Stacklogger(this));

        popupsummary = (LinearLayout) findViewById(R.id.popupsummary);
        ImageView closebutton = (ImageView) findViewById(R.id.closebutton);
        closebutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                popupsummary.setVisibility(View.GONE);
            }
        });


        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        menu = navigationView.getMenu();
        View header = navigationView.getHeaderView(0);
        db = new DatabaseHandler(getApplicationContext());
        //JSONArray audio = db.getAudiosToSync();
        pref = getApplicationContext()
                .getSharedPreferences("Config",
                        MODE_PRIVATE);


        String salespersonsync = db.getrecentsynctime();
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            Date temp = dateTimeFormat.parse(salespersonsync);
            dateTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            salespersonsync = dateTimeFormat.format(temp);
        } catch (ParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        TextView txt = header.findViewById(R.id.textdsename);
        txt.setText(pref.getString("personname", ""));
        txt = header.findViewById(R.id.texttype);
        txt.setText(pref.getString("personrole", ""));
        txt = header.findViewById(R.id.textcode);
        txt.setText(pref.getString("pesronorg", ""));
        txt = header.findViewById(R.id.textdate);
        txt.setText(salespersonsync);

        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.CAMERA,
                Manifest.permission.READ_CALL_LOG,
                Manifest.permission.RECORD_AUDIO
        };

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }


        startTimerService();


        String scheduleid = db.getactualscheduleid();
        String beginTime = db.getscheduleheaderbegintime(scheduleid);


        try {


            //check gps on
            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            boolean statusOfGPS = manager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (!statusOfGPS) {

                AlertDialog.Builder builder = new AlertDialog.Builder(
                        LandingActivity.this);
                builder.setTitle(R.string.switch_on_gps);
                builder.setMessage(R.string.gps_off_alert_message);
                builder.setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                startActivityForResult(
                                        new Intent(
                                                Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                        0);
                            }
                        });
                builder.show();
                return;
            }


//            if (beginTime == null) {
//                String schDate = db.GetCurrentScheduledate();
//                Calendar c = Calendar.getInstance();
//                dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd",
//                        Locale.US);
//                String currenttime = dateTimeFormat.format(c.getTime());
//                if (currenttime.contentEquals(schDate)) {
//                    //final LinearLayout schbgnpopup= (LinearLayout) findViewById(R.id.testid);
//                    LayoutInflater layoutInflater = (LayoutInflater) LandingActivity.this.getSystemService(LAYOUT_INFLATER_SERVICE);
//                    View customView = layoutInflater.inflate(R.layout.popuplayout, null);
//                    Button closePopupBtn = (Button) customView.findViewById(R.id.close);
//                    Button confirm = (Button) customView.findViewById(R.id.yes);
//                    TextView title = (TextView) customView.findViewById(R.id.header);
//                    title.setText(R.string.begin_schedule);
//                    TextView message = (TextView) customView.findViewById(R.id.message);
//                    message.setText(R.string.begin_schedule_message);
//
//                    //instantiate popup window
//
//                    int width = LinearLayout.LayoutParams.MATCH_PARENT;
//                    int height = LinearLayout.LayoutParams.MATCH_PARENT;
//                    final PopupWindow popupWindow = new PopupWindow(customView, width, height);
//                    // popupWindow.showAtLocation(customView , Gravity.CENTER, 0, 0);
//
//                    //display the popup window
//                    // popupWindow.showAtLocation(schbgnpopup, Gravity.CENTER, 0, 0);
//
//                    new Handler().postDelayed(new Runnable() {
//
//                        public void run() {
//                            //popupWindow.showAtLocation(CustomerListActivity.this.getWindow().getDecorView(), Gravity.CENTER,0,0);
//                            //popupWindow.showAtLocation(schbgnpopup, Gravity.CENTER, 0, 0);
//                            popupWindow.showAsDropDown(LandingActivity.this.getWindow().getDecorView());
//                        }
//
//                    }, 600L);
//                    popupWindow.setFocusable(true);
//                    popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//                    //schbgnpopup.setAlpha(0.5F);
//                    //close the popup window on button click
//                    closePopupBtn.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            popupWindow.dismiss();
//                            return;
//                        }
//                    });
//                    confirm.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            popupWindow.dismiss();
//                            Calendar c = Calendar.getInstance();
//                            DateFormat dateTimeFormat = new SimpleDateFormat(
//                                    "yyyyMMddHHmmss", Locale.US);
//                            DateFormat dateTimeFormat_readable = new SimpleDateFormat(
//                                    "dd/MM/yyyy HH:mm:ss", Locale.US);
//                            String currenttime = dateTimeFormat.format(c
//                                    .getTime());
//                            String readabletime = dateTimeFormat_readable
//                                    .format(c.getTime());
//
//                            db.updatescheduleheaderbegintime(currenttime);
//
//
//                            db.intialisesynctable(currenttime);
//
//                            SharedPreferences.Editor editor = pref.edit();
//                            editor.putString("Schbegintime", currenttime);
//                            editor.commit();
//                            Toast.makeText(LandingActivity.this, R.string.schedule_started, Toast.LENGTH_SHORT).show();
//                            /*Global.Toast(CustomerListActivity.this,
//                                    "Schedule Started!",
//                                    Toast.LENGTH_SHORT, Font.Regular);*/
//                            finish();
//                            startActivity(getIntent());
//                            overridePendingTransition(0, 0);
//                            return;
//
//
//                        }
//                    });
//
//                }
//            }//beginTime == null end


        } catch (Exception e2) {
            // TODO Auto-generated catch block

            e2.printStackTrace();
        }
        NavigationView mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        if (mNavigationView != null) {
            mNavigationView.setNavigationItemSelectedListener(this);
        }


        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;

        fabLayoutNewCust = (LinearLayout) findViewById(R.id.fabLayoutNewCust);
        fabLayoutCustSrch = (LinearLayout) findViewById(R.id.fabLayoutCustSrch);
        fabLayoutCustSrchRefresh = (LinearLayout) findViewById(R.id.fabLayoutCustSrchRefresh);

        TextView fabNewCustText = (TextView) findViewById(R.id.fabNewCustText);
        fabNewCustText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PackageManager manager = getApplicationContext()
                        .getPackageManager();
                PackageInfo info = null;
                try {
                    info = manager.getPackageInfo(getApplicationContext()
                            .getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                //if(Global.isNewVersionAvailable(getApplicationContext()))
                //
                // {
                if(info.versionCode < pref.getInt("version",0)){
                    Global.Toast(LandingActivity.this,"New version Available. Refresh cache data!",Toast.LENGTH_LONG,Font.Regular);
                }
                else {
                if(isDateValid()) {
                    Intent i = new Intent(LandingActivity.this,
                            SurrogatesActivity.class);
                    startActivity(i);
                    finish();
                }else{
                    Global.Toast(LandingActivity.this,"Refresh cache data to start the new day!",Toast.LENGTH_LONG,Font.Regular);
                }
            }
            }
        });

        FloatingActionButton fabNewCust = (FloatingActionButton) findViewById(R.id.fabNewCust);
        fabNewCust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PackageManager manager = getApplicationContext()
                        .getPackageManager();
                PackageInfo info = null;
                try {
                    info = manager.getPackageInfo(getApplicationContext()
                            .getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                //if(Global.isNewVersionAvailable(getApplicationContext())) {
                if(info.versionCode < pref.getInt("version",0)){
                    Global.Toast(LandingActivity.this,"New version Available. Refresh cache data!",Toast.LENGTH_LONG,Font.Regular);
                }
                else {
                if(isDateValid()) {
                    Intent i = new Intent(LandingActivity.this,
                            SurrogateImageActivity.class);
                    startActivity(i);
                    finish();
                }else{
                    Global.Toast(LandingActivity.this, "Refresh cache data to start the new day!",Toast.LENGTH_LONG,Font.Regular);
                }
            }
            }
        });



        FloatingActionButton recenter = (FloatingActionButton) findViewById(R.id.recenter);
        recenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = getIntent();
                startActivity(i);
                finish();
                overridePendingTransition(0, 0);
            }
        });

        TextView fabCustSrchTextRefresh = (TextView) findViewById(R.id.fabCustSrchTextRefresh);
        fabCustSrchTextRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = getIntent();
                startActivity(i);
                finish();
                overridePendingTransition(0, 0);
            }
        });
        FloatingActionButton fabCustSrchRefresh = (FloatingActionButton) findViewById(R.id.fabCustSrchRefresh);
        fabCustSrchRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = getIntent();
                startActivity(i);
                finish();
                overridePendingTransition(0, 0);
            }
        });


        TextView fabCustSrchText = (TextView) findViewById(R.id.fabCustSrchText);
        fabCustSrchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LandingActivity.this,
                        CustomerListActivity.class);
                startActivity(i);
                finish();
            }
        });
        FloatingActionButton fabCustSrch = (FloatingActionButton) findViewById(R.id.fabCustSrch);
        fabCustSrch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LandingActivity.this,
                        CustomerListActivity.class);
                startActivity(i);
                finish();
            }
        });

        FloatingActionButton nearestcustviewbtn = (FloatingActionButton) findViewById(R.id.viewmore);
        nearestcustviewbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent iobj = new Intent(getApplicationContext(), NearestCustomerActivity.class);
                startActivity(iobj);

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isFABOpen) {
                    showFABMenu();
                } else {
                    closeFABMenu();
                }
            }
        });

//        FloatingActionButton recenter = (FloatingActionButton) findViewById(R.id.center);
//        recenter.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(getApplicationContext(),"Recenter",Toast.LENGTH_SHORT).show();
//            }
//        });

        if (!isNetworkAvailable()) {
            Global.Toast(LandingActivity.this,
                    "No Network Found. Please Try Again.!",
                    Toast.LENGTH_SHORT, Font.Regular);
            return;
        }

        mapurl = db.getmapurl();
        mapurloptions = db.getmapurloptions();
        Log.e("mapurloptions", mapurl + mapurloptions);
        try {
            Log.e("re", "readingregions");
            regions = db.getregionlatlngsNew();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        latt2 = 0;
        longi2 = 0;
        // accuracy = 0;
        PlayLocation tracker = new PlayLocation();
        // Vehicletracker tracker = new Vehicletracker();
        latt2 = tracker.getlattitude();
        longi2 = tracker.getlongitude();
        accuracy = tracker.getaccuracy();
        String myloc = "{\"lat\":\"0\",\"long\":\"0\"}";
        loc = tracker.getMyLocation();
        try {
            locations = db.getalllatlongs();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (locations.length() > 0) {
            key1 = locations.toString();
        }
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        WebView mWebView = (WebView) findViewById(R.id.map);

        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                progressBar.setProgress(progress);
                if (progress == 100)
                    progressBar.setVisibility(View.GONE);
            }
        });
        mWebView.setWebViewClient(new WebViewClient());
//        String url = "file:///android_asset/landingmap.html";
         String url = "file:///android_asset/Maps_Google.html";
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(url);
        mWebView.addJavascriptInterface(
                new StringGetter(LandingActivity.this), "Route");
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;


        Toast.makeText(getApplicationContext(), R.string.touch_again_to_exit,
                Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void showFABMenu() {
        isFABOpen = true;
        fabLayoutNewCust.setVisibility(View.VISIBLE);
        fabLayoutNewCust.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        fabLayoutCustSrch.setVisibility(View.VISIBLE);
        fabLayoutCustSrch.animate().translationY(-getResources().getDimension(R.dimen.standard_105));
//        fabLayoutCustSrchRefresh.setVisibility(View.VISIBLE);
//        fabLayoutCustSrchRefresh.animate().translationY(-getResources().getDimension(R.dimen.standard_155));
    }

    private void closeFABMenu() {
        isFABOpen = false;
        fabLayoutNewCust.animate().translationY(0);
        fabLayoutNewCust.setVisibility(View.GONE);
        fabLayoutCustSrch.animate().translationY(0);
        fabLayoutCustSrch.setVisibility(View.GONE);
//        fabLayoutCustSrchRefresh.animate().translationY(0);
//        fabLayoutCustSrchRefresh.setVisibility(View.GONE);
    }

    private void startTimerService() {
        // Toast.makeText(getApplicationContext(), "startTimerService on sync",
        // Toast.LENGTH_LONG).show();
        // startService(new Intent(this, CopyOfVehicletracker.class));
        // startService(new Intent(this, Vehicletracker.class));
        Calendar cal = Calendar.getInstance();
        Intent intent = new Intent(LandingActivity.this,
                CopyOfVehicletracker.class);
        PendingIntent pintent = PendingIntent.getService(
                LandingActivity.this, 0, intent, 0);
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                1000, pintent);

        Intent intentn = new Intent(LandingActivity.this, PlayTracker.class);
        PendingIntent pintentn = PendingIntent.getService(
                LandingActivity.this, 0, intentn, 0);
        AlarmManager alarmn = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmn.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                1000, pintentn);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Synchronize synchronize = new Synchronize(LandingActivity.this, false, true);

        try {

//            synchronize.syncData(true,getApplicationContext());
            synchronize.syncData();
        } catch (Exception e) {

            e.printStackTrace();
            //   Toast.makeText(getApplicationContext(), "Please try again..", Toast.LENGTH_SHORT).show();
        }
    }

    private void openorclosenavmenu() {

        DrawerLayout DrawerLayout = (android.support.v4.widget.DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView nav = (NavigationView) findViewById(R.id.nav_view);
        if (nav.isShown())
            DrawerLayout.closeDrawers();
        else
            DrawerLayout.openDrawer(nav);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        DrawerLayout schbgnpopup = (DrawerLayout) findViewById(R.id.drawer_layout);
        Intent intent;
        switch (id) {
            case R.id.nav_refreshmap:
                Intent i = getIntent();
                startActivity(i);
                finish();
                overridePendingTransition(0, 0);
                return true;
            case R.id.nav_dashboard:
                intent = new Intent(LandingActivity.this, SummaryActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
                return true;
            case R.id.nav_marketactivities:
                intent = new Intent(LandingActivity.this, MarketActivities.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
                return true;
            case R.id.nav_refreshcache:
                try {
                    refreshcachedata();
                }catch(Exception e){
                    e.printStackTrace();
                }
//                if (db.anyoperation()) {
//                    if (db.isanycompleteschedule()) {
//                        refreshOk();
//                    } else {
//                        Global.Toast(LandingActivity.this,
//                                "Please Complete Schedule!", Toast.LENGTH_LONG,
//                                Font.Regular);
//                    }
//                } else {
//                    refreshOk();
//                }
                return true;
            case R.id.nav_fclrdata:
                try {
                    Global.force = true;
                    forceclearalldata();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                return true;
            case R.id.nav_chkupdate:
                Global.Toast(
                        LandingActivity.this,
                        "Checking for new version. You will be alerted if available.",
                        Toast.LENGTH_SHORT, Font.Regular);
                checkversion();
                return true;
            case R.id.nav_resetpswd:
                intent = new Intent(LandingActivity.this, ResetPasswordActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
//                Toast.makeText(getApplicationContext(), "nav_resetpswd", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.nav_about:
                Intent intent2 = new Intent(LandingActivity.this, AboutActivity.class);
                startActivity(intent2);
                finish();
                overridePendingTransition(0, 0);
                return true;
            case R.id.nav_logout:
                if (db.anyoperation()) {
                    if (db.isanycompleteschedule()) {
                        logoutOK();
                    } else {
                        Global.Toast(LandingActivity.this,
                                "Please Complete Schedule!", Toast.LENGTH_LONG,
                                Font.Regular);
                    }
                } else {
                    logoutOK();
                }
                return true;
//            case R.id.nav_completeschedl:
//                try {
//                    if (scheduledata.length <= 0) {
//                        Toast.makeText(LandingActivity.this,R.string.no_schedules_available, Toast.LENGTH_SHORT).show();
//                        return true;
//                    }


//                    completeschedule();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                return true;
            case R.id.nav_sync:
                syncData(menuItem);
                return true;

//            case R.id.nav_multilingual:
//                intent = new Intent(CustomerListActivity.this, MultiLanguage.class);
//                startActivity(intent);
//                finish();
//                overridePendingTransition(0, 0);
//                Toast.makeText(getApplicationContext(), "nav_resetpswd", Toast.LENGTH_SHORT).show();
//                return true;

        }
        return false;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void logoutOK() {
        try {
            Global.force = true;
            Global.resStart = false;
            logout();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void refreshOk() {

        if (isNetworkAvailable()) {
            webconfigurration.status = "Refresh";
            Global.force = false;
            Global.resStart = false;
            requestschedules();

        } else {
            Global.Toast(LandingActivity.this,
                    "You Are Offline..Check Your Connection",
                    Toast.LENGTH_SHORT, Font.Regular);
        }

    }

    public void requestschedules() {

        NavigationView nav = (NavigationView) findViewById(R.id.nav_view);
        LayoutInflater layoutInflater = (LayoutInflater) LandingActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.custom_refreshcache_alert, null);
        Button closePopupBtn = (Button) customView.findViewById(R.id.close);
        Button confirm = (Button) customView.findViewById(R.id.yes);
        //instantiate popup window

        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        final PopupWindow popupWindow = new PopupWindow(customView, width, height);
        popupWindow.setFocusable(true);

        //display the popup window
        popupWindow.showAtLocation(nav, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        //close the popup window on button click
        closePopupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                if (!isNetworkAvailable()) {
                    Global.Toast(LandingActivity.this,
                            "You Are Offline. Check your Connection",
                            Toast.LENGTH_SHORT, Font.Regular);
                    return;
                }

                try {
                    refreshbutton = true;
                    uploadordedata(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void syncData(MenuItem menuItem) {

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ImageView iv = (ImageView) inflater.inflate(R.layout.nav_menu_item_sync, null);

        if (!isNetworkAvailable()) {
            Global.Toast(LandingActivity.this,
                    "You Are Offline. Check your Connection",
                    Toast.LENGTH_SHORT, Font.Regular);
            return;
        }

        Animation rotation = AnimationUtils.loadAnimation(LandingActivity.this, R.anim.rotate);
        rotation.setRepeatCount(Animation.INFINITE);
        iv.startAnimation(rotation);

        menuItem.setActionView(iv);

        Synchronize synchronize = new Synchronize(LandingActivity.this, false, false);

        try {
//            synchronize.syncData(false, getApplicationContext());
            synchronize.syncData();
        } catch (Exception e) {

            e.printStackTrace();
            Toast.makeText(getApplicationContext(), R.string.try_again, Toast.LENGTH_SHORT).show();
            completeSync();
        }

//        completeSync(menuItem);
    }

    public void completeSync() {

//        MenuItem menuItem = menu.findItem(R.id.nav_sync);
//
//        menuItem.getActionView().clearAnimation();
//        menuItem.setActionView(null);
        MenuItem menuItem = menu.findItem(R.id.nav_sync);
        if (menuItem != null && menuItem.getActionView() != null) {
            menuItem.getActionView().clearAnimation();
            menuItem.setActionView(null);
        }
    }

    private void checkversion() {
        JSONObject params = new JSONObject();
        try {
            params.put("AppName", "dsale");
            params.put("Version", "1");
            params.put("email", "");
            params.put("password", "");
            String auth = webconfigurration.auth;
            auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
            params.put("auth", auth);
            params.put("Operation", "VersionCheck");

            new LongOperation().execute(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // ///force clear all data
    public void forceclearalldata() throws JSONException {
        NavigationView nav = (NavigationView) findViewById(R.id.nav_view);
        LayoutInflater layoutInflater = (LayoutInflater) LandingActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View customView = layoutInflater.inflate(R.layout.forceclr_popuplayout, null);
        Button closePopupBtn = (Button) customView.findViewById(R.id.close);
        Button confirm = (Button) customView.findViewById(R.id.yes);
        TextView title = (TextView) customView.findViewById(R.id.header);
        TextView msg = (TextView) customView.findViewById(R.id.message);
        title.setText(R.string.force_clear_confirmation_title);
        msg.setText(R.string.force_clear_confirmation_message);
        final EditText forcepass = (EditText) customView.findViewById(R.id.forcepass);
        forcepass.setInputType(InputType.TYPE_CLASS_TEXT
                | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        //instantiate popup window

        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        final PopupWindow popupWindow = new PopupWindow(customView, width, height);
        popupWindow.setFocusable(true);

        //display the popup window
        popupWindow.showAtLocation(nav, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        //close the popup window on button click
        closePopupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                String userinput = forcepass.getText().toString();
                SharedPreferences pref = getApplicationContext()
                        .getSharedPreferences("Config", MODE_PRIVATE);
                String orginalpassword = pref.getString("passcode", "");

                if (userinput.equals(orginalpassword)) {
//                                               confirnlogout();
                    confirmlogout();
//                                               try {
//                                                   logout();
//                                               } catch (JSONException e) {
//                                                   e.printStackTrace();
//                                               }

                } else {
                    Global.Toast(LandingActivity.this, "Wrong Password",
                            Toast.LENGTH_SHORT, Font.Regular);

                }

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case R.id.action_create_alarm:
                openorclosenavmenu();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void confirnlogout() {
        //====================================================logout data
        Global.force = true;
        JSONArray uplatlong = new JSONArray();
        JSONArray datadb = new JSONArray();
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        try {
            JSONArray closingdayupdates = db.getclosingdayupdates(false);
            JSONArray unvisitedstores = db.getunvisitedstores(false);
            JSONArray orderpayments = db.getorderpaymentstosync(false);
            JSONArray datatoupload = new JSONArray();

            JSONArray denominationdata = db.getdenominationdatatatosync(false);
            JSONArray orderdata = db.getorderdatatosync(false);
            JSONArray paymentlist = db.getpaymentstosync(false);
            JSONArray salesreturn = db.getsalesreturndatatosync(false);
            JSONArray creditnote = db.getcreditnotestosync(false);
            JSONArray newcostomerdata = uploadlocations();

            JSONArray updatecustomers = db.getupdatecustomerstosync(false);
            JSONArray updatephotos = db.getphotostosync(false);
            JSONArray cratesdata = db.getcratestosync(false);
            JSONArray passbyupdated = db.getpassbyupdatedsync(false);

            JSONArray expenses = new JSONArray();//db.getExpenseDataForSync(false) ;
            if (db.isserviceexist("Van Sales")) {
                orderdata = new JSONArray();//db.getvansalesdatatosync(false);
                paymentlist = new JSONArray();//db.getvansalespaymentstosync(false);
            }
            datatoupload = db.getorders(false);
            uplatlong = db.getupdatedlaatlong(false);

            datadb = datatoupload;

            if (db.getpendingNotApproved()) {
               /* Global.Toast(CustomerListActivity.this, R.string.payment_approval_pending,
                        Toast.LENGTH_SHORT, Font.Regular);*/
                Toast.makeText(LandingActivity.this, R.string.payment_approval_pending,
                        Toast.LENGTH_SHORT).show();
                return;
            }
            if (db.getpendingNotApprovedOrders()) {
                /*Global.Toast(LandingActivity.this, "Order Approvals Pending!",
                        Toast.LENGTH_SHORT, Font.Regular);*/

                Toast.makeText(LandingActivity.this, R.string.order_approval_pending,
                        Toast.LENGTH_SHORT).show();
                return;
            }
            if (expenses.length() > 0 || db.anypendingorderexist() || uplatlong.length() > 0
                    || newcostomerdata.length() > 0 || paymentlist.length() > 0
                    || orderpayments.length() > 0 || unvisitedstores.length() > 0
                    || closingdayupdates.length() > 0
                    || denominationdata.length() > 0 || orderdata.length() > 0
                    || salesreturn.length() > 0 || creditnote.length() > 0
                    || updatecustomers.length() > 0 || cratesdata.length() > 0
                    || updatephotos.length() > 0 || passbyupdated.length() > 0 || isphotostosync()) {
                // AlertDialog.Builder builder = new AlertDialog.Builder(
                // DashboardActivity.this);
                // builder.setTitle("Sync Data");
                // builder.setMessage("You need to sync your data before logout");
                AlertDialog builder = new AlertDialog.Builder(LandingActivity.this)
                        .setTitle(R.string.sync_data)
                        .setMessage(R.string.logout_sync_data_warning_message)
                        .setPositiveButton(
                                android.R.string.yes,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(
                                            DialogInterface dialog,
                                            int which) {
                                    }
                                })
                        .setNegativeButton(
                                android.R.string.cancel,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(
                                            DialogInterface dialog,
                                            int which) {
                                    }
                                })
                        .show();

            } else {
                String status = db.getschedulestatus();
//                if (status.equals("Completed")) {
                NavigationView nav = (NavigationView) findViewById(R.id.nav_view);
                LayoutInflater layoutInflater = (LayoutInflater) LandingActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View customView = layoutInflater.inflate(R.layout.popuplayout, null);
                TextView header = (TextView) customView.findViewById(R.id.header);
                header.setText(R.string.logout_confirmation_title);
                TextView msg = (TextView) customView.findViewById(R.id.message);
                msg.setText(R.string.logout_confirmation_message);
                Button closePopupBtn = (Button) customView.findViewById(R.id.close);
                Button confirm = (Button) customView.findViewById(R.id.yes);

                //instantiate popup window

                int width = LinearLayout.LayoutParams.MATCH_PARENT;
                int height = LinearLayout.LayoutParams.MATCH_PARENT;
                final PopupWindow popupWindow = new PopupWindow(customView, width, height);
                popupWindow.setFocusable(true);

                //display the popup window
                popupWindow.showAtLocation(nav, Gravity.CENTER, 0, 0);
                popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                //close the popup window on button click
                closePopupBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });
                confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                        try {
                            logout();
                        } catch (Exception c) {
                            c.printStackTrace();
                        }
                    }
                });

//                } else {
//                    Toast.makeText(getApplicationContext(), R.string.complete_schedule_before_Logout, Toast.LENGTH_SHORT).show();
//                    return;
//                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    // /upload locations
    public JSONArray uploadlocations() {

        String data = "";
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        JSONArray datatoupload = new JSONArray();
        try {
            datatoupload = db.getlocationdata();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return datatoupload;
        // uploadorders(data);

    }

    // called when logout called
    public void logout() throws JSONException {
        Global.force = true;
        JSONArray uplatlong = new JSONArray();
        JSONArray newcostomerdata = uploadlocations();
        JSONArray datadb = new JSONArray();
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        JSONArray closingdayupdates = db.getclosingdayupdates();
        JSONArray unvisitedstores = db.getunvisitedstores();
        JSONArray orderpayments = db.getorderpaymentstosync();
        JSONArray datatoupload = new JSONArray();
        JSONArray paymentlist = db.getpaymentstosync();
        JSONArray denominationdata = db.getdenominationdatatatosync();
        JSONArray appusagedata = db.appusagedatatosync();
        // JSONArray orderdata = db.getorderdatatosync();
        JSONArray salesreturn = db.getsalesreturndatatosync();
        JSONArray creditnote = db.getcreditnotestosync();
        //JSONArray stockentry = db.getstockentrydatatosync();
        JSONArray updatecustomers = db.getupdatecustomerstosync();
        JSONArray updatephotos = db.getphotostosync();
        JSONArray cratesdata = db.getcratestosync();
        JSONArray passbyupdated = db.getpassbyupdatedsync();
        JSONArray assetentry = db.getassetstosync();
        //JSONArray sendbackentry = db.sendbackstosync();
        //JSONArray deliveryupdate = db.getdeliverystatusforsync();
        JSONArray schedulebegin = db.getschedulebeginsync();

        //if (db.NavDatacounttoSync() > 5)
        syncnavdata();


        try {
            datatoupload = db.getorders();
            uplatlong = db.getupdatedlaatlong();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        datadb = datatoupload;
        if (db.getpendingNotApproved()) {
            Global.Toast(LandingActivity.this, "Payment Approvals Pending!",
                    Toast.LENGTH_SHORT, Font.Regular);
            return;
        }

//        if (db.getpendingNotApprovedOrders()) {
//            Global.Toast(LandingActivity.this, "Order Approvals Pending!",
//                    Toast.LENGTH_SHORT, Font.Regular);
//            return;
//        }
//        if (db.getpendingNotApprovedOrdersstk()) {
//            Global.Toast(LandingActivity.this,
//                    "Stock Entry Approvals Pending!", Toast.LENGTH_SHORT,
//                    Font.Regular);
//            return;
//        }
        // inputParamObj.put("DhiToken", SecurityHandler.Encrypt() );
         if (!isNetworkAvailable()) {
                Global.Toast(LandingActivity.this,
                        "You Are Offline. Check your Connection",
                        Toast.LENGTH_SHORT, Font.Regular);
                return;
            }
        if (db.anypendingorderexist() || uplatlong.length() > 0
                || newcostomerdata.length() > 0 || paymentlist.length() > 0
                || orderpayments.length() > 0 || unvisitedstores.length() > 0
                || closingdayupdates.length() > 0
                || denominationdata.length() > 0 || appusagedata.length() > 0
                //|| orderdata.length() > 0
                || salesreturn.length() > 0
                || creditnote.length() > 0 || updatecustomers.length() > 0
                || cratesdata.length() > 0 || updatephotos.length() > 0
                || passbyupdated.length() > 0
                //|| stockentry.length() > 0
                || assetentry.length() > 0
                || isphotostosync()
                //|| sendbackentry.length() > 0
                //|| PFDdata.length() > 0 || deliveryupdate.length() > 0
                || schedulebegin.length() > 0 || db.NavDatacounttoSync() > 4) {


            NavigationView nav = (NavigationView) findViewById(R.id.nav_view);
            LayoutInflater layoutInflater = (LayoutInflater) LandingActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View customView = layoutInflater.inflate(R.layout.popuplayout, null);
            TextView header = (TextView) customView.findViewById(R.id.header);
            header.setText(R.string.sync_data);
            TextView msg = (TextView) customView.findViewById(R.id.message);
            msg.setText(R.string.logout_sync_data_warning_message);
            Button closePopupBtn = (Button) customView.findViewById(R.id.close);
            Button confirm = (Button) customView.findViewById(R.id.yes);

            //instantiate popup window

            int width = LinearLayout.LayoutParams.MATCH_PARENT;
            int height = LinearLayout.LayoutParams.MATCH_PARENT;
            final PopupWindow popupWindow = new PopupWindow(customView, width, height);
            popupWindow.setFocusable(true);

            //display the popup window
            popupWindow.showAtLocation(nav, Gravity.CENTER, 0, 0);
            popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            //close the popup window on button click
            closePopupBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                }
            });
            confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                }
            });

//            AlertDialog.Builder builder = Global.Alert(LandingActivity.this,
//                    "Sync Data", "You need to sync your data before logout.");
//            builder.setPositiveButton("OK",
//                    new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//
//                        }
//                    });
//
//            builder.setNeutralButton("CANCEL",
//                    new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//
//                        }
//                    });
//
//            AlertDialog dialog = builder.create();
//            dialog.show();
//            Global.setDialogFont(LandingActivity.this, dialog);

        } else {


            NavigationView nav = (NavigationView) findViewById(R.id.nav_view);
            LayoutInflater layoutInflater = (LayoutInflater) LandingActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View customView = layoutInflater.inflate(R.layout.popuplayout, null);
            TextView header = (TextView) customView.findViewById(R.id.header);
            header.setText(R.string.logout_confirmation_title);
            TextView msg = (TextView) customView.findViewById(R.id.message);
            msg.setText(R.string.logout_confirmation_message);
            Button closePopupBtn = (Button) customView.findViewById(R.id.close);
            Button confirm = (Button) customView.findViewById(R.id.yes);

            //instantiate popup window

            int width = LinearLayout.LayoutParams.MATCH_PARENT;
            int height = LinearLayout.LayoutParams.MATCH_PARENT;
            final PopupWindow popupWindow = new PopupWindow(customView, width, height);
            popupWindow.setFocusable(true);

            //display the popup window
            popupWindow.showAtLocation(nav, Gravity.CENTER, 0, 0);
            popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            //close the popup window on button click
            closePopupBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                }
            });
            confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                    confirmlogout();
                }
            });

        }

    }

    private void syncnavdata() {
        db = new DatabaseHandler(getApplicationContext());
        List<Vehicledata> Navigdata = db.Getallnavigationdata();
        if (Navigdata.size() > 0) {
            try {
                Log.d("Insert:", "Reading all data..");
                Log.d("Insert:", "allvalues are readed");
                String count = String.valueOf(Navigdata.size());
                Log.d("Insert:", count);
                String params = jsonSerialise(Navigdata);
                Log.d("Insert:", params);
                String ids = getpkidstring(Navigdata);
                Log.d("Insert:", ids);
                for (int i = 0; i < Navigdata.size(); i++) {
                    Log.d("Insert:",
                            Navigdata.get(i).PlanId + " "
                                    + Navigdata.get(i).VehicleId + " "
                                    + Navigdata.get(i).DeviceTime + " "
                                    + Navigdata.get(i).Latitude + " "
                                    + Navigdata.get(i).Longitude + " "
                                    + Navigdata.get(i).Speed);
                }
                excutePostdatstream(params, ids);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public void excutePostdatstream(String urlParameters, String pkidlist)
            throws JSONException {
        webconfigurration C = new webconfigurration(getApplicationContext());
        String inputParamsStr = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"SalesproOperations\",\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";

        JSONObject inputParamObj = new JSONObject(inputParamsStr);
        JSONObject data = new JSONObject();
        data.put("operation", "SalesPersonNav").put("data",
                new JSONArray(urlParameters));
        inputParamObj.getJSONObject("data").getJSONArray("data").put(data);
        //Log.e("Salespersonnav", urlParameters);
        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        inputParamObj.put("auth", auth);
        String string = "";
        try {
            string = inputParamObj.toString();
        } catch (Exception ex) {
            ex.toString();
        }
        if (!string.contentEquals("")) {
            MyRunnable2 myRunnable = new MyRunnable2(string, pkidlist);
            Thread t = new Thread(myRunnable);
            if (!Global.synnav) {
                t.start();
                Global.synnav = true;
            }
            //Global.synnavlock.lock();
            //boolean isAcquired = Global.synnavlock.tryLock();
            //if(isAcquired){
            //Global.synnavlock.lock();
            //t.start();
            //}
        }
    }

    public String jsonSerialise(List<Vehicledata> Vehdata) throws JSONException {
        JSONArray data = new JSONArray();
        PackageInfo pinfo;
        int versionNumber = 0;
        String versionName = "";
        String android_id = "";
        String imei = "";

        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionNumber = pinfo.versionCode;
            versionName = pinfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            android_id = Settings.Secure.getString(getApplicationContext()
                    .getContentResolver(), Settings.Secure.ANDROID_ID);

            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return "NA";
            }
            imei = telephonyManager.getDeviceId();
        } catch (Exception e) {
            if (android_id != null && android_id.equals(""))
                android_id = "NA";

            if (imei != null && imei.equals(""))
                imei = "NA";

            e.printStackTrace();
        }

        for (int i = 0; i < Vehdata.size(); i++) {
            JSONObject obj = new JSONObject();
            obj.put("SalesPersonId", Vehdata.get(i).PlanId);
            obj.put("NavTime", Vehdata.get(i).DeviceTime);
            obj.put("lat", Vehdata.get(i).Latitude);
            obj.put("lng", Vehdata.get(i).Longitude);
            obj.put("provider", Vehdata.get(i).provider);
            obj.put("accuracy", Vehdata.get(i).accuracy);
            obj.put("speed", Vehdata.get(i).Speed);
            obj.put("bearing", Vehdata.get(i).bearing);
            obj.put("distance", Vehdata.get(i).distance);
            obj.put("AppVersion", versionName);
            obj.put("AndroidId", android_id);
            obj.put("imei", imei);
            obj.put("customer", Vehdata.get(i).customer);
            obj.put("timediff", Vehdata.get(i).timediff);
            obj.put("inactiveflag", Vehdata.get(i).inactive);
            data.put(obj);

        }
        return data.toString();
    }

    public void syncmobilelog() throws Exception {
        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = dateTimeFormat.format(c.getTime());

        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);
        String personid = pref.getString("personid", "0");

        webconfigurration C = new webconfigurration(getApplicationContext());

        String inputParamsStr = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"SalesproOperations\",\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";
        JSONObject inputParamObj = new JSONObject(inputParamsStr);

        JSONObject updatestatus = null;
        try {
            webconfigurration.status = "Logout";
            updatestatus = getstatusobject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        inputParamObj.put("updatestatus", updatestatus);
        JSONObject data = new JSONObject();
        data.put("operation", "PutMobileLog");

        inputParamObj.getJSONObject("data").getJSONArray("data").put(data);
        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        inputParamObj.put("auth", auth);

        new LongOperation2().execute(inputParamObj);

    }

    public void confirmlogout() {

        try {
            syncmobilelog();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // stopService(new Intent(this, Vehicletracker.class));
        // SharedPreferences pref =
        // getApplicationContext().getSharedPreferences(
        // "Config", MODE_PRIVATE);
        // Editor editor = pref.edit();
        // editor.clear();
        // editor.commit();
        // DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        // db.Deletetabledata();
        // Intent intent = new Intent(this, MainActivity.class);
        // finish();
        // overridePendingTransition(0, 0);
        // startActivity(intent);
        // overridePendingTransition(0, 0);

    }

    private JSONObject getstatusobject() throws Exception {

        JSONObject temp = new JSONObject();
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", MODE_PRIVATE);

        String person = "";
        String lat = "";
        String lng = "";
        String operation = "";
        String version = "";
        String createdon = "";
        String uuid = "";
        String appname = "";
        String versioncode = "";
        person = pref.getString("personid", "0");

        operation = webconfigurration.status;
        appname = webconfigurration.appname;
        lat = webconfigurration.lat;
        lng = webconfigurration.lng;

        PackageInfo pInfo = getPackageManager().getPackageInfo(
                getPackageName(), 0);
        version = pInfo.versionName;
        versioncode = String.valueOf(pInfo.versionCode);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        createdon = df.format(c.getTime());

        uuid = UUID.randomUUID().toString();
        String battery = batteryLevel();
        // Global.Toast(DashboardActivity.this, battery, Toast.LENGTH_LONG,
        // Font.Regular);
        temp.put("person", person).put("lat", lat).put("lng", lng)
                .put("operation", operation).put("version", version)
                .put("versioncode", versioncode).put("appname", appname)
                .put("createdon", createdon).put("uuid", uuid)
                .put("battery", battery);
        return temp;
    }

    private String batteryLevel() {
        // BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver() {
        // public void onReceive(Context context, Intent intent) {
        // context.unregisterReceiver(this);
        // int rawlevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL,
        // -1);
        // int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        // int level = -1;
        // if (rawlevel >= 0 && scale > 0) {
        // level = (rawlevel * 100) / scale;
        // }
        // bat = level + "";
        // }
        // };
        // IntentFilter batteryLevelFilter = new IntentFilter(
        // Intent.ACTION_BATTERY_CHANGED);
        // registerReceiver(batteryLevelReceiver, batteryLevelFilter);
        // return bat;

        Intent batteryIntent = registerReceiver(null, new IntentFilter(
                Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        // Error checking that probably isn't needed but I added just in case.
        if (level == -1 || scale == -1) {
            return String.valueOf(50.0f);
        }

        return String.valueOf(((float) level / (float) scale) * 100.0f);
    }


    public void uploadordedata(Boolean isrequestnew) throws JSONException {

        JSONArray uplatlong = new JSONArray();
        JSONArray newcostomerdata = uploadlocations();
        JSONArray datadb = new JSONArray();
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());

        JSONArray datatoupload = new JSONArray();
        JSONArray paymentlist = db.getpaymentstosync();
        JSONArray orderpayments = db.getorderpaymentstosync();
        JSONArray unvisitedstores = db.getunvisitedstores();
        JSONArray closingdayupdates = db.getclosingdayupdates();
        JSONArray denominationdata = db.getdenominationdatatatosync();
        JSONArray appusagedata = db.appusagedatatosync();
//        JSONArray orderdata = db.getorderdatatosync();
//        JSONArray stockentry = db.getstockentrydatatosync();
        JSONArray salesreturn = db.getsalesreturndatatosync();
        JSONArray creditnote = db.getcreditnotestosync();
        JSONArray updatecustomers = db.getupdatecustomerstosync();
        JSONArray updatephotos = db.getphotostosync();
        JSONArray cratesdata = db.getcratestosync();
        JSONArray passbyupdated = db.getpassbyupdatedsync();
        JSONArray assetentry = db.getassetstosync();
//        JSONArray sendbackentry = db.sendbackstosync();
//        JSONArray deliveryupdate = db.getdeliverystatusforsync();
//        JSONArray PFDdata = db.PFDdatasync();
        JSONArray schedulebegin = db.getschedulebeginsync();

        try {
            datatoupload = db.getorders();
            uplatlong = db.getupdatedlaatlong();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        datadb = datatoupload;

        webconfigurration C = new webconfigurration(getApplicationContext());

        String inputParamsStr = "{\"data\": {\"User\": \""
                + C.user
                + "\",\"DBname\": \""
                + C.dbname
                + "\", \"BussinessUnit\": \""
                + C.bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"SalesproOperations\",\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                + C.appid
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                + C.orgid + "\"}," + "\"meta\": []}";

        JSONObject inputParamObj = new JSONObject(inputParamsStr);

        JSONObject updatestatus = null;
        try {
            webconfigurration.status = "Sync";
            updatestatus = getstatusobject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        inputParamObj.put("updatestatus", updatestatus);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "Config", Context.MODE_PRIVATE);
        JSONObject data = new JSONObject();
        data.put("operation", "PutOrders").put("data", datadb)
                .put("customers", newcostomerdata)
                .put("updatelatlong", uplatlong).put("payments", paymentlist)
                .put("reasons", unvisitedstores)
                .put("orderamountfromstores", orderpayments)
                .put("closingdayupdates", closingdayupdates)
                .put("Denominationdata", denominationdata)
                .put("AppUsageData", appusagedata)
                //.put("OrderTaking", orderdata)
                .put("SalesReturn", salesreturn)
                //.put("StockEntry", stockentry)
                .put("CreditNotes", creditnote)
                .put("updatecustomers", updatecustomers)
                .put("updatephotos", updatephotos)
                .put("cratesdata", cratesdata)
                .put("passbyupdated", passbyupdated)
                .put("AssetEntry", assetentry)
                //.put("SendBackEntry", sendbackentry).put("pfddata", PFDdata)
                //.put("deliveryupdate", deliveryupdate)
                .put("schedulebegin", schedulebegin);
        inputParamObj.getJSONObject("data").getJSONArray("data").put(data);
        inputParamObj.getJSONObject("data")
                .put("personid", pref.getString("personid", ""))
                .put("PersonOrg", pref.getString("tmsOrgId", ""));
        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        inputParamObj.put("auth", auth);

        Log.e("inputParamObj",inputParamObj.toString());
        // Dataformator obj=new Dataformator();
        // String test= obj.format(inputParamObj);

        // inputParamObj.put("DhiToken", SecurityHandler.Encrypt() );
        if (db.getpendingNotApproved()) {
            if (syncbutton || refreshbutton)
                Global.Toast(LandingActivity.this,
                        "Payment Approvals Pending!", Toast.LENGTH_SHORT,
                        Font.Regular);
//            enablesyncbutton();
        }
//        else if (db.getpendingNotApprovedOrders()) {
//            if (syncbutton || refreshbutton)
//                Global.Toast(LandingActivity.this,
//                        "Order Approvals Pending!", Toast.LENGTH_SHORT,
//                        Font.Regular);
//            enablesyncbutton();
//        } else if (db.getpendingNotApprovedOrdersstk()) {
//            if (syncbutton || refreshbutton)
//                Global.Toast(DashboardActivity.this,
//                        "Stock Entry Approvals Pending!", Toast.LENGTH_SHORT,
//                        Font.Regular);
//            enablesyncbutton();
//        }
        else if (db.anypendingorderexist() || uplatlong.length() > 0
                || newcostomerdata.length() > 0 || paymentlist.length() > 0
                || orderpayments.length() > 0 || unvisitedstores.length() > 0
                || closingdayupdates.length() > 0
                || denominationdata.length() > 0 || appusagedata.length() > 0
                //|| orderdata.length() > 0
                || salesreturn.length() > 0
                || creditnote.length() > 0 || updatecustomers.length() > 0
                || updatephotos.length() > 0 || cratesdata.length() > 0
                || passbyupdated.length() > 0
                //|| stockentry.length() > 0
                || isphotostosync()
                || assetentry.length() > 0
                //|| sendbackentry.length() > 0
                //|| PFDdata.length() > 0 || deliveryupdate.length() > 0
                || schedulebegin.length() > 0 || db.NavDatacounttoSync() > 4) {

            if (syncbutton || refreshbutton)
                Global.Toast(LandingActivity.this, "Syncing...",
                        Toast.LENGTH_SHORT, Font.Regular);
            uploadorders(inputParamObj.toString(), isrequestnew);
            if (db.NavDatacounttoSync() > 5)
                syncnavdata();

        } else {
            if (syncbutton || refreshbutton)
                Global.Toast(LandingActivity.this, "All data are synced",
                        Toast.LENGTH_SHORT, Font.Regular);
//            if (db.NavDatacounttoSync() > 5)
//                syncnavdata();
            syncbutton = false;
            refreshbutton = false;
            // reenabling sync button
            // ImageButton btnsync = (ImageButton)
            // findViewById(R.id.ScheduleSynchImageIcon);
//            syncnow.setEnabled(true);
Log.e("isrequestnew",isrequestnew+"");
            if (isrequestnew) {
                pref = getApplicationContext().getSharedPreferences("Config",
                        MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putFloat("distancetravelled", 0);
                editor.commit();
                // stopService(new Intent(this, Vehicletracker.class));
                db.Deletetabledata();
                reloaddata(false);
                // force update change
                //updateApp();
                //checkversion();
            }


        }

    }

    public void uploadorders(String urlParameters, Boolean callfrom) {

        MyRunnable myRunnable = new MyRunnable(urlParameters, callfrom);
        Thread t = new Thread(myRunnable);
        t.start();
    }

    public void reloaddata(boolean flag) {
		/*
		disconnectgoogleclient();
		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"Config", MODE_PRIVATE);
		String code = pref.getString("code", "");
		String password = pref.getString("passcode", "");
		webconfigurration.status = "Refresh";
		Editor editor = pref.edit();
		editor.putString("pdfdate", "");
		editor.commit();
		Intent intent = new Intent(this, Loading.class);
		intent.putExtra("code", code);
		intent.putExtra("passcode", password);
		finish();
		overridePendingTransition(0, 0);
		startActivity(intent);
		overridePendingTransition(0, 0);*/

        checkversion();

    }

    private boolean isphotostosync() {
        String path = Environment.getExternalStorageDirectory().toString() + "/Aries";
        Log.d("Files", "Path: " + path);
        final File directory = new File(path);

        final File[] filesRoot = directory.listFiles();
        if (filesRoot != null) {
            for (int i = 0; i < filesRoot.length; i++) {
                if (filesRoot[i].getName().contains("draft"))
                    filesRoot[i].delete();
            }
            final File[] files = directory.listFiles();
            final int size = (files == null) ? 0 : files.length;
            return size > 0 ? true : false;
        }

        return false;
    }


    // /////////////////////////syncdata/////////////////////////////////////////////////////////////////////////

    public class MyRunnable2 implements Runnable {

        private String urlParameters;
        private String pkidlist;

        public MyRunnable2(String urlParameters, String pkidlist) {
            this.urlParameters = urlParameters;
            this.pkidlist = pkidlist;
        }

        @Override
        public void run() {
            //boolean isAcquired = Global.synnavlock.tryLock();
            //if(isAcquired){
            /*Global.synnavlock.lock();	*/
            /*if(db.NavDatacounttoSync()!=pkidlist.split(",").length)
			{
				return;
			}*/
            // code in the other thread, can reference "var" variable
            // String targetURL =
            // "http://demo.dhicubes.com:8080/dhi/services/dhiservices/service";
            // String targetURL =
            // "http://192.168.2.360:8080/EMRCodes/services/dhiservices/service";
            // String targetURL =
            // "http://192.168.2.250:8080/dhicubes/services/dhiservices/service";
            // String targetURL =
            // "http://demo.dhicubes.com:8080/dhi/services/dhiservices/service";
            // QA
            webconfigurration C = new webconfigurration(getApplicationContext());
            // String targetURL = C.url;
            String targetURL = C.syncUrl.contentEquals("") ? C.url : C.syncUrl;
            // String targetURL =
            // "http://54.149.9.238:7070/Plan.svc/Savevehiclenavigationlist?";
            // String targetURL =
            // "http://192.168.2.250:8080/dhi/services/dhiservices/service";
            // String targetURL =
            // "http://192.168.2.24:8080/EMRCodes/services/dhiservices/service";
            // String targetURL =
            // "http://192.168.2.6:8081/EMRCodes/services/dhiservices/service";
            // String targetURL =
            // "http://52.88.168.158:8080/dhi/services/dhiservices/service";
            // String token = "DhiToken=";
            // String output = SecurityHandler.Encrypt();

            // targetURL = targetURL.concat(token).concat(output);
            try {
                if (db.NavDatacounttoSync() != pkidlist.split(",").length) {


                    throw new Exception();
                    //return;
                }
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-8");
                conn.setReadTimeout(30000);
                conn.setConnectTimeout(5000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);

                conn.setRequestMethod("POST");
                // conn.setRequestProperty("Connection", "Keep-Alive");
                // conn.setRequestProperty("Cache-Control", "no-cache");

                int totalLength = (urlParameters.getBytes().length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.writeBytes(urlParameters);
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();
                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }

                String responsefrom = response.toString();
                JSONObject resp = new JSONObject(responsefrom);
                responsefrom = resp.getString("data");
                Log.d("post", responsefrom);
                rd.close();

                if (responsefrom.equals("Success")) {
                    // DatabaseHandler db = new DatabaseHandler(
                    // getApplicationContext());
                    db.cleartable(pkidlist);
                    notifyReturn(resp);
                    //Global.synnavlock.unlock();
                }

                if (conn != null) {
                    conn.disconnect();
                }

            } catch (Exception e) {
                Log.d("post", "cannot create connection");
                //Global.synnavlock.unlock();
                e.printStackTrace();
            } finally {
                //boolean isAcquired = Global.synnavlock.tryLock();
                //if(!isAcquired){
                //Global.synnavlock.unlock();
                if (Global.synnav) {
                    Global.synnav = false;
                }
                //}
            }

        }//}
    }
// ////////UPLOAD ORDERS TO SERVER//////////

    private class LongOperation extends AsyncTask<JSONObject, Void, String> {

        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(JSONObject... credentials) {
            String responsefrom = null;
            webconfigurration C = new webconfigurration(getApplicationContext());
            String targetURL = C.loginurl;
            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-16");
                conn.setRequestProperty("Accept-Encoding", "identity");

                conn.setConnectTimeout(10000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setReadTimeout(10000);
                conn.setRequestMethod("POST");

                int totalLength = (credentials[0].toString().getBytes("utf-16").length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.write(credentials[0].toString().getBytes("utf-16"));
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                responsefrom = response.toString();
                rd.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responsefrom;
        }

        protected void onPostExecute(final String response) {
            // dialog.dismiss();
            if (response != null) {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        String replay = "";
                        try {
                            JSONObject resp = new JSONObject(response);
                            if (resp.optString("status").contentEquals(
                                    "success")) {
                                replay = resp.optString("Version");
                                int vcode = Integer.parseInt(replay);
                                int currnt = 0;
                                try {
                                    PackageManager manager = getApplicationContext()
                                            .getPackageManager();
                                    PackageInfo info = manager.getPackageInfo(
                                            getApplicationContext()
                                                    .getPackageName(), 0);
                                    currnt = info.versionCode;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                if (vcode > currnt) {
                                    Global.Toast(LandingActivity.this,"New Version Available!", Toast.LENGTH_LONG, Font.Regular);
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putBoolean("versionavailable",true);
                                    editor.putInt("version",vcode);
                                    editor.commit();
//                                    startActivity(getIntent());
//                                    finish();
//                                    overridePendingTransition(0,0);
//                                    AlertDialog builder = new AlertDialog.Builder(
//                                            LandingActivity.this)
//                                            .setTitle(getString(R.string.alert))
//                                            .setMessage(getString(R.string.new_version))
//                                            .setPositiveButton(
//                                                    android.R.string.yes,
//                                                    new DialogInterface.OnClickListener() {
//                                                        public void onClick(
//                                                                DialogInterface dialog,
//                                                                int which) {
//                                                            updateApp();
//                                                        }
//
//                                                        private void updateApp() {
//                                                            Intent intent = new Intent(
//                                                                    LandingActivity.this,
//                                                                    DownloadActivity.class);
//                                                            startActivity(intent);
//                                                        }
//                                                    })
//                                            .setNegativeButton(
//                                                    android.R.string.no,
//                                                    new DialogInterface.OnClickListener() {
//                                                        public void onClick(
//                                                                DialogInterface dialog,
//                                                                int which) {
//                                                        }
//                                                    })
//                                            .setIcon(
//                                                    android.R.drawable.ic_dialog_alert)
//                                            .show();
                                }
                            } else
                                replay = resp.optString("Error");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // Toast.makeText(getApplicationContext(), replay,
                        // Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                // Toast.makeText(getApplicationContext(),
                // "Cannot connect to server", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class LongOperation2 extends AsyncTask<JSONObject, Void, String> {
        private ProgressDialog mProgress;

        protected void onPreExecute() {
            try {
                mProgress = new ProgressDialog(LandingActivity.this);
                mProgress.setTitle("");
                mProgress.setContentView(R.layout.adapter_loading);
                mProgress.setMessage(getString(R.string.loading));
                mProgress.setCancelable(false);
                mProgress.setIndeterminate(true);
                mProgress.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... credentials) {
            String responsefrom = null;
            webconfigurration C = new webconfigurration(getApplicationContext());
            String targetURL = C.url;
            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-16");
                conn.setRequestProperty("Accept-Encoding", "identity");

                conn.setConnectTimeout(100000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setReadTimeout(200000);
                conn.setRequestMethod("POST");

                int totalLength = (credentials[0].toString().getBytes("utf-16").length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.write(credentials[0].toString().getBytes("utf-16"));
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                responsefrom = response.toString();
                rd.close();
            } catch (Exception e) {
                e.printStackTrace();

            }
            return responsefrom;
        }

        protected void onPostExecute(final String response) {
            try {
                if (response != null) {

                    JSONObject resp = new JSONObject(response);
                    if (resp.optString("status").contentEquals("success")) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                SharedPreferences pref = getApplicationContext()
                                        .getSharedPreferences("Config",
                                                MODE_PRIVATE);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.clear();
                                editor.commit();
                                DatabaseHandler db = new DatabaseHandler(
                                        getApplicationContext());
                                Global.force = true;
                                db.Deletetabledata();
                                Intent intent = new Intent(
                                        LandingActivity.this,
                                        LoginActivity.class);
                                finish();
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                                overridePendingTransition(0, 0);
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), R.string.cannot_connect_to_server, Toast.LENGTH_SHORT).show();
                                if (mProgress != null) {
                                    mProgress.dismiss();
                                }
                            }
                        });
                    }
                } else {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), R.string.cannot_connect_to_server, Toast.LENGTH_SHORT).show();
                            if (mProgress != null) {
                                mProgress.dismiss();
                            }
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), R.string.cannot_connect_to_server, Toast.LENGTH_SHORT).show();
                        if (mProgress != null) {
                            mProgress.dismiss();
                        }
                    }
                });
            }
        }
    }

    public class StringGetter {
        Context jContext;

        StringGetter(Context context) {
            jContext = context;
        }

        @JavascriptInterface
        public String  getPolygonActive(int i){
            return regions.get(i).discount;
        }
        @JavascriptInterface
        public void regionStatus(final int id) {

            try {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {



                            FourStrings reg = regions.get(id);

                            double tgt=0,achv=0;
                        try {
                            if (!reg.rate.contentEquals(""))
                                tgt = Double.parseDouble(reg.rate);
                            if (!reg.packageType.contentEquals(""))
                                achv = Double.parseDouble(reg.packageType);
                        }catch (Exception e){
                            e.printStackTrace();
                            Log.e("error",e.getMessage());
                        }
                            double pending=tgt-achv;
                            TextView txt=(TextView)findViewById(R.id.tgt);
                            txt.setText(tgt+"");
                            txt=(TextView)findViewById(R.id.achv);
                            txt.setText(achv+"");
                            txt=(TextView)findViewById(R.id.pending);
                            txt.setText(pending+"");
                            txt=(TextView)findViewById(R.id.title);
                            txt.setText(reg.mrp);
                            String val=db.getTodayCollectionCount();
                            txt=(TextView)findViewById(R.id.todaycol);
                            txt.setText(val);
                            popupsummary.setVisibility(View.VISIBLE);
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

            //Toast.makeText(getApplicationContext(), id+"", Toast.LENGTH_SHORT).show();
        }
        @JavascriptInterface
        public void gotoEdit(String SID){
            scheduleid=SID;
            Intent intent = new Intent(LandingActivity.this, SurrogatesEditActivity.class);
            intent.putExtra("scheduledetailid", scheduleid);
            //intent.putExtra("orgid",OrgID);
            startActivity(intent);

        }

        @JavascriptInterface
        public void getnavlatlong(String lat,String lng) {
            String navlat=lat;
            String navlng = lng;
            try {
                if (!lat.equals("")
                        && !lat.equals("0")
                        && !lat.equals("0.0") && !lng.equals("")
                        && !lng.equals("0")
                        && !lng.equals("0.0")) {

                    Uri gmmIntentUri = Uri.parse("google.navigation:q="
                            + lat + "," + lng);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW,
                            gmmIntentUri);
                    mapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mapIntent
                            .setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent); //getApplicationContext().

                } else {
                    Toast.makeText(getApplicationContext(),
                            "No Location Data Found..!",
                            Toast.LENGTH_LONG).show();
                }
            } catch (Exception ex) {

                Toast.makeText(getApplicationContext(),
                        "Install Google Maps or Check Internet Connectivity!", Toast.LENGTH_LONG)
                        .show();

            }
        }

        @JavascriptInterface
        public int getPolygonCount() {
            if(regions==null)
                return 0;
            Log.e("polycount", regions.size() + "");
            return regions.size();
        }

        @JavascriptInterface
        public String getPolygon(int i) {

           // Log.e("regionsofi", regions.get(i).qty);
            return regions.get(i).qty;
        }

        @JavascriptInterface
        public void getmaptype(String type) {
            mapstatus = type;
        }
        @JavascriptInterface
        public String choosemaptype() {
            return mapstatus;
        }

        @JavascriptInterface
        public String getString(String string) {
            if (string.contentEquals("live"))
                return livedata;
                // return
                // "[{\"lattitude\":\"10.25\",\"longitude\":\"76.5\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"11.25\",\"longitude\":\"76\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"12.25\",\"longitude\":\"76.5\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"13.25\",\"longitude\":\"76\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"14.25\",\"longitude\":\"76.5\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"15.25\",\"longitude\":\"76\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"16.25\",\"longitude\":\"76.5\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"17.25\",\"longitude\":\"76\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"18.25\",\"longitude\":\"76.5\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"19.25\",\"longitude\":\"76\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"20.25\",\"longitude\":\"76.5\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"21.25\",\"longitude\":\"76\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"22.25\",\"longitude\":\"76.5\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"23.25\",\"longitude\":\"76\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"24.25\",\"longitude\":\"76.5\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"25.25\",\"longitude\":\"76\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"26.25\",\"longitude\":\"76.5\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"27.25\",\"longitude\":\"76\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"28.25\",\"longitude\":\"76.5\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"},{\"lattitude\":\"29.25\",\"longitude\":\"76\",\"Name\":\"name1\",\"navtime\":\"123\",\"speed\":\"5\",\"provider\":\"gps\"}]";
            else
                return key1;
        }

        @JavascriptInterface
        public String getRole() {
            SharedPreferences pref = getApplicationContext().getSharedPreferences("Config",
                    MODE_PRIVATE);
            return pref.getString("personrole", "");
            //return "30";
        }


        @JavascriptInterface
        public String mapURL() {
            return mapurl;// == null ?
            // "https://maps.dhisigma.com/osm_tiles/{z}/{x}/{y}.png"
            // : mapurl;
        }

        @JavascriptInterface
        public String mapURLOptions() {
            return mapurloptions;// == null ?
            // "{minZoom: 4, maxZoom:18,attribution: '&copy <a href=\"http://openstreetmap.org\">OpenStreetMap</a>'}"
            // : mapurloptions;
        }

        @JavascriptInterface
        public String getOrg() {
            return pref.getString("tmsOrgId", "");
            //return "30";
        }
        @JavascriptInterface
        public String initialdata() {
            JSONObject obj = new JSONObject();
            try {
                webconfigurration C = new webconfigurration(getApplicationContext());
                String targetURL = C.syncUrl.contentEquals("") ? C.url : C.syncUrl;
                Calendar c = Calendar.getInstance();
                DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = dateTimeFormat.format(c.getTime());

                obj.put("dateT", formattedDate);
                obj.put("bu", pref.getString("BussinessUnit", ""));
                obj.put("usr", pref.getString("UserName", ""));
                obj.put("mOrg", pref.getString("orgId", ""));
                obj.put("dbname", pref.getString("orgDb", ""));
                obj.put("tmsorgid", pref.getString("tmsOrgId", ""));
                obj.put("personid", pref.getString("personid", ""));
                obj.put("syncUrl", targetURL);//schid
                if(Global.schedule.contentEquals(""))
                    obj.put("schid", "All");//
                else
                    obj.put("schid", Global.schedule);//
            } catch (Exception e) {
                e.printStackTrace();
            }
            return obj.toString();
        }

        @JavascriptInterface
        public String syncUrl() {
            webconfigurration C = new webconfigurration(getApplicationContext());
            // String targetURL = C.url;
            String targetURL = C.syncUrl.contentEquals("") ? C.url : C.syncUrl;
            return targetURL;
        }


        //syncUrl
        @JavascriptInterface
        public String myLoc() {
            String myloc = "";

            Location loc = null;
            if (PlayTracker.getLastLocation() != null) {
                loc = PlayTracker.getLastLocation();


                myloc = "{\"lat\":\"" + String.valueOf(loc.getLatitude())
                        + "\",\"long\":\"" + String.valueOf(loc.getLongitude())
                        + "\",\"accuracy\":\""
                        + String.valueOf(loc.getAccuracy()) + "\",\"val\":\""
                        + loc.getAccuracy() + "\"}";
                // else
                //Log.e("myloc",myloc);
                final float ac = loc.getAccuracy() / 2;

            }
            return myloc;
        }

        @JavascriptInterface
        public void getnearestcustomer(String type) {

            try {

                JSONObject jsnobj = new JSONObject(type);

                JSONArray jsonArray = jsnobj.getJSONObject("data").getJSONArray("GetNearestCustomers");

                if(jsonArray.length() == 0){
                    Toast.makeText(getApplicationContext(),"No Data Available!",Toast.LENGTH_LONG).show();
                }

                db.insert_nearestcust(jsonArray);
                //Toast.makeText(LandingActivity.this,jsonArray.toString(),Toast.LENGTH_LONG).show();
             /*   custNear = new String[jsonArray.length()];
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject explrObject = jsonArray.getJSONObject(i);

                    String n = explrObject.getString("CustName");
                   // Toast.makeText(LandingActivity.this,n,Toast.LENGTH_LONG).show();
                    custNear[i] = n;
                    //Toast.makeText(LandingActivity.this,custNear[i].toString(),Toast.LENGTH_LONG).show();
                }
*/
                //Toast.makeText(LandingActivity.this,custNear[0].toString(),Toast.LENGTH_LONG).show();
                //JSONArray nearlist = new JSONArray(type);
                //Toast.makeText(LandingActivity.this,nearlist.toString(),Toast.LENGTH_LONG).show();
                //JSONObject o = new JSONObject(nearcustlist);
                //JSONObject obj = new JSONObject();
                /*for(int i=0;i<nearlist.length();i++){
                    JSONObject obj = nearlist.getJSONObject(i);
                    custNear[i] = obj.getString("CustName");



                }*/
            }
            catch (Exception e) {
                e.printStackTrace();
            }


        }

    }

    public class MyRunnable implements Runnable {

        private String urlParameters;
        private Boolean isrequestnewschedule = false;

        public MyRunnable(String urlParameters, Boolean callfrom) {
            this.urlParameters = urlParameters;
            this.isrequestnewschedule = callfrom;
        }

        @Override
        public void run() {

            webconfigurration C = new webconfigurration(getApplicationContext());
            final String targetURL = C.syncUrl.contentEquals("") ? C.url
                    : C.syncUrl;
            // runOnUiThread(new Runnable() {
            // public void run() {
            // Global.Toast(DashboardActivity.this, targetURL,
            // Toast.LENGTH_LONG, Font.Regular);
            // }
            // });

            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-16");
                conn.setRequestProperty("Accept-Encoding", "identity");

                conn.setReadTimeout(60000);
                conn.setConnectTimeout(10000);
                //conn.setReadTimeout(9000);
                //conn.setConnectTimeout(5000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);

                conn.setRequestMethod("POST");
                // conn.setRequestProperty("Connection", "Keep-Alive");
                // conn.setRequestProperty("Cache-Control", "no-cache");

                // getting syncevent trigger time
                Calendar c = Calendar.getInstance();
                DateFormat dateTimeFormat = new SimpleDateFormat(
                        "yyyyMMddHHmmss");
                String formattedDate = dateTimeFormat.format(c.getTime());

                int totalLength = (urlParameters.getBytes("utf-16").length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.write(urlParameters.getBytes("utf-16"));
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                String responsefrom = response.toString();
                JSONObject resp = new JSONObject(responsefrom);
                responsefrom = resp.getString("data");
                Log.d("post", responsefrom);
                rd.close();
                if (responsefrom.equals("Success")) {

                    DatabaseHandler db = new DatabaseHandler(
                            getApplicationContext());
                    db.updatesynctable(formattedDate);
                    db.updatesynctablelocationsync(formattedDate);

                    if (isrequestnewschedule) {
//                        stopsevice();
                        db.Deletetabledata();
                        reloaddata(true);
                    }

                }

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        // update ui here
                        // display toast here
                        if (syncbutton || refreshbutton)
                            Global.Toast(LandingActivity.this,
                                    "Syncing Completed", Toast.LENGTH_SHORT,
                                    Font.Regular);
                        syncbutton = false;
                        refreshbutton = false;
                    }
                });

                if (conn != null) {
                    conn.disconnect();
                }
//                enablesyncbutton();

            } catch (final Exception e) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        // Global.Toast(DashboardActivity.this, e.toString(),
                        // Toast.LENGTH_LONG, Font.Regular);
                    }
                });
                Log.d("post", "cannot create connection");
                e.printStackTrace();

//                enablesyncbutton();
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        // update ui here
                        // display toast here
                        if (syncbutton || webconfigurration.status == "Refresh")
                            Global.Toast(LandingActivity.this,
                                    "Cannot connect to server",
                                    Toast.LENGTH_SHORT, Font.Regular);
                    }
                });

            }

        }
    }

    private void refreshcachedata() throws JSONException {


            Global.force = true;
            JSONArray uplatlong = new JSONArray();
            JSONArray newcostomerdata = uploadlocations();
            JSONArray datadb = new JSONArray();
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());

            JSONArray closingdayupdates = db.getclosingdayupdates();
            JSONArray unvisitedstores = db.getunvisitedstores();
            JSONArray orderpayments = db.getorderpaymentstosync();
            JSONArray datatoupload = new JSONArray();
            JSONArray paymentlist = db.getpaymentstosync();
            JSONArray denominationdata = db.getdenominationdatatatosync();
            JSONArray appusagedata = db.appusagedatatosync();
            // JSONArray orderdata = db.getorderdatatosync();
            JSONArray salesreturn = db.getsalesreturndatatosync();
            JSONArray creditnote = db.getcreditnotestosync();
            //JSONArray stockentry = db.getstockentrydatatosync();
            JSONArray updatecustomers = db.getupdatecustomerstosync();
            JSONArray updatephotos = db.getphotostosync();
            JSONArray cratesdata = db.getcratestosync();
            JSONArray passbyupdated = db.getpassbyupdatedsync();
            JSONArray assetentry = db.getassetstosync();
            //JSONArray sendbackentry = db.sendbackstosync();
            //JSONArray deliveryupdate = db.getdeliverystatusforsync();
            JSONArray schedulebegin = db.getschedulebeginsync();

            //if (db.NavDatacounttoSync() > 5)
            syncnavdata();


            try {
                datatoupload = db.getorders();
                uplatlong = db.getupdatedlaatlong();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            datadb = datatoupload;
            if (db.getpendingNotApproved()) {
                Global.Toast(LandingActivity.this, "Payment Approvals Pending!",
                        Toast.LENGTH_SHORT, Font.Regular);
                return;
            }

//        if (db.getpendingNotApprovedOrders()) {
//            Global.Toast(LandingActivity.this, "Order Approvals Pending!",
//                    Toast.LENGTH_SHORT, Font.Regular);
//            return;
//        }
//        if (db.getpendingNotApprovedOrdersstk()) {
//            Global.Toast(LandingActivity.this,
//                    "Stock Entry Approvals Pending!", Toast.LENGTH_SHORT,
//                    Font.Regular);
//            return;
//        }
          if (!isNetworkAvailable()) {
                    Global.Toast(LandingActivity.this,
                            "You Are Offline. Check your Connection",
                            Toast.LENGTH_SHORT, Font.Regular);
                    return;
                }
            // inputParamObj.put("DhiToken", SecurityHandler.Encrypt() );
            if (db.anypendingorderexist() || uplatlong.length() > 0
                    || newcostomerdata.length() > 0 || paymentlist.length() > 0
                    || orderpayments.length() > 0 || unvisitedstores.length() > 0
                    || closingdayupdates.length() > 0
                    || denominationdata.length() > 0 || appusagedata.length() > 0
                    //|| orderdata.length() > 0
                    || salesreturn.length() > 0
                    || creditnote.length() > 0 || updatecustomers.length() > 0
                    || cratesdata.length() > 0 || updatephotos.length() > 0
                    || passbyupdated.length() > 0
                    //|| stockentry.length() > 0
                    || assetentry.length() > 0
                    || isphotostosync()
                    //|| sendbackentry.length() > 0
                    //|| PFDdata.length() > 0 || deliveryupdate.length() > 0
                    || schedulebegin.length() > 0 || db.NavDatacounttoSync() > 4) {

                Synchronize synchronize = new Synchronize(LandingActivity.this, true, true);

                try {

//            synchronize.syncData(true,getApplicationContext());
                    synchronize.syncData();
                } catch (Exception e) {

                    e.printStackTrace();
                    //   Toast.makeText(getApplicationContext(), "Please try again..", Toast.LENGTH_SHORT).show();
                }
                Integer syncphoto=0;
                String path = Environment.getExternalStorageDirectory().toString() + "/Aries";
                Log.e("Files", "Path: " + path);
                final File directory = new File(path);
                final File[] filesRoot = directory.listFiles();
                if (filesRoot != null) {
                    syncphoto = filesRoot.length;
                }


                NavigationView nav = (NavigationView) findViewById(R.id.nav_view);
                LayoutInflater layoutInflater = (LayoutInflater) LandingActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View customView = layoutInflater.inflate(R.layout.popuplayout, null);
                TextView header = (TextView) customView.findViewById(R.id.header);
                header.setText(R.string.sync_data);
                TextView msg = (TextView) customView.findViewById(R.id.message);
                msg.setText(R.string.logout_sync_data_warning_refresh);
                if(syncphoto>0){
                    String toastMsg = getResources().getString(R.string.logout_sync_data_warning_refresh);
                    msg.setText(toastMsg + "\n"+ syncphoto +" Photos pending to be synced");
                }

                Button closePopupBtn = (Button) customView.findViewById(R.id.close);
                Button confirm = (Button) customView.findViewById(R.id.yes);

                //instantiate popup window

                int width = LinearLayout.LayoutParams.MATCH_PARENT;
                int height = LinearLayout.LayoutParams.MATCH_PARENT;
                final PopupWindow popupWindow = new PopupWindow(customView, width, height);
                popupWindow.setFocusable(true);

                //display the popup window
                popupWindow.showAtLocation(nav, Gravity.CENTER, 0, 0);
                popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                //close the popup window on button click
                closePopupBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });
                confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });

//            AlertDialog.Builder builder = Global.Alert(LandingActivity.this,
//                    "Sync Data", "You need to sync your data before logout.");
//            builder.setPositiveButton("OK",
//                    new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//
//                        }
//                    });
//
//            builder.setNeutralButton("CANCEL",
//                    new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//
//                        }
//                    });
//
//            AlertDialog dialog = builder.create();
//            dialog.show();
//            Global.setDialogFont(LandingActivity.this, dialog);

            } else {
                NavigationView nav = (NavigationView) findViewById(R.id.nav_view);
                LayoutInflater layoutInflater = (LayoutInflater) LandingActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View customView = layoutInflater.inflate(R.layout.custom_refreshcache_alert, null);
                Button closePopupBtn = (Button) customView.findViewById(R.id.close);
                Button confirm = (Button) customView.findViewById(R.id.yes);

                //instantiate popup window

                int width = LinearLayout.LayoutParams.MATCH_PARENT;
                int height = LinearLayout.LayoutParams.MATCH_PARENT;
                final PopupWindow popupWindow = new PopupWindow(customView, width, height);
                popupWindow.setFocusable(true);

                //display the popup window
                popupWindow.showAtLocation(nav, Gravity.CENTER, 0, 0);
                popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                //close the popup window on button click
                closePopupBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();
                    }
                });
                confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();


                        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                                "Config", MODE_PRIVATE);
                        final String loginstring = pref.getString("datafromlogin", "");
                        Log.d("prefer", loginstring);
                        String user = pref.getString("UserName", "");
                        String passcode = pref.getString("passcode", "");
                        Log.d("User", pref.getString("User", ""));
                        int verscode=pref.getInt("version",0);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.clear();
                        editor.commit();
                        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                        db.Deletetabledata();

                        SharedPreferences.Editor editor1 = pref.edit();
                        editor1.putString("UserName", user);
                        editor1.putString("passcode", passcode);
                        editor1.putInt("version", verscode);
                        editor1.commit();

                        Intent login = new Intent(LandingActivity.this, LoginActivity.class);
                        login.putExtra("refresh", true);
                        login.putExtra("prefer", loginstring);
                        login.putExtra("UserName", user);
                        startActivity(login);
                        finish();
                    }
                });
            }


    }
    private boolean isDateValid() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String day = dateFormat.format(c.getTime());

        String inpref=pref.getString("day","");
        if(inpref.contentEquals(day))
            return true;
        else
            return false;
    }
}
