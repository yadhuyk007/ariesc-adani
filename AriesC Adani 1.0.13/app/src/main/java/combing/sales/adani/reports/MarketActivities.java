package combing.sales.adani.reports;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import combing.sales.adani.ariesc.R;
import combing.sales.adani.database.DatabaseHandler;
import combing.sales.adani.landing.LandingActivity;
import combing.sales.adani.objects.FourStrings;

//created by yadhu

public class MarketActivities extends AppCompatActivity {
    private DatabaseHandler db;
    ArrayList<FourStrings> schedulelist;
    private JSONArray custCount;
    private int i;
    private int j;
    private String schedule_name;
    private String cust_count;
    private String visited;
    private TextView TSchduleName;
    private TextView TCustCount;
    private TextView TVisited;
    private TextView TPending;
    private TextView HName;
    private TextView HCount;
    private TextView HVisited;
    private TextView HPending;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
        setContentView(R.layout.activity_market_activities);
        db = new DatabaseHandler(getApplicationContext());
        try {
            ScheduleCustomers();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void ScheduleCustomers()throws JSONException {

        custCount=db.CustCount();
        TableLayout heads=(TableLayout)findViewById(R.id.heads);
        TableRow th=new TableRow(this);
        TableRow.LayoutParams h=new TableRow.LayoutParams(4);
        th.setLayoutParams(h);
        TableLayout listTable=(TableLayout)findViewById(R.id.DisplayTable);

        HName=new TextView(this);
        HName.setText("BEAT NAME");
        HName.setTextColor(Color.DKGRAY);
        HName.setLayoutParams(new TableRow.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 2f));
        HName.setPadding(10,20,0,20);
        HName.setTextSize(12);
        th.addView(HName);

        HCount=new TextView(this);
        HCount.setText("COUNT");
        HCount.setTextColor(Color.DKGRAY);
        HCount.setLayoutParams(new TableRow.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        HCount.setPadding(20,20,0,20);
        HCount.setTextSize(12);
        th.addView(HCount);

        HVisited=new TextView(this);
        HVisited.setText("VISITED");
        HVisited.setTextColor(Color.DKGRAY);
        HVisited.setLayoutParams(new TableRow.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        HVisited.setPadding(20,20,0,20);
        HVisited.setTextSize(12);
        th.addView(HVisited);

        HPending=new TextView(this);
        HPending.setText("PENDING");
        HPending.setTextColor(Color.DKGRAY);
        HPending.setTextSize(12);
        HPending.setLayoutParams(new TableRow.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
        HPending.setPadding(0,20,0,20);
        th.addView(HPending);

        heads.addView(th);
        for (i=0;i<custCount.length();i++){
            TableRow row=new TableRow(this);
            TableRow.LayoutParams lp=new TableRow.LayoutParams(4);
            row.setLayoutParams(lp);
            row.setBackgroundResource(R.drawable.table_row_border);
                JSONObject ob= custCount.getJSONObject(i);
                schedule_name=ob.getString("ScheduleName");
                cust_count=ob.getString("custCount");
                visited=ob.getString("visited");
                int pend=Integer.parseInt(cust_count)-Integer.parseInt(visited);
                String pending=String.valueOf(pend);

                TSchduleName= new TextView(this);
                TSchduleName.setText(schedule_name);
                TSchduleName.setTextColor(Color.DKGRAY);
                TSchduleName.setLayoutParams(new TableRow.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 2f));
                TSchduleName.setPadding(10,40,0,40);
                TSchduleName.setTextSize(12);
                row.addView(TSchduleName);

                TCustCount=new TextView(this);
                TCustCount.setText(cust_count);
                TCustCount.setTextColor(Color.DKGRAY);
                TCustCount.setTextSize(12);
                TCustCount.setLayoutParams(new TableRow.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
                TCustCount.setPadding(50,40,0,40);
                row.addView(TCustCount);

                TVisited=new TextView(this);
                TVisited.setText(visited);
                TVisited.setTextSize(12);
                TVisited.setTextColor(Color.DKGRAY);
                TVisited.setLayoutParams(new TableRow.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
                TVisited.setPadding(50,40,0,40);
                row.addView(TVisited);

                TPending=new TextView(this);
                TPending.setText(pending);
                TPending.setLayoutParams(new TableRow.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
                TPending.setPadding(30,40,0,40);
                TPending.setTextSize(12);
                TPending.setTextColor(Color.DKGRAY);
                row.addView(TPending);
                listTable.addView(row);
        }
    }
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(MarketActivities.this, LandingActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(0, 0);
    }
}