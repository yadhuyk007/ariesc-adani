package combing.sales.adani.customerlist;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;


import combing.sales.adani.ariesc.R;
import combing.sales.adani.gps.PlayLocation;
import combing.sales.adani.navmap.NavMapActivity;
import combing.sales.adani.objects.Font;
import combing.sales.adani.objects.Global;

import static android.content.Context.MODE_PRIVATE;

public class CustomerListViewHolder extends ChildViewHolder implements View.OnClickListener {

    private TextView mCustListTextView;
    private TextView mCustomerAddress;
    private TextView mInvoice;
    private TextView mCustomercategory;
    private TextView mCustCode;
    private LinearLayout mcustListRowLayout;
    private String scheduledetailid;
    private TextView mScheduledetailid;
    private ImageView imgNavigation;
    private Activity mAct;
    private String sid;
    private CustomerList customer;
    private ImageView imgNavigatordynamic;
    private ImageView CustomerOrderStatusIcon;
    private TextView CustomerOrderStatusIconText;
    private LayoutInflater mInflater;

    public CustomerListViewHolder(Activity activity, View itemView) {
        super(itemView);
        mAct = activity;
        mCustListTextView = (TextView) itemView.findViewById(R.id.CustomerName);
        mCustomerAddress = (TextView) itemView.findViewById(R.id.CustomerAddress);
        mInvoice = (TextView) itemView.findViewById(R.id.Invoice);
        mCustomercategory = (TextView) itemView.findViewById(R.id.Customercategory);
        mCustCode = (TextView) itemView.findViewById(R.id.CustCode);
        mcustListRowLayout = itemView.findViewById(R.id.custListRowLayout);
        mScheduledetailid = (TextView) itemView.findViewById(R.id.mScheduledetailid);
        imgNavigation = (ImageView) itemView.findViewById(R.id.imgNavigation);
        imgNavigatordynamic = (ImageView) itemView.findViewById(R.id.imgNavigatordynamic);
        CustomerOrderStatusIcon=(ImageView) itemView.findViewById(R.id.CustomerOrderStatusIcon);
        CustomerOrderStatusIconText=(TextView) itemView.findViewById(R.id.CustomerOrderStatusIconText);
        imgNavigation.setOnClickListener(this);
        imgNavigatordynamic.setOnClickListener(this);
        mcustListRowLayout.setOnClickListener(this);
        mInflater = LayoutInflater.from(itemView.getContext());
    }

    public void bind(CustomerList customerList) {
        customer = customerList;
        mCustListTextView.setText(customerList.getName());
        mCustomerAddress.setText(customerList.getCustomerAddress());
        String otp= customerList.getotp();
        if(otp.equals("1")) {
            mInvoice.setText("OTP Verified");
        }
        mCustomercategory.setText(customerList.getCategory());
        mScheduledetailid.setText(customerList.getScheduledetailid());
        sid = mScheduledetailid.getText().toString();
        mCustCode.setText("CN : " + customerList.getCustomercode());
        //for status


        if (Integer.parseInt(customerList.getApprove()) > 0
                || Integer.parseInt(customerList.getApproveOrder()) > 0) {
            CustomerOrderStatusIcon
                    .setImageResource(R.drawable.dhi_circle_notapproved);
            CustomerOrderStatusIconText.setText("NA");
        } else if (customerList.getStatus().equals("Completed")) {
            CustomerOrderStatusIcon
                    .setImageResource(R.drawable.dhi_circle_completed);
            CustomerOrderStatusIconText.setText("C");
        } else {
            CustomerOrderStatusIcon.setImageResource(R.drawable.dhi_circle);
            CustomerOrderStatusIconText.setText("P");
        }
        PlayLocation tracker  = new PlayLocation();
        double latitd = tracker.getlattitude();
        double lngtd = tracker.getlongitude();
        if (!customerList.getLatitude().equals("")
                && !customerList.getLatitude().equals("0")
                && !customerList.getLatitude().equals("0.0")) {
            Double costlat = Double.parseDouble(customerList.getLatitude());
            Double costlong = Double.parseDouble(customerList.getLongitude());
            double distance = distance(latitd, lngtd, costlat, costlong);
            if (distance < 70) {
                // when location locked
                if (customerList.getLocationLock().equals("1")) {
                    imgNavigation.setImageResource(R.drawable.locationcorrect_locked);
                } else {
                    imgNavigation.setImageResource(R.drawable.locationcorrect);
                }
            }else {
                if (customerList.getLocationLock().equals("1")) {
                    imgNavigation.setImageResource(R.drawable.locationerrorfront_locked);
                }
                else {
                    imgNavigation.setImageResource(R.drawable.locationerrorfront);
                }
            }

        }else{
            imgNavigation.setImageResource(R.drawable.nolatlongfound);
        }
    }

    @Override
    public void onClick(final View v) {
        String Lat = customer.getLatitude();
        String Longi = customer.getLongitude();
        String storename = customer.getName();
        String storeaddress = customer.getCustomerAddress();

        PackageManager manager = mAct.getApplicationContext()
                .getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(mAct.getApplicationContext()
                    .getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        SharedPreferences pref = mAct.getApplicationContext()
                .getSharedPreferences("Config", MODE_PRIVATE);
//if(Global.isNewVersionAvailable(getApplicationContext())) {
        if(info.versionCode < pref.getInt("version",0)){
            Global.Toast(mAct,"Refresh cache data to start the schedule!",Toast.LENGTH_LONG, Font.Regular);
        }
        else {
            if (isDateValid()) {
                //Intent intent1 = new Intent(mAct, SurrogatesEditActivity.class);
                //intent1.putExtra("scheduledetailid",sid);
                //mAct.startActivity(intent1);
                //mAct.finish();
            } else {
                Global.Toast(mAct, "Refresh cache data to start the new day!", Toast.LENGTH_LONG, Font.Regular);
            }
        }

        switch (v.getId()) {
            case R.id.imgNavigatordynamic:
                try {
                    if (!Lat.toString().equals("")
                            && !Lat.toString().equals("0")
                            && !Lat.toString().equals("0.0") && !Longi.toString().equals("")
                            && !Longi.toString().equals("0")
                            && !Longi.toString().equals("0.0")) {

                        Uri gmmIntentUri = Uri.parse("google.navigation:q="
                                + Lat + "," + Longi);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW,
                                gmmIntentUri);
                        mapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mapIntent
                                .setPackage("com.google.android.apps.maps");
                        v.getContext().startActivity(mapIntent);

                    } else {
                        Toast.makeText(v.getContext(),
                                "No Location Data Found..!",
                                Toast.LENGTH_LONG).show();
                    }
                } catch (Exception ex) {

                    Toast.makeText(v.getContext(),
                            "Install Google Maps or Check Internet Connectivity!", Toast.LENGTH_LONG)
                            .show();

                }
                break;
            case R.id.imgNavigation:
                if (!Lat.toString().equals("")
                        && !Lat.toString().equals("0")
                        && !Lat.toString().equals("0.0") && !Longi.toString().equals("")
                        && !Longi.toString().equals("0")
                        && !Longi.toString().equals("0.0")) {
                   Intent intent = new Intent(mAct,
                            NavMapActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("storename", storename);
                    intent.putExtra("storeaddress", storeaddress);
                    mAct.startActivity(intent);
                } else {
                    Toast.makeText(v.getContext(),
                            "No Location Data Found..!",
                            Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.custListRowLayout:

//                DatabaseHandler db = new DatabaseHandler(v.getContext());
//                String scheduleid = db.getactualscheduleid();
//                String begintime = db.getscheduleheaderbegintime(scheduleid);
//
//                final String sid = ((TextView) v.findViewById(R.id.mScheduledetailid)).getText().toString();
//                if (begintime == null) {
//                    String schDate = db.GetCurrentScheduledate();
//                    Calendar c = Calendar.getInstance();
//                    DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd",
//                            Locale.US);
//                    String currenttime = dateTimeFormat.format(c.getTime());
//                    if (currenttime.contentEquals(schDate)) {
//                        View customView = mInflater.inflate(R.layout.popuplayout, null);
//                        Button closePopupBtn = (Button) customView.findViewById(R.id.close);
//                        Button confirm = (Button) customView.findViewById(R.id.yes);
//                        TextView title = (TextView) customView.findViewById(R.id.header);
//                        title.setText("Begin Schedule");
//                        TextView message = (TextView) customView.findViewById(R.id.message);
//                        message.setText("Do you want to start schedule?");
//                        //instantiate popup window
//                        int width = LinearLayout.LayoutParams.MATCH_PARENT;
//                        int height = LinearLayout.LayoutParams.MATCH_PARENT;
//                        final PopupWindow popupWindow = new PopupWindow(customView, width, height);
//                        popupWindow.setFocusable(true);
//                        //display the popup window
//                        popupWindow.showAtLocation(mcustListRowLayout, Gravity.CENTER, 0, 0);
//                        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                        closePopupBtn.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                popupWindow.dismiss();
//                                return;
//                            }
//                        });
//                        confirm.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                popupWindow.dismiss();
//                                DatabaseHandler db = new DatabaseHandler(v.getContext());
//                                Calendar c = Calendar.getInstance();
//                                DateFormat dateTimeFormat = new SimpleDateFormat(
//                                        "yyyyMMddHHmmss", Locale.US);
//                                DateFormat dateTimeFormat_readable = new SimpleDateFormat(
//                                        "dd/MM/yyyy HH:mm:ss", Locale.US);
//                                String currenttime = dateTimeFormat.format(c
//                                        .getTime());
//                                String readabletime = dateTimeFormat_readable
//                                        .format(c.getTime());
//
//                                db.updatescheduleheaderbegintime(currenttime);
//                                db.intialisesynctable(currenttime);
//
//                                SharedPreferences pref = v.getContext()
//                                        .getSharedPreferences("Config",
//                                                MODE_PRIVATE);
//                                SharedPreferences.Editor editor = pref.edit();
//                                editor.putString("Schbegintime", currenttime);
//                                editor.commit();
//                                Toast.makeText(v.getContext(),
//                                        "Schedule Started!",
//                                        Toast.LENGTH_SHORT).show();
//                                //String sid = ((TextView) v.findViewById(R.id.mScheduledetailid)).getText().toString();
//                              /*  Intent i = new Intent(mAct, OrderTakingActivity.class);
//                                i.putExtra("complimentary",false);
//                                i.putExtra("parentInvoiceNo","");
//                                i.putExtra("scheduledetailid",sid);
//                                //mAct.finish();
//                                mAct.startActivity(i);*/
//                                //mAct.overridePendingTransition(0,0);
//                            }
//                        });
//                    }else {
//                        Toast.makeText(v.getContext(),
//                                "You are not allowed to start a future date schedule!",
//                                Toast.LENGTH_SHORT).show();
//                    }
//                }else{
//                    String scheduletype = "";
//
//                        String status = db.getschedulestatus();
//                        if (status.equals("Completed")) {
//
//                            Toast.makeText(v.getContext(),
//                                    "Schedule Already Completed",
//                                    Toast.LENGTH_SHORT).show();
//
//                        }else{
//                           /* Intent i = new Intent(v.getContext(), OrderTakingActivity.class);
//                            i.putExtra("complimentary",false);
//                            i.putExtra("parentInvoiceNo","");
//                            i.putExtra("scheduledetailid",sid);
//                            mAct.finish();
//                            v.getContext().startActivity(i);
//                            mAct.overridePendingTransition(0,0);*/
//                        }
//                }
                break;
        }
    }


    public static float distance(double lat1, double lng1, double lat2,
                                 double lng2) {
        double earthRadius = 6371000; // meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2)
                * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        float dist = (float) (earthRadius * c);

        return dist;
    }
    private boolean isDateValid() {
        SharedPreferences pref = mAct.getApplicationContext()
                .getSharedPreferences("Config", MODE_PRIVATE);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String day = dateFormat.format(c.getTime());

        String inpref=pref.getString("day","");
        if(inpref.contentEquals(day))
            return true;
        else
            return false;
}
}
