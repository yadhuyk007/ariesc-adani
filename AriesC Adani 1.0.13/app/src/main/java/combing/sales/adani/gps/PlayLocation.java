package combing.sales.adani.gps;

import android.location.Location;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PlayLocation {
	public static Location reallocation;
	public static String fixedtime;

	public double getlattitude() {
		try {
			return reallocation.getLatitude();
		} catch (Exception e) {
			return 0;
		}
	}

	public double getlongitude() {
		try {
			return reallocation.getLongitude();
		} catch (Exception e) {
			return 0;
		}
	}

	public float getaccuracy() {
		try {
			return reallocation.getAccuracy();
		} catch (Exception e) {
			return 0;
		}
	}

	public Location getMyLocation() {
		try {
			return reallocation;
		} catch (Exception e) {
			return null;
		}
	}

	public String getprovider() {
		try {
			float accuracy = reallocation.getAccuracy();
			String prov = "gps";
			if (accuracy > 30) {
				prov = "network";
			}
			final String provider = prov;
			return provider;
		} catch (Exception e) {
			return "network";
		}
	}

	public String getfixtime() {
		try {
			if (fixedtime != null)
				return fixedtime;
			else
				return "01/01/1900 00:00:00";
		} catch (Exception e) {
			return "01/01/1900 00:00:00";
		}
	}

	public String getCapturedTime() {
		try {
			DateFormat dateTimeFormat_gps = new SimpleDateFormat(
					"dd/MM/yyyy HH:mm:ss");
			Date fix = dateTimeFormat_gps.parse(reallocation.getTime() + "");
			return fix.toString();
		} catch (Exception e) {
			return "0";
		}
	}
}
