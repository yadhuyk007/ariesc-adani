package combing.sales.adani.customerlist;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

/**
 * ViewHolder for a child list
 * item.
 * <p>
 * The user should extend this class and implement as they wish for their
 * child list item.
 *
 *
 */
public class ChildViewHolder extends RecyclerView.ViewHolder {

    /**
     * Default constructor.
     *
     * @param itemView The {@link View} being hosted in this ViewHolder
     */
    LinearLayout mcustListRowLayout;
    public ChildViewHolder(View itemView) {
        super(itemView);

    }

}
