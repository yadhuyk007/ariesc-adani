package combing.sales.adani.registration;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.CallLog;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import combing.sales.adani.ariesc.LoginActivity;
import combing.sales.adani.ariesc.R;
import combing.sales.adani.database.DatabaseHandler;
import combing.sales.adani.objects.Font;
import combing.sales.adani.objects.Global;
import combing.sales.adani.objects.webconfigurration;

public class RegistrationActivity extends AppCompatActivity {
    private Button submit;
    private TextView status;
    private static final int CAMERA_REQUEST = 1888;
    private static Uri mImageUri;
    private static String type = "";
    private Bitmap customerphoto;
    private Bitmap proofphoto;
    private String phone;
    private String name;
    private String email;
    private String address;
    private DatabaseHandler db;
    private ProgressDialog mProgress;
    private PopupWindow statuswindow;
    private View statusview;
    final int REQUEST_PERMISSION_CODE = 1888;
    private String uuid;
    public static boolean OTP_verificationstatus;
    private Button verification;
    private static ImageView tick;
    private Activity mAct = RegistrationActivity.this;
    private static String mobilenumber;
    private CountDownTimer yourCountDownTimer;
    private TextView mTextField;
    public static String OTP_submitTime = "";
    private boolean verify = false;
    private EditText contactnumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
        submit = findViewById(R.id.submit);
        status = findViewById(R.id.check_status);
        db = new DatabaseHandler(getApplicationContext());
        contactnumber = findViewById(R.id.ContactNumber);
        if (!checkPermissionFromDevice()) {
            requestPermission();
        }

        //check status
        verification = (Button) findViewById(R.id.verify);
        tick = (ImageView) findViewById(R.id.tickbutton);
        OTP_verificationstatus = false;
        Button verify = (Button) findViewById(R.id.verify);
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verify();
            }
        });

        status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout layout = (LinearLayout) findViewById(R.id.mainlay);
                LayoutInflater layoutInflater = (LayoutInflater) RegistrationActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                statusview = layoutInflater.inflate(R.layout.checkstatus, null);
                TextView header = (TextView) statusview.findViewById(R.id.header);
                header.setText("Check Registration Status");
                TextView msg = (TextView) statusview.findViewById(R.id.message);
                msg.setText("Please Enter Phone number or Emai-ID");
                Button closePopupBtn = (Button) statusview.findViewById(R.id.close);
                Button confirm = (Button) statusview.findViewById(R.id.yes);
                int width = LinearLayout.LayoutParams.MATCH_PARENT;
                int height = LinearLayout.LayoutParams.MATCH_PARENT;
                statuswindow = new PopupWindow(statusview, width, height);
                statuswindow.setFocusable(true);
                statuswindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
                statuswindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        EditText input = statusview.findViewById(R.id.input);
                        String param = input.getText().toString();
                        String valid = statusvalidate(param);
                        if (!valid.contentEquals("true")) {
                            Toast.makeText(RegistrationActivity.this, valid, Toast.LENGTH_SHORT).show();
                            return;
                        }
//                        if (param.contentEquals("")) {
//                            Toast.makeText(getApplicationContext(), "Please enter mail/phone", Toast.LENGTH_LONG).show();
//                        } else {
                        JSONObject obj = new JSONObject();

                        try {
                            obj.put("param", param);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        new StatusOperation().execute(obj.toString());
                        //}

                    }
                });
            }
        });

        //id proof
        ImageButton proof_camera = (ImageButton) findViewById(R.id.proof_camera);
        proof_camera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(RegistrationActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(RegistrationActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_REQUEST);
                } else {
                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    File photo;
                    try {
                        photo = createTemporaryFile("picture", ".jpg");
                        photo.delete();
                    } catch (Exception e) {
                        Toast.makeText(RegistrationActivity.this, R.string.shot_is_impossible, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    mImageUri = Uri.fromFile(photo);
                    type = "proof";
                    startActivityForResult(intent, CAMERA_REQUEST);
                }
            }
        });

        //person photo
        ImageButton photo_camera = (ImageButton) findViewById(R.id.photo_camera);
        photo_camera.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(RegistrationActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(RegistrationActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CAMERA_REQUEST);
                } else {
                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    File photo;
                    try {
                        photo = createTemporaryFile("picture", ".jpg");
                        photo.delete();
                    } catch (Exception e) {
                        Toast.makeText(RegistrationActivity.this, R.string.shot_is_impossible, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    mImageUri = Uri.fromFile(photo);
                    type = "photo";
                    startActivityForResult(intent, CAMERA_REQUEST);
                }
            }
        });

        //submit button
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
    }

    private String statusvalidate(String param) {
        String val = "true";
        if (param.contentEquals("")) {
            return "Please Enter your Phone Number/Email";
        }
        if (param.contains("@")) {
            if (!param.contains(".")) {
                return "Please enter a valid Email ID(.)";
            }
        } else {
            if (param.length() < 10) {
                return "Please enter a valid Phone Number";
            }
        }
        return val;
    }

    private void verify() {
        View mView = RegistrationActivity.this.getWindow().getDecorView();
        InputMethodManager imm = (InputMethodManager) mAct.getSystemService(Context.INPUT_METHOD_SERVICE);
        EditText vertxt = (mView.findViewById(R.id.ContactNumber));
        mAct.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mobilenumber = ((EditText) mView.findViewById(R.id.ContactNumber)).getText().toString();
        // TODO Auto-generated method stub

        if (OTP_verificationstatus) {
            Global.Toast(mAct, "OTP already Verified",
                    Toast.LENGTH_SHORT, Font.Regular);
            return;
        }
        if (mobilenumber.length() < 10) {
            Global.Toast(mAct, "Enter a valid mobile number!",
                    Toast.LENGTH_SHORT, Font.Regular);
            return;
        }
        LayoutInflater inflater = getLayoutInflater();
        final View popupView1 = inflater.inflate(
                R.layout.enter_otp_option, null);
        final PopupWindow popupWindow1 = new PopupWindow(popupView1,
                WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        popupWindow1.setTouchable(true);
        popupWindow1.setOutsideTouchable(false);
        popupWindow1.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        popupWindow1.setTouchInterceptor(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    popupWindow1.dismiss();
                    return true;
                }
                return false;
            }

        });
        LinearLayout l1 = (LinearLayout) mView.findViewById(R.id.linear1);
        popupWindow1.showAtLocation(l1, Gravity.CENTER, 0, 0);
        popupWindow1.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        l1.setFocusable(false);
        //InputMethodManager immr = (InputMethodManager) mAct.getSystemService(Context.INPUT_METHOD_SERVICE);
        ImageView close1 = popupView1.findViewById(R.id.close_window);
        Button call = popupView1.findViewById(R.id.call);
        Button sms = popupView1.findViewById(R.id.sms);
        TextView txtnum = popupView1.findViewById(R.id.textView2);
        txtnum.setText("Verify " + mobilenumber);
        (popupView1.findViewById(R.id.timer)).setVisibility(View.GONE);
        close1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (yourCountDownTimer != null) {
                    yourCountDownTimer.cancel();
                }
                popupWindow1.dismiss();
            }
        });
        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject cred = new JSONObject();
                try {
                    cred = GetOtprequest();
                    popupWindow1.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String[] params = new String[1];
                params[0] = cred.toString();
                if (isNetworkAvailable())
                    new OTPrequest(verification, mobilenumber, cred
                            .toString()).execute(params);
                else
                    Toast.makeText(mAct,
                            "Check Your Network Connection",
                            Toast.LENGTH_SHORT).show();
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (yourCountDownTimer != null) {
                    yourCountDownTimer.cancel();
                }
                (popupView1.findViewById(R.id.call))
                        .setEnabled(false);
                (popupView1.findViewById(R.id.sms))
                        .setEnabled(false);
                (popupView1.findViewById(R.id.timer))
                        .setVisibility(View.VISIBLE);
                mTextField = (TextView) popupView1
                        .findViewById(R.id.message);
                Calendar c = Calendar.getInstance();
                @SuppressLint("SimpleDateFormat") DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                final String submittime = dateTimeFormat.format(c
                        .getTime());
                yourCountDownTimer = new CountDownTimer(60000, 1000) {
                    @SuppressLint("SetTextI18n")
                    public void onTick(long millisUntilFinished) {
                        mTextField.setText("" + millisUntilFinished / 1000);
                        if (((millisUntilFinished / 1000) % 3) == 0)
                            if (getCallDetails(mAct, mobilenumber, popupWindow1, submittime)) {
                                verify = true;
                                onFinish();
                            }
                    }

                    @Override
                    public void onFinish() {
                        if (yourCountDownTimer != null) {
                            yourCountDownTimer.cancel();
                        }
                        // TODO Auto-generated method stub
                        mTextField.setText("0");
                        (popupView1.findViewById(R.id.timer)).setVisibility(View.GONE);
                        (popupView1.findViewById(R.id.call)).setEnabled(true);
                        (popupView1.findViewById(R.id.sms)).setEnabled(true);
                        if (verify) {
                            Global.Toast(mAct, "Mobile Number Verified", Toast.LENGTH_SHORT, Font.Regular);
                        } else {
                            Global.Toast(mAct, "Mobile Number Verification Failed", Toast.LENGTH_SHORT, Font.Regular);
                        }
                    }
                }.start();
            }
        });


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onBackPressed() {
        LinearLayout layout = (LinearLayout) findViewById(R.id.mainlay);
        LayoutInflater layoutInflater = (LayoutInflater) RegistrationActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.popuplayout, null);
        TextView header = (TextView) customView.findViewById(R.id.header);
        header.setText("Cancel Registration");
        TextView msg = (TextView) customView.findViewById(R.id.message);
        msg.setText("Are You Sure You you Want to Cancel Registration?");
        Button closePopupBtn = (Button) customView.findViewById(R.id.close);
        Button confirm = (Button) customView.findViewById(R.id.yes);
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        final PopupWindow popupWindow = new PopupWindow(customView, width, height);
        popupWindow.setFocusable(true);
        popupWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        closePopupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();

            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                Bitmap mImageBitmap = (Bitmap) extras.get("data");
                ImageView proof;
                ImageView inside;
                if (type == "photo") {
                    customerphoto = getResizedBitmap(mImageBitmap, 500, 500);
                    proof = (ImageView) findViewById(R.id.addperson);
                    proof.setImageBitmap(customerphoto);
                } else if (type == "proof") {
                    proofphoto = getResizedBitmap(mImageBitmap, 500, 500);
                    inside = (ImageView) findViewById(R.id.proof);
                    inside.setImageBitmap(proofphoto);
                }
            }
        } catch (Exception e) {
            Toast.makeText(this, R.string.turn_off_high_quality_img, Toast.LENGTH_SHORT).show();
        }
    }

    private File createTemporaryFile(String part, String ext) throws Exception {

        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth,
                                   int bitmapHeight) {
        return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
    }

    public JSONObject GetOtprequest() throws JSONException {
        webconfigurration C = new webconfigurration(getApplicationContext());
        //int orgid = 50; //prod
        int orgid = 54;
        //int bu = 78; //prod
        int bu = 21;
        //String user= "50LCKWADNDBSR25"; //prod
        String user = "testcomb";
        //String dbname = "Org50"; //prod
        String dbname = "Org54";
        String appname = "app38";
        String params = "{\"data\": {\"User\": \""
                //+ C.user
                + user
                + "\",\"DBname\": \""
                //+ C.dbname
                + dbname
                + "\", \"BussinessUnit\": \""
                //+ C.bu
                + bu
                + "\","
                + "\"Operation\": \"Custom\","
                + "\"columnlist\": [],"
                + "\"keys\": [{\"Name\": \"\",\"operationmethod\": \"OtpGeneration\",\"operationtype\": \"custom\",\"Value\": \"\","
                + "\"op\": \"\",\"LogicalOperation\": \"\",\"Level\": \"\",\"Depends\": \"\",\"type\": \"\",\"Column\": \"\"}],"
                + "\"appname\": \""
                //+ C.appid
                + appname
                + "\","
                + "\"data\": [],"
                + "\"keyattr\": {\"operation\": \"\",\"SortBy\": \"\",\"SortByField\": \"\",\"StartIndex\": \"\",\"Limit\": \"\"},\"OrganizationId\": \""
                //+ C.orgid + "\"}," + "\"meta\": []}";
                + orgid + "\"}," + "\"meta\": []}";

        JSONObject cred = new JSONObject(params);
        try {
            JSONObject updatestatus = getstatusobject();
            cred.put("updatestatus", updatestatus);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject data = new JSONObject();
        data.put("operation", "verifyOtp").put("Operation", "verifyOtp")
                .put("MobileNumber", mobilenumber).put("custid", "");
        cred.getJSONObject("data").getJSONArray("data").put(data);

        String auth = webconfigurration.auth;
        auth = Base64.encodeToString(auth.getBytes(), Base64.NO_WRAP);
        cred.put("auth", auth);

        return cred;
    }

    private JSONObject getstatusobject() throws Exception {
        JSONObject temp = new JSONObject();
        SharedPreferences pref = getSharedPreferences("Config",
                Context.MODE_PRIVATE);

        String person = "";
        String lat = "";
        String lng = "";
        String operation = "";
        String version = "";
        String createdon = "";
        String uuid = "";
        String appname = "";
        String versioncode = "";
        person = pref.getString("personid", "0");

        operation = webconfigurration.status;
        appname = webconfigurration.appname;
        lat = webconfigurration.lat;
        lng = webconfigurration.lng;

        PackageInfo pInfo = getPackageManager().getPackageInfo(
                getPackageName(), 0);
        version = pInfo.versionName;
        versioncode = String.valueOf(pInfo.versionCode);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        createdon = df.format(c.getTime());

        uuid = UUID.randomUUID().toString();

        temp.put("person", person).put("lat", lat).put("lng", lng)
                .put("operation", operation).put("version", version)
                .put("versioncode", versioncode).put("appname", appname)
                .put("createdon", createdon).put("uuid", uuid);
        return temp;
    }

    public void save() {

        if (verify = false) {
            Toast.makeText(RegistrationActivity.this, "Please verify Mobile Number", Toast.LENGTH_SHORT).show();
            return;
        }
        name = ((TextView) findViewById(R.id.personname)).getText().toString();
        address = ((TextView) findViewById(R.id.personaddress)).getText().toString();
        phone = ((TextView) findViewById(R.id.ContactNumber)).getText().toString();
        email = ((TextView) findViewById(R.id.personemail)).getText().toString();
        String valid = isValid(name, address, phone, email,OTP_verificationstatus);

        if (!valid.contentEquals("true")) {
            Toast.makeText(RegistrationActivity.this, valid, Toast.LENGTH_SHORT).show();
            return;
        }
        uuid = UUID.randomUUID().toString();
        if (customerphoto != null) {
            String filename = uuid + ".png";
            createDirectoryAndsavetempfile(customerphoto, "photo_" + filename);
        }
        if (proofphoto != null) {
            String filename = uuid + ".png";
            createDirectoryAndsavetempfile(proofphoto, "proof_" + filename);
        }

        LinearLayout layout = (LinearLayout) findViewById(R.id.mainlay);
        LayoutInflater layoutInflater = (LayoutInflater) RegistrationActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View customView = layoutInflater.inflate(R.layout.submit, null);
        TextView header = (TextView) customView.findViewById(R.id.header);
        header.setText("Confirm Registration");
        final TextView msg = (TextView) customView.findViewById(R.id.message);
        msg.setText("Are you sure you want to proceed with this Registration?");
        final Button closePopupBtn = (Button) customView.findViewById(R.id.close);
        final Button okBtn = (Button) customView.findViewById(R.id.ok);
        final Button confirm = (Button) customView.findViewById(R.id.yes);
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        final PopupWindow popupWindow = new PopupWindow(customView, width, height);
        popupWindow.setFocusable(true);
        popupWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        closePopupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                try {
                    Sync();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void Sync() throws JSONException {
//        Synchronize s = new Synchronize(getApplicationContext(), false, false);
//        s.syncphotos();
        webconfigurration C = new webconfigurration(getApplicationContext());
        JSONObject obj = new JSONObject();
        obj.put("name", name);
        obj.put("phone", phone);
        obj.put("email", email);
        obj.put("address", address);
        obj.put("uuid", uuid);
        obj.put("status", "Pending");
        new RegistrationOperation().execute(obj.toString());

    }

    private class RegistrationOperation extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            mProgress = new ProgressDialog(RegistrationActivity.this);
            mProgress.setView(getLayoutInflater().inflate(R.layout.adapter_loading, null));
            mProgress.setTitle("");
            mProgress.setMessage("Loading...");
            mProgress.setCancelable(false);
            mProgress.setIndeterminate(true);
            mProgress.create();

            if (!mProgress.isShowing()) {
                mProgress.show();
            }
        }

        @Override
        protected String doInBackground(String... urlParameters) {
            webconfigurration C = new webconfigurration(getApplicationContext());
            String targetURL = C.registrationurl;
            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-8");
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(10000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);

                conn.setRequestMethod("POST");

                int totalLength = (urlParameters[0].getBytes().length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.writeBytes(urlParameters[0]);
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();
                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }

                final String responsefrom = response.toString();
                return responsefrom;
            } catch (final Exception e) {

                Log.e("Error", "---------------------------------------------------------");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

                return "issue";
            }
        }

        @Override
        protected void onPostExecute(final String responsefrom) {
            super.onPostExecute(responsefrom);
            if (mProgress != null) {
                mProgress.dismiss();
            }
            JSONObject obj = new JSONObject();
            try {
                obj = new JSONObject(responsefrom);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String result = obj.optString("result", "");
            if (result == null || result.contentEquals("issue")) {
                LinearLayout layout = (LinearLayout) findViewById(R.id.mainlay);
                LayoutInflater layoutInflater = (LayoutInflater) RegistrationActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View customView = layoutInflater.inflate(R.layout.ok, null);
                TextView header = (TextView) customView.findViewById(R.id.header);
                header.setText("Error");
                final TextView msg = (TextView) customView.findViewById(R.id.message);
                msg.setText("An error occured, please check your internet connectivity or try after some time");
                final Button closePopupBtn = (Button) customView.findViewById(R.id.close);
                closePopupBtn.setText("OK");
                int width = LinearLayout.LayoutParams.MATCH_PARENT;
                int height = LinearLayout.LayoutParams.MATCH_PARENT;
                final PopupWindow popupWindow = new PopupWindow(customView, width, height);
                popupWindow.setFocusable(true);
                popupWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
                popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                closePopupBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();

                        Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(0, 0);

                    }
                });
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Toast.makeText(getApplicationContext(), "Cannot connect to Server!", Toast.LENGTH_SHORT).show();
                    }
                });
                return;
            } else if (result.contentEquals("success")) {


                LinearLayout layout = (LinearLayout) findViewById(R.id.mainlay);
                LayoutInflater layoutInflater = (LayoutInflater) RegistrationActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View customView = layoutInflater.inflate(R.layout.ok, null);
                TextView header = (TextView) customView.findViewById(R.id.header);
                header.setText("Submitted");
                final TextView msg = (TextView) customView.findViewById(R.id.message);
                msg.setText("Application has been submitted, Please wait for conformation mail..");
                final Button closePopupBtn = (Button) customView.findViewById(R.id.close);
                closePopupBtn.setText("OK");
                int width = LinearLayout.LayoutParams.MATCH_PARENT;
                int height = LinearLayout.LayoutParams.MATCH_PARENT;
                final PopupWindow popupWindow = new PopupWindow(customView, width, height);
                popupWindow.setFocusable(true);
                popupWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
                popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                closePopupBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popupWindow.dismiss();

                        Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(0, 0);

                    }
                });

            } else if (responsefrom.contains("duplicate")) {
                try {
                    String phone = obj.optString("phone", "");
                    String mail = obj.optString("mail", "");
                    LinearLayout layout = (LinearLayout) findViewById(R.id.mainlay);
                    LayoutInflater layoutInflater = (LayoutInflater) RegistrationActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View customView = layoutInflater.inflate(R.layout.ok, null);
                    TextView header = (TextView) customView.findViewById(R.id.header);
                    header.setText("Already Exist");
                    final TextView msg = (TextView) customView.findViewById(R.id.message);
                    if (phone.length() > 0 && mail.length() > 0) {
                        msg.setText("Person with Same Mail ID & Phone Number Already Exist!..Please choose Another");
                    } else if (phone.length() > 0) {
                        msg.setText("Person with Same Phone Number Already Exist!..Please choose Another");
                    } else if (mail.length() > 0) {
                        msg.setText("Person with Same Mail ID Already Exist!..Please choose Another");
                    }
                    final Button closePopupBtn = (Button) customView.findViewById(R.id.close);
                    closePopupBtn.setText("OK");
                    int width = LinearLayout.LayoutParams.MATCH_PARENT;
                    int height = LinearLayout.LayoutParams.MATCH_PARENT;
                    final PopupWindow popupWindow = new PopupWindow(customView, width, height);
                    popupWindow.setFocusable(true);
                    popupWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
                    popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                    closePopupBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            popupWindow.dismiss();
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }


    private class StatusOperation extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            mProgress = new ProgressDialog(RegistrationActivity.this);
            mProgress.setView(getLayoutInflater().inflate(R.layout.adapter_loading, null));
            mProgress.setTitle("");
            mProgress.setMessage("Loading...");
            mProgress.setCancelable(false);
            mProgress.setIndeterminate(true);
            mProgress.create();

            if (!mProgress.isShowing()) {
                mProgress.show();
            }
        }

        @Override
        protected String doInBackground(String... urlParameters) {
            webconfigurration C = new webconfigurration(getApplicationContext());
            String targetURL = C.statusurl;
            try {
                URL connUrl = new URL(targetURL);
                HttpURLConnection conn = (HttpURLConnection) connUrl
                        .openConnection();
                conn.setRequestProperty("Content-Type",
                        "application/json; charset=utf-8");
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(10000);
                conn.setUseCaches(false);
                conn.setDoInput(true);
                conn.setDoOutput(true);

                conn.setRequestMethod("POST");

                int totalLength = (urlParameters[0].getBytes().length);
                conn.setFixedLengthStreamingMode(totalLength);
                DataOutputStream request = new DataOutputStream(
                        conn.getOutputStream());
                request.writeBytes(urlParameters[0]);
                request.flush();
                request.close();

                InputStream is = conn.getInputStream();
                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }

                final String responsefrom = response.toString();
                return responsefrom;
            } catch (final Exception e) {

                Log.e("Error", "---------------------------------------------------------");
                Log.e("Error", e.getMessage());
                e.printStackTrace();
                return "issue";
            }
        }

        @Override
        protected void onPostExecute(final String responsefrom) {
            super.onPostExecute(responsefrom);
            if (mProgress != null) {
                mProgress.dismiss();
            }
            statuswindow.dismiss();
            LinearLayout layout = (LinearLayout) findViewById(R.id.mainlay);
            LayoutInflater layoutInflater = (LayoutInflater) RegistrationActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.statusresult, null);
            TextView header = (TextView) view.findViewById(R.id.header);
            header.setText("Registration Status");
            Button ok = (Button) view.findViewById(R.id.close);
            TextView rejected = view.findViewById(R.id.rejected);
            TextView approved = view.findViewById(R.id.approved);
            TextView pending = view.findViewById(R.id.pending);
            TextView nodata = view.findViewById(R.id.nodata);
            int width = LinearLayout.LayoutParams.MATCH_PARENT;
            int height = LinearLayout.LayoutParams.MATCH_PARENT;
            final PopupWindow window = new PopupWindow(view, width, height);
            window.setFocusable(true);
            window.showAtLocation(layout, Gravity.CENTER, 0, 0);
            window.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            if (responsefrom == null || responsefrom.contentEquals("issue")) {
                rejected.setVisibility(View.GONE);
                approved.setVisibility(View.GONE);
                pending.setVisibility(View.GONE);
            } else if (responsefrom.contains("success")) {
                try {
                    JSONObject obj = new JSONObject(responsefrom);
                    String status = obj.optString("status", "");
                    if (status.contentEquals("Pending")) {
                        pending.setVisibility(View.VISIBLE);
                        nodata.setVisibility(View.GONE);
                    } else if (status.contentEquals("Approved")) {
                        approved.setVisibility(View.VISIBLE);
                        nodata.setVisibility(View.GONE);
                    } else if (status.contentEquals("Rejected")) {
                        rejected.setVisibility(View.VISIBLE);
                        nodata.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    window.dismiss();
                }
            });

            return;

        }
    }

    private String isValid(String name, String address, String phone, String email,Boolean otp) {
        String temp = "true";
        if (name.contentEquals("")) {
            return "Please Enter Your Name";
        }
        if (address.contentEquals("")) {
            return "Please Enter Your Address";
        }
        if (phone.contentEquals("")) {
            return "Please Enter your Phone Number";
        }
        if (phone.length() < 10) {
            return "Please enter a valid Phone Number";
        }
        if (email.contentEquals("")) {
            return "Please Enter your Email ID";
        }
        if (!email.contains(".")) {
            return "Please emter a valid Email ID";
        }
        if (!email.contains("@")) {
            return "Please Enter a Valid Email ID";
        }
        if (proofphoto == null) {
            return "Please Take the ID Proof Photo";
        }
        if (customerphoto == null) {
            return "Please Take your Photo";
        }
//        if (!otp){
//            return "Please Verify Phone Number";
//        }

        return temp;
    }

    private boolean checkPermissionFromDevice() {
        int net = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int cam = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        return net == PackageManager.PERMISSION_GRANTED && cam == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
        }, CAMERA_REQUEST);
    }

    private void createDirectoryAndsavetempfile(Bitmap imageToSave,
                                                String fileName) {

        File rootsd = Environment.getExternalStorageDirectory();
        File direct = new File(rootsd.getAbsolutePath() + "/Aries");

        if (!direct.exists()) {
            direct.mkdirs();
        }

        File file = new File(direct, fileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 75, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class OTPrequest extends AsyncTask<String, Integer, String> {
        final Dialog innerdialog = new Dialog(mAct);
        private View v;
        private String mobilenumber;
        private String Requeststring;

        public OTPrequest(View v, String mobilenumber, String Requeststring) {
            this.v = v;
            this.mobilenumber = mobilenumber;
            this.Requeststring = Requeststring;
        }

        @Override
        protected void onPreExecute() {
            innerdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            innerdialog.getWindow().clearFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            innerdialog.setContentView(R.layout.adapter_progressdialog);
            TextView txt = (TextView) innerdialog.findViewById(R.id.text);
            txt.setText("Please wait..");
            innerdialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(Color.TRANSPARENT));
            innerdialog.show();
            innerdialog.setCancelable(false);

        }

        @Override
        protected String doInBackground(String... credentials) {

            boolean result = false;
            int trycount = 0;
            while (!result && trycount < 2) {
                trycount++;
                try {

                    String targetURL = webconfigurration.url;
                    URL connUrl = new URL(targetURL);
                    HttpURLConnection conn = (HttpURLConnection) connUrl
                            .openConnection();
                    conn.setRequestProperty("Content-Type",
                            "application/json; charset=utf-16");
                    conn.setRequestProperty("Accept-Encoding", "identity");

                    conn.setConnectTimeout(10000);
                    conn.setUseCaches(false);
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.setReadTimeout(10000);
                    conn.setRequestMethod("POST");

                    int totalLength = (credentials[0].toString().getBytes(
                            "utf-16").length);
                    conn.setFixedLengthStreamingMode(totalLength);
                    DataOutputStream request = new DataOutputStream(
                            conn.getOutputStream());
                    request.write(credentials[0].toString().getBytes("utf-16"));
                    request.flush();
                    request.close();

                    InputStream is = conn.getInputStream();

                    BufferedReader rd = new BufferedReader(
                            new InputStreamReader(is));
                    String line;
                    StringBuffer response = new StringBuffer();
                    while ((line = rd.readLine()) != null) {
                        response.append(line);
                    }
                    final String responsefrom = response.toString();
                    rd.close();
                    result = true;
                    return responsefrom;
                } catch (Exception e) {
                    result = false;
                    e.printStackTrace();
                }
            }
            return null;
        }

        protected void onPostExecute(String result) {
            innerdialog.dismiss();

            // validating status of sms send
            String otp = "";
            boolean send_status = true;
            if (result == null) {
                send_status = false;

            } else {

                try {
                    JSONObject resp = new JSONObject(result);
                    String status = resp.getString("status");
                    otp = resp.getString("otp");
                    if (otp.equals("") || !status.equalsIgnoreCase("success"))
                        send_status = false;
                } catch (JSONException e) {
                    send_status = false;
                }
            }

            Intialise_OTP_popup(v, mobilenumber, Requeststring, send_status,
                    otp);
        }

    }

    public void Intialise_OTP_popup(View v, String mobilenumber_in,
                                    String Requeststring_in, boolean send_status, String otp) {

        final String otp_gen = otp;
        final String mobilenumber = mobilenumber_in;
        final String Requeststring = Requeststring_in;

        AlertDialog.Builder builder = new AlertDialog.Builder(mAct);
        LayoutInflater inflater = getLayoutInflater();
        final View popupView = inflater.inflate(R.layout.enter_otp, null);
        builder.setView(popupView);
        final AlertDialog popupWindow = builder.create();
        popupWindow.show();
        popupWindow.setCanceledOnTouchOutside(false);
        ImageView close = popupView.findViewById(R.id.close_window);
        final TextView error = popupView
                .findViewById(R.id.textview4);
        error.setVisibility(View.GONE);

        ImageView resend = popupView.findViewById(R.id.Resend);
        final TextView staus_message = popupView
                .findViewById(R.id.textView2);

        final EditText code = popupView.findViewById(R.id.otpvalue);

        Button confirm = popupView.findViewById(R.id.confirm);

        Button cancel = popupView.findViewById(R.id.cancel);
        resend.setVisibility(View.VISIBLE);
        (popupView.findViewById(R.id.textview6))
                .setVisibility(View.VISIBLE);
        if (!send_status) {
            staus_message.setText("*Cannot send OTP to " + mobilenumber
                    + "..Try Resend");
            staus_message.setTextColor(Color.parseColor("#FF0000"));
            confirm.setEnabled(false);

        } else {
            staus_message.setText("OTP Send to " + mobilenumber);
            staus_message.setTextColor(Color.parseColor("#008000"));
            confirm.setEnabled(true);
        }

        close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                popupWindow.dismiss();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {

            private String verificationcode_entered;

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String code_value = code.getText().toString();
                if (otp_gen.equals(code_value) && !code_value.equals("")) {
                    error.setVisibility(View.VISIBLE);
                    error.setText("Code Matched");
                    error.setTextColor(Color.parseColor("#008000"));
                    popupWindow.dismiss();
                    OTP_verificationstatus = true;
                    verificationcode_entered = code_value;
                    contactnumber.setEnabled(false);
                    verification.setVisibility(View.GONE);
                    Button verified = findViewById(R.id.verified);
                    verified.setVisibility(View.VISIBLE);
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat(
                            "dd/MM/yyyy HH:mm:ss");
                    String current = sdf.format(c.getTime());
                    OTP_submitTime = current;
                } else {
                    error.setText("* Error Code");
                    error.setTextColor(Color.parseColor("#FF0000"));
                    error.setVisibility(View.VISIBLE);

                }
            }
        });

        resend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String[] cred = new String[1];
                cred[0] = Requeststring;
                if (isNetworkAvailable()) {
                    Toast.makeText(mAct, "Resending code", Toast.LENGTH_SHORT).show();
                    popupWindow.dismiss();
                    new OTPrequest(v, mobilenumber, Requeststring).execute(cred);
                } else {
                    error.setVisibility(View.VISIBLE);
                    error.setText("* You are offline");
                }
            }

        });

        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                popupWindow.dismiss();

            }
        });

    }

    private Boolean getCallDetails(Context context, String mobilenumber1, PopupWindow popupWindow1, String submittime) {
        StringBuffer stringBuffer = new StringBuffer();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {

            return false;
        }
        Cursor cursor = getContentResolver().query(
                CallLog.Calls.CONTENT_URI, null, null, null,
                CallLog.Calls.DATE + " DESC");
        int number = cursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = cursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = cursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = cursor.getColumnIndex(CallLog.Calls.DURATION);
        Boolean verified = false;
        int count = 0;
        while (cursor.moveToNext()) {
            if (count < 3) {

                String phNumber = cursor.getString(number);
                String callType = cursor.getString(type);
                String callDate = cursor.getString(date);
                Date callDayTime = new Date(Long.valueOf(callDate));
                String callDuration = cursor.getString(duration);
                String dir = null;
                long dateTimeMillis = cursor.getLong(cursor
                        .getColumnIndex(CallLog.Calls.DATE));
                Date datetime = new Date(dateTimeMillis);
                String dateString = new SimpleDateFormat(
                        "yyyyMMddHHmmss").format(datetime);
                double intdateString = Double.parseDouble(dateString);
                double intsubmittime = Double.parseDouble(submittime);
                if (intdateString > intsubmittime) {
                    int len = phNumber.length();
                    phNumber = phNumber.substring(len - 10, len);
                    int dircode = Integer.parseInt(callType);
                    switch (dircode) {
                        case 1:
                            if (mobilenumber.contentEquals(phNumber)) {
                                popupWindow1.dismiss();
                                OTP_verificationstatus = true;
                                // verificationcode_entered = code_value;
                                contactnumber.setEnabled(false);
                                verification.setText("VERIFIED");
                                if (OTP_verificationstatus) {
                                    ImageView tickbutton = (ImageView) findViewById(R.id.tickbutton);
                                    tickbutton.setVisibility(View.VISIBLE);
                                }
                                Calendar c = Calendar.getInstance();
                                SimpleDateFormat sdf = new SimpleDateFormat(
                                        "dd/MM/yyyy HH:mm:ss");
                                String current = sdf.format(c.getTime());
                                OTP_submitTime = current;
                                verified = true;
                            }
                            break;
                        case 2:// CallLog.Calls.INCOMING_TYPE: dir =
                            // "INCOMING";
                            // Toast.makeText(context,
                            // "OUTGOING call Number=="+phNumber
                            // +" Type=="+callType + "date=="
                            // +callDayTime+
                            // "duration=="+callDuration,
                            // Toast.LENGTH_SHORT).show();
                            break;
                        case 3:
                            if (mobilenumber.contentEquals(phNumber)) {
                                popupWindow1.dismiss();
                                OTP_verificationstatus = true;
                                contactnumber.setEnabled(false);
                                verification.setText("VERIFIED");
                                if (OTP_verificationstatus) {
                                    ImageView tickbutton = (ImageView) findViewById(R.id.tickbutton);
                                    tickbutton.setVisibility(View.VISIBLE);
                                }
                                Calendar c = Calendar.getInstance();
                                SimpleDateFormat sdf = new SimpleDateFormat(
                                        "dd/MM/yyyy HH:mm:ss");
                                String current = sdf.format(c.getTime());
                                OTP_submitTime = current;
                                verified = true;

                            }
                            break;

                        case 5:
                            if (mobilenumber.contentEquals(phNumber)) {
                                popupWindow1.dismiss();
                                OTP_verificationstatus = true;
                                contactnumber.setEnabled(false);
                                verification.setText("VERIFIED");
                                if (OTP_verificationstatus) {
                                    ImageView tickbutton = (ImageView) findViewById(R.id.tickbutton);
                                    tickbutton.setVisibility(View.VISIBLE);
                                }
                                Calendar c = Calendar.getInstance();
                                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat(
                                        "dd/MM/yyyy HH:mm:ss");
                                String current = sdf.format(c.getTime());
                                OTP_submitTime = current;
                                verified = true;

                            }
                            break;
                    }
                    count++;
                }
            }

        }
        cursor.close();
        return verified;// .toString();
    }
}