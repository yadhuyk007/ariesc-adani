package combing.sales.adani.objects;

public class Scheduledetail {

	public String Scheduledetailid;
	public String Scheduleheadid;
	public String locationid;
	public String locationname;
	public String locationadress;
	public String sequencenumber;
	public String status;
	public String latitude;
	public String longitude;
	public String customercode;
	public String Customercatogory;
	public String Rettail;
	public String invoicenumber;
	public String picklistnumber;
	public String locationlock;
	public String Closingday;
	public String CustomercatogoryId;
	public String mobilenumber;
	public String OTP;
	public String tinnumber;
	public String pan;
	public String photoUUID;
	public String landmark;
	public String town;
	public String contactperson;
	public String othercompany;
	public String crates;
	public String otpverified;
	public String locprovider;
	public String locaccuracy;
	public String email;
	public String directcov;
	public String selectedproductcategories;
	public String storeclosed;
	public String pngcovered;
	public String branch;
	public String alternatecustcode;

	public String drugLicienceNo;
	public String locality;
	public String city;
	public String state;
	public String pin;
	public String coverageDay;
	public String week1;
	public String week2;
	public String week3;
	public String week4;
	public String visitFrequency;
	public String type;
	public String wholeSale;
	public String metro;
	public String classification;

	public String latitudePhoto;
	public String longitudePhoto;
	public String custtype;
	public String remarks;
	public String street;
	public String area;
	public String landLine;
	public String shopname;
	public String plan;
	public String DeliveryImgFlg;
	public String StartTime;
	public String EndTime;
	public String needotp;
	public String otpsubtime;
	public String otplat;
	public String otplong;

	public String gsttype;
	public String fssi;
    public String custimgurl;
    public String visited;

    public Scheduledetail() {
		// TODO Auto-generated constructor stub
	}

}

