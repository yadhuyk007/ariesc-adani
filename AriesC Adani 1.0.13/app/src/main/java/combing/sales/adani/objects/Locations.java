package combing.sales.adani.objects;

public class Locations {

    public String Locationid;
    public String Locationname;
    public String Locationadress;
    public String Latitude;
    public String Longitude;
    public String createtime;
    public String costomercode;
    public String customercategory;
    public String tinnumber;

    public String nearestlandmarkvalue;
    public String contactpersonvalue;
    public String Contactpersonnumbervalue;
    public String anyothercompanyvalue;
    public String townclassvalue;
    public String selectedproductcategories;
    public String customercatogorycode;
    public String schdulineguid;
    public String pan;
    public String otpverified;
    public String locprovider;
    public String locaccuracy;
    public String email;
    public String photouuid;
    public String directcov;
    public String closingday;
    public String storeclosed;
    public String pngcovered;
    public String branch;

    public String drugLicienceNo;
    public String locality;
    public String city;
    public String state;
    public String pin;
    public String coverageDay;
    public String week1;
    public String week2;
    public String week3;
    public String week4;
    public String visitFrequency;
    public String type;
    public String wholeSale;
    public String metro;
    public String classification;

    public String latitudePhoto;
    public String longitudePhoto;

    public String remarks;
    public String marketname;


    public String startTime;
    public String endTime;
    public String otpsubtime;
    public String otplat;
    public String otplong;
    public String fixtime;


    public Locations() {
        // TODO Auto-generated constructor stub
    }



}
