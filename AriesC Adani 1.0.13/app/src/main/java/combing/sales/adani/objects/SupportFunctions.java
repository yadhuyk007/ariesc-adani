package combing.sales.adani.objects;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.BatteryManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.UUID;

/**
 * Created by hp on 04/04/2019.
 */

public class SupportFunctions {
    public static JSONObject getstatusobject(Context context, String operation,String person) throws Exception {


        JSONObject temp = new JSONObject();

        String lat = "";
        String lng = "";
        String version = "";
        String createdon = "";
        String uuid = "";
        String appname = "";
        String versioncode = "";
        String imei = "";
        String androidId = "";


        operation = webconfigurration.status;
        appname = webconfigurration.appname;
        lat = webconfigurration.lat;
        lng = webconfigurration.lng;

        PackageInfo pInfo = context.getPackageManager().getPackageInfo(
                context.getPackageName(), 0);
        version = pInfo.versionName;
        versioncode = String.valueOf(pInfo.versionCode);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        createdon = df.format(c.getTime());

        uuid = UUID.randomUUID().toString();

        String battery = batteryLevel(context);

        try {
            androidId = Settings.Secure.getString(context
                    .getContentResolver(), Settings.Secure.ANDROID_ID);

            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_PHONE_STATE}, 112);
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return null;
            }
            imei = telephonyManager.getDeviceId();
        } catch (Exception e) {
            if (androidId != null && androidId.equals(""))
                androidId = "NA";

            if (imei != null && imei.equals(""))
                imei = "NA";

            e.printStackTrace();
        }

        temp.put("person", person).put("lat", lat).put("lng", lng)
                .put("operation", operation).put("version", version)
                .put("versioncode", versioncode).put("appname", appname)
                .put("createdon", createdon).put("uuid", uuid)
                .put("battery", battery);

        temp.put("AndroidId", androidId);
        temp.put("IMEI", imei);
        return temp;

    }

    private static String batteryLevel(Context context) {
        Intent batteryIntent = context.registerReceiver(null, new IntentFilter(
                Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        if (level == -1 || scale == -1) {
            return String.valueOf(50.0f);
        }

        return String.valueOf(((float) level / (float) scale) * 100.0f);
    }
}
