package combing.sales.adani.objects;


import android.content.Context;
import android.content.SharedPreferences;

import combing.sales.adani.database.DatabaseHandler;

public class webconfigurration {

    /***************************** DMSLite **********************/
    // public static String bucketaddress = "dms";
    // public static String mainurl = "http://demotms.dhicubes.com:8080/dms";
    // public static String mainurl = "http://demotms.dhicubes.com:8080/dhi";
    // public static String mainurl = "http://52.32.56.127:8080/dhi";
    // public static String mainurl = "https://dms.dhisigma.com/dhi";// dmslite

    /***************************** Sabari ERP **********************/
    // public static String bucketaddress = "sabarierp";
    // public static String mainurl="https://sabarierp.dhisigma.com/dhi";

    /***************************** Salespro **********************/
    public static String bucketaddress = "salespro";
      // public static String mainurl = "https://qasalepro.dhisigma.com/salespro";
    //public static String mainurl = "https://qasalespro.dhisigma.com/dhi";  //QA
       // public static String mainurl = " http://192.168.2.127:8080/rbprod";
    //public static String mainurl = " http://192.168.2.24:8081/EMRCodes";
     //public static String mainurl = "https://prod.dhisigma.com/dhi";
     //public static String mainurl = "https://dhisigmalbmob.dhisigma.com/dhi";

    /**************************** Local ***************************/
//     public static String mainurl = "http://192.168.2.144:8082/EMRCodes";
    // //sravan
    // My Local
    // public static String mainurl = "http://192.168.1.103:8082/EMRCodes";

    //yadhu
    public static String mainurl = "http://192.168.1.5:8080/EMRCodes"; //primary
   // public static String mainurl = "http://192.168.42.31:8080/EMRCodes"; //secondary

    // //sravan
    // public static String mainurl = "http://192.168.2.3:8080/EMRCodes"; //
    // swathy
    // public static String mainurl = "http://192.168.2.138:8080/EMRCodes";
    // public static String mainurl = "http://192.168.2.178:8080/EMRCodes";
    // public static String mainurl = "http://192.168.2.16:8081/EMRCodes";

    /********************** do not modify this values **************/
    public static String loginurl = mainurl
            + "/services/dhiservices/userloginsalespro";
    public static String url = mainurl + "/services/dhiservices/service";
    public static String syncUrl = mainurl + "/services/dhiservices/service";
    public static String uploadUrl = mainurl + "/services/dhiservices/upload";
    public static String registrationurl = mainurl + "/services/service/registration";
    public static String statusurl = mainurl + "/services/service/status";
    public static String bucketapp = bucketaddress + "/AriesC Adani.apk";
    public static String auth = "-1396247038";
    public static String status = "Refresh";
    public static String appname = "AriesC Adani";
    public static String lat = "";
    public static String lng = "";

    public String bu = "";
    public String orgid = "";
    public String appid = "";
    public String user = "";
    public String dbname = "";

    public webconfigurration(Context c) {
        SharedPreferences pref = c.getSharedPreferences("Config",
                Context.MODE_PRIVATE);
        bu = pref.getString("BussinessUnit", "");
        orgid = pref.getString("orgId", "");
        dbname = pref.getString("orgDb", "");
        user = pref.getString("UserName", "");
        appid = pref.getString("appid", "");
        DatabaseHandler db = new DatabaseHandler(c);
        String val = "";
        try {
            val = db.getConstantValue("syncURL");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!val.contentEquals(""))
            syncUrl = val;
    }
}
