package combing.sales.adani.customer;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import combing.sales.adani.ariesc.LoginActivity;
import combing.sales.adani.ariesc.R;
import combing.sales.adani.customerlist.SurrogateImagesAdapter;
import combing.sales.adani.database.DatabaseHandler;
import combing.sales.adani.landing.LandingActivity;
import combing.sales.adani.objects.Global;
import combing.sales.adani.objects.Stacklogger;

import static com.amazonaws.regions.RegionUtils.init;

public class SurrogateImageActivity extends AppCompatActivity {
    private DatabaseHandler db;
    private SharedPreferences pref;
    private int pos;
    public static String[] productcategories;
    public static boolean[] selectedarray;
    public static String[] categorycode;
    public static String[] typearray;
    public static String[] selectedarraytext;
    private static ViewPager mPager;
    private static String[] IMAGES;
    private ArrayList<String> ImagesArray = new ArrayList<String>();
    private Button no;
    private Button yes;
    private ImageView surrogateimg;
    private Button next;
    private LinearLayout btllay;
    private TextView prodname;
    MediaRecorder mediaRecorder;
    final int REQUEST_PERMISSION_CODE = 1000;
    private String AudioPathSave="";
    private String AudioUUID;
    File direct;
    private String fileName;
    String custUUID;
    int delay = 60000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_surrogate_images);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);
        no = findViewById(R.id.no);
        yes = findViewById(R.id.yes);
        next = findViewById(R.id.next);
        btllay = findViewById(R.id.btllay);
        prodname = findViewById(R.id.prodname);
        pos=0;
        custUUID = UUID.randomUUID().toString();
        if (!checkPermissionFromDevice()){
            requestPermission();
        }
        Global.RecState=true;
        Global.CustUUID = custUUID;
        final Intent ServiceIntent = new Intent(getApplicationContext(),RecordingService.class);
        startService(ServiceIntent);
        Global.ServiceIntent = ServiceIntent;
        db = new DatabaseHandler(getApplicationContext());
        try {
            intialisedialoguedata();
            init();

        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),"yep... crashed", Toast.LENGTH_LONG).show();
        }


        pref = getApplicationContext()
                .getSharedPreferences("Config",
                        MODE_PRIVATE);

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedarray[pos]=false;
                pos++;
                if (pos<ImagesArray.size()){
                    String url = ImagesArray.get(pos);
                    if (url.contentEquals("")){
                        Toast.makeText(getApplicationContext(),"Image URL Not Found",Toast.LENGTH_LONG).show();
                        surrogateimg.setImageResource(R.drawable.imgnotavail);
                        prodname.setText(productcategories[pos]);;
                    }else{
                        new ImageLoadTask(url, surrogateimg,pos).execute();
                    }

                }else{
                    no.setVisibility(View.GONE);
                    yes.setVisibility(View.GONE);
                    next.setVisibility(View.VISIBLE);
                }


            }
        });
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedarray[pos]=true;
                pos++;
                if (pos<ImagesArray.size()){
                    String url = ImagesArray.get(pos);
                    if (url.contentEquals("")){
                        Toast.makeText(getApplicationContext(),"Image URL Not Found",Toast.LENGTH_LONG).show();
                        surrogateimg.setImageResource(R.drawable.imgnotavail);
                        prodname.setText(productcategories[pos]);;
                    }else{
                        new ImageLoadTask(url, surrogateimg,pos).execute();
                    }
                }else{
                    no.setVisibility(View.GONE);
                    yes.setVisibility(View.GONE);
                    next.setVisibility(View.VISIBLE);
                }

            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SurrogateImageActivity.this,NewCustomerActivity.class);
                intent.putExtra("selectedarray",selectedarray);
                intent.putExtra("categorycode",categorycode);
                intent.putExtra("custuuid",custUUID);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {

        ImageView layout = (ImageView) findViewById(R.id.surrogateimg);
        LayoutInflater layoutInflater = (LayoutInflater) SurrogateImageActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.popuplayout, null);
        TextView header = (TextView) customView.findViewById(R.id.header);
        header.setText(R.string.savecustomer);
        TextView msg = (TextView) customView.findViewById(R.id.message);
        msg.setText(R.string.savecustomermsg);
        Button closePopupBtn = (Button) customView.findViewById(R.id.close);
        Button confirm = (Button) customView.findViewById(R.id.yes);
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        final PopupWindow popupWindow = new PopupWindow(customView, width, height);
        popupWindow.setFocusable(true);

        //display the popup window
        popupWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        //close the popup window on button click
        closePopupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.RecFlag = 0;
                stopService(Global.ServiceIntent);
                    String directory = Environment
                            .getExternalStorageDirectory()
                            .toString()
                            + "/AriesAudio";
                    for (File file : new java.io.File(directory).listFiles())
                        if (!file.isDirectory())
                            file.delete();

                popupWindow.dismiss();
                //super.onBackPressed();
                Intent intent = new Intent(SurrogateImageActivity.this, LandingActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                finish();
                overridePendingTransition(0, 0);
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
    }

    public void init(){

        String url = ImagesArray.get(pos);
        surrogateimg = findViewById(R.id.surrogateimg);
        if (url.contentEquals("")){
            Toast.makeText(getApplicationContext(),"Image URL Not Found",Toast.LENGTH_LONG).show();
            surrogateimg.setImageResource(R.drawable.imgnotavail);
            prodname.setText(productcategories[pos]);;
        }else{
            new ImageLoadTask(url, surrogateimg,pos).execute();
        }

    }

    public class ImageLoadTask extends AsyncTask<Void, Void, Bitmap> {
        private ProgressDialog mProgress;
        private String url;
        private ImageView imageView;

        public ImageLoadTask(String url, ImageView imageView,int pos) {
            this.url = url;
            this.imageView = imageView;
        }

        @Override
        protected void onPreExecute() {
            mProgress = new ProgressDialog(SurrogateImageActivity.this);
            mProgress.setView(getLayoutInflater().inflate(R.layout.adapter_loading, null));
            mProgress.setTitle("");
            mProgress.setMessage("Loading Image...");
            mProgress.setCancelable(false);
            mProgress.setIndeterminate(true);
            mProgress.create();
            if (!mProgress.isShowing()){
                mProgress.show();
            }
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                URL urlConnection = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) urlConnection
                        .openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (mProgress.isShowing()){
                mProgress.dismiss();
            }

            super.onPostExecute(result);
            imageView.setImageBitmap(result);
            prodname.setText(productcategories[pos]);;
        }

    }

    public void intialisedialoguedata() throws JSONException {
        JSONArray data = db.getchecklist();
        productcategories = new String[data.length()];
        categorycode = new String[data.length()];
        selectedarray = new boolean[data.length()];
        typearray = new String[data.length()];
        selectedarraytext = new String[data.length()];

        for (int i = 0; i < data.length(); i++) {
            JSONObject dataobj = data.getJSONObject(i);
            productcategories[i] = dataobj.getString("catname");
            categorycode[i] = dataobj.getString("catid");
            selectedarray[i] = false;
            typearray[i] = dataobj.getString("type");
            ImagesArray.add(dataobj.getString("url"));
        }
    }
    private void requestPermission(){
        ActivityCompat.requestPermissions(this,new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO
        },REQUEST_PERMISSION_CODE);
    }
    private boolean checkPermissionFromDevice(){
        int write_external_storage_result = ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int record_audio_result = ContextCompat.checkSelfPermission(this,Manifest.permission.RECORD_AUDIO);
        return write_external_storage_result == PackageManager.PERMISSION_GRANTED && record_audio_result == PackageManager.PERMISSION_GRANTED;
    }
}
