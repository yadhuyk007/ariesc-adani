package combing.sales.adani.customer;

import android.app.Service;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import combing.sales.adani.database.DatabaseHandler;
import combing.sales.adani.objects.Global;

public class RecordingService extends Service {
    private String AudioUUID;
    private File direct;
    private String fileName;
    private String AudioPathSave;
    private  MediaRecorder mediaRecorder;
    private Timer myTimer;
    private int startdelay=0;
    private int stopdelay=60000;
    private DatabaseHandler db;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        if (Global.RecFlag==1){
            stopRecording();
        }else{
            stopRecordingandSetDeleteFlag();
        }

        super.onDestroy();
    }




    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        db = new DatabaseHandler(getApplicationContext());
        Thread t = new Thread(new Runnable() {

            @Override
            public void run() {
                myTimer = new Timer();
                while (Global.RecState==true) {
                    try {

                        startRecording();
                        startdelay = stopdelay + 60000;
                        stopdelay = startdelay +60000;
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
        t.start();

        return super.onStartCommand(intent, flags, startId);
    }

    private void startRecording() {

        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {

                try {
                    AudioUUID = UUID.randomUUID().toString();

                    File rootsd = Environment.getExternalStorageDirectory();
                    direct = new File(rootsd.getAbsolutePath() + "/AriesAudio");

                    if (!direct.exists()) {
                        direct.mkdirs();
                    }
                    fileName = AudioUUID + ".3gp";
                    AudioPathSave = direct +"/"+ fileName;

                    mediaRecorder = new MediaRecorder();
                    Global.MEDIA_RECORDER = mediaRecorder;
                    mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                    mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                    mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
                    mediaRecorder.setOutputFile(AudioPathSave);
                    mediaRecorder.prepare();
                    mediaRecorder.start();
                    Calendar c = Calendar.getInstance();
                    DateFormat dateTimeFormat = new SimpleDateFormat(
                            "yyyyMMddHHmmss");
                    String formattedDate = dateTimeFormat.format(c.getTime());
                    db.insertToCustAudioRel(Global.CustUUID,fileName,0,formattedDate);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (RuntimeException | IOException e) {
                    e.printStackTrace();
                }
            }

        }, startdelay);
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {

                try {
                    mediaRecorder.stop();
                    mediaRecorder.reset();
                    mediaRecorder = null;
                    //db.insertToCustAudioRel(Global.CustUUID,fileName,0,formattedDate);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
            }

        }, stopdelay);

    }
    private void stopRecording() {
        if (mediaRecorder!=null){
            mediaRecorder.stop();
            Calendar c = Calendar.getInstance();
            DateFormat dateTimeFormat = new SimpleDateFormat(
                    "yyyyMMddHHmmss");
            String formattedDate = dateTimeFormat.format(c.getTime());
            //db.insertToCustAudioRel(Global.CustUUID,fileName,0,formattedDate);
            mediaRecorder.reset();
            mediaRecorder.release();
            mediaRecorder = null;
            Global.CustUUID="";
            Global.RecState=false;
            Global.MEDIA_RECORDER= null ;
            Global.ServiceIntent = null;
        }
    }

    private void stopRecordingandSetDeleteFlag() {
        mediaRecorder.stop();
        Calendar c = Calendar.getInstance();
        DateFormat dateTimeFormat = new SimpleDateFormat(
                "yyyyMMddHHmmss");
        String formattedDate = dateTimeFormat.format(c.getTime());
        int y = db.insertToCustAudioRel(Global.CustUUID,fileName,1,formattedDate);
        mediaRecorder.reset();
        mediaRecorder.release();
        mediaRecorder = null;
        Global.RecState=false;
        Global.MEDIA_RECORDER= null ;
        Global.ServiceIntent = null;
    }
}
