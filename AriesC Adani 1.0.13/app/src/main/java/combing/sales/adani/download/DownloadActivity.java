package combing.sales.adani.download;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.File;

import combing.sales.adani.ariesc.R;
import combing.sales.adani.objects.Stacklogger;
import combing.sales.adani.objects.webconfigurration;


public class DownloadActivity extends AppCompatActivity {

    File fileToUpload = new File("/storage/sdcard0/Storage/ADF.png");
    File fileToDownload = new File("/storage/sdcard0/AriesC Adani.apk");
    AmazonS3Client s3;
    TransferUtility transferUtility;
    TransferObserver transferObserver;
    private File tempdir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        Thread.setDefaultUncaughtExceptionHandler(new Stacklogger(this));

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);

        if (ContextCompat.checkSelfPermission(DownloadActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(DownloadActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1888);
        }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder(); StrictMode.setVmPolicy(builder.build());

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.ic_dhi_newlogo);

        tempdir = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/temp_apk/");

        if (!tempdir.exists())
            tempdir.mkdir();

        fileToDownload = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/temp_apk/AriesC Adani.apk");

        // callback method to call credentialsProvider method.
        credentialsProvider();
        // List<Bucket> ab = s3.listBuckets();
        // callback method to call the setTransferUtility method
        setTransferUtility();
        setFileToDownload2();
    }

    public void credentialsProvider() {

        // Initialize the Amazon Cognito credentials provider
        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                // "us-west-2:97024f04-79b3-4f55-9fca-43e75e5abe94", // Identity
               // "us-west-2:9c3a58b3-ea3b-44da-a0d9-ef224150568c", // Pool Prod
 "us-west-2:07eb794e-e6f0-47a3-bc74-0a2d114b1a94",   // QA

                // ID
                Regions.US_WEST_2 // Region
        );

        setAmazonS3Client(credentialsProvider);
    }

    /**
     * Create a AmazonS3Client constructor and pass the credentialsProvider.
     *
     * @param credentialsProvider
     */
    public void setAmazonS3Client(
            CognitoCachingCredentialsProvider credentialsProvider) {

        // Create an S3 client
        s3 = new AmazonS3Client(credentialsProvider);

        // Set the region of your S3 bucket
        s3.setRegion(Region.getRegion(Regions.US_WEST_2));

    }

    public void setTransferUtility() {

        transferUtility = new TransferUtility(s3, getApplicationContext());
    }



    public void setFileToDownload2() {

        //transferObserver = transferUtility.download("spr53prodmobileversion", //The bucket to download from    Prod

        transferObserver = transferUtility.download("spr53qamobileversion", /*QA S3 bucket*/


                webconfigurration.bucketapp, /*
					      * The key for the object to
					      * download
					      */
                fileToDownload /* The file to download the object to */
        );

        transferObserverListener(transferObserver);

    }

    /**
     * This is listener method of the TransferObserver Within this listener
     * method, we got status of uploading and downloading file, to diaplay
     * percentage of the part of file to be uploaded or downloaded to S3 It
     * display error, when there is problem to upload and download file to S3.
     *
     * @param transferObserver
     */

    public void transferObserverListener(TransferObserver transferObserver) {

        transferObserver.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
                Log.e("statechange", state + "");
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent,
                                          long bytesTotal) {

                if (bytesTotal != 0)
                    updateprogress(bytesCurrent, bytesTotal);
                try {
                    int percentage = 0;
                    if (bytesTotal != 0)
                        percentage = (int) (bytesCurrent * 100.0 / bytesTotal);
                    Log.e("percentage", percentage + "");

                    if (percentage == 100) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            Uri apkUri = FileProvider.getUriForFile(DownloadActivity.this,"combing.sales.adani.download.DownloadActivity",fileToDownload);
                            Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);

                            intent.setData(apkUri);
                            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(intent);
                        } else {
                            Uri apkUri = Uri.fromFile(fileToDownload);
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("error", "error");
                finish();
            }

        });
    }

    public void updateprogress(long bytesCurrent, long bytesTotal) {
        ProgressBar horzntal = (ProgressBar) findViewById(R.id.progressBarHorz);
        TextView percent = (TextView) findViewById(R.id.percent);
        Log.e("bytesCurrent", String.valueOf(bytesCurrent));
        Log.e("bytesTotal", String.valueOf(bytesTotal));
        int hprgrss = (int) (bytesCurrent * 100.0 / bytesTotal);
        Log.e("Percent", String.valueOf(hprgrss));
        horzntal.setProgress(hprgrss);
        percent.setText(String.valueOf(hprgrss));
        }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            int id = transferObserver.getId();
            transferUtility.cancel(id);
            Log.e("s3-status", "Download Cancelled");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
