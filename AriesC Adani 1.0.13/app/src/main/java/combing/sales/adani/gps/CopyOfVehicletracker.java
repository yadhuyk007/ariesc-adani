package combing.sales.adani.gps;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import combing.sales.adani.database.DatabaseHandler;
import combing.sales.adani.objects.Global;
import combing.sales.adani.objects.Vehicledata;


public class CopyOfVehicletracker extends Service {
    private static String TAG = "MyService";
    private final int runTime = 1000 * 60 * 1;
    private Handler handler;
    private Runnable runnable;
    private DatabaseHandler db;

    public static String getpkidstring(List<Vehicledata> Navigationdata) {
        // (1, 4, 6, 7)
        String pkdata = "(";
        int last = Navigationdata.size() - 1;
        for (int i = 0; i < (Navigationdata.size() - 1); i++) {
            pkdata = pkdata + String.valueOf(Navigationdata.get(i).pkid) + ",";

        }
        pkdata = pkdata + String.valueOf(Navigationdata.get(last).pkid) + ")";
        return pkdata;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        BroadcastReceiver gpsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().matches(
                        LocationManager.PROVIDERS_CHANGED_ACTION)) {
                    // syncnavdata();
                    LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    boolean statusOfGPS = manager
                            .isProviderEnabled(LocationManager.GPS_PROVIDER);

                    // if (statusOfGPS)
                    // Toast.makeText(context, "Your GPS is ON now!",
                    // Toast.LENGTH_LONG).show();
                    // else
                    // Toast.makeText(context, "Your GPS is OFF now!",
                    // Toast.LENGTH_LONG).show();
                }
            }
        };
        registerReceiver(gpsReceiver, new IntentFilter(
                LocationManager.PROVIDERS_CHANGED_ACTION));
        Log.i(TAG, "onCreate");
        // Log.e("In Handler", "running");
        handler = new Handler();
        runnable = new Runnable() {

            @Override
            public void run() {
                try {
                    startService(new Intent(getApplicationContext(),
                            PlayTracker.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.e("In Handler", "running");
                DatabaseHandler db = new DatabaseHandler(
                        getApplicationContext());
                SharedPreferences pref = getApplicationContext()
                        .getSharedPreferences("Config", MODE_PRIVATE);
                int planid = Integer.parseInt(pref.getString("personid", "0"));
                if (planid != 0) {
                    // syncnavdata();
                    Calendar c1 = Calendar.getInstance();
                    DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                    String cmpDate = sdf.format(c1.getTime());
                    DateFormat mongoFormat = new SimpleDateFormat(
                            "yyyy-MM-dd'T'HH:mm:ss'Z'");
                    String mongoDate = mongoFormat.format(c1.getTime());
                    DateFormat dateTimeFormat = new SimpleDateFormat(
                            "dd/MM/yyyy HH:mm:ss");
                    String formattedDate = dateTimeFormat.format(c1.getTime());
                    boolean foreground = true;
                    int inminutes = 0;
                    try {
                        Date today = sdf.parse(cmpDate);
                        Date lasts;
                        if (Global.lastacttime.contentEquals(""))
                            lasts = sdf.parse(cmpDate);
                        else
                            lasts = sdf.parse(Global.lastacttime);
                        inminutes = (int) ((today.getTime() - lasts.getTime()) / (1000 * 60));
                        if (inminutes > 14)
                            foreground = false;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String PlanId = String.valueOf(planid);
                    String VehicleId = "6";
                    String DeviceTime;
                    String Latitude = "0";
                    String Longitude = "0";
                    String Speed = "0";
                    String provider = "";
                    LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    boolean statusOfGPS = manager
                            .isProviderEnabled(LocationManager.GPS_PROVIDER);
                    if (!statusOfGPS)
                        provider = "gps off";
                    else
                        provider = "gps on";
                    String accuracy = "0";
                    String bearing = "0";
                    String distance = "0";
                    String customer = "0";
                    String timediff = "0";
                    Vehicledata Vehdata = new Vehicledata(PlanId, VehicleId,
                            formattedDate, Latitude, Longitude, Speed,
                            provider, accuracy, bearing, distance, customer,
                            timediff, mongoDate, foreground);
                    long id = db.AddNavigationdata(Vehdata);
//					 Toast.makeText(getApplicationContext(), id + "",
//					 Toast.LENGTH_SHORT).show();
                }
                handler.postDelayed(runnable, runTime);
            }
        };
        handler.post(runnable);

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e(TAG, "onTaskRemoved");

        if (handler != null) {
            handler.removeCallbacks(runnable);
        }
        Intent intent = new Intent(getApplicationContext(),
                CopyOfVehicletracker.class);
        PendingIntent pendingIntent = PendingIntent.getService(this, 1, intent,
                PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP,
                SystemClock.elapsedRealtime() + 5000, pendingIntent);
        super.onTaskRemoved(rootIntent);
    }


}
