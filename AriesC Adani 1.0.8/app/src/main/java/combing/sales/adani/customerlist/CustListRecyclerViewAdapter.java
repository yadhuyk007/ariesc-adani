package combing.sales.adani.customerlist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import combing.sales.adani.ariesc.R;


public class CustListRecyclerViewAdapter extends RecyclerView.Adapter<CustListRecyclerViewAdapter.ViewHolder>{
    private List<String> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    Context context;



    // data is passed into the constructor
    CustListRecyclerViewAdapter(Context context, List<String> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context=context;

    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recyclerview_row_customerlist, parent, false);
        return new ViewHolder(view);


    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String animal = mData.get(position);
        holder.myTextView.setText(animal);

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
       // Spinner spinner;
       LinearLayout custListRow;

        ViewHolder(View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.CustomerName);
            custListRow=itemView.findViewById(R.id.custListRowLayout) ;
            itemView.setOnClickListener(this);
             //spinner = itemView.findViewById(R.id.itemDropdown);
          //  ArrayAdapter adapter1 = (ArrayAdapter) ArrayAdapter.createFromResource(context, R.array.packagelist, R.layout.spinner_additem);
          //  spinner.setAdapter(adapter1);



        }

        @Override
        public void onClick(View view) {
           // if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
            if (view.getId()==R.id.custListRowLayout){
               // context.startActivity(new Intent(context, Testorderdltadditem.class));
            }



            }
    }

    // convenience method for getting data at click position
    String getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
