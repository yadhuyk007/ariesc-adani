package combing.sales.adani.customerlist;

import org.json.JSONArray;

public class ScheduledRouteObject {
	public String RouteId;
	public String RouteName;
	public String RouteTime;
	public String scheduletype;
	public JSONArray CustomerList;
	public String schedulecompletionstatus;

	public ScheduledRouteObject(String RouteId, String RouteName, String RouteTime, JSONArray CustomerList, String Scheduletype, String schedulecompletionstatus)
	{
		this.RouteId=RouteId;
		this.RouteName=RouteName;
		this.RouteTime=RouteTime;
		this.scheduletype=Scheduletype;
		this.CustomerList=CustomerList;
		this.schedulecompletionstatus=schedulecompletionstatus;
	}
}
