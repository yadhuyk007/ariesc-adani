package combing.sales.adani.objects;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class Stacklogger implements Thread.UncaughtExceptionHandler {
    private Thread.UncaughtExceptionHandler defaultUEH;
    private Activity app = null;

    public Stacklogger(Activity app) {
	this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
	this.app = app;
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
	StackTraceElement[] arr = e.getStackTrace();
	String report = "";
	// add device information

	String device = "";
	String appversion = "";
	String appversioncode = "";
	try {
	    device = Build.MANUFACTURER
		    + " "
		    + Build.MODEL
		    + " "
		    + Build.VERSION.RELEASE
		    + " "
		    + Build.VERSION_CODES.class.getFields()[Build.VERSION.SDK_INT]
			    .getName();

	    PackageInfo pinfo = app.getPackageManager().getPackageInfo(
		    app.getPackageName(), 0);
	    appversion = pinfo.versionName;
	    appversioncode = pinfo.versionCode + "";

	} catch (Exception err) {

	}
	Calendar c = Calendar.getInstance();
	String myFormat = "dd-MM-yyyy HH:mm:ss";
	SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
	String date = sdf.format(c.getTime());
	myFormat = "yyyyMMddHHmmss";
	sdf = new SimpleDateFormat(myFormat, Locale.US);
	String filedate = sdf.format(c.getTime());
	report += "\nAppCrashedOn: " + date + "\n";
	report += "-------- Device_Info ---------\n\n";
	report += "AppVersion : " + appversion + "\n";
	report += "Versioncode : " + appversioncode + "\n";
	report += "Device : " + device + "\n";
	report += "-------------------\n\n";

	// read above information from device

	report += e.toString() + "\n\n";
	report += "--------- Stack trace ---------\n\n";
	for (int i = 0; i < arr.length; i++) {
	    report += "    " + arr[i].toString() + "\n";
	}
	report += "-------------------------------\n\n";

	// If the exception was thrown in a background thread inside
	// AsyncTask, then the actual exception can be found with getCause

	report += "--------- Cause ---------\n\n";
	Throwable cause = e.getCause();
	if (cause != null) {
	    report += cause.toString() + "\n\n";
	    arr = cause.getStackTrace();
	    for (int i = 0; i < arr.length; i++) {
		report += "    " + arr[i].toString() + "\n";
	    }
	}
	report += "-------------------------------\n\n";

	try {
	    File tempDir = Environment.getExternalStorageDirectory();
	    tempDir = new File(tempDir.getAbsolutePath() + "/AriesErrors/");
	    if (!tempDir.exists()) {
		tempDir.mkdirs();
	    }
	    File myExternalFile = new File(tempDir, "errorlog_" + filedate
		    + ".txt");

	    FileOutputStream fos = new FileOutputStream(myExternalFile);
	    OutputStreamWriter osw = new OutputStreamWriter(fos);
	    osw.append(report.toString());
	    osw.close();
	    // FileOutputStream trace = app.openFileOutput("stack.trace",
	    // Context.MODE_PRIVATE);
	    // trace.write(report.getBytes());
	    // trace.flush();
	    // trace.close();
	} catch (IOException ioe) {
	    // ...
	}

	defaultUEH.uncaughtException(t, e);
    }
}