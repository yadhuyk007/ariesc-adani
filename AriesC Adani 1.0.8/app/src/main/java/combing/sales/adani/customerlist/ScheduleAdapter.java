package combing.sales.adani.customerlist;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import combing.sales.adani.ariesc.R;
import combing.sales.adani.objects.Schedule;


public class ScheduleAdapter extends ExpandableRecyclerAdapter<ScheduleViewHolder, CustomerListViewHolder> {

    private LayoutInflater mInflator;
    private Activity mAct;

    public ScheduleAdapter(Activity activity, List<? extends ParentListItem> parentItemList) {
        super(parentItemList);
        mAct = activity;
        mInflator = LayoutInflater.from(mAct);
    }

    @Override
    public ScheduleViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View ScheduleView = mInflator.inflate(R.layout.recyclerview_parent_row_customerlist, parentViewGroup, false);
        return new ScheduleViewHolder(ScheduleView);
    }

    @Override
    public CustomerListViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View custlistView = mInflator.inflate(R.layout.recyclerview_row_customerlist, childViewGroup, false);

        return new CustomerListViewHolder(mAct,custlistView);
    }

    @Override
    public void onBindParentViewHolder(ScheduleViewHolder scheduleViewHolder, int position, ParentListItem parentListItem) {
        Schedule schedule = (Schedule) parentListItem;
        scheduleViewHolder.bind(schedule);
    }

    @Override
    public void onBindChildViewHolder(CustomerListViewHolder custlistViewHolder, int position, Object childListItem) {
        CustomerList custlist = (CustomerList) childListItem;
        custlistViewHolder.bind(custlist);
    }
}
